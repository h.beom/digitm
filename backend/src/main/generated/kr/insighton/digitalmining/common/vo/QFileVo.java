package kr.insighton.digitalmining.common.vo;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QFileVo is a Querydsl query type for FileVo
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QFileVo extends EntityPathBase<FileVo> {

    private static final long serialVersionUID = -661645109L;

    public static final QFileVo fileVo = new QFileVo("fileVo");

    public final StringPath extension = createString("extension");

    public final StringPath group = createString("group");

    public final NumberPath<Integer> idx = createNumber("idx", Integer.class);

    public final StringPath name = createString("name");

    public final StringPath originName = createString("originName");

    public final DateTimePath<java.sql.Timestamp> regDate = createDateTime("regDate", java.sql.Timestamp.class);

    public QFileVo(String variable) {
        super(FileVo.class, forVariable(variable));
    }

    public QFileVo(Path<? extends FileVo> path) {
        super(path.getType(), path.getMetadata());
    }

    public QFileVo(PathMetadata metadata) {
        super(FileVo.class, metadata);
    }

}

