package kr.insighton.digitalmining.common.vo;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QCodeVo is a Querydsl query type for CodeVo
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QCodeVo extends EntityPathBase<CodeVo> {

    private static final long serialVersionUID = -742229764L;

    public static final QCodeVo codeVo = new QCodeVo("codeVo");

    public final StringPath code = createString("code");

    public final StringPath group = createString("group");

    public final NumberPath<Integer> idx = createNumber("idx", Integer.class);

    public final StringPath item1 = createString("item1");

    public final StringPath item2 = createString("item2");

    public final StringPath item3 = createString("item3");

    public final StringPath item4 = createString("item4");

    public final StringPath item5 = createString("item5");

    public final StringPath name = createString("name");

    public final DateTimePath<java.sql.Timestamp> regDate = createDateTime("regDate", java.sql.Timestamp.class);

    public final NumberPath<Integer> sort = createNumber("sort", Integer.class);

    public QCodeVo(String variable) {
        super(CodeVo.class, forVariable(variable));
    }

    public QCodeVo(Path<? extends CodeVo> path) {
        super(path.getType(), path.getMetadata());
    }

    public QCodeVo(PathMetadata metadata) {
        super(CodeVo.class, metadata);
    }

}

