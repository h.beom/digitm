package kr.insighton.digitalmining.api.vo;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QSensorOriginalInfoVo is a Querydsl query type for SensorOriginalInfoVo
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QSensorOriginalInfoVo extends EntityPathBase<SensorOriginalInfoVo> {

    private static final long serialVersionUID = 2026460847L;

    public static final QSensorOriginalInfoVo sensorOriginalInfoVo = new QSensorOriginalInfoVo("sensorOriginalInfoVo");

    public final NumberPath<Double> air = createNumber("air", Double.class);

    public final NumberPath<Double> co = createNumber("co", Double.class);

    public final NumberPath<Double> co2 = createNumber("co2", Double.class);

    public final NumberPath<Integer> idx = createNumber("idx", Integer.class);

    public final NumberPath<Double> no = createNumber("no", Double.class);

    public final NumberPath<Double> no2 = createNumber("no2", Double.class);

    public final DateTimePath<java.sql.Timestamp> regDate = createDateTime("regDate", java.sql.Timestamp.class);

    public final NumberPath<Double> sensorData = createNumber("sensorData", Double.class);

    public final NumberPath<Double> sensorDataY = createNumber("sensorDataY", Double.class);

    public final NumberPath<Integer> sensorIdx = createNumber("sensorIdx", Integer.class);

    public final StringPath sensorType = createString("sensorType");

    public final NumberPath<Double> so2 = createNumber("so2", Double.class);

    public QSensorOriginalInfoVo(String variable) {
        super(SensorOriginalInfoVo.class, forVariable(variable));
    }

    public QSensorOriginalInfoVo(Path<? extends SensorOriginalInfoVo> path) {
        super(path.getType(), path.getMetadata());
    }

    public QSensorOriginalInfoVo(PathMetadata metadata) {
        super(SensorOriginalInfoVo.class, metadata);
    }

}

