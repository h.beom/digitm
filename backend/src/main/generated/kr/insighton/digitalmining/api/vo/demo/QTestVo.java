package kr.insighton.digitalmining.api.vo.demo;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QTestVo is a Querydsl query type for TestVo
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QTestVo extends EntityPathBase<TestVo> {

    private static final long serialVersionUID = -138546727L;

    public static final QTestVo testVo = new QTestVo("testVo");

    public final StringPath name = createString("name");

    public final StringPath text = createString("text");

    public QTestVo(String variable) {
        super(TestVo.class, forVariable(variable));
    }

    public QTestVo(Path<? extends TestVo> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTestVo(PathMetadata metadata) {
        super(TestVo.class, metadata);
    }

}

