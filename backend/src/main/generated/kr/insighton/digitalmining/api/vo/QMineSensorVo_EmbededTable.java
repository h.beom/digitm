package kr.insighton.digitalmining.api.vo;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QMineSensorVo_EmbededTable is a Querydsl query type for EmbededTable
 */
@Generated("com.querydsl.codegen.EmbeddableSerializer")
public class QMineSensorVo_EmbededTable extends BeanPath<MineSensorVo.EmbededTable> {

    private static final long serialVersionUID = 621266945L;

    public static final QMineSensorVo_EmbededTable embededTable = new QMineSensorVo_EmbededTable("embededTable");

    public final NumberPath<Double> co2 = createNumber("co2", Double.class);

    public final NumberPath<Double> hcho = createNumber("hcho", Double.class);

    public final NumberPath<Double> humi = createNumber("humi", Double.class);

    public final NumberPath<Integer> mineLocation = createNumber("mineLocation", Integer.class);

    public final NumberPath<Double> pm10 = createNumber("pm10", Double.class);

    public final NumberPath<Double> pm25 = createNumber("pm25", Double.class);

    public final NumberPath<Double> temp = createNumber("temp", Double.class);

    public final DatePath<java.sql.Date> time = createDate("time", java.sql.Date.class);

    public final NumberPath<Double> tvoc = createNumber("tvoc", Double.class);

    public QMineSensorVo_EmbededTable(String variable) {
        super(MineSensorVo.EmbededTable.class, forVariable(variable));
    }

    public QMineSensorVo_EmbededTable(Path<? extends MineSensorVo.EmbededTable> path) {
        super(path.getType(), path.getMetadata());
    }

    public QMineSensorVo_EmbededTable(PathMetadata metadata) {
        super(MineSensorVo.EmbededTable.class, metadata);
    }

}

