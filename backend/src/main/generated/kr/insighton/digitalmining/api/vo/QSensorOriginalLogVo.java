package kr.insighton.digitalmining.api.vo;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QSensorOriginalLogVo is a Querydsl query type for SensorOriginalLogVo
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QSensorOriginalLogVo extends EntityPathBase<SensorOriginalLogVo> {

    private static final long serialVersionUID = -1178755723L;

    public static final QSensorOriginalLogVo sensorOriginalLogVo = new QSensorOriginalLogVo("sensorOriginalLogVo");

    public final StringPath data = createString("data");

    public final NumberPath<Integer> idx = createNumber("idx", Integer.class);

    public final DateTimePath<java.sql.Timestamp> regDate = createDateTime("regDate", java.sql.Timestamp.class);

    public final StringPath state = createString("state");

    public QSensorOriginalLogVo(String variable) {
        super(SensorOriginalLogVo.class, forVariable(variable));
    }

    public QSensorOriginalLogVo(Path<? extends SensorOriginalLogVo> path) {
        super(path.getType(), path.getMetadata());
    }

    public QSensorOriginalLogVo(PathMetadata metadata) {
        super(SensorOriginalLogVo.class, metadata);
    }

}

