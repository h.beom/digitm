package kr.insighton.digitalmining.api.vo;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QMineFanVo_EmbededTable is a Querydsl query type for EmbededTable
 */
@Generated("com.querydsl.codegen.EmbeddableSerializer")
public class QMineFanVo_EmbededTable extends BeanPath<MineFanVo.EmbededTable> {

    private static final long serialVersionUID = 1578819752L;

    public static final QMineFanVo_EmbededTable embededTable = new QMineFanVo_EmbededTable("embededTable");

    public final DatePath<java.sql.Date> fanStarttime = createDate("fanStarttime", java.sql.Date.class);

    public final DatePath<java.sql.Date> fanStoptime = createDate("fanStoptime", java.sql.Date.class);

    public final NumberPath<Integer> mineHz = createNumber("mineHz", Integer.class);

    public final NumberPath<Integer> mineLocation = createNumber("mineLocation", Integer.class);

    public final StringPath mineStatus = createString("mineStatus");

    public final DatePath<java.sql.Date> now = createDate("now", java.sql.Date.class);

    public QMineFanVo_EmbededTable(String variable) {
        super(MineFanVo.EmbededTable.class, forVariable(variable));
    }

    public QMineFanVo_EmbededTable(Path<? extends MineFanVo.EmbededTable> path) {
        super(path.getType(), path.getMetadata());
    }

    public QMineFanVo_EmbededTable(PathMetadata metadata) {
        super(MineFanVo.EmbededTable.class, metadata);
    }

}

