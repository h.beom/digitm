package kr.insighton.digitalmining.api.vo;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QMineFanVo is a Querydsl query type for MineFanVo
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QMineFanVo extends EntityPathBase<MineFanVo> {

    private static final long serialVersionUID = 1050173852L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QMineFanVo mineFanVo = new QMineFanVo("mineFanVo");

    public final QMineFanVo_EmbededTable id;

    public QMineFanVo(String variable) {
        this(MineFanVo.class, forVariable(variable), INITS);
    }

    public QMineFanVo(Path<? extends MineFanVo> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QMineFanVo(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QMineFanVo(PathMetadata metadata, PathInits inits) {
        this(MineFanVo.class, metadata, inits);
    }

    public QMineFanVo(Class<? extends MineFanVo> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.id = inits.isInitialized("id") ? new QMineFanVo_EmbededTable(forProperty("id")) : null;
    }

}

