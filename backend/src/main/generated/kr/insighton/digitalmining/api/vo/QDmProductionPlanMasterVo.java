package kr.insighton.digitalmining.api.vo;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QDmProductionPlanMasterVo is a Querydsl query type for DmProductionPlanMasterVo
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QDmProductionPlanMasterVo extends EntityPathBase<DmProductionPlanMasterVo> {

    private static final long serialVersionUID = -536483101L;

    public static final QDmProductionPlanMasterVo dmProductionPlanMasterVo = new QDmProductionPlanMasterVo("dmProductionPlanMasterVo");

    public final NumberPath<Integer> planId = createNumber("planId", Integer.class);

    public final StringPath planName = createString("planName");

    public QDmProductionPlanMasterVo(String variable) {
        super(DmProductionPlanMasterVo.class, forVariable(variable));
    }

    public QDmProductionPlanMasterVo(Path<? extends DmProductionPlanMasterVo> path) {
        super(path.getType(), path.getMetadata());
    }

    public QDmProductionPlanMasterVo(PathMetadata metadata) {
        super(DmProductionPlanMasterVo.class, metadata);
    }

}

