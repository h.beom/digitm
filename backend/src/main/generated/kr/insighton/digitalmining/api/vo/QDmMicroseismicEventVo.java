package kr.insighton.digitalmining.api.vo;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QDmMicroseismicEventVo is a Querydsl query type for DmMicroseismicEventVo
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QDmMicroseismicEventVo extends EntityPathBase<DmMicroseismicEventVo> {

    private static final long serialVersionUID = -1403362394L;

    public static final QDmMicroseismicEventVo dmMicroseismicEventVo = new QDmMicroseismicEventVo("dmMicroseismicEventVo");

    public final StringPath category = createString("category");

    public final DateTimePath<java.sql.Timestamp> datetime = createDateTime("datetime", java.sql.Timestamp.class);

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final NumberPath<Integer> number = createNumber("number", Integer.class);

    public final NumberPath<Integer> radius = createNumber("radius", Integer.class);

    public final StringPath type = createString("type");

    public final NumberPath<Integer> x = createNumber("x", Integer.class);

    public final NumberPath<Integer> y = createNumber("y", Integer.class);

    public final NumberPath<Integer> z = createNumber("z", Integer.class);

    public QDmMicroseismicEventVo(String variable) {
        super(DmMicroseismicEventVo.class, forVariable(variable));
    }

    public QDmMicroseismicEventVo(Path<? extends DmMicroseismicEventVo> path) {
        super(path.getType(), path.getMetadata());
    }

    public QDmMicroseismicEventVo(PathMetadata metadata) {
        super(DmMicroseismicEventVo.class, metadata);
    }

}

