package kr.insighton.digitalmining.api.vo;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QDmDpmLogVo is a Querydsl query type for DmDpmLogVo
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QDmDpmLogVo extends EntityPathBase<DmDpmLogVo> {

    private static final long serialVersionUID = -884373694L;

    public static final QDmDpmLogVo dmDpmLogVo = new QDmDpmLogVo("dmDpmLogVo");

    public final NumberPath<Double> co = createNumber("co", Double.class);

    public final DatePath<java.sql.Date> date = createDate("date", java.sql.Date.class);

    public final NumberPath<Double> dpm10m = createNumber("dpm10m", Double.class);

    public final NumberPath<Double> dpm30m = createNumber("dpm30m", Double.class);

    public final NumberPath<Double> humidity = createNumber("humidity", Double.class);

    public final NumberPath<Double> idx = createNumber("idx", Double.class);

    public final NumberPath<Double> no = createNumber("no", Double.class);

    public final NumberPath<Double> sensorIdx = createNumber("sensorIdx", Double.class);

    public final NumberPath<Double> temp = createNumber("temp", Double.class);

    public final TimePath<java.sql.Time> time = createTime("time", java.sql.Time.class);

    public final NumberPath<Double> x = createNumber("x", Double.class);

    public final NumberPath<Double> y = createNumber("y", Double.class);

    public final NumberPath<Double> z = createNumber("z", Double.class);

    public QDmDpmLogVo(String variable) {
        super(DmDpmLogVo.class, forVariable(variable));
    }

    public QDmDpmLogVo(Path<? extends DmDpmLogVo> path) {
        super(path.getType(), path.getMetadata());
    }

    public QDmDpmLogVo(PathMetadata metadata) {
        super(DmDpmLogVo.class, metadata);
    }

}

