package kr.insighton.digitalmining.api.vo;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QDmGradeControlLogVo is a Querydsl query type for DmGradeControlLogVo
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QDmGradeControlLogVo extends EntityPathBase<DmGradeControlLogVo> {

    private static final long serialVersionUID = -2050121999L;

    public static final QDmGradeControlLogVo dmGradeControlLogVo = new QDmGradeControlLogVo("dmGradeControlLogVo");

    public final NumberPath<Double> al2o3 = createNumber("al2o3", Double.class);

    public final NumberPath<Double> al2o3Cumulative = createNumber("al2o3Cumulative", Double.class);

    public final NumberPath<Double> cao = createNumber("cao", Double.class);

    public final NumberPath<Double> caoCumulative = createNumber("caoCumulative", Double.class);

    public final DatePath<java.sql.Date> date = createDate("date", java.sql.Date.class);

    public final NumberPath<Double> fe2o3 = createNumber("fe2o3", Double.class);

    public final NumberPath<Double> fe2o3Cumulative = createNumber("fe2o3Cumulative", Double.class);

    public final NumberPath<Integer> idx = createNumber("idx", Integer.class);

    public final NumberPath<Integer> input = createNumber("input", Integer.class);

    public final NumberPath<Integer> inputCumulative = createNumber("inputCumulative", Integer.class);

    public final NumberPath<Integer> inputInner = createNumber("inputInner", Integer.class);

    public final NumberPath<Integer> inputOuter = createNumber("inputOuter", Integer.class);

    public final NumberPath<Double> k2o = createNumber("k2o", Double.class);

    public final NumberPath<Double> k2oCumulative = createNumber("k2oCumulative", Double.class);

    public final NumberPath<Double> mgo = createNumber("mgo", Double.class);

    public final NumberPath<Double> mgoCumulative = createNumber("mgoCumulative", Double.class);

    public final NumberPath<Double> na2o = createNumber("na2o", Double.class);

    public final NumberPath<Double> na2oCumulative = createNumber("na2oCumulative", Double.class);

    public final StringPath note = createString("note");

    public final NumberPath<Integer> number = createNumber("number", Integer.class);

    public final NumberPath<Double> sio2 = createNumber("sio2", Double.class);

    public final NumberPath<Double> sio2Cumulative = createNumber("sio2Cumulative", Double.class);

    public final NumberPath<Double> so3 = createNumber("so3", Double.class);

    public final NumberPath<Double> so3Cumulative = createNumber("so3Cumulative", Double.class);

    public final NumberPath<Double> sum = createNumber("sum", Double.class);

    public final NumberPath<Double> sumCumulative = createNumber("sumCumulative", Double.class);

    public final TimePath<java.sql.Time> time = createTime("time", java.sql.Time.class);

    public QDmGradeControlLogVo(String variable) {
        super(DmGradeControlLogVo.class, forVariable(variable));
    }

    public QDmGradeControlLogVo(Path<? extends DmGradeControlLogVo> path) {
        super(path.getType(), path.getMetadata());
    }

    public QDmGradeControlLogVo(PathMetadata metadata) {
        super(DmGradeControlLogVo.class, metadata);
    }

}

