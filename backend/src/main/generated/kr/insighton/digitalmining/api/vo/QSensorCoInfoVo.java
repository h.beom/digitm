package kr.insighton.digitalmining.api.vo;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QSensorCoInfoVo is a Querydsl query type for SensorCoInfoVo
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QSensorCoInfoVo extends EntityPathBase<SensorCoInfoVo> {

    private static final long serialVersionUID = -608805302L;

    public static final QSensorCoInfoVo sensorCoInfoVo = new QSensorCoInfoVo("sensorCoInfoVo");

    public final NumberPath<Integer> idx = createNumber("idx", Integer.class);

    public final DateTimePath<java.sql.Timestamp> regDate = createDateTime("regDate", java.sql.Timestamp.class);

    public final StringPath sensorCo = createString("sensorCo");

    public final StringPath sensorCoValue = createString("sensorCoValue");

    public final StringPath type = createString("type");

    public QSensorCoInfoVo(String variable) {
        super(SensorCoInfoVo.class, forVariable(variable));
    }

    public QSensorCoInfoVo(Path<? extends SensorCoInfoVo> path) {
        super(path.getType(), path.getMetadata());
    }

    public QSensorCoInfoVo(PathMetadata metadata) {
        super(SensorCoInfoVo.class, metadata);
    }

}

