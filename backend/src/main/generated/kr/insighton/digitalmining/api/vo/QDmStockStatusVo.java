package kr.insighton.digitalmining.api.vo;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QDmStockStatusVo is a Querydsl query type for DmStockStatusVo
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QDmStockStatusVo extends EntityPathBase<DmStockStatusVo> {

    private static final long serialVersionUID = -1861180357L;

    public static final QDmStockStatusVo dmStockStatusVo = new QDmStockStatusVo("dmStockStatusVo");

    public final NumberPath<Integer> idx = createNumber("idx", Integer.class);

    public final NumberPath<Integer> procurementChange = createNumber("procurementChange", Integer.class);

    public final NumberPath<Integer> procurementInput = createNumber("procurementInput", Integer.class);

    public final NumberPath<Integer> procurementOutput = createNumber("procurementOutput", Integer.class);

    public final NumberPath<Integer> procurementPostAmount = createNumber("procurementPostAmount", Integer.class);

    public final NumberPath<Integer> procurementPreAmount = createNumber("procurementPreAmount", Integer.class);

    public final NumberPath<Integer> productionChange = createNumber("productionChange", Integer.class);

    public final NumberPath<Integer> productionInput = createNumber("productionInput", Integer.class);

    public final NumberPath<Integer> productionOutput = createNumber("productionOutput", Integer.class);

    public final NumberPath<Integer> productionPostAmount = createNumber("productionPostAmount", Integer.class);

    public final NumberPath<Integer> productionPreAmount = createNumber("productionPreAmount", Integer.class);

    public final NumberPath<Integer> weighbridgeLogIdx = createNumber("weighbridgeLogIdx", Integer.class);

    public QDmStockStatusVo(String variable) {
        super(DmStockStatusVo.class, forVariable(variable));
    }

    public QDmStockStatusVo(Path<? extends DmStockStatusVo> path) {
        super(path.getType(), path.getMetadata());
    }

    public QDmStockStatusVo(PathMetadata metadata) {
        super(DmStockStatusVo.class, metadata);
    }

}

