package kr.insighton.digitalmining.api.vo;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QDmWeighbridgeLogVo is a Querydsl query type for DmWeighbridgeLogVo
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QDmWeighbridgeLogVo extends EntityPathBase<DmWeighbridgeLogVo> {

    private static final long serialVersionUID = -240768738L;

    public static final QDmWeighbridgeLogVo dmWeighbridgeLogVo = new QDmWeighbridgeLogVo("dmWeighbridgeLogVo");

    public final StringPath company = createString("company");

    public final DatePath<java.sql.Date> date = createDate("date", java.sql.Date.class);

    public final NumberPath<Integer> idx = createNumber("idx", Integer.class);

    public final StringPath manager = createString("manager");

    public final StringPath productName = createString("productName");

    public final StringPath source = createString("source");

    public final TimePath<java.sql.Time> time1 = createTime("time1", java.sql.Time.class);

    public final TimePath<java.sql.Time> time2 = createTime("time2", java.sql.Time.class);

    public final StringPath truckCarNumber = createString("truckCarNumber");

    public final NumberPath<Integer> truckId = createNumber("truckId", Integer.class);

    public final StringPath type = createString("type");

    public final NumberPath<Integer> weighNumber = createNumber("weighNumber", Integer.class);

    public final NumberPath<Integer> weight1 = createNumber("weight1", Integer.class);

    public final NumberPath<Integer> weight2 = createNumber("weight2", Integer.class);

    public final NumberPath<Integer> weightActual = createNumber("weightActual", Integer.class);

    public final NumberPath<Integer> weightLoss = createNumber("weightLoss", Integer.class);

    public final NumberPath<Double> weightLossRate = createNumber("weightLossRate", Double.class);

    public QDmWeighbridgeLogVo(String variable) {
        super(DmWeighbridgeLogVo.class, forVariable(variable));
    }

    public QDmWeighbridgeLogVo(Path<? extends DmWeighbridgeLogVo> path) {
        super(path.getType(), path.getMetadata());
    }

    public QDmWeighbridgeLogVo(PathMetadata metadata) {
        super(DmWeighbridgeLogVo.class, metadata);
    }

}

