package kr.insighton.digitalmining.api.vo;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QMineSensorVo is a Querydsl query type for MineSensorVo
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QMineSensorVo extends EntityPathBase<MineSensorVo> {

    private static final long serialVersionUID = 1651080611L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QMineSensorVo mineSensorVo = new QMineSensorVo("mineSensorVo");

    public final QMineSensorVo_EmbededTable id;

    public QMineSensorVo(String variable) {
        this(MineSensorVo.class, forVariable(variable), INITS);
    }

    public QMineSensorVo(Path<? extends MineSensorVo> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QMineSensorVo(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QMineSensorVo(PathMetadata metadata, PathInits inits) {
        this(MineSensorVo.class, metadata, inits);
    }

    public QMineSensorVo(Class<? extends MineSensorVo> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.id = inits.isInitialized("id") ? new QMineSensorVo_EmbededTable(forProperty("id")) : null;
    }

}

