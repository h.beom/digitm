package kr.insighton.digitalmining.api.vo;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QRtlsTagLogVo is a Querydsl query type for RtlsTagLogVo
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QRtlsTagLogVo extends EntityPathBase<RtlsTagLogVo> {

    private static final long serialVersionUID = -1475269591L;

    public static final QRtlsTagLogVo rtlsTagLogVo = new QRtlsTagLogVo("rtlsTagLogVo");

    public final NumberPath<Integer> idx = createNumber("idx", Integer.class);

    public final DateTimePath<java.sql.Timestamp> regDate = createDateTime("regDate", java.sql.Timestamp.class);

    public final StringPath tagId = createString("tagId");

    public final StringPath x = createString("x");

    public final StringPath y = createString("y");

    public final StringPath z = createString("z");

    public QRtlsTagLogVo(String variable) {
        super(RtlsTagLogVo.class, forVariable(variable));
    }

    public QRtlsTagLogVo(Path<? extends RtlsTagLogVo> path) {
        super(path.getType(), path.getMetadata());
    }

    public QRtlsTagLogVo(PathMetadata metadata) {
        super(RtlsTagLogVo.class, metadata);
    }

}

