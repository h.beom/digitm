package kr.insighton.digitalmining.api.vo;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QUserVo is a Querydsl query type for UserVo
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QUserVo extends EntityPathBase<UserVo> {

    private static final long serialVersionUID = 1791912385L;

    public static final QUserVo userVo = new QUserVo("userVo");

    public final NumberPath<Integer> authority = createNumber("authority", Integer.class);

    public final StringPath email = createString("email");

    public final StringPath id = createString("id");

    public final NumberPath<Integer> idx = createNumber("idx", Integer.class);

    public final StringPath info = createString("info");

    public final StringPath ip = createString("ip");

    public final StringPath name = createString("name");

    public final StringPath password = createString("password");

    public final DateTimePath<java.sql.Timestamp> regDate = createDateTime("regDate", java.sql.Timestamp.class);

    public final StringPath tel = createString("tel");

    public QUserVo(String variable) {
        super(UserVo.class, forVariable(variable));
    }

    public QUserVo(Path<? extends UserVo> path) {
        super(path.getType(), path.getMetadata());
    }

    public QUserVo(PathMetadata metadata) {
        super(UserVo.class, metadata);
    }

}

