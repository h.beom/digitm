package kr.insighton.digitalmining.api.vo;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QDmGroundMonitoringLogVo is a Querydsl query type for DmGroundMonitoringLogVo
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QDmGroundMonitoringLogVo extends EntityPathBase<DmGroundMonitoringLogVo> {

    private static final long serialVersionUID = -905100312L;

    public static final QDmGroundMonitoringLogVo dmGroundMonitoringLogVo = new QDmGroundMonitoringLogVo("dmGroundMonitoringLogVo");

    public final DatePath<java.sql.Date> date = createDate("date", java.sql.Date.class);

    public final NumberPath<Double> dxDt = createNumber("dxDt", Double.class);

    public final NumberPath<Double> dyDt = createNumber("dyDt", Double.class);

    public final NumberPath<Integer> idx = createNumber("idx", Integer.class);

    public final NumberPath<Integer> sensorIdx = createNumber("sensorIdx", Integer.class);

    public final TimePath<java.sql.Time> time = createTime("time", java.sql.Time.class);

    public final NumberPath<Double> xDegree = createNumber("xDegree", Double.class);

    public final NumberPath<Double> yDegree = createNumber("yDegree", Double.class);

    public QDmGroundMonitoringLogVo(String variable) {
        super(DmGroundMonitoringLogVo.class, forVariable(variable));
    }

    public QDmGroundMonitoringLogVo(Path<? extends DmGroundMonitoringLogVo> path) {
        super(path.getType(), path.getMetadata());
    }

    public QDmGroundMonitoringLogVo(PathMetadata metadata) {
        super(DmGroundMonitoringLogVo.class, metadata);
    }

}

