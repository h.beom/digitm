package kr.insighton.digitalmining.api.vo;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QDmSensorVo is a Querydsl query type for DmSensorVo
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QDmSensorVo extends EntityPathBase<DmSensorVo> {

    private static final long serialVersionUID = -1619971303L;

    public static final QDmSensorVo dmSensorVo = new QDmSensorVo("dmSensorVo");

    public final StringPath division = createString("division");

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final StringPath sensorId = createString("sensorId");

    public final StringPath sensorName = createString("sensorName");

    public final StringPath type = createString("type");

    public final NumberPath<Integer> x = createNumber("x", Integer.class);

    public final NumberPath<Integer> y = createNumber("y", Integer.class);

    public final NumberPath<Integer> z = createNumber("z", Integer.class);

    public QDmSensorVo(String variable) {
        super(DmSensorVo.class, forVariable(variable));
    }

    public QDmSensorVo(Path<? extends DmSensorVo> path) {
        super(path.getType(), path.getMetadata());
    }

    public QDmSensorVo(PathMetadata metadata) {
        super(DmSensorVo.class, metadata);
    }

}

