package kr.insighton.digitalmining.api.vo;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QDmProductionPlanVo is a Querydsl query type for DmProductionPlanVo
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QDmProductionPlanVo extends EntityPathBase<DmProductionPlanVo> {

    private static final long serialVersionUID = 2127794913L;

    public static final QDmProductionPlanVo dmProductionPlanVo = new QDmProductionPlanVo("dmProductionPlanVo");

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final NumberPath<Integer> planAmount = createNumber("planAmount", Integer.class);

    public final DatePath<java.sql.Date> planDate = createDate("planDate", java.sql.Date.class);

    public final NumberPath<Integer> planId = createNumber("planId", Integer.class);

    public QDmProductionPlanVo(String variable) {
        super(DmProductionPlanVo.class, forVariable(variable));
    }

    public QDmProductionPlanVo(Path<? extends DmProductionPlanVo> path) {
        super(path.getType(), path.getMetadata());
    }

    public QDmProductionPlanVo(PathMetadata metadata) {
        super(DmProductionPlanVo.class, metadata);
    }

}

