package kr.insighton.digitalmining.api.vo;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QGwInfoVo is a Querydsl query type for GwInfoVo
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QGwInfoVo extends EntityPathBase<GwInfoVo> {

    private static final long serialVersionUID = -433408076L;

    public static final QGwInfoVo gwInfoVo = new QGwInfoVo("gwInfoVo");

    public final NumberPath<Integer> gwConnection = createNumber("gwConnection", Integer.class);

    public final StringPath gwIp = createString("gwIp");

    public final StringPath gwName = createString("gwName");

    public final StringPath gwPoint = createString("gwPoint");

    public final NumberPath<Integer> gwType = createNumber("gwType", Integer.class);

    public final NumberPath<Integer> gwUserIdx = createNumber("gwUserIdx", Integer.class);

    public final NumberPath<Integer> idx = createNumber("idx", Integer.class);

    public final DateTimePath<java.sql.Timestamp> regDate = createDateTime("regDate", java.sql.Timestamp.class);

    public QGwInfoVo(String variable) {
        super(GwInfoVo.class, forVariable(variable));
    }

    public QGwInfoVo(Path<? extends GwInfoVo> path) {
        super(path.getType(), path.getMetadata());
    }

    public QGwInfoVo(PathMetadata metadata) {
        super(GwInfoVo.class, metadata);
    }

}

