package kr.insighton.digitalmining.api.vo;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QDmMicroseismicEventSensorDetailVo is a Querydsl query type for DmMicroseismicEventSensorDetailVo
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QDmMicroseismicEventSensorDetailVo extends EntityPathBase<DmMicroseismicEventSensorDetailVo> {

    private static final long serialVersionUID = -1197536719L;

    public static final QDmMicroseismicEventSensorDetailVo dmMicroseismicEventSensorDetailVo = new QDmMicroseismicEventSensorDetailVo("dmMicroseismicEventSensorDetailVo");

    public final NumberPath<Integer> eventId = createNumber("eventId", Integer.class);

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final StringPath imgLink = createString("imgLink");

    public final NumberPath<Integer> sensorId = createNumber("sensorId", Integer.class);

    public QDmMicroseismicEventSensorDetailVo(String variable) {
        super(DmMicroseismicEventSensorDetailVo.class, forVariable(variable));
    }

    public QDmMicroseismicEventSensorDetailVo(Path<? extends DmMicroseismicEventSensorDetailVo> path) {
        super(path.getType(), path.getMetadata());
    }

    public QDmMicroseismicEventSensorDetailVo(PathMetadata metadata) {
        super(DmMicroseismicEventSensorDetailVo.class, metadata);
    }

}

