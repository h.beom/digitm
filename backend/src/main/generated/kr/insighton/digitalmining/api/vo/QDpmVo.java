package kr.insighton.digitalmining.api.vo;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QDpmVo is a Querydsl query type for DpmVo
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QDpmVo extends EntityPathBase<DpmVo> {

    private static final long serialVersionUID = 319115901L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QDpmVo dpmVo = new QDpmVo("dpmVo");

    public final QDpmVo_EmbededTable id;

    public QDpmVo(String variable) {
        this(DpmVo.class, forVariable(variable), INITS);
    }

    public QDpmVo(Path<? extends DpmVo> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QDpmVo(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QDpmVo(PathMetadata metadata, PathInits inits) {
        this(DpmVo.class, metadata, inits);
    }

    public QDpmVo(Class<? extends DpmVo> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.id = inits.isInitialized("id") ? new QDpmVo_EmbededTable(forProperty("id")) : null;
    }

}

