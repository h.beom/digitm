package kr.insighton.digitalmining.api.vo;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QRtlsVo is a Querydsl query type for RtlsVo
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QRtlsVo extends EntityPathBase<RtlsVo> {

    private static final long serialVersionUID = 1707157951L;

    public static final QRtlsVo rtlsVo = new QRtlsVo("rtlsVo");

    public final NumberPath<Integer> count = createNumber("count", Integer.class);

    public final StringPath department = createString("department");

    public final NumberPath<Integer> idx = createNumber("idx", Integer.class);

    public final NumberPath<Integer> isMoving = createNumber("isMoving", Integer.class);

    public final StringPath memo = createString("memo");

    public final StringPath phone = createString("phone");

    public final NumberPath<Integer> pointH = createNumber("pointH", Integer.class);

    public final StringPath pointText = createString("pointText");

    public final NumberPath<Integer> pointW = createNumber("pointW", Integer.class);

    public final DatePath<java.sql.Date> readTime = createDate("readTime", java.sql.Date.class);

    public final StringPath regDate = createString("regDate");

    public final StringPath rtlsName = createString("rtlsName");

    public final StringPath rType = createString("rType");

    public final NumberPath<Integer> sensorIdx = createNumber("sensorIdx", Integer.class);

    public final StringPath tagUid = createString("tagUid");

    public final NumberPath<Integer> uesType = createNumber("uesType", Integer.class);

    public final NumberPath<Double> xPoint = createNumber("xPoint", Double.class);

    public final NumberPath<Double> yPoint = createNumber("yPoint", Double.class);

    public QRtlsVo(String variable) {
        super(RtlsVo.class, forVariable(variable));
    }

    public QRtlsVo(Path<? extends RtlsVo> path) {
        super(path.getType(), path.getMetadata());
    }

    public QRtlsVo(PathMetadata metadata) {
        super(RtlsVo.class, metadata);
    }

}

