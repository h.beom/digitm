package kr.insighton.digitalmining.api.vo;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QSensorInfoVo is a Querydsl query type for SensorInfoVo
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QSensorInfoVo extends EntityPathBase<SensorInfoVo> {

    private static final long serialVersionUID = 631416958L;

    public static final QSensorInfoVo sensorInfoVo = new QSensorInfoVo("sensorInfoVo");

    public final NumberPath<Double> ePoint = createNumber("ePoint", Double.class);

    public final StringPath gateName = createString("gateName");

    public final StringPath gatePoint = createString("gatePoint");

    public final NumberPath<Integer> idx = createNumber("idx", Integer.class);

    public final StringPath mainName = createString("mainName");

    public final StringPath memo = createString("memo");

    public final StringPath organizationName = createString("organizationName");

    public final StringPath placeType = createString("placeType");

    public final DateTimePath<java.sql.Timestamp> regDate = createDateTime("regDate", java.sql.Timestamp.class);

    public final StringPath sensorInfo = createString("sensorInfo");

    public final StringPath sensorName = createString("sensorName");

    public final StringPath sensorNewname = createString("sensorNewname");

    public final StringPath sensorPoint = createString("sensorPoint");

    public final StringPath sensorSn = createString("sensorSn");

    public final StringPath sensorType = createString("sensorType");

    public final StringPath setupNumber = createString("setupNumber");

    public final StringPath showText = createString("showText");

    public final NumberPath<Double> sPoint = createNumber("sPoint", Double.class);

    public final StringPath standard1down = createString("standard1down");

    public final StringPath standard1up = createString("standard1up");

    public final StringPath standard2down = createString("standard2down");

    public final StringPath standard2up = createString("standard2up");

    public final StringPath standard3down = createString("standard3down");

    public final StringPath standard3up = createString("standard3up");

    public final StringPath standardNy = createString("standardNy");

    public final StringPath standardStep = createString("standardStep");

    public final StringPath standardValue = createString("standardValue");

    public final StringPath subName = createString("subName");

    public final StringPath warningStep = createString("warningStep");

    public final StringPath warningText = createString("warningText");

    public final StringPath warningType = createString("warningType");

    public QSensorInfoVo(String variable) {
        super(SensorInfoVo.class, forVariable(variable));
    }

    public QSensorInfoVo(Path<? extends SensorInfoVo> path) {
        super(path.getType(), path.getMetadata());
    }

    public QSensorInfoVo(PathMetadata metadata) {
        super(SensorInfoVo.class, metadata);
    }

}

