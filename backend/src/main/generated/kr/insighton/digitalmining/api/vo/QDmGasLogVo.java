package kr.insighton.digitalmining.api.vo;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QDmGasLogVo is a Querydsl query type for DmGasLogVo
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QDmGasLogVo extends EntityPathBase<DmGasLogVo> {

    private static final long serialVersionUID = -206788406L;

    public static final QDmGasLogVo dmGasLogVo = new QDmGasLogVo("dmGasLogVo");

    public final NumberPath<Double> co = createNumber("co", Double.class);

    public final NumberPath<Double> co2 = createNumber("co2", Double.class);

    public final DatePath<java.sql.Date> date = createDate("date", java.sql.Date.class);

    public final NumberPath<Double> humidity = createNumber("humidity", Double.class);

    public final NumberPath<Double> idx = createNumber("idx", Double.class);

    public final NumberPath<Double> no = createNumber("no", Double.class);

    public final NumberPath<Double> no2 = createNumber("no2", Double.class);

    public final NumberPath<Double> sensorIdx = createNumber("sensorIdx", Double.class);

    public final NumberPath<Double> so2 = createNumber("so2", Double.class);

    public final NumberPath<Double> temp = createNumber("temp", Double.class);

    public final TimePath<java.sql.Time> time = createTime("time", java.sql.Time.class);

    public final NumberPath<Double> x = createNumber("x", Double.class);

    public final NumberPath<Double> y = createNumber("y", Double.class);

    public final NumberPath<Double> z = createNumber("z", Double.class);

    public QDmGasLogVo(String variable) {
        super(DmGasLogVo.class, forVariable(variable));
    }

    public QDmGasLogVo(Path<? extends DmGasLogVo> path) {
        super(path.getType(), path.getMetadata());
    }

    public QDmGasLogVo(PathMetadata metadata) {
        super(DmGasLogVo.class, metadata);
    }

}

