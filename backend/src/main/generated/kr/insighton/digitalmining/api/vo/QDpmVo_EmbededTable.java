package kr.insighton.digitalmining.api.vo;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QDpmVo_EmbededTable is a Querydsl query type for EmbededTable
 */
@Generated("com.querydsl.codegen.EmbeddableSerializer")
public class QDpmVo_EmbededTable extends BeanPath<DpmVo.EmbededTable> {

    private static final long serialVersionUID = -1977473689L;

    public static final QDpmVo_EmbededTable embededTable = new QDpmVo_EmbededTable("embededTable");

    public final NumberPath<Double> ecValue = createNumber("ecValue", Double.class);

    public final NumberPath<Integer> faultCode = createNumber("faultCode", Integer.class);

    public final NumberPath<Integer> flowLevel = createNumber("flowLevel", Integer.class);

    public final NumberPath<Integer> flowSensor = createNumber("flowSensor", Integer.class);

    public final NumberPath<Double> n5MinEc = createNumber("n5MinEc", Double.class);

    public final NumberPath<Integer> rawSensor = createNumber("rawSensor", Integer.class);

    public final DateTimePath<java.sql.Timestamp> regDate = createDateTime("regDate", java.sql.Timestamp.class);

    public final NumberPath<Integer> sensorGain = createNumber("sensorGain", Integer.class);

    public final NumberPath<Integer> siteNum = createNumber("siteNum", Integer.class);

    public final NumberPath<Double> twaValue = createNumber("twaValue", Double.class);

    public QDpmVo_EmbededTable(String variable) {
        super(DpmVo.EmbededTable.class, forVariable(variable));
    }

    public QDpmVo_EmbededTable(Path<? extends DpmVo.EmbededTable> path) {
        super(path.getType(), path.getMetadata());
    }

    public QDpmVo_EmbededTable(PathMetadata metadata) {
        super(DpmVo.EmbededTable.class, metadata);
    }

}

