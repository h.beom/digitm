/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.api.service;

import kr.insighton.digitalmining.api.repository.DmSensorRepository;
import kr.insighton.digitalmining.common.util.CommonUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 *
 * title : DmMicroseismicSensor Service
 * author : 윤영수
 * date : 2020.09.08
 *
 * */

@Service
@Transactional
public class DmSensorService {

    @Resource
    private DmSensorRepository dmSensorRepository;

    public Map<String, Object> findAllByDivision(String division) {
        Map<String, Object> data = new HashMap<>();
        List<Map<String, Object>> sensors = dmSensorRepository.findAllByDivisionWithNativeQuery(division);
        if (sensors != null && sensors.size() > 0) {
            data.put("sensors", sensors);
        } else {
            data.put("sensors", new ArrayList<>());
        }

        return CommonUtil.setResult(data);
    }
}
