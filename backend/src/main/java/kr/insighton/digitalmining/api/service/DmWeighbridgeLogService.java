/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.api.service;

import kr.insighton.digitalmining.api.repository.DmStockStatusRepository;
import kr.insighton.digitalmining.api.repository.DmWeighbridgeLogRepository;
import kr.insighton.digitalmining.common.service.CodeService;
import kr.insighton.digitalmining.common.util.CommonUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 *
 * title : DmWeighbridgeLog Service
 * author : 윤영수
 * date : 2020.08.19
 *
 * */

@Service
@Transactional
public class DmWeighbridgeLogService {

    @Resource
    private CodeService codeService;

    @Resource
    private DmWeighbridgeLogRepository dmWeighbridgeLogRepository;

    @Resource
    private DmStockStatusRepository dmStockStatusRepository;

    public Map<String, Object> findProductionWeighbridgeRealtime() {
        // 조건용 공통코드 셋
        String sourceA = codeService.findByName("갱내파쇄", "거래처");
        String sourceB = codeService.findByName("원석야적", "거래처");
        String sourceC = codeService.findByName("고품위석회석", "거래처");
        String sourceD = codeService.findByName("고품위석회석 원석야적", "거래처");
        String productA = codeService.findByName("건동석회석", "품명");
        String productB = codeService.findByName("원석야적", "품명");
        String typeA = codeService.findByName("입고", "구분");
        String typeB = codeService.findByName("출고", "구분");

        if (sourceA == null || sourceB == null || sourceC == null || sourceD == null ||productA == null
                || productB == null || typeA == null || typeB == null) {
            return CommonUtil.setError("데이터 조회 중 오류가 발생했습니다.");
        }

        Map<String, Object> data = new HashMap<>();

        data.put("datetime", CommonUtil.getTodayTimeAll());
        // 생산실적 생산량
        // 조건1 - 거래처: 갱내파쇄, 품명: 건동석회석, 구분: 입고
        // 또는
        // 조건2 - 거래처: 갱내파쇄, 품명: 원석야적, 구분: 입고
        Map<String, Object> production = new HashMap<>();
        // 일 생산량
        Map<String, Object> productionCurrent = dmWeighbridgeLogRepository.findTodayAmountAndSourceWithNativeQuery(CommonUtil.getToday()
                , sourceA, productA, typeA, sourceA, productB, typeA);
        if (CommonUtil.isNullMap(productionCurrent, "total")) {
            production.put("current", 0);
        } else {
            production.put("current", productionCurrent.get("total"));
        }
        // 월 누계 생산량
        Map<String, Object> productionMonth = dmWeighbridgeLogRepository.findGroupByDateAndSourceWithNativeQuery(CommonUtil.getTodayYearMonth()
                , sourceA, productA, typeA, sourceA, productB, typeA);
        if (CommonUtil.isNullMap(productionMonth, "total")) {
            production.put("month", 0);
        } else {
            production.put("month", productionMonth.get("total"));
        }
        // 연 누계 생산량
        Map<String, Object> productionYear = dmWeighbridgeLogRepository.findGroupByDateAndSourceWithNativeQuery(CommonUtil.getTodayYear()
                , sourceA, productA, typeA, sourceA, productB, typeA);
        if (CommonUtil.isNullMap(productionYear, "total")) {
            production.put("year", 0);
        } else {
            production.put("year", productionYear.get("total"));
        }
        // 생산실적 출고량
        // 조건1 - 거래처: 갱내파쇄, 품명: 건동석회석, 구분: 입고
        // 또는
        // 조건2 - 거래처: 원석야적, 품명: 건동석회석, 구분: 출고
        // 출고량
        Map<String, Object> productionCurrentOut = dmWeighbridgeLogRepository.findTodayAmountAndSourceWithNativeQuery(CommonUtil.getToday()
                , sourceA, productA, typeA, sourceB, productA, typeB);
        if (CommonUtil.isNullMap(productionCurrentOut, "total")) {
            production.put("currentOut", 0);
        } else {
            production.put("currentOut", productionCurrentOut.get("total"));
        }
        // 월 누계 출고량
        Map<String, Object> productionMonthOut = dmWeighbridgeLogRepository.findGroupByDateAndSourceWithNativeQuery(CommonUtil.getTodayYearMonth()
                , sourceA, productA, typeA, sourceB, productA, typeB);
        if (CommonUtil.isNullMap(productionMonthOut, "total")) {
            production.put("monthOut", 0);
        } else {
            production.put("monthOut", productionMonthOut.get("total"));
        }
        // 연 누계 출고량
        Map<String, Object> productionYearOut = dmWeighbridgeLogRepository.findGroupByDateAndSourceWithNativeQuery(CommonUtil.getTodayYear()
                , sourceA, productA, typeA, sourceB, productA, typeB);
        if (CommonUtil.isNullMap(productionYearOut, "total")) {
            production.put("yearOut", 0);
        } else {
            production.put("yearOut", productionYearOut.get("total"));
        }

        data.put("production", production);

        // 구매실적 입고량
        // 조건1 - 거래처: 고품위석회석, 품명: 원석야적, 구분: 입고
        // 또는
        // 조건2 - 거래처: 고품위석회석, 품명: 건동석회석, 구분: 입고
        Map<String, Object> procurement = new HashMap<>();
        // 입고량
        Map<String, Object> procurementCurrent = dmWeighbridgeLogRepository.findTodayAmountAndSourceWithNativeQuery(CommonUtil.getToday()
                , sourceC, productB, typeA, sourceC, productA, typeA);
        if (CommonUtil.isNullMap(procurementCurrent, "total")) {
            procurement.put("current", 0);
        } else {
            procurement.put("current", procurementCurrent.get("total"));
        }
        // 월 누계 입고량
        Map<String, Object> procurementMonth = dmWeighbridgeLogRepository.findGroupByDateAndSourceWithNativeQuery(CommonUtil.getTodayYearMonth()
                , sourceC, productB, typeA, sourceC, productA, typeA);
        if (CommonUtil.isNullMap(procurementMonth, "total")) {
            procurement.put("month", 0);
        } else {
            procurement.put("month", procurementMonth.get("total"));
        }
        // 연 누계 입고량
        Map<String, Object> procurementYear = dmWeighbridgeLogRepository.findGroupByDateAndSourceWithNativeQuery(CommonUtil.getTodayYear()
                , sourceC, productB, typeA, sourceC, productA, typeA);
        if (CommonUtil.isNullMap(procurementYear, "total")) {
            procurement.put("year", 0);
        } else {
            procurement.put("year", procurementYear.get("total"));
        }
        // 구매실적 출고량
        // 조건1 - 거래처: 고품위석회석, 품명: 건동석회석, 구분: 입고
        // 또는
        // 조건2 - 거래처: 고품위석회석 원석야적, 품명: 건동석회석, 구분: 출고
        // 출고량
        Map<String, Object> procurementCurrentOut = dmWeighbridgeLogRepository.findTodayAmountAndSourceWithNativeQuery(CommonUtil.getToday()
                , sourceC, productA, typeA, sourceD, productA, typeB);
        if (CommonUtil.isNullMap(procurementCurrentOut, "total")) {
            procurement.put("currentOut", 0);
        } else {
            procurement.put("currentOut", procurementCurrentOut.get("total"));
        }
        // 월 누계 출고량
        Map<String, Object> procurementMonthOut = dmWeighbridgeLogRepository.findGroupByDateAndSourceWithNativeQuery(CommonUtil.getTodayYearMonth()
                , sourceC, productA, typeA, sourceD, productA, typeB);
        if (CommonUtil.isNullMap(procurementMonthOut, "total")) {
            procurement.put("monthOut", 0);
        } else {
            procurement.put("monthOut", procurementMonthOut.get("total"));
        }
        // 연 누계 출고량
        Map<String, Object> procurementYearOut = dmWeighbridgeLogRepository.findGroupByDateAndSourceWithNativeQuery(CommonUtil.getTodayYear()
                , sourceC, productA, typeA, sourceD, productA, typeB);
        if (CommonUtil.isNullMap(procurementYearOut, "total")) {
            procurement.put("yearOut", 0);
        } else {
            procurement.put("yearOut", procurementYearOut.get("total"));
        }

        data.put("procurement", procurement);

        // 계근회자
        Map<String, Object> lastNumber = dmWeighbridgeLogRepository.findByMaxWeighNumber(CommonUtil.getToday());
        if (CommonUtil.isNullMap(lastNumber, "weighNumber")) {
            data.put("lastNumber", null);
        } else {
            data.put("lastNumber", lastNumber.get("weighNumber"));
        }

        return CommonUtil.setResult(data);
    }

    public Map<String, Object> findProductionWeighbridgeRealtimeDetail(int number) {
        Map<String, Object> data = new HashMap<>();
        // 현재일자의 계근회차에 해당되는 데이터
        List<Map<String, Object>> list = dmWeighbridgeLogRepository.findByWeighNumberWithNativeQuery(number);
        if (list != null && list.size() > 0) {
            data.put("list", list);
        } else {
            return CommonUtil.setError("요청하신 계근회차에 대한 데이터가 존재하지 않습니다.");
        }

        Map<String, Object> productionMap = dmStockStatusRepository.findByWeighbridgeLogIdxProductionWithNativeQuery(number);
        if (CommonUtil.isNullMap(productionMap, "input")) {
            return CommonUtil.setError("요청하신 계근회차에 대한 생산 재고데이터가 존재하지 않습니다.");
        } else {
            data.put("production", productionMap);
        }

        Map<String, Object> procurementMap = dmStockStatusRepository.findByWeighbridgeLogIdxProcurementWithNativeQuery(number);
        if (CommonUtil.isNullMap(procurementMap, "input")) {
            return CommonUtil.setError("요청하신 계근회차에 대한 구매 재고데이터가 존재하지 않습니다.");
        } else {
            data.put("procurement", procurementMap);
        }

        return CommonUtil.setResult(data);
    }

    public Map<String, Object> findProductionWeighbridgeAnalysis(String date) {
        // 조건용 공통코드 셋
        String sourceA = codeService.findByName("갱내파쇄", "거래처");
        String sourceB = codeService.findByName("원석야적", "거래처");
        String sourceC = codeService.findByName("고품위석회석", "거래처");
        String productA = codeService.findByName("건동석회석", "품명");
        String productB = codeService.findByName("원석야적", "품명");
        String typeA = codeService.findByName("입고", "구분");
        String typeB = codeService.findByName("출고", "구분");

        if (sourceA == null || sourceB == null || sourceC == null || productA == null
                || productB == null || typeA == null || typeB == null) {
            return CommonUtil.setError("데이터 조회 중 오류가 발생했습니다.");
        }

        Map<String, Object> data = new HashMap<>();
        List<Map<String, Object>> list = dmWeighbridgeLogRepository.findByDateWithNativeQuery(date);
        // 생산운반량
        Map<String, Object> productionAmount = dmWeighbridgeLogRepository.findGroupByDateAndSourceWithNativeQuery(date, sourceA, productA, typeA, sourceA, productB, typeA);
        // 구매운반량
        Map<String, Object> procurementAmount = dmWeighbridgeLogRepository.findGroupByDateAndSourceWithNativeQuery(date, sourceC, productB, typeA, sourceC, productA, typeA);
        // 생산운반량 + 구매운반량
        Integer accAmount = 0;
        if (list != null & list.size() > 0) {
            data.put("accCount", list.size());
            if (!CommonUtil.isNullMap(productionAmount, "total")) {
                accAmount += ((BigDecimal)productionAmount.get("total")).intValue();
            }
            if (!CommonUtil.isNullMap(procurementAmount, "total")) {
                accAmount += ((BigDecimal)procurementAmount.get("total")).intValue();
            }
            data.put("accAmount", accAmount);
            data.put("list", list);
        } else {
            data.put("accCount" , 0);
            data.put("accAmount", accAmount);
            data.put("list", new ArrayList<>());
        }
        return CommonUtil.setResult(data);
    }

    public Map<String, Object> findProductionWeighbridgeAnalysisChart(String date) {
        // 조건용 공통코드 셋
        String sourceA = codeService.findByName("갱내파쇄", "거래처");
        String sourceB = codeService.findByName("원석야적", "거래처");
        String sourceC = codeService.findByName("고품위석회석", "거래처");
        String productA = codeService.findByName("건동석회석", "품명");
        String productB = codeService.findByName("원석야적", "품명");
        String typeA = codeService.findByName("입고", "구분");
        String typeB = codeService.findByName("출고", "구분");

        if (sourceA == null || sourceB == null || sourceC == null || productA == null
                || productB == null || typeA == null || typeB == null) {
            return CommonUtil.setError("데이터 조회 중 오류가 발생했습니다.");
        }

        List<Map<String, Object>> data =new ArrayList<>();
        // 요청일자에서 7일전 데이터의 생산운반량, 생산비축량, 구매운반량, 구매비축량
        for (int i = -6, len = 0; i <= len; i++) {
            // 생산운반량
            Map<String, Object> prodMove =new HashMap<>();
            prodMove.put("name", "prodMove");
            prodMove.put("date", CommonUtil.getDayMonthDate(date, "/", i));
            Map<String, Object> prodMoveAmount = dmWeighbridgeLogRepository.findGroupByDateAndSourceWithNativeQuery(date, sourceA, productA, typeA, sourceA, productB, typeA);
            if (CommonUtil.isNullMap(prodMoveAmount, "total")) {
                prodMove.put("value", 0);
            } else {
                prodMove.put("value", prodMoveAmount.get("total"));
            }
            data.add(prodMove);
            // 구매운반량
            Map<String, Object> purMove =new HashMap<>();
            purMove.put("name", "purMove");
            purMove.put("date", CommonUtil.getDayMonthDate(date, "/", i));
            Map<String, Object> purMoveAmount = dmWeighbridgeLogRepository.findGroupByDateAndSourceWithNativeQuery(date, sourceC, productB, typeA, sourceC, productA, typeA);
            if (CommonUtil.isNullMap(purMoveAmount, "total")) {
                purMove.put("value", 0);
            } else {
                purMove.put("value", purMoveAmount.get("total"));
            }
            data.add(purMove);
            // 생산비축량
            Map<String, Object> prodAcc =new HashMap<>();
            prodAcc.put("name", "prodAcc");
            prodAcc.put("date", CommonUtil.getDayMonthDate(date, "/", i));
            Map<String, Object> prodAccAmount = dmWeighbridgeLogRepository.findGroupByDateReserveWithNativeQuery(date, sourceA, productB, typeA);
            if (CommonUtil.isNullMap(prodAccAmount, "total")) {
                prodAcc.put("value", 0);
            } else {
                prodAcc.put("value", prodAccAmount.get("total"));
            }
            data.add(prodAcc);
            // 구매비축량
            Map<String, Object> purAcc =new HashMap<>();
            purAcc.put("name", "purAcc");
            purAcc.put("date", CommonUtil.getDayMonthDate(date, "/", i));
            Map<String, Object> purAccAmount = dmWeighbridgeLogRepository.findGroupByDateReserveWithNativeQuery(date, sourceC, productB, typeA);
            if (CommonUtil.isNullMap(purAccAmount, "total")) {
                purAcc.put("value", 0);
            } else {
                purAcc.put("value", purAccAmount.get("total"));
            }
            data.add(purAcc);
        }

        return CommonUtil.setResultList(data);
    }
}
