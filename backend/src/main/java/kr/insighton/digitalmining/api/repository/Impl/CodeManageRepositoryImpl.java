package kr.insighton.digitalmining.api.repository.Impl;

import java.util.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;
import org.hibernate.transform.*;
import kr.insighton.digitalmining.api.repository.CodeManageRepositoryCustom;
import kr.insighton.digitalmining.common.vo.CodeVo;

@Repository
public class CodeManageRepositoryImpl implements CodeManageRepositoryCustom
{
    @PersistenceContext
    private EntityManager entityManager;
    @Override
    public List<CodeVo> getCodeList(String codeGroup, String codeName)
    {
        String sqlString = " select t.idx       " +
                     "      , t.code      " +
                     "      , t.name      " +
                     "      , t.grp_code  " +
                     "      , t.sort      " +
                     "      , t.reg_date  " +
                     "      , t.item_1    " +
                     "      , t.item_2    " +
                     "      , t.item_3    " +
                     "      , t.item_4    " +
                     "      , t.item_5    " +
                     "   from t_dm_code t " +
                     "  where 1=1         ";
        
        if(StringUtils.isEmpty(codeGroup) == false)
        {
            sqlString += " AND grp_code = '" + codeGroup + "'";
        }

        if(StringUtils.isEmpty(codeName) == false)
        {
            sqlString += " AND name = '" + codeName + "'";
        }
                     
        List<CodeVo> list = entityManager.createNativeQuery(sqlString, CodeVo.class).getResultList();
        
        return list ;
    }
}