/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.api.controller.environment;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import kr.insighton.digitalmining.api.service.DmSensorService;
import kr.insighton.digitalmining.common.util.CommonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/*
 *
 * title : Sensor Controller
 * author : 윤영수
 * date : 2020.09.18
 *
 * */
@Api(tags = "채광환경 제어 - 센서")
@RestController
public class SensorController {

    @Autowired
    private DmSensorService dmSensorService;

    @ApiOperation(value = "센서 목록"
            , notes =
            "Request: {}\n" +
                    "Response: {\n" +
                    "    data: {\n" +
                    "        sensors: [\n" +
                    "            {\n" +
                    "                id: 1,\n" +
                    "                sensorId: 'Microseismic  G-7-1',\n" +
                    "                x: 178925,\n" +
                    "                y: 197397,\n" +
                    "                z: 95,\n" +
                    "                type: 1,\n" +
                    "                typeName: '단축'\n" +
                    "            },\n" +
                    "            {\n" +
                    "                id: 2,\n" +
                    "                sensorId: 'Microseismic  G-7-2',\n" +
                    "                x: 178970,\n" +
                    "                y: 197489,\n" +
                    "                z: 95,\n" +
                    "                type: 1,\n" +
                    "                typeName: '단축'\n" +
                    "            },\n" +
                    "            {\n" +
                    "                id: 3,\n" +
                    "                sensorId: 'Microseismic  G-7-6',\n" +
                    "                x: 178994,\n" +
                    "                y: 197223,\n" +
                    "                z: 95,\n" +
                    "                type: 2,\n" +
                    "                typeName: '3축'\n" +
                    "            }\n" +
                    "        ]\n" +
                    "    }\n" +
                    "}"
    )
    @GetMapping(value = "/api/environment/sensors", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> apiEnvironmentSensors(@RequestParam("division") String division) {
        return new ResponseEntity<>(dmSensorService.findAllByDivision(division), HttpStatus.OK);
    }

}
