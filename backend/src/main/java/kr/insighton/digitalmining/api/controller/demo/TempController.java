package kr.insighton.digitalmining.api.controller.demo;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TempController {

    @GetMapping(value = "/api/temp", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> apiTemp() {
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
