/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.api.repository;

import kr.insighton.digitalmining.api.vo.UserVo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/*
 *
 * title : User Repository
 * author : 윤영수
 * date : 2020.08.05
 *
 * */

@Repository
public interface UserRepository extends JpaRepository<UserVo, Integer> {

    Optional<UserVo> findById(String id);

}
