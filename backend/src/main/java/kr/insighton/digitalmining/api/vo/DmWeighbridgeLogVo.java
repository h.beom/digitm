/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.api.vo;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;

/*
 *
 * title : DmWeighbridgeLog Vo
 * author : 윤영수
 * date : 2020.08.26
 *
 * */

@Getter
@Setter
@Entity
@Table(name = "t_dm_weighbridge_log")
public class DmWeighbridgeLogVo implements Serializable {

    private static final long serialVersionUID = -357897650149306480L;

    @Id
    @Column(name = "idx")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idx;

    @Column(name = "date")
    private Date date;

    @Column(name = "time1")
    private Time time1;

    @Column(name = "time2")
    private Time time2;

    @Column(name = "weigh_number")
    private Integer weighNumber;

    @Column(name = "truck_id")
    private Integer truckId;

    @Column(name = "source")
    private String source;

    @Column(name = "product_name")
    private String productName;

    @Column(name = "company")
    private String company;

    @Column(name = "truck_car_number")
    private String truckCarNumber;

    @Column(name = "weight1")
    private Integer weight1;

    @Column(name = "weight2")
    private Integer weight2;

    @Column(name = "weight_loss")
    private Integer weightLoss;

    @Column(name = "weight_loss_rate")
    private Double weightLossRate;

    @Column(name = "weight_actual")
    private Integer weightActual;

    @Column(name = "type")
    private String type;

    @Column(name = "manager")
    private String manager;

}
