/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.api.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Delegate;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;

/*
 *
 * title : DmProductionPlan Vo
 * author : 이형범
 * date : 2020.08.19
 *
 * */

@Getter
@Setter
@Entity
@Table(name = "t_dm_production_plan")
public class DmProductionPlanVo implements Serializable {

    private static final long serialVersionUID = 6891213400391262853L;

    @Id
    @Column(name = "idx")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "plan_idx")
    private Integer planId;

    @Delegate
    @Column(name = "plan_date")
    private Date planDate;

    @Column(name = "plan_Amount")
    private Integer planAmount;
}
