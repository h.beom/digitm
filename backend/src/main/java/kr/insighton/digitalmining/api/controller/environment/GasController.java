/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.api.controller.environment;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import kr.insighton.digitalmining.api.service.DmGasLogService;
import kr.insighton.digitalmining.common.util.CommonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/*
 *
 * title : Gas Controller
 * author : 윤영수
 * date : 2020.09.18
 *
 * */

@Api(tags = "채광환경 제어 - 유해가스")
@RestController
public class GasController {

    @Autowired
    private DmGasLogService dmGasLogService;

    @ApiOperation(value = "유해가스 - 실시간 로그"
            , notes =
            "Request: {\n" +
                    "    sensorId: 1\n" +
                    "}\n" +
                    "Response: {\n" +
                    "    data: {\n" +
                    "        'logs': [\n" +
                    "            {\n" +
                    "              'id': 1,\n" +
                    "              'sensorId': 1,\n" +
                    "              'date': '2020-09-10',\n" +
                    "              'time': '09:10',\n" +
                    "              'temp': 18.1,\n" +
                    "              'humidity': 82.9,\n" +
                    "              'co2': 879,\n" +
                    "              'co': 0.45,\n" +
                    "              'no': 0.61,\n" +
                    "              'no2': 2.41,\n" +
                    "              'so2': 3.36,\n" +
                    "            },\n" +
                    "            {\n" +
                    "              'id': 1,\n" +
                    "              'sensorId': 1,\n" +
                    "              'date': '2020-09-10',\n" +
                    "              'time': '10:00',\n" +
                    "              'temp': 18.1,\n" +
                    "              'humidity': 82.9,\n" +
                    "              'co2': 879,\n" +
                    "              'co': 0.45,\n" +
                    "              'no': 0.61,\n" +
                    "              'no2': 2.41,\n" +
                    "              'so2': 3.36,\n" +
                    "            },\n" +
                    "            {\n" +
                    "              'id': 1,\n" +
                    "              'sensorId': 1,\n" +
                    "              'date': '2020-09-10',\n" +
                    "              'time': '10:20',\n" +
                    "              'temp': 18.1,\n" +
                    "              'humidity': 82.9,\n" +
                    "              'co2': 879,\n" +
                    "              'co': 0.45,\n" +
                    "              'no': 0.61,\n" +
                    "              'no2': 2.41,\n" +
                    "              'so2': 3.36,\n" +
                    "            },\n" +
                    "            {\n" +
                    "              'id': 1,\n" +
                    "              'sensorId': 1,\n" +
                    "              'date': '2020-09-10',\n" +
                    "              'time': '11:45',\n" +
                    "              'temp': 18.1,\n" +
                    "              'humidity': 82.9,\n" +
                    "              'co2': 879,\n" +
                    "              'co': 0.45,\n" +
                    "              'no': 0.61,\n" +
                    "              'no2': 2.41,\n" +
                    "              'so2': 3.36,\n" +
                    "            }\n" +
                    "        ]\n" +
                    "    }\n" +
                    "    status: 'OK',\n" +
                    "    message: ''\n" +
                    "}"
    )
    @GetMapping(value = "/api/environment/gas-realtime")
    public ResponseEntity<?> apiEnvironmentGasRealtime(@RequestParam("sensorId") Integer sensorId) {
        // 요청 파라미터 검증
        if (!CommonUtil.isValidNumber(sensorId)) {
            return new ResponseEntity<>(CommonUtil.setError("요청 파라미터 'sensorId' 숫자형식이 아닙니다."), HttpStatus.OK);
        }

        return new ResponseEntity<>(dmGasLogService.findAllByNowAndSensorId(sensorId), HttpStatus.OK);
    }

    @ApiOperation(value = "유해가스 - 실시간 로그 차트"
            , notes =
            "Request: {\n" +
                    "    sensorId: 1\n" +
                    "}\n" +
                    "Response: {\n" +
                    "    data: {\n" +
                    "        'charts': [\n" +
                    "            {\n" +
                    "                'date': '10:00',\n" +
                    "                'type': 'co',\n" +
                    "                'value': '0.8'\n" +
                    "            },\n" +
                    "            {\n" +
                    "                'date': '10:00',\n" +
                    "                'type': 'no',\n" +
                    "                'value': '0.81'\n" +
                    "            },\n" +
                    "            {\n" +
                    "                'date': '10:00',\n" +
                    "                'type': 'no2',\n" +
                    "                'value': '2.72'\n" +
                    "            },\n" +
                    "            {\n" +
                    "                'date': '10:00',\n" +
                    "                'type': 'so2',\n" +
                    "                'value': '3.81'\n" +
                    "            },\n" +
                    "            {\n" +
                    "                'date': '10:05',\n" +
                    "                'type': 'co',\n" +
                    "                'value': '0.8'\n" +
                    "            },\n" +
                    "            {\n" +
                    "                'date': '10:05',\n" +
                    "                'type': 'no',\n" +
                    "                'value': '0.81'\n" +
                    "            },\n" +
                    "            {\n" +
                    "                'date': '10:05',\n" +
                    "                'type': 'no2',\n" +
                    "                'value': '2.72'\n" +
                    "            },\n" +
                    "            {\n" +
                    "                'date': '10:05',\n" +
                    "                'type': 'so2',\n" +
                    "                'value': '3.81'\n" +
                    "            }\n" +
                    "        ]\n" +
                    "    }\n" +
                    "    status: 'OK',\n" +
                    "    message: ''\n" +
                    "}"
    )
    @GetMapping(value = "/api/environment/gas-realtime/chart")
    public ResponseEntity<?> apiEnvironmentGasRealtimeChart(@RequestParam("sensorId") Integer sensorId) {
        // 요청 파라미터 검증
        if (!CommonUtil.isValidNumber(sensorId)) {
            return new ResponseEntity<>(CommonUtil.setError("요청 파라미터 'sensorId' 숫자형식이 아닙니다."), HttpStatus.OK);
        }

        return new ResponseEntity<>(dmGasLogService.findAllChartByNowAndSensorId(sensorId), HttpStatus.OK);
    }

    @ApiOperation(value = "유해가스 - 일자 로그"
            , notes =
            "Request: {\n" +
                    "    sensorId: 1,\n" +
                    "    date: '2020-09-10'\n" +
                    "}\n" +
                    "Response: {\n" +
                    "    data: {\n" +
                    "        'logs': [\n" +
                    "            {\n" +
                    "              'id': 1,\n" +
                    "              'sensorId': 1,\n" +
                    "              'date': '2020-09-10',\n" +
                    "              'time': '09:10',\n" +
                    "              'temp': 18.1,\n" +
                    "              'humidity': 82.9,\n" +
                    "              'co2': 879,\n" +
                    "              'co': 0.45,\n" +
                    "              'no': 0.61,\n" +
                    "              'no2': 2.41,\n" +
                    "              'so2': 3.36,\n" +
                    "            },\n" +
                    "            {\n" +
                    "              'id': 1,\n" +
                    "              'sensorId': 1,\n" +
                    "              'date': '2020-09-10',\n" +
                    "              'time': '10:00',\n" +
                    "              'temp': 18.1,\n" +
                    "              'humidity': 82.9,\n" +
                    "              'co2': 879,\n" +
                    "              'co': 0.45,\n" +
                    "              'no': 0.61,\n" +
                    "              'no2': 2.41,\n" +
                    "              'so2': 3.36,\n" +
                    "            },\n" +
                    "            {\n" +
                    "              'id': 1,\n" +
                    "              'sensorId': 1,\n" +
                    "              'date': '2020-09-10',\n" +
                    "              'time': '10:20',\n" +
                    "              'temp': 18.1,\n" +
                    "              'humidity': 82.9,\n" +
                    "              'co2': 879,\n" +
                    "              'co': 0.45,\n" +
                    "              'no': 0.61,\n" +
                    "              'no2': 2.41,\n" +
                    "              'so2': 3.36,\n" +
                    "            },\n" +
                    "            {\n" +
                    "              'id': 1,\n" +
                    "              'sensorId': 1,\n" +
                    "              'date': '2020-09-10',\n" +
                    "              'time': '11:45',\n" +
                    "              'temp': 18.1,\n" +
                    "              'humidity': 82.9,\n" +
                    "              'co2': 879,\n" +
                    "              'co': 0.45,\n" +
                    "              'no': 0.61,\n" +
                    "              'no2': 2.41,\n" +
                    "              'so2': 3.36,\n" +
                    "            }\n" +
                    "        ]\n" +
                    "    }\n" +
                    "    status: 'OK',\n" +
                    "    message: ''\n" +
                    "}"
    )
    @GetMapping(value = "/api/environment/gas-analysis")
    public ResponseEntity<?> apiEnvironmentGasAnalysis(@RequestParam("sensorId") Integer sensorId, @RequestParam("date") String date) {
        // 요청 파라미터 검증
        if (date == null || (date !=null && date.equals("") || !CommonUtil.isValidDate(date, 10))) {
            return new ResponseEntity<>(CommonUtil.setError("요청 파라미터 'from' 날짜형식이 아닙니다."), HttpStatus.OK);
        }
        if (!CommonUtil.isValidNumber(sensorId)) {
            return new ResponseEntity<>(CommonUtil.setError("요청 파라미터 'sensorId' 숫자형식이 아닙니다."), HttpStatus.OK);
        }

        return new ResponseEntity<>(dmGasLogService.findAllByDateAndSensorId(sensorId, date), HttpStatus.OK);
    }

    @ApiOperation(value = "유해가스 - 일자 로그 차트"
            , notes =
            "Request: {\n" +
                    "    sensorId: 1,\n" +
                    "    date: '2020-09-10'\n" +
                    "}\n" +
                    "Response: {\n" +
                    "    data: {\n" +
                    "        'charts': [\n" +
                    "            {\n" +
                    "                'date': '10:00',\n" +
                    "                'type': 'co',\n" +
                    "                'value': '0.8'\n" +
                    "            },\n" +
                    "            {\n" +
                    "                'date': '10:00',\n" +
                    "                'type': 'no',\n" +
                    "                'value': '0.81'\n" +
                    "            },\n" +
                    "            {\n" +
                    "                'date': '10:00',\n" +
                    "                'type': 'no2',\n" +
                    "                'value': '2.72'\n" +
                    "            },\n" +
                    "            {\n" +
                    "                'date': '10:00',\n" +
                    "                'type': 'so2',\n" +
                    "                'value': '3.81'\n" +
                    "            },\n" +
                    "            {\n" +
                    "                'date': '10:05',\n" +
                    "                'type': 'co',\n" +
                    "                'value': '0.8'\n" +
                    "            },\n" +
                    "            {\n" +
                    "                'date': '10:05',\n" +
                    "                'type': 'no',\n" +
                    "                'value': '0.81'\n" +
                    "            },\n" +
                    "            {\n" +
                    "                'date': '10:05',\n" +
                    "                'type': 'no2',\n" +
                    "                'value': '2.72'\n" +
                    "            },\n" +
                    "            {\n" +
                    "                'date': '10:05',\n" +
                    "                'type': 'so2',\n" +
                    "                'value': '3.81'\n" +
                    "            }\n" +
                    "        ]\n" +
                    "    }\n" +
                    "    status: 'OK',\n" +
                    "    message: ''\n" +
                    "}"
    )
    @GetMapping(value = "/api/environment/gas-analysis/chart")
    public ResponseEntity<?> apiEnvironmentGasAnalysisChart(@RequestParam("sensorId") Integer sensorId, @RequestParam("date") String date) {
        // 요청 파라미터 검증
        if (date == null || (date !=null && date.equals("") || !CommonUtil.isValidDate(date, 10))) {
            return new ResponseEntity<>(CommonUtil.setError("요청 파라미터 'date' 날짜형식이 아닙니다."), HttpStatus.OK);
        }
        if (!CommonUtil.isValidNumber(sensorId)) {
            return new ResponseEntity<>(CommonUtil.setError("요청 파라미터 'sensorId' 숫자형식이 아닙니다."), HttpStatus.OK);
        }

        return new ResponseEntity<>(dmGasLogService.findAllChartByDateAndSensorId(sensorId, date), HttpStatus.OK);
    }

}
