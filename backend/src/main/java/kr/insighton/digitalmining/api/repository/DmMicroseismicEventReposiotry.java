/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.api.repository;

import kr.insighton.digitalmining.api.vo.DmMicroseismicEventVo;
import org.springframework.data.domain.Example;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/*
 *
 * title : DmMicroseismicEvent Reposiotry
 * author : 이형범
 * date : 2020.09.08
 *
 * */

@Repository
public interface DmMicroseismicEventReposiotry extends JpaRepository<DmMicroseismicEventVo, Integer> {

    /*
    * 미소진동 마지막 이벤트 조회
    * */
    @Query(value = "" +
            " select t.idx as id" +
            " , t.number" +
            " , cast(str_to_date(t.datetime, '%Y-%m-%d %H:%i:%s') as char(19)) as datetime" +
            " , t.category" +
            " , (select name from t_dm_code where code = t.category and use_yn = 'Y') as categoryName" +
            " , t.type" +
            " , (select name from t_dm_code where code = t.type and use_yn = 'Y') as typeName" +
            " , t.radius" +
            " , t.x" +
            " , t.y" +
            " , t.z" +
            " from t_dm_microseismic_event t" +
            " where str_to_date(t.datetime, '%Y-%m-%d') = :date" +
            " and t.number in (" +
            "     select max(m.number) as id" +
            "     from t_dm_microseismic_event m" +
            "     where str_to_date(m.datetime, '%Y-%m-%d') = :date" +
            " )"
            , nativeQuery = true)
    Map<String, Object> findAllByDateAndNumberMaxWithNativeQuery(@Param("date") String date);

    /*
     * 미소진동 이벤트 조회(all인 경우 미소진동종류 전체)
     * */
    @Query(value = "" +
            " select t.idx as id" +
            " , t.number" +
            " , cast(str_to_date(t.datetime, '%Y-%m-%d %H:%i:%s') as char(19)) as datetime" +
            " , t.category" +
            " , (select name from t_dm_code where code = t.category and use_yn = 'Y') as categoryName" +
            " , t.type" +
            " , (select name from t_dm_code where code = t.type and use_yn = 'Y') as typeName" +
            " , t.radius" +
            " , t.x" +
            " , t.y" +
            " , t.z" +
            " from t_dm_microseismic_event t" +
            " where str_to_date(t.datetime, '%Y-%m-%d') = :date" +
            " and case when :category = 'all' then t.category <> :category" +
            "          else t.category = :category" +
            "     end"
            , nativeQuery = true)
    List<Map<String, Object>> findAllByDateAndCategoryWithNativeQuery(@Param("date") String date, @Param("category") String category);

    /*
    * 미소진동 미소진동종류별 3개월단위 카운트
    * */
    @Query(value = "" +
            " select cast(str_to_date(t.datetime, '%Y-%m-%d') as char(10)) as date" +
            " , (select name from t_dm_code where code = t.category and use_yn = 'Y') as category" +
            " , count(t.category) as count" +
            " from t_dm_microseismic_event t" +
            " where t.datetime > date_add(:date, interval -3 month)" +
            " and t.datetime < date_add(:date, interval 1 day)" +
            " and t.idx not in (select idx from t_dm_microseismic_event where datetime > now())" +
            " group by cast(str_to_date(t.datetime, '%Y-%m-%d') as char(10))" +
            " , (select name from t_dm_code where code = t.category and use_yn = 'Y')"
            , nativeQuery = true)
    List<Map<String, Object>> findCountCategoryGroupByDateWithNativeQuery(@Param("date") String date);
}
