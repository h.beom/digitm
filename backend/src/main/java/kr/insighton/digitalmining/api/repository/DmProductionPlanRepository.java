/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.api.repository;

import kr.insighton.digitalmining.api.vo.DmProductionPlanVo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/*
 *
 * title : DmProductionPlan Repository
 * author : 윤영수
 * date : 2020.08.26
 *
 * */

@Repository
public interface DmProductionPlanRepository extends JpaRepository<DmProductionPlanVo, Integer> {

    /*
    * 가장 마지막 차수에 해당하는 일자 생산 계획량
    * (해당하는 일자가 존재하는 마지막차수임)
    * */
    @Query(value = "" +
            " select" +
            "   t.plan_idx as planId" +
            " , t.plan_amount as planAmount" +
            " from t_dm_production_plan t" +
            " where t.plan_date = :date" +
            " and t.plan_idx = (select max(plan_idx) from t_dm_production_plan where plan_date = :date)"
            , nativeQuery = true)
    Map<String, Object> findByPlanDateWithNativeQuery(@Param("date") String date);

    /*
    * 1일부터 해당일자까지 생산 계획량
    * */
    @Query(value = "" +
            " select" +
            "   sum(t.plan_amount) as total" +
            " from t_dm_production_plan t" +
            " where t.plan_idx = :planId" +
            " and t.plan_date >= :startDate" +
            " and t.plan_date <= :endDate"
            , nativeQuery = true)
    Map<String, Object> findGroupByDateWithNativeQuery(@Param("planId") Integer planId, @Param("startDate") String startDate, @Param("endDate") String endDate);

    /*
     * 계획차수에 해당하는 연단위, 월단위, 일단위 생산 계획량
     * */
    @Query(value = "" +
            " select" +
            "   date_format(t.plan_date, '%Y-%m-%d') as 'date'" +
            " , t.plan_amount as amount" +
            " from t_dm_production_plan t" +
            " where t.plan_idx = :id" +
            " and t.plan_date like concat(:date, '%')"
            ,nativeQuery = true)
    List<Map<String, Object>> findByVersionAndDateWithNativeQuery(@Param("id") Integer id, @Param("date") String date);

    /*
     * 계획차수에 해당하는 연단위, 월단위, 일단위 생산계획량 평균, 누계
     * */
    @Query(value = "" +
            " select" +
            "   round(avg(t.plan_amount)) as average" +
            " , sum(t.plan_amount) as total" +
            " from t_dm_production_plan t" +
            " where t.plan_idx = :id" +
            " and t.plan_date like concat(:date, '%')"
            ,nativeQuery = true)
    Map<String, Object> findGroupByVersionAndDateWithNativeQuery(@Param("id") Integer id, @Param("date") String date);
}
