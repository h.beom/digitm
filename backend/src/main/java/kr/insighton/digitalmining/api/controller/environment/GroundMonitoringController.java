/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.api.controller.environment;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import kr.insighton.digitalmining.api.service.DmGroundMonitoringLogService;
import kr.insighton.digitalmining.common.util.CommonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/*
 *
 * title : GroundMonitoring Controller
 * author : 윤영수
 * date : 2020.09.18
 *
 * */

@Api(tags = "채광환경 제어 - Ground Control")
@RestController
public class GroundMonitoringController {

    @Autowired
    private DmGroundMonitoringLogService dmGroundMonitoringLogService;

    @ApiOperation(value = "지반변위 - 실시간 로그"
            , notes =
            "Request: {\n" +
                    "    sensorId: 1\n" +
                    "}\n" +
                    "Response: {\n" +
                    "    data: {\n" +
                    "        'logs': [\n" +
                    "            {\n" +
                    "              'id': 1,\n" +
                    "              'sensorId': 1,\n" +
                    "              'date': '2020-09-10',\n" +
                    "              'time': '09:10',\n" +
                    "              'xDegree': -8.638,\n" +
                    "              'xDegreeDisplacement': 0.03,\n" +
                    "              'yDegree': 0.12,\n" +
                    "              'yDegreeDisplacement': 0.001,\n" +
                    "            },\n" +
                    "            {\n" +
                    "              'id': 1,\n" +
                    "              'sensorId': 1,\n" +
                    "              'date': '2020-09-10',\n" +
                    "              'time': '10:00',\n" +
                    "              'xDegree': -8.638,\n" +
                    "              'xDegreeDisplacement': 0.03,\n" +
                    "              'yDegree': 0.12,\n" +
                    "              'yDegreeDisplacement': 0.001,\n" +
                    "            },\n" +
                    "            {\n" +
                    "              'id': 1,\n" +
                    "              'sensorId': 1,\n" +
                    "              'date': '2020-09-10',\n" +
                    "              'time': '10:20',\n" +
                    "              'xDegree': -8.638,\n" +
                    "              'xDegreeDisplacement': 0.03,\n" +
                    "              'yDegree': 0.12,\n" +
                    "              'yDegreeDisplacement': 0.001,\n" +
                    "            },\n" +
                    "            {\n" +
                    "              'id': 1,\n" +
                    "              'sensorId': 1,\n" +
                    "              'date': '2020-09-10',\n" +
                    "              'time': '11:45',\n" +
                    "              'xDegree': -8.638,\n" +
                    "              'xDegreeDisplacement': 0.03,\n" +
                    "              'yDegree': 0.12,\n" +
                    "              'yDegreeDisplacement': 0.001,\n" +
                    "            }\n" +
                    "        ]\n" +
                    "    }\n" +
                    "    status: 'OK',\n" +
                    "    message: ''\n" +
                    "}"
    )
    @GetMapping(value = "/api/environment/ground-monitoring-realtime")
    public ResponseEntity<?> apiEnvironmentGroundMonitoringRealtime(@RequestParam("sensorId") Integer sensorId) {
        // 요청 파라미터 검증
        if (!CommonUtil.isValidNumber(sensorId)) {
            return new ResponseEntity<>(CommonUtil.setError("요청 파라미터 'sensorId' 숫자형식이 아닙니다."), HttpStatus.OK);
        }

        return new ResponseEntity<>(dmGroundMonitoringLogService.findAllByNowAndSensorId(sensorId), HttpStatus.OK);
    }


    @ApiOperation(value = "지반변위 - 실시간 로그 차트"
            , notes =
            "Request: {\n" +
                    "    sensorId: 1\n" +
                    "}\n" +
                    "Response: {\n" +
                    "    data: {\n" +
                    "        'charts': [\n" +
                    "            {\n" +
                    "              'date': '09:00',\n" +
                    "              'type': '지반변위(수평)',\n" +
                    "              'value': -8.638,\n" +
                    "            },\n" +
                    "            {\n" +
                    "              'date': '09:00',\n" +
                    "              'type': '지반변위 변화율(수평)',\n" +
                    "              'value': 0.23,\n" +
                    "            },\n" +
                    "            {\n" +
                    "              'date': '09:00',\n" +
                    "              'type': '지반변위(수직)',\n" +
                    "              'value': -7.638,\n" +
                    "            },\n" +
                    "            {\n" +
                    "              'date': '09:00',\n" +
                    "              'type': '지반변위 변화율(수직)',\n" +
                    "              'value': 0.12,\n" +
                    "            },\n" +
                    "            {\n" +
                    "              'date': '10:00',\n" +
                    "              'type': '지반변위(수평)',\n" +
                    "              'value': -8.638,\n" +
                    "            },\n" +
                    "            {\n" +
                    "              'date': '10:00',\n" +
                    "              'type': '지반변위 변화율(수평)',\n" +
                    "              'value': 0.23,\n" +
                    "            },\n" +
                    "            {\n" +
                    "              'date': '10:00',\n" +
                    "              'type': '지반변위(수직)',\n" +
                    "              'value': -7.638,\n" +
                    "            },\n" +
                    "            {\n" +
                    "              'date': '10:00',\n" +
                    "              'type': '지반변위 변화율(수직)',\n" +
                    "              'value': 0.12,\n" +
                    "            }\n" +
                    "        ]\n" +
                    "    }\n" +
                    "    status: 'OK',\n" +
                    "    message: ''\n" +
                    "}"
    )
    @GetMapping(value = "/api/environment/ground-monitoring-realtime/chart")
    public ResponseEntity<?> apiEnvironmentGroundMonitoringRealtimeChart(@RequestParam("sensorId") Integer sensorId) {
        // 요청 파라미터 검증
        if (!CommonUtil.isValidNumber(sensorId)) {
            return new ResponseEntity<>(CommonUtil.setError("요청 파라미터 'sensorId' 숫자형식이 아닙니다."), HttpStatus.OK);
        }

        return new ResponseEntity<>(dmGroundMonitoringLogService.findAllChartByNowAndSensorId(sensorId), HttpStatus.OK);
    }

    @ApiOperation(value = "지반변위 - 일자 범위 로그"
            , notes =
            "Request: {\n" +
                    "    from: '2020-09-10',\n" +
                    "    to: '2020-09-30',\n" +
                    "    sensorId: 1,\n" +
                    "}\n" +
                    "Response: {\n" +
                    "    data: {\n" +
                    "        'logs': [\n" +
                    "            {\n" +
                    "              'id': 1,\n" +
                    "              'sensorId': 1,\n" +
                    "              'date': '2020-09-10',\n" +
                    "              'time': '09:10',\n" +
                    "              'xDegree': -8.638,\n" +
                    "              'xDegreeDisplacement': 0.03,\n" +
                    "              'yDegree': 0.12,\n" +
                    "              'yDegreeDisplacement': 0.001,\n" +
                    "            },\n" +
                    "            {\n" +
                    "              'id': 1,\n" +
                    "              'sensorId': 1,\n" +
                    "              'date': '2020-09-10',\n" +
                    "              'time': '10:00',\n" +
                    "              'xDegree': -8.638,\n" +
                    "              'xDegreeDisplacement': 0.03,\n" +
                    "              'yDegree': 0.12,\n" +
                    "              'yDegreeDisplacement': 0.001,\n" +
                    "            },\n" +
                    "            {\n" +
                    "              'id': 1,\n" +
                    "              'sensorId': 1,\n" +
                    "              'date': '2020-09-10',\n" +
                    "              'time': '10:20',\n" +
                    "              'xDegree': -8.638,\n" +
                    "              'xDegreeDisplacement': 0.03,\n" +
                    "              'yDegree': 0.12,\n" +
                    "              'yDegreeDisplacement': 0.001,\n" +
                    "            },\n" +
                    "            {\n" +
                    "              'id': 1,\n" +
                    "              'sensorId': 1,\n" +
                    "              'date': '2020-09-10',\n" +
                    "              'time': '11:45',\n" +
                    "              'xDegree': -8.638,\n" +
                    "              'xDegreeDisplacement': 0.03,\n" +
                    "              'yDegree': 0.12,\n" +
                    "              'yDegreeDisplacement': 0.001,\n" +
                    "            }\n" +
                    "        ]\n" +
                    "    }\n" +
                    "    status: 'OK',\n" +
                    "    message: ''\n" +
                    "}"
    )
    @GetMapping(value = "/api/environment/ground-monitoring-analysis")
    public ResponseEntity<?> apiEnvironmentGroundMonitoringAnalysis(@RequestParam("sensorId") Integer sensorId, @RequestParam("from") String from, @RequestParam("to") String to) {
        // 요청 파라미터 검증
        if (from == null || (from !=null && from.equals("") || !CommonUtil.isValidDate(from, 10))) {
            return new ResponseEntity<>(CommonUtil.setError("요청 파라미터 'from' 날짜형식이 아닙니다."), HttpStatus.OK);
        }
        if (to == null || (to !=null && to.equals("") || !CommonUtil.isValidDate(to, 10))) {
            return new ResponseEntity<>(CommonUtil.setError("요청 파라미터 'to' 날짜형식이 아닙니다."), HttpStatus.OK);
        }
        if (!CommonUtil.isValidNumber(sensorId)) {
            return new ResponseEntity<>(CommonUtil.setError("요청 파라미터 'sensorId' 숫자형식이 아닙니다."), HttpStatus.OK);
        }

        return new ResponseEntity<>(dmGroundMonitoringLogService.findAllByDateFromToAndSensorId(sensorId, from, to), HttpStatus.OK);
    }

    @ApiOperation(value = "지반변위 - 일자 범위 로그 차트"
            , notes =
            "Request: {\n" +
                    "    from: '2020-09-10',\n" +
                    "    to: '2020-09-30',\n" +
                    "    sensorId: 1,\n" +
                    "}\n" +
                    "Response: {\n" +
                    "    data: {\n" +
                    "        'charts': [\n" +
                    "            {\n" +
                    "              'date': '09:00',\n" +
                    "              'type': '지반변위(수평)',\n" +
                    "              'value': -8.638,\n" +
                    "            },\n" +
                    "            {\n" +
                    "              'date': '09:00',\n" +
                    "              'type': '지반변위 변화율(수평)',\n" +
                    "              'value': 0.23,\n" +
                    "            },\n" +
                    "            {\n" +
                    "              'date': '09:00',\n" +
                    "              'type': '지반변위(수직)',\n" +
                    "              'value': -7.638,\n" +
                    "            },\n" +
                    "            {\n" +
                    "              'date': '09:00',\n" +
                    "              'type': '지반변위 변화율(수직)',\n" +
                    "              'value': 0.12,\n" +
                    "            },\n" +
                    "            {\n" +
                    "              'date': '10:00',\n" +
                    "              'type': '지반변위(수평)',\n" +
                    "              'value': -8.638,\n" +
                    "            },\n" +
                    "            {\n" +
                    "              'date': '10:00',\n" +
                    "              'type': '지반변위 변화율(수평)',\n" +
                    "              'value': 0.23,\n" +
                    "            },\n" +
                    "            {\n" +
                    "              'date': '10:00',\n" +
                    "              'type': '지반변위(수직)',\n" +
                    "              'value': -7.638,\n" +
                    "            },\n" +
                    "            {\n" +
                    "              'date': '10:00',\n" +
                    "              'type': '지반변위 변화율(수직)',\n" +
                    "              'value': 0.12,\n" +
                    "            }\n" +
                    "        ]\n" +
                    "    }\n" +
                    "    status: 'OK',\n" +
                    "    message: ''\n" +
                    "}"
    )
    @GetMapping(value = "/api/environment/ground-monitoring-analysis/chart")
    public ResponseEntity<?> apiEnvironmentGroundMonitoringAnalysisChart(@RequestParam("sensorId") Integer sensorId, @RequestParam("from") String from, @RequestParam("to") String to) {
        // 요청 파라미터 검증
        if (from == null || (from !=null && from.equals("") || !CommonUtil.isValidDate(from, 10))) {
            return new ResponseEntity<>(CommonUtil.setError("요청 파라미터 'from' 날짜형식이 아닙니다."), HttpStatus.OK);
        }
        if (to == null || (to !=null && to.equals("") || !CommonUtil.isValidDate(to, 10))) {
            return new ResponseEntity<>(CommonUtil.setError("요청 파라미터 'to' 날짜형식이 아닙니다."), HttpStatus.OK);
        }
        if (!CommonUtil.isValidNumber(sensorId)) {
            return new ResponseEntity<>(CommonUtil.setError("요청 파라미터 'sensorId' 숫자형식이 아닙니다."), HttpStatus.OK);
        }

        return new ResponseEntity<>(dmGroundMonitoringLogService.findAllChartByDateFromToAndSensorId(sensorId, from, to), HttpStatus.OK);
    }

}
