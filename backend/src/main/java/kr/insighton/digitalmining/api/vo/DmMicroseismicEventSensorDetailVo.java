/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.api.vo;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/*
 *
 * title : DmMicroseismicEventSensorDetail Vo
 * author : 이형범
 * date : 2020.09.08
 *
 * */

@Getter
@Setter
@Entity
@Table(name = "t_dm_microseismic_event_sensor_detail")
public class DmMicroseismicEventSensorDetailVo implements Serializable {

    private static final long serialVersionUID = -7252234199609598030L;

    @Id
    @Column(name = "idx")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "event_idx")
    private Integer eventId;

    @Column(name = "sensor_idx")
    private Integer sensorId;

    @Column(name = "img_link")
    private String imgLink;

}
