/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.api.service.demo;

import kr.insighton.digitalmining.api.repository.demo.TestRepository;
import kr.insighton.digitalmining.api.vo.demo.TestVo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/*
 *
 * title : test Service
 * author : 윤영수
 * date : 2020.08.05
 *
 * */

@Service
@Transactional
public class TestService {

    @Resource
    TestRepository testRepository;

    public List<TestVo> findAll() {
        return testRepository.findAll();
    }

    public TestVo saveTest(TestVo testVo) {
        return testRepository.save(testVo);
    }

    public TestVo updateTest(TestVo testVo) {
        return testRepository.save(testVo);
    }

    public void deleteTest(String name) {
        testRepository.deleteById(name);
    }

    public TestVo findByName(String name) {
        return testRepository.findByName(name).orElse(null);
    }

    public List<TestVo> findByNameIgnoreCaseContaining(String keyword) {
        return testRepository.findByNameIgnoreCaseContaining(keyword);
    }

    public Long numberOfTest(){
        return testRepository.count();
    }

}