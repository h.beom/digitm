/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.api.vo;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/*
 *
 * title : Dpm Vo
 * author : 윤영수
 * date : 2020.08.05
 *
 * */

@Getter
@Setter
@Entity
@Table(name = "t_dpm")
public class DpmVo implements Serializable {

    private static final long serialVersionUID = 4021226077941086595L;

    @Embeddable
    public static class EmbededTable implements Serializable {

        private static final long serialVersionUID = 1L;

        @Column(name = "reg_date")
        private Timestamp regDate;

        @Column(name = "site_num")
        private Integer siteNum;

        @Column(name = "raw_sensor")
        private Integer rawSensor;

        @Column(name = "flow_sensor")
        private Integer flowSensor;

        @Column(name = "flow_level")
        private Integer flowLevel;

        @Column(name = "sensor_gain")
        private Integer sensorGain;

        @Column(name = "twa_value")
        private Double twaValue;

        @Column(name = "5_min_ec")
        private Double n5MinEc; // 변수명 오류로 n을 붙임

        @Column(name = "ec_value")
        private Double ecValue;

        @Column(name = "fault_code")
        private Integer faultCode;

    }

    @EmbeddedId EmbededTable id = new EmbededTable();

}

