/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.api.vo;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Time;

/*
 *
 * title : DmGroundMonitoringLog Vo
 * author : 윤영수
 * date : 2020.09.10
 *
 * */

@Getter
@Setter
@Entity
@Table(name = "t_dm_ground_monitoring_log")
public class DmGroundMonitoringLogVo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Integer idx;
    
    @Column(name = "sensor_idx")
    private Integer sensorIdx;
    
    @Column(name = "date")
    private Date date;
    
    @Column(name = "time")
    private Time time;
    
    @Column(name = "x_degree")
    private Double xDegree;
    
    @Column(name = "dx_dt")
    private Double dxDt;
    
    @Column(name = "y_degree")
    private Double yDegree;
    
    @Column(name = "dy_dt")
    private Double dyDt;
    
}
