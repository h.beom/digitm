package kr.insighton.digitalmining.api.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.*;

import kr.insighton.digitalmining.api.repository.CodeManageRepository;
import kr.insighton.digitalmining.api.repository.CodeManageRepositoryCustom;
import kr.insighton.digitalmining.common.util.*;
import kr.insighton.digitalmining.common.vo.CodeVo;

/*
 *
 * title : CodeManage Service
 * author : 이동현
 * date : 2020.09.10
 *
 * */

@Service

public class CodeManageService {
    @Resource
    private CodeManageRepositoryCustom codeManageRepository;

    public Map<String, Object> getCodeList(String codeGroup, String codeName)
    {
        Map<String, Object> data = new HashMap<>();
        
        List<CodeVo> list = codeManageRepository.getCodeList(codeGroup, codeName);

        if (list != null && list.size() > 0) {
            data.put("list", list);
        } else {
            return CommonUtil.setError("요청하신 계근회차에 대한 데이터가 존재하지 않습니다.");
        }

        return CommonUtil.setResult(data);
    }
}
