package kr.insighton.digitalmining.api.repository;

import kr.insighton.digitalmining.api.vo.DmDpmLogVo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Map;

public interface DmDpmAnalysisRepository extends JpaRepository<DmDpmLogVo, Integer> {
    @Query(value = "" +
            " select t.idx as id" +
            " , t.sensor_idx as sensorId" +
            " , cast(date_format(t.date, '%Y-%m-%d') as char(10)) as 'date'" +
            " , t.time as 'time'" +
            " , truncate(t.temp, 1) as temp" +
            " , truncate(t.humidity, 1) as humidity" +
            " , truncate(t.co, 1) as co" +
            " , truncate(t.no, 1) as no" +
            " , truncate(t.dpm_10m, 2) as dpm10m" +
            " , truncate(t.dpm_30m, 2) as dpm30m" +
            " from t_dm_dpm_log t" +
            " where t.date = date_format(:date, '%Y-%m-%d')" +
            " and t.sensor_idx = :sensorId"+
            " order by t.time desc"
            , nativeQuery = true)
    List<Map<String, Object>> getBySensorIdAndDate(@Param("sensorId") Integer sensorId, @Param("date") String date);
}
