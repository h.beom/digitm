/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.api.vo;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/*
 *
 * title : SensorOriginalInfo Vo
 * author : 윤영수
 * date : 2020.08.05
 *
 * */

@Getter
@Setter
@Entity
@Table(name = "t_sensor_original_info")
public class SensorOriginalInfoVo implements Serializable {

    private static final long serialVersionUID = -5151591010879166757L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Integer idx;

    @Column(name = "sensor_idx")
    private Integer sensorIdx;

    @Column(name = "sensor_type", length = 45)
    private String sensorType;

    @Column(name = "sensor_data")
    private Double sensorData;

    @Column(name = "sensor_data_y")
    private Double sensorDataY;

    @Column(name = "reg_date")
    private Timestamp regDate;

    @Column(name = "co")
    private Double co;

    @Column(name = "co2")
    private Double co2;

    @Column(name = "no")
    private Double no;

    @Column(name = "no2")
    private Double no2;

    @Column(name = "so2")
    private Double so2;

    @Column(name = "air")
    private Double air;

}
