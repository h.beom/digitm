/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.api.vo;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/*
 *
 * title : DmMicroseismicSensor Vo
 * author : 윤영수
 * date : 2020.09.08
 *
 * */

@Getter
@Setter
@Entity
@Table(name = "t_dm_sensor")
public class DmSensorVo implements Serializable {

    private static final long serialVersionUID = 5658960536609866041L;

    @Id
    @Column(name = "idx")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "sensor_id")
    private String sensorId;

    @Column(name = "sensor_name")
    private String sensorName;

    @Column(name = "division")
    private String division;

    @Column(name = "x")
    private Integer x;

    @Column(name = "y")
    private Integer y;

    @Column(name = "z")
    private Integer z;

    @Column(name = "type")
    private String type;


}
