/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.api.vo;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;

/*
 *
 * title : MineFan Vo
 * author : 윤영수
 * date : 2020.08.05
 *
 * */

@Getter
@Setter
@Entity
@Table(name = "mine_fan")
public class MineFanVo implements Serializable {

    private static final long serialVersionUID = -9084819045790579347L;

    @Embeddable
    public static class EmbededTable implements Serializable {

        private static final long serialVersionUID = -6526879531679401245L;

        @Column(name = "mine_location")
        private Integer mineLocation;

        @Column(name = "mine_Hz")
        private Integer mineHz;

        @Column(name = "mine_status", length = 45)
        private String mineStatus;

        @Column(name = "fan_startTime")
        private Date fanStarttime;

        @Column(name = "fan_stopTime")
        private Date fanStoptime;

        @Column(name = "NOW")
        private Date now;

    }

    @EmbeddedId EmbededTable id = new EmbededTable();

}
