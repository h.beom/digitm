/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.api.repository;

import kr.insighton.digitalmining.api.vo.DmWeighbridgeLogVo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/*
 *
 * title : DmWeighbridgeLog Repository
 * author : 이형범
 * date : 2020.08.26
 *
 * */

@Repository
public interface DmWeighbridgeLogRepository extends JpaRepository<DmWeighbridgeLogVo, Integer> {

    /*
    * 실시간 계근대 테이블에서 마지막 회차
    * */
    @Query(value = "" +
            " select" +
            "   max(t.weigh_number) as weighNumber" +
            " from t_dm_weighbridge_log t" +
            " where t.date = :today" +
            " and t.idx not in (" +
            "       select x.idx" +
            "       from t_dm_weighbridge_log x" +
            "       where x.date > date_format(now(), '%Y-%m-%d')" +
            "       or (x.date = date_format(now(), '%Y-%m-%d') and x.time1 > date_format(now(), '%H:%i:%s'))" +
            " )"
            , nativeQuery = true)
    Map<String, Object> findByMaxWeighNumber(@Param("today") String today);

    /*
    * 일간 누적 생산량
    * */
    @Query(value = "" +
            " select" +
            "   round(sum(t.weight_actual) / 1000) as total" +
            " from t_dm_weighbridge_log t" +
            " where t.date = :today" +
            " and t.source = :sourceA" +
            " and t.type = :typeA" +
            " and t.idx not in (" +
            "       select x.idx" +
            "       from t_dm_weighbridge_log x" +
            "       where x.date > date_format(now(), '%Y-%m-%d')" +
            "       or (x.date = date_format(now(), '%Y-%m-%d') and x.time1 > date_format(now(), '%H:%i:%s'))" +
            " )"
            , nativeQuery = true)
    Map<String, Object> findTodayAmountWithNativeQuery(@Param("today") String today
            , @Param("sourceA") String sourceA
            , @Param("typeA") String typeA);

    /*
    * 생산실적 일간 누적 생산량
    * */
    @Query(value = "" +
            " select" +
            "   round(sum(t.weight_actual) / 1000) as total" +
            " from t_dm_weighbridge_log t" +
            " where t.date = :today" +
            " and (" +
            "   (t.source = :searchA1 and t.product_name = :searchA2 and t.type = :searchA3)" +
            " or" +
            "   (t.source = :searchB1 and t.product_name = :searchB2 and t.type = :searchB3)" +
            " )" +
            " and t.idx not in (" +
            "       select x.idx" +
            "       from t_dm_weighbridge_log x" +
            "       where x.date > date_format(now(), '%Y-%m-%d')" +
            "       or (x.date = date_format(now(), '%Y-%m-%d') and x.time1 > date_format(now(), '%H:%i:%s'))" +
            " )"
            , nativeQuery = true)
    Map<String, Object> findTodayAmountAndSourceWithNativeQuery(@Param("today") String today
            , @Param("searchA1") String searchA1
            , @Param("searchA2") String searchA2
            , @Param("searchA3") String searchA3
            , @Param("searchB1") String searchB1
            , @Param("searchB2") String searchB2
            , @Param("searchB3") String searchB3);

    /*
    * 날짜범위에 해당하는 누적 생산량
    * */
    @Query(value = "" +
            " select" +
            "   round(sum(t.weight_actual) / 1000) as total" +
            " from t_dm_weighbridge_log t" +
            " where t.date >= :startDate" +
            " and t.date <= :endDate" +
            " and t.source = :sourceA" +
            " and t.type = :typeA" +
            " and t.idx not in (" +
            "       select x.idx" +
            "       from t_dm_weighbridge_log x" +
            "       where x.date > date_format(now(), '%Y-%m-%d')" +
            "       or (x.date = date_format(now(), '%Y-%m-%d') and x.time1 > date_format(now(), '%H:%i:%s'))" +
            " )"
    , nativeQuery = true)
    Map<String, Object> findMonthAmountWithNativeQuery(@Param("startDate") String startDate
            , @Param("endDate") String endDate
            , @Param("sourceA") String sourceA
            , @Param("typeA") String typeA);

    /*
     * 지정한 월단위, 연단위 누적 생산량
     * */
    @Query(value = "" +
            " select" +
            "   round(sum(t.weight_actual) / 1000) as total" +
            " from t_dm_weighbridge_log t" +
            " where t.date like concat(:date, '%')" +
            " and t.source = :sourceA" +
            " and t.type = :typeA" +
            " and t.idx not in (" +
            "       select x.idx" +
            "       from t_dm_weighbridge_log x" +
            "       where x.date > date_format(now(), '%Y-%m-%d')" +
            "       or (x.date = date_format(now(), '%Y-%m-%d') and x.time1 > date_format(now(), '%H:%i:%s'))" +
            " )"
            , nativeQuery = true)
    Map<String, Object> findGroupByDateWithNativeQuery(@Param("date") String date
            , @Param("sourceA") String sourceA
            , @Param("typeA") String typeA);

    /*
     * 생산실적 월단위, 연단위 누적 생산량
     * */
    @Query(value = "" +
            " select" +
            "   round(sum(t.weight_actual) / 1000) as total" +
            " from t_dm_weighbridge_log t" +
            " where t.date like concat(:date, '%')" +
            " and (" +
            "   (t.source = :searchA1 and t.product_name = :searchA2 and t.type = :searchA3)" +
            " or" +
            "   (t.source = :searchB1 and t.product_name = :searchB2 and t.type = :searchB3)" +
            ")" +
            " and t.idx not in (" +
            "       select x.idx" +
            "       from t_dm_weighbridge_log x" +
            "       where x.date > date_format(now(), '%Y-%m-%d')" +
            "       or (x.date = date_format(now(), '%Y-%m-%d') and x.time1 > date_format(now(), '%H:%i:%s'))" +
            " )"
            , nativeQuery = true)
    Map<String, Object> findGroupByDateAndSourceWithNativeQuery(@Param("date") String date
            , @Param("searchA1") String searchA1
            , @Param("searchA2") String searchA2
            , @Param("searchA3") String searchA3
            , @Param("searchB1") String searchB1
            , @Param("searchB2") String searchB2
            , @Param("searchB3") String searchB3);

    /*
     * 일단위, 월단위, 연단위 생산비축량 또는 구매비축량
     * */
    @Query(value = "" +
            " select" +
            "   round(sum(t.weight_actual) / 1000) as total" +
            " from t_dm_weighbridge_log t" +
            " where t.date like concat(:date, '%')" +
            " and t.source = :searchA1" +
            " and t.product_name = :searchA2" +
            " and t.type = :searchA3" +
            " and t.idx not in (" +
            "       select x.idx" +
            "       from t_dm_weighbridge_log x" +
            "       where x.date > date_format(now(), '%Y-%m-%d')" +
            "       or (x.date = date_format(now(), '%Y-%m-%d') and x.time1 > date_format(now(), '%H:%i:%s'))" +
            " )"
            , nativeQuery = true)
    Map<String, Object> findGroupByDateReserveWithNativeQuery(@Param("date") String date
            , @Param("searchA1") String searchA1
            , @Param("searchA2") String searchA2
            , @Param("searchA3") String searchA3);

    /*
     * 실시간 생산&구매 실적 계근회차별
     * */
    @Query(value = "" +
            " select" +
            "   t.weigh_number as number" +
            " , time_format(t.time1, '%H:%i') as 'time'" +
            " , t.truck_id as id" +
            " , (select name from t_dm_code where code = t.source and use_yn = 'Y') as source" +
            " , (select name from t_dm_code where code = t.product_name and use_yn = 'Y') as productName" +
            " , t.weight_actual as weight" +
            " , (select name from t_dm_code where code = t.type and use_yn = 'Y') as type" +
            " from t_dm_weighbridge_log t" +
            " where t.date = date_format(now(), '%Y-%m-%d')" +
            " and t.weigh_number = :number"
            , nativeQuery = true)
    List<Map<String, Object>> findByWeighNumberWithNativeQuery(@Param("number") Integer number);

    /*
     * 일자별 생산&구매 실적 계근회차 전체
     * */
    @Query(value = "" +
            " select" +
            "   t.weigh_number as number" +
            " , time_format(t.time1, '%H:%i') as 'time'" +
            " , (select name from t_dm_code where code = t.source and use_yn = 'Y') as source" +
            " , (select name from t_dm_code where code = t.product_name and use_yn = 'Y') as productName" +
            " , t.weight_actual as weight" +
            " , (select name from t_dm_code where code = t.type and use_yn = 'Y') as type" +
            " from t_dm_weighbridge_log t" +
            " where t.date = :date" +
            " and t.idx not in (" +
            "       select x.idx" +
            "       from t_dm_weighbridge_log x" +
            "       where x.date > date_format(now(), '%Y-%m-%d')" +
            "       or (x.date = date_format(now(), '%Y-%m-%d') and x.time1 > date_format(now(), '%H:%i:%s'))" +
            " )"
            , nativeQuery = true)
    List<Map<String, Object>> findByDateWithNativeQuery(@Param("date") String date);
}
