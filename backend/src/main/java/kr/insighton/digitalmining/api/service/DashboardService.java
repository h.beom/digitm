/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.api.service;

import kr.insighton.digitalmining.api.repository.DmProductionPlanMasterRepository;
import kr.insighton.digitalmining.api.repository.DmProductionPlanRepository;
import kr.insighton.digitalmining.api.repository.DmWeighbridgeLogRepository;
import kr.insighton.digitalmining.common.service.CodeService;
import kr.insighton.digitalmining.common.util.CommonUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 *
 * title : Dashboard Service
 * author : 이형범
 * date : 2020.08.19
 *
 * */
@Slf4j
@Service
@Transactional
public class DashboardService {

    @Resource
    private CodeService codeService;

    @Resource
    private DmWeighbridgeLogRepository dmWeighbridgeLogRepository;

    @Resource
    private DmProductionPlanMasterRepository dmProductionPlanMasterRepository;

    @Resource
    private DmProductionPlanRepository dmProductionPlanRepository;

    public Map<String, Object> findDashboardProductionAmount() {

        String sourceA = codeService.findByName("갱내파쇄", "거래처");
        String typeA = codeService.findByName("입고", "구분");

        if (sourceA == null || typeA == null) {
            return CommonUtil.setError("데이터 조회 중 오류가 발생헀습니다.");
        }
        // 조건설정 - 현재일자 기준
        // 마지막 차수
        String startDate = CommonUtil.getTodayYearMonth() + "-01";
        String endDate = CommonUtil.getToday();

        // 단일 데이터 : 일 생산량(계획포함), 누적 생산량(계획포함), 총 생산량(계획포함) - 화면에서 처리하기로 함
        Map<String, Object> data = new HashMap<>();
        // 일 생산량
        Map<String, Object> todayAmount = dmWeighbridgeLogRepository.findTodayAmountWithNativeQuery(CommonUtil.getToday(), sourceA, typeA);
        if (CommonUtil.isNullMap(todayAmount, "total")) {
            data.put("currentProd", 0);
        } else {
            data.put("currentProd", todayAmount.get("total"));
        }
        // 해당월 누적 생산량
        Map<String, Object> monthAmount = dmWeighbridgeLogRepository.findMonthAmountWithNativeQuery(startDate, endDate, sourceA, typeA);
        if (CommonUtil.isNullMap(monthAmount, "total")) {
            data.put("accProd", 0);
        } else {
            if (CommonUtil.getTodayDate() == 1) {
                data.put("accProd", 0);
            } else {
                data.put("accProd", monthAmount.get("total"));
            }
        }
        // 계획 생산량
        Map<String, Object> todayPlan = dmProductionPlanRepository.findByPlanDateWithNativeQuery(CommonUtil.getToday());
        if (CommonUtil.isNullMap(todayPlan, "planAmount")) {
            // 오늘 날짜에 해당하는 생산 계획이 존재하지 않는 경우
            data.put("plan", 0);
        } else {
            data.put("plan", todayPlan.get("planAmount"));
        }
        // 해당월 계획 누적 생산량
        Map<String, Object> monthPlan = dmProductionPlanRepository.findGroupByDateWithNativeQuery((Integer)todayPlan.get("planId"), startDate, endDate);
        if (CommonUtil.isNullMap(monthPlan, "total")) {
            data.put("accPlan", 0);
        } else {
            if (CommonUtil.getTodayDate() == 1) {
                data.put("accPlan", 0);
            } else {
                data.put("accPlan", monthPlan.get("total"));
            }
        }

        return CommonUtil.setResult(data);
    }

    public Map<String, Object> findDashboardProductionChart() {
        String sourceA = codeService.findByName("갱내파쇄", "거래처");
        String typeA = codeService.findByName("입고", "구분");

        if (sourceA == null || typeA == null) {
            return CommonUtil.setError("데이터 조회 중 오류가 발생헀습니다.");
        }
        // 조건설정
        // 현재일자 기준
        String startDate = CommonUtil.getTodayYearMonth() + "-01";
        String endDate = CommonUtil.getToday();
        // 차트 데이터 (광석 생산량)
        // 범례 : 누적생산량, 일간 생산량
        // 단위 : 톤
        // 조건 : 최근 ~ 7일전 데이터
        Map<String, Object> data = new HashMap<>();
        List<Map<String, Object>> acc = new ArrayList<>();
        List<Map<String, Object>> daily = new ArrayList<>();

        // 7일전 ~ 현재일자까지 누적 생산량
        for (int i = -6, len = 0; i <= len; i++) {
            Map<String, Object> accMap = new HashMap<>();
            if ( i < 0) {
                accMap.put("date", CommonUtil.getDayMonthDate(CommonUtil.getToday(), "/", i));
                String aaa = CommonUtil.getBeforeOrAfterToday(i);
                String bbb = CommonUtil.getBeforeOrAfterToday(i).substring(0, 7) + "-01";
                Map<String, Object> accResultMap = dmWeighbridgeLogRepository.findMonthAmountWithNativeQuery(CommonUtil.getBeforeOrAfterToday(i).substring(0, 7) + "-01", CommonUtil.getBeforeOrAfterToday(i), sourceA, typeA);
                if (CommonUtil.isNullMap(accResultMap, "total")) {
                    accMap.put("acc", 0);
                } else {
                    accMap.put("acc", accResultMap.get("total"));
                }
            } else {
                accMap.put("date", CommonUtil.getTodayMonthDate("/"));
                Map<String, Object> accResultMap = dmWeighbridgeLogRepository.findMonthAmountWithNativeQuery(startDate, endDate, sourceA, typeA);
                if (CommonUtil.isNullMap(accResultMap, "total")) {
                    accMap.put("acc", 0);
                } else {
                    accMap.put("acc", accResultMap.get("total"));
                }
            }
            acc.add(accMap);
        }

        // 7일전 ~ 현재일자까지 일별 생산량
        for (int i = -6, len = 0; i <= len; i++) {
            Map<String, Object> dailyMap = new HashMap<>();
            if ( i < 0) {
                dailyMap.put("date", CommonUtil.getDayMonthDate(CommonUtil.getToday(), "/", i));
                Map<String, Object> dailyResultMap = dmWeighbridgeLogRepository.findTodayAmountWithNativeQuery(CommonUtil.getBeforeOrAfterToday(i), sourceA, typeA);
                if (CommonUtil.isNullMap(dailyResultMap, "total")) {
                    dailyMap.put("daily", 0);
                } else {
                    dailyMap.put("daily", dailyResultMap.get("total"));
                }
            } else {
                dailyMap.put("date", CommonUtil.getTodayMonthDate("/"));
                Map<String, Object> dailyResultMap = dmWeighbridgeLogRepository.findTodayAmountWithNativeQuery(CommonUtil.getToday(), sourceA, typeA);
                if (CommonUtil.isNullMap(dailyResultMap, "total")) {
                    dailyMap.put("daily", 0);
                } else {
                    dailyMap.put("daily", dailyResultMap.get("total"));
                }
            }
            daily.add(dailyMap);
        }

        data.put("acc", acc);
        data.put("daily", daily);

        return CommonUtil.setResult(data);
    }
}
