/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.api.vo;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/*
 *
 * title : DmProductionPlanMaster Vo
 * author : 윤영수
 * date : 2020.08.19
 *
 * */

@Getter
@Setter
@Entity
@Table(name = "t_dm_production_plan_master")
public class DmProductionPlanMasterVo implements Serializable {

    private static final long serialVersionUID = -6068111242332161609L;

    @Id
    @Column(name = "idx")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer planId;

    @Column(name = "plan_name")
    private String planName;
}
