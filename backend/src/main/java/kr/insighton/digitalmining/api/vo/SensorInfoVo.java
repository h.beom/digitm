/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.api.vo;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/*
 *
 * title : SensorInfo Vo
 * author : 윤영수
 * date : 2020.08.05
 *
 * */

@Getter
@Setter
@Entity
@Table(name = "t_sensor_info")
public class SensorInfoVo implements Serializable {

    private static final long serialVersionUID = 1670708792025841539L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Integer idx;

    @Column(name = "main_name", length = 45)
    private String mainName;

    @Column(name = "sub_name", length = 45)
    private String subName;

    @Column(name = "sensor_name", length = 45)
    private String sensorName;

    @Column(name = "sensor_sn", length = 45)
    private String sensorSn;

    @Column(name = "organization_name", length = 45)
    private String organizationName;

    @Column(name = "sensor_info", length = 45)
    private String sensorInfo;

    @Column(name = "sensor_point", length = 45)
    private String sensorPoint;

    @Column(name = "sensor_type", length = 45)
    private String sensorType;

    @Column(name = "s_point")
    private Double sPoint;

    @Column(name = "e_point")
    private Double ePoint;

    @Column(name = "memo", length = 200)
    private String memo;

    @Column(name = "reg_date")
    private Timestamp regDate;

    @Column(name = "place_type", length = 45)
    private String placeType;

    @Column(name = "setup_number", length = 45)
    private String setupNumber;

    @Column(name = "standard_step", length = 45)
    private String standardStep;

    @Column(name = "warning_step", length = 45)
    private String warningStep;

    @Column(name = "show_text", length = 45)
    private String showText;

    @Column(name = "warning_type", length = 45)
    private String warningType;

    @Column(name = "warning_text", length = 45)
    private String warningText;

    @Column(name = "standard_ny", length = 45)
    private String standardNy;

    @Column(name = "standard_1up", length = 45)
    private String standard1up;

    @Column(name = "standard_1down", length = 45)
    private String standard1down;

    @Column(name = "standard_2up", length = 45)
    private String standard2up;

    @Column(name = "standard_2down", length = 45)
    private String standard2down;

    @Column(name = "standard_3up", length = 45)
    private String standard3up;

    @Column(name = "standard_3down", length = 45)
    private String standard3down;

    @Column(name = "standard_value", length = 45)
    private String standardValue;

    @Column(name = "gate_name", length = 45)
    private String gateName;

    @Column(name = "gate_point", length = 45)
    private String gatePoint;

    @Column(name = "sensor_newname", length = 45)
    private String sensorNewname;

}
