/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.api.repository;

import kr.insighton.digitalmining.api.vo.DmGradeControlLogVo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/*
 *
 * title : DmGradeControlLog Repository
 * author : 윤영수
 * date : 2020.08.26
 *
 * */

@Repository
public interface DmGradeControlLogRepository extends JpaRepository<DmGradeControlLogVo, Integer> {

    @Query(value = "" +
            " select" +
            "   t.number as number" +
            " , concat('(', t.number, ')', ' ', date_format(t.time, '%H:%i')) as text" +
            " from t_dm_grade_control_log t" +
            " where t.date = date_format(now(), '%Y-%m-%d')" +
            " and t.idx not in (" +
            "       select x.idx" +
            "       from t_dm_grade_control_log x" +
            "       where x.date > date_format(now(), '%Y-%m-%d')" +
            "       or (x.date = date_format(now(), '%Y-%m-%d') and x.time > date_format(now(), '%H:%i:%s'))" +
            " )" +
            " order by t.number desc"
            , nativeQuery = true)
    List<Map<String, Object>> findNumberByDateWithNativeQuery();

    @Query(value = "" +
            " select" +
            "   date_format(t.date, '%c/%d') as 'date'" +
            " , date_format(t.time, '%H:%i') as 'time'" +
            " , t.input_outer as inOuter" +
            " , t.input_inner as inInner" +
            " , t.note as note" +
            " , t.input as 'input'" +
            " , t.input_cumulative as inputAcc" +
            " , t.number as number" +
            " , t.sio2 as sio2" +
            " , t.al2o3 as al2o3" +
            " , t.fe2o3 as fe2o3" +
            " , t.cao as cao" +
            " , t.mgo as mgo" +
            " , t.so3 as so3" +
            " , t.k2o as k2o" +
            " , t.na2o as na2o" +
            " , t.sum as 'sum'" +
            " , t.sio2_cumulative as sio2Acc" +
            " , t.al2o3_cumulative as al2o3Acc" +
            " , t.fe2o3_cumulative as fe2o3Acc" +
            " , t.cao_cumulative as caoAcc" +
            " , t.mgo_cumulative as mgoAcc" +
            " , t.so3_cumulative as so3Acc" +
            " , t.k2o_cumulative as k2oAcc" +
            " , t.na2o_cumulative as na2oAcc" +
            " , t.sum_cumulative as sumAcc" +
            " from t_dm_grade_control_log t" +
            " where t.date = date_format(now(), '%Y-%m-%d')" +
            " and t.number = :number"
            , nativeQuery = true)
    List<Map<String, Object>> findByDateAndNumberWithNativeQuery(@Param("number") Integer number);

    @Query(value = "" +
            " select" +
            "   date_format(t.time, '%H:%i') as 'time'" +
            " , t.input as 'input'" +
            " , t.number as number" +
            " , t.sio2 as sio2" +
            " , t.al2o3 as al2o3" +
            " , t.fe2o3 as fe2o3" +
            " , t.cao as cao" +
            " , t.mgo as mgo" +
            " , t.so3 as so3" +
            " , t.k2o as k2o" +
            " , t.na2o as na2o" +
            " , t.sum as 'sum'" +
            " , t.sio2_cumulative as sio2Acc" +
            " , t.al2o3_cumulative as al2o3Acc" +
            " , t.fe2o3_cumulative as fe2o3Acc" +
            " , t.cao_cumulative as caoAcc" +
            " , t.mgo_cumulative as mgoAcc" +
            " , t.so3_cumulative as so3Acc" +
            " , t.k2o_cumulative as k2oAcc" +
            " , t.na2o_cumulative as na2oAcc" +
            " , t.sum_cumulative as sumAcc" +
            " from t_dm_grade_control_log t" +
            " where t.date = :date" +
            " and t.idx not in (" +
            "       select x.idx" +
            "       from t_dm_grade_control_log x" +
            "       where x.date > date_format(now(), '%Y-%m-%d')" +
            "       or (x.date = date_format(now(), '%Y-%m-%d') and x.time > date_format(now(), '%H:%i:%s'))" +
            " )" +
            " order by t.number"
            , nativeQuery = true)
    List<Map<String, Object>> findByDateWithNativeQuery(@Param("date") String date);
}
