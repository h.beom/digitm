package kr.insighton.digitalmining.api.service;

import kr.insighton.digitalmining.api.repository.DmDpmRealtimeRepository;
import kr.insighton.digitalmining.common.util.CommonUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;

@Service
@Transactional
public class DmDpmRealtimeService {

    @Resource
    private DmDpmRealtimeRepository dmDpmRealtimeRepository;

    public Map<String, Object> getBySensorId(Integer sensorId) {
        Map<String, Object> data = new HashMap<>();
        List<Map<String, Object>> logs = dmDpmRealtimeRepository.getBySEnsorId(sensorId);
        if (logs != null && logs.size() > 0) {
            data.put("logs", logs);
        } else {
            data.put("logs", new ArrayList<>());
        }

        return CommonUtil.setResult(data);
    }
}
