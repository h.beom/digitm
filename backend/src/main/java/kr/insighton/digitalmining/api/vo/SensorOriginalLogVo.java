/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.api.vo;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/*
 *
 * title : SensorOoriginalLog Vo
 * author : 윤영수
 * date : 2020.08.05
 *
 * */

@Getter
@Setter
@Entity
@Table(name = "sensor_original_log")
public class SensorOriginalLogVo implements Serializable {

    private static final long serialVersionUID = 519249127769375820L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Integer idx;

    @Column(name = "reg_date")
    private Timestamp regDate;

    @Column(name = "data", length = 500)
    private String data;

    @Column(name = "state", length = 45)
    private String state;

}
