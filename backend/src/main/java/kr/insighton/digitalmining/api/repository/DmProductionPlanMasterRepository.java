/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.api.repository;

import kr.insighton.digitalmining.api.vo.DmProductionPlanMasterVo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/*
 *
 * title : DmProductionPlanMaster Repository
 * author : 이형범
 * date : 2020.08.26
 *
 * */

@Repository
public interface DmProductionPlanMasterRepository extends JpaRepository<DmProductionPlanMasterVo, Integer> {

    DmProductionPlanMasterVo findByPlanId(Integer planId);

    /*
    * id, name 형으로 전체리스트 반환
    * */
    @Query(value = "" +
            " select" +
            "   t.idx as id" +
            " , t.plan_name as name" +
            " from t_dm_production_plan_master t" +
            " order by t.idx desc"
            , nativeQuery = true)
    List<Map<String, Object>> findVersionListWithNativeQuery();
}
