/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.api.repository;

import kr.insighton.digitalmining.api.vo.DmMicroseismicEventSensorDetailVo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/*
 *
 * title : DmMicroseismicEventSensorDetail Repository
 * author : 이형범
 * date : 2020.09.08
 *
 * */

@Repository
public interface DmMicroseismicEventSensorDetailRepository extends JpaRepository<DmMicroseismicEventSensorDetailVo, Integer> {

    /*
     * 미소진동 이벤트 센서 상세
     * */
    @Query(value = "" +
            " select t.idx as id" +
            " , t.event_idx as eventId" +
            " , t.sensor_idx as sensorId" +
            " , (" +
            "       select sensor_name" +
            "       from t_dm_sensor" +
            "       where idx = t.sensor_idx" +
            " ) as sensorName" +
            " , t.img_link as imgLink" +
            " from t_dm_microseismic_event_sensor_detail t" +
            " where t.event_idx = :id"
            , nativeQuery = true)
    List<Map<String, Object>> findAllByEventIdWithNativeQuery(@Param("id") Integer eventId);
}
