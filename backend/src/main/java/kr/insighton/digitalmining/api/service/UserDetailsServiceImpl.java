/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.api.service;

import kr.insighton.digitalmining.api.repository.UserRepository;
import kr.insighton.digitalmining.api.vo.UserDetailsVo;
import kr.insighton.digitalmining.common.exception.UserNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collections;

/*
 *
 * title : UserDetails Service
 * author : 이형범
 * date : 2020.08.05
 *
 * */

@AllArgsConstructor
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private UserRepository userRepository;

    @Override
    public UserDetailsVo loadUserByUsername(String id) throws UsernameNotFoundException {
        return userRepository.findById(id).map(u -> new UserDetailsVo(u, Collections.singleton(new SimpleGrantedAuthority(u.getRole())))).orElseThrow(() -> new UserNotFoundException(id));
    }
}
