/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.api.service;

import kr.insighton.digitalmining.api.repository.DmMicroseismicEventReposiotry;
import kr.insighton.digitalmining.api.repository.DmMicroseismicEventSensorDetailRepository;
import kr.insighton.digitalmining.api.vo.DmMicroseismicEventVo;
import kr.insighton.digitalmining.common.util.CommonUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;

/*
 *
 * title : DmMicroseismicEvent Service
 * author : 윤영수
 * date : 2020.09.08
 *
 * */

@Service
@Transactional
public class DmMicroseismicEventService {

    @Resource
    private DmMicroseismicEventReposiotry dmMicroseismicEventReposiotry;

    @Resource
    private DmMicroseismicEventSensorDetailRepository dmMicroseismicEventSensorDetailRepository;

    /*
    * 미소진동 일자별 마지막 이벤트
    * */
    public Map<String, Object> findAllByDateAndNumberMax(String date) {
        Map<String, Object> data = new HashMap<>();
        Map<String, Object> event = dmMicroseismicEventReposiotry.findAllByDateAndNumberMaxWithNativeQuery(date);
        Map<String, Object> cloneEvent = new HashMap<>();
        if (CommonUtil.isNullMap(event, "id")) {
            return CommonUtil.setError("요청하신 날짜에 해당되는 데이터가 존재하지 않습니다.");
        } else {
            data.put("id", event.get("id"));
            data.put("number", event.get("number"));
            data.put("datetime", event.get("datetime"));
            data.put("category", event.get("category"));
            data.put("categoryName", event.get("categoryName"));
            data.put("type", event.get("type"));
            data.put("typeName", event.get("typeName"));
            data.put("radius", event.get("radius"));
            data.put("x", event.get("x"));
            data.put("y", event.get("y"));
            data.put("z", event.get("z"));
            List<Map<String, Object>> sensors = dmMicroseismicEventSensorDetailRepository.findAllByEventIdWithNativeQuery(Integer.valueOf(event.get("id").toString()));
            if(sensors != null && sensors.size() > 0) {
                data.put("sensors", sensors);
            } else {
                data.put("sensors", new ArrayList<>());
            }
            return CommonUtil.setResult(data);
        }
    }

    /*
    * 미소진동 일자별, 미소진동종류별 이벤트 목록
    * */
    public Map<String, Object> findAllByDateAndCategory(String date, String category) {
        Map<String, Object> data = new HashMap<>();
        List<Map<String, Object>> events = dmMicroseismicEventReposiotry.findAllByDateAndCategoryWithNativeQuery(date, category);
        List<Map<String, Object>> cloneEvents = new ArrayList<>();
        if (events != null && events.size() > 0) {
            for (int i = 0; i < events.size(); i++) {
                Map<String, Object> event = new HashMap<>();
                event.put("id", events.get(i).get("id"));
                event.put("number", events.get(i).get("number"));
                event.put("datetime", events.get(i).get("datetime"));
                event.put("category", events.get(i).get("category"));
                event.put("categoryName", events.get(i).get("categoryName"));
                event.put("type", events.get(i).get("type"));
                event.put("typeName", events.get(i).get("typeName"));
                event.put("radius", events.get(i).get("radius"));
                event.put("x", events.get(i).get("x"));
                event.put("y", events.get(i).get("y"));
                event.put("z", events.get(i).get("z"));
                List<Map<String, Object>> sensors = dmMicroseismicEventSensorDetailRepository.findAllByEventIdWithNativeQuery(Integer.valueOf(event.get("id").toString()));
                if (sensors != null && sensors.size() > 0) {
                    event.put("sensors", sensors);
                } else {
                    event.put("sensors", new ArrayList<>());
                }
                cloneEvents.add(event);
            }

            data.put("events", cloneEvents);
        } else {
            data.put("events", new ArrayList<>());
        }
        return CommonUtil.setResult(data);
    }

    /*
    * 미소진동 이벤트에서 미소진동종류 수정
    * */
    public Map<String, Object> updateCategoryById(int id, String category) {
        DmMicroseismicEventVo dmMicroseismicEventVo = dmMicroseismicEventReposiotry.findById(id).orElse(null);
        if (dmMicroseismicEventVo == null) {
            return CommonUtil.setError("존재하지 않는 이벤트 ID입니다.");
        } else {
            dmMicroseismicEventVo.setCategory(category);
            dmMicroseismicEventReposiotry.save(dmMicroseismicEventVo);
            return CommonUtil.setResult(null);
        }
    }

    /*
    * 미소진동 이벤트 삭제(외래키 t_dm_microseismic_event_sensor_detail -> evnet_idx 같이 삭제됨)
    * */
    public Map<String, Object> deleteById(int id) {
        DmMicroseismicEventVo dmMicroseismicEventVo = dmMicroseismicEventReposiotry.findById(id).orElse(null);
        if (dmMicroseismicEventVo == null) {
            return CommonUtil.setError("존재하지 않는 이벤트 ID입니다.");
        } else {
            dmMicroseismicEventReposiotry.deleteById(id);
            return CommonUtil.setResult(null);
        }
    }

    /*
    * 미소진동 미소진동종류별 3개월단위 카운트
    * */
    public Map<String, Object> findCountCategoryGroupByDate(String date) {
        List<Map<String, Object>> data = dmMicroseismicEventReposiotry.findCountCategoryGroupByDateWithNativeQuery(date);
        if (data != null && data.size() > 0) {
            return CommonUtil.setResultList(data);
        } else {
            return CommonUtil.setResultList(new ArrayList<>());
        }
    }
}
