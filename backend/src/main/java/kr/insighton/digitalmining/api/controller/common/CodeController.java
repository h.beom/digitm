/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.api.controller.common;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import kr.insighton.digitalmining.common.service.CodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/*
 *
 * title : Code Controller
 * author : 윤영수
 * date : 2020.09.04
 *
 * */

@Api(tags = "공통코드")
@RestController
@RequestMapping("/api/common/code")
public class CodeController {

    @Autowired
    private CodeService codeService;

    @ApiOperation(value = "코드리스트 조회"
            , notes = "{\n" +
            "    Request: {}\n" +
            "    Response: {\n" +
            "        data: [\n" +
            "            {code: 1, name: '종류1'},\n" +
            "            {code: 2, name: '종류2'},\n" +
            "            {code: 3, name: '종류3'},\n" +
            "            {code: 4, name: '종류4'}\n" +
            "        ]\n" +
            "    }\n" +
            "}\n"
    )
    @GetMapping(value = "/codes/{group_id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> apiCommonCodeGroup(@PathVariable("group_id") String group) {
        return new ResponseEntity<>(codeService.findAllByGroup(group), HttpStatus.OK);
    }

    @ApiOperation(value = "코드아이템리스트 조회"
            , notes = "{\n" +
            "    Request: {}\n" +
            "    Response: {\n" +
            "        data: [\n" +
            "            {item: 1, name: '종류1'},\n" +
            "            {item: 2, name: '종류2'},\n" +
            "            {item: 3, name: '종류3'},\n" +
            "            {item: 4, name: '종류4'}\n" +
            "        ]\n" +
            "    }\n" +
            "}\n"
    )
    @GetMapping(value = "/codes/{group_id}/{item_id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> apiCommonCodeGroup(@PathVariable("group_id") String group, @PathVariable("item_id") Integer item) {
        return new ResponseEntity<>(codeService.findAllByGroupAndItem(group, item), HttpStatus.OK);
    }

    @ApiOperation(value = "코드그룹리스트 조회"
            , notes = "{\n" +
            "    Request: {}\n" +
            "    Response: {\n" +
            "        data: [\n" +
            "            {id: 1, name: '구분'},\n" +
            "            {id: 2, name: '종류'},\n" +
            "            {id: 3, name: '유형'}\n" +
            "        ]\n" +
            "    }\n" +
            "}\n"
    )
    @GetMapping(value = "/groups", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> apiCommonCodeGroups() {
        return new ResponseEntity<>(codeService.findAllGroup(), HttpStatus.OK);
    }

}
