/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.api.service;

import kr.insighton.digitalmining.api.repository.DmGradeControlLogRepository;
import kr.insighton.digitalmining.common.util.CommonUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;

/*
 *
 * title : DmGradeControlLog Service
 * author : 윤영수
 * date : 2020.08.19
 *
 * */

@Service
@Transactional
public class DmGradeControlLogService {

    @Resource
    private DmGradeControlLogRepository dmGradeControlLogRepository;

    public Map<String, Object> findProductionGradeControlRealtimeNumber() {
        Map<String, Object> data = new HashMap<>();
        List<Map<String, Object>> list = dmGradeControlLogRepository.findNumberByDateWithNativeQuery();
        List<LinkedHashMap<String, Object>> numbers = new ArrayList<>();
        if (list != null && list.size() > 0) {
            for(int i = 0, len = list.size(); i < len; i++) {
                LinkedHashMap<String, Object> item = new LinkedHashMap<>();
                item.put("number", list.get(i).get("number"));
                item.put("text", list.get(i).get("text"));
                numbers.add(item);
            }

            data.put("numbers", numbers);
        } else {
            data.put("numbers", new ArrayList<>());
        }

        return CommonUtil.setResult(data);
    }

    public Map<String, Object> findProductionGradeControlRealtime(int number) {
        List<Map<String, Object>> data = dmGradeControlLogRepository.findByDateAndNumberWithNativeQuery(number);
        if( data != null && data.size() > 0) {
            return CommonUtil.setResultList(data);
        } else {
            return CommonUtil.setResultList(new ArrayList<>());
        }
    }

    public Map<String, Object> findProductionGradeControlRealtimeChart(int number) {
        List<Map<String, Object>> list = dmGradeControlLogRepository.findByDateAndNumberWithNativeQuery(number);
        List<Map<String, Object>> data = new ArrayList<>();
        List<String> grade = new ArrayList<>();

        grade.add("cao");
        grade.add("caoAcc");
        grade.add("sio2");
        grade.add("sio2Acc");
        grade.add("al2o3");
        grade.add("al2o3Acc");
        grade.add("mgo");
        grade.add("mgoAcc");
        grade.add("fe2o3");
        grade.add("fe2o3Acc");
        grade.add("k2o");
        grade.add("k2oAcc");
        grade.add("so3");
        grade.add("so3Acc");
        grade.add("na2o");
        grade.add("na2oAcc");

        if( list != null && list.size() == 1) {
            // 분석항목 8개
            for (int l = 0, lLen = list.size(); l < lLen; l++) {
                for (int i = 0, iLen = grade.size(); i < iLen; i++) {
                    Map<String, Object> item = new HashMap<>();
                    item.put("category", grade.get(i));
                    item.put("value", list.get(l).get(grade.get(i)));
                    data.add(item);
                }
            }
        }
        
        return CommonUtil.setResultList(data);
    }

    public Map<String, Object> findProductionGradeControlAnalysis(String date) {
        Map<String, Object> data = new HashMap<>();
        List<Map<String, Object>> list = dmGradeControlLogRepository.findByDateWithNativeQuery(date);
        if (list != null && list.size() > 0) {
            data.put("list", list);
        } else {
            data.put("list", new ArrayList<>());
        }

        return CommonUtil.setResult(data);
    }

    public Map<String, Object> findProductionGradeControlAnalysisChart(String date, String chemical) {
        List<Map<String, Object>> data = new ArrayList<>();
        List<Map<String, Object>> list = dmGradeControlLogRepository.findByDateWithNativeQuery(date);
        if (list != null && list.size() > 0) {
            for (int i = 0; i < 2; i++) {
                for (int l = 0, len = list.size(); l < len; l++) {
                    Map<String, Object> item = new HashMap<>();
                    if (i == 0) {
                        item.put("time", list.get(l).get("time"));
                        item.put("category", "chemical");
                        item.put("value", list.get(l).get(chemical));

                        data.add(item);
                    } else {
                        item.put("time", list.get(l).get("time"));
                        item.put("category", "chemicalAcc");
                        item.put("value", list.get(l).get(chemical+"Acc"));

                        data.add(item);
                    }
                }
            }
        } else {
            return CommonUtil.setError("데이터 처리중 오류가 발생했습니다.");
        }

        return CommonUtil.setResultList(data);
    }
}
