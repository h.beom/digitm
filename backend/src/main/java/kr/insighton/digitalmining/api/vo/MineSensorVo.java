/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.api.vo;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;

/*
 *
 * title : MineSensor Vo
 * author : 윤영수
 * date : 2020.08.05
 *
 * */

@Getter
@Setter
@Entity
@Table(name = "mine_sensor")
public class MineSensorVo implements Serializable {

    private static final long serialVersionUID = 2005172143492156470L;

    @Embeddable
    public static class EmbededTable implements Serializable {

        private static final long serialVersionUID = 2255947033695148264L;

        @Column(name = "mine_location")
        private Integer mineLocation;

        @Column(name = "PM25")
        private Double pm25;

        @Column(name = "PM10")
        private Double pm10;

        @Column(name = "CO2")
        private Double co2;

        @Column(name = "HCHO")
        private Double hcho;

        @Column(name = "TVOC")
        private Double tvoc;

        @Column(name = "TEMP")
        private Double temp;

        @Column(name = "humi")
        private Double humi;

        @Column(name = "TIME")
        private Date time;

    }

    @EmbeddedId EmbededTable id = new EmbededTable();

}

