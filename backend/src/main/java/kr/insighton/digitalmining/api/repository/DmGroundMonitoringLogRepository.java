/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.api.repository;

import kr.insighton.digitalmining.api.vo.DmGroundMonitoringLogVo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/*
 *
 * title : DmGroundMonitoringLog Repository
 * author : 이형범
 * date : 2020.09.10
 *
 * */

@Repository
public interface DmGroundMonitoringLogRepository extends JpaRepository<DmGroundMonitoringLogVo, Integer> {

    @Query(value = "" +
            " select t.idx as id" +
            " , t.sensor_idx as sensorId" +
            " , cast(date_format(t.date, '%Y-%m-%d') as char(10)) as 'date'" +
            " , t.time as 'time'" +
            " , truncate(t.x_degree, 3) as xDegree" +
            " , truncate(t.dx_dt, 3) as xDegreeDisplacement" +
            " , truncate(t.y_degree, 3) as yDegree" +
            " , truncate(t.dy_dt, 3) as yDegreeDisplacement" +
            " from t_dm_ground_monitoring_log t" +
            " where t.sensor_idx = :sensorId" +
            " and date_format(concat(date, ' ', time), '%Y-%m-%d %H:%i:%s') < now()" +
            " and date_format(concat(date, ' ', time), '%Y-%m-%d %H:%i:%s') > date_add(date_format(now(), '%Y-%m-%d %H:%i:%s'), interval -1 day)"
            , nativeQuery = true)
    List<Map<String, Object>> findAllByNowAndSensorIdWithNativeQuery(@Param("sensorId") Integer sensorId);

    @Query(value = "" +
            " select t.idx as id" +
            " , t.sensor_idx as sensorId" +
            " , cast(date_format(t.date, '%Y-%m-%d') as char(10)) as 'date'" +
            " , t.time as 'time'" +
            " , truncate(t.x_degree, 3) as xDegree" +
            " , truncate(t.dx_dt, 3) as xDegreeDisplacement" +
            " , truncate(t.y_degree, 3) as yDegree" +
            " , truncate(t.dy_dt, 3) as yDegreeDisplacement" +
            " from t_dm_ground_monitoring_log t" +
            " where t.date >= date_format(:from, '%Y-%m-%d')" +
            " and t.date <= date_format(:to, '%Y-%m-%d')" +
            " and t.sensor_idx = :sensorId"
            , nativeQuery = true)
    List<Map<String, Object>> findAllByDateFromToAndSensorIdWithNativeQuery(@Param("sensorId") Integer sensorId, @Param("from") String from, @Param("to") String to);
}
