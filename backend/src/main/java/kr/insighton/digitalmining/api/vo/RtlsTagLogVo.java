/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.api.vo;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/*
 *
 * title : RtlsTagLog Vo
 * author : 윤영수
 * date : 2020.08.05
 *
 * */

@Getter
@Setter
@Entity
@Table(name = "rtls_tag_log")
public class RtlsTagLogVo implements Serializable {

    private static final long serialVersionUID = -5178310836241849400L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Integer idx;

    @Column(name = "tag_id", length = 100)
    private String tagId;

    @Column(name = "x", length = 45)
    private String x;

    @Column(name = "y", length = 45)
    private String y;

    @Column(name = "z", length = 45)
    private String z;

    @Column(name = "reg_date")
    private Timestamp regDate;

}
