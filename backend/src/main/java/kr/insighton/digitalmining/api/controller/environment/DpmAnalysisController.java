package kr.insighton.digitalmining.api.controller.environment;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import kr.insighton.digitalmining.api.service.DmDpmAnalysisService;
import kr.insighton.digitalmining.common.util.CommonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "채광환경 제어 - DPM 일자별 분석")
@RestController
public class DpmAnalysisController {

    @Autowired
    private DmDpmAnalysisService dmDpmAnalysisService;


    @ApiOperation(value = "DPM - 일자 로그")
    @GetMapping(value = "/api/environment/dpm-analysis")
    public ResponseEntity<?> apiEnvironmentDpmAnalysis(@RequestParam("sensorId") Integer sensorId, @RequestParam("date") String date) {
        // 요청 파라미터 검증
        if (date == null || date.equals("") || !CommonUtil.isValidDate(date, 10)) {
            return new ResponseEntity<>(CommonUtil.setError("요청 파라미터 'from' 날짜형식이 아닙니다."), HttpStatus.OK);
        }
        if (!CommonUtil.isValidNumber(sensorId)) {
            return new ResponseEntity<>(CommonUtil.setError("요청 파라미터 'sensorId' 숫자형식이 아닙니다."), HttpStatus.OK);
        }

        return new ResponseEntity<>(dmDpmAnalysisService.getBySensorIdAndDate(sensorId, date), HttpStatus.OK);
    }

}
