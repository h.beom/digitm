/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.api.vo;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/*
 *
 * title : SensorCoInfo Vo
 * author : 이형범
 * date : 2020.08.05
 *
 * */

@Getter
@Setter
@Entity
@Table(name = "t_sensor_co_info")
public class SensorCoInfoVo implements Serializable {

    private static final long serialVersionUID = -1238588507793487L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx", length = 45)
    private Integer idx;

    @Column(name = "sensor_co", length = 45)
    private String sensorCo;

    @Column(name = "sensor_co_value", length = 45)
    private String sensorCoValue;

    @Column(name = "type", length = 45)
    private String type;

    @Column(name = "reg_date")
    private Timestamp regDate;

}
