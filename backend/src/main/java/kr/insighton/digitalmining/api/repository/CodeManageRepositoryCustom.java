package kr.insighton.digitalmining.api.repository;

import java.util.*;

import kr.insighton.digitalmining.common.vo.CodeVo;

public interface CodeManageRepositoryCustom {
    List<CodeVo> getCodeList(String codeGroup, String codeName);
}
