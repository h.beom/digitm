/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.api.vo;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;

/*
 *
 * title : DmDpmLog Vo
 * author : 윤영수
 * date : 2020.09.11
 *
 * */

@Getter
@Setter
@Entity
@Table(name = "t_dm_dpm_log")
public class DmDpmLogVo implements Serializable {

    private static final long serialVersionUID = -6538489686021214953L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Double idx;

    @Column(name = "sensor_idx")
    private Double sensorIdx;

    @Column(name = "date")
    private Date date;

    @Column(name = "time")
    private Time time;

    @Column(name = "x")
    private Double x;

    @Column(name = "y")
    private Double y;

    @Column(name = "z")
    private Double z;

    @Column(name = "temp")
    private Double temp;

    @Column(name = "humidity")
    private Double humidity;

    @Column(name = "co")
    private Double co;

    @Column(name = "no")
    private Double no;

    @Column(name = "dpm_10m")
    private Double dpm10m;

    @Column(name = "dpm_30m")
    private Double dpm30m;

}
