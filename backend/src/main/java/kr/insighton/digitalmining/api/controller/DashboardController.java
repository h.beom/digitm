/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.api.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import kr.insighton.digitalmining.api.service.DashboardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/*
 *
 * title : 대쉬보드 Controller
 * author : 윤영수
 * date : 2020.08.05
 *
 * */

@Api(tags = "대쉬보드")
@RestController
@RequestMapping(value = "/api/dashboard")
public class DashboardController {

    @Autowired
    private DashboardService dashboardService;

    @ApiOperation(value = "생산량"
            , notes =
                "Request: {}\n" +
                "Response: {\n" +
                "    data: {\n" +
                "        currentProd: 4500, -- 일생산량\n" +
                "        accProd: 398800, -- 누적 생산량\n" +
                "        plan: 4300, -- 계획 생산량\n" +
                "        accPlan: 400000, -- 계획 누적 생산량\n" +
                "    }\n" +
                "}"
    )
    @GetMapping(value = "/production/amount", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> apiProductionAmount() {
        return new ResponseEntity<>(dashboardService.findDashboardProductionAmount(), HttpStatus.OK);
    }

    @ApiOperation(value = "차트"
            , notes =
                "Request: {}\n" +
                "Response: {\n" +
                "    data: {\n" +
                "        acc: {\n" +
                "            {date: '8/3', acc: '378500'},\n" +
                "            {date: '8/4', acc: '382700'},\n" +
                "            {date: '8/5', acc: '386700'},\n" +
                "            {date: '8/6', acc: '390800'},\n" +
                "            {date: '8/7', acc: '382700'},\n" +
                "            {date: '8/8', acc: '386700'},\n" +
                "            {date: '8/9', acc: '390800'}\n" +
                "        },\n" +
                "        daily: {\n" +
                "            {date: '8/3', daily: '4500'},\n" +
                "            {date: '8/4', daily: '4200'},\n" +
                "            {date: '8/5', daily: '4000'},\n" +
                "            {date: '8/6', daily: '4100'},\n" +
                "            {date: '8/7', daily: '3800'},\n" +
                "            {date: '8/8', daily: '4200'},\n" +
                "            {date: '8/9', daily: '4500'}\n" +
                "        }\n" +
                "    }\n" +
                "}"
    )
    @GetMapping(value = "/production/chart", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> apiProductionChart() {
        return new ResponseEntity<>(dashboardService.findDashboardProductionChart(), HttpStatus.OK);
    }
}
