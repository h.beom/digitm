/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.api.service;

import kr.insighton.digitalmining.api.repository.DmMicroseismicEventSensorDetailRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/*
 *
 * title : DmMicroseismicEventSensorDetail Service
 * author : 이형범
 * date : 2020.09.08
 *
 * */

@Service
@Transactional
public class DmMicroseismicEventSensorDetailService {

    @Resource
    private DmMicroseismicEventSensorDetailRepository dmMicroseismicEventSensorDetailRepository;

}
