package kr.insighton.digitalmining.api.service;

import kr.insighton.digitalmining.api.repository.DmDpmAnalysisRepository;
import kr.insighton.digitalmining.common.util.CommonUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class DmDpmAnalysisService {

    @Resource
    private DmDpmAnalysisRepository dmDpmAnalysisRepository;

    public Map<String, Object> getBySensorIdAndDate(Integer sensorId, String date) {
        Map<String, Object> data = new HashMap<>();

        List<Map<String, Object>> logs = dmDpmAnalysisRepository.getBySensorIdAndDate(sensorId, date);

        if (logs != null && logs.size() > 0) {
            data.put("logs", logs);
        } else {
            data.put("logs", new ArrayList<>());
        }

        return CommonUtil.setResult(data);
    }
}
