/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.api.repository;

import kr.insighton.digitalmining.api.vo.DmSensorVo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/*
 *
 * title : DmMicroseismicSensor Repository
 * author : 윤영수
 * date : 2020.09.08
 *
 * */

@Repository
public interface DmSensorRepository extends JpaRepository<DmSensorVo, Integer> {

    /*
    * 센서 조회( all인 경우 전체 센서 조회)
    * */
    @Query(value = "" +
            " select t.idx as id" +
            " , t.division" +
            " , (select name from t_dm_code where code = t.division and use_yn = 'Y') as divisionName" +
            " , t.sensor_id as sensorId" +
            " , t.sensor_name as sensorName" +
            " , t.x" +
            " , t.y" +
            " , t.z" +
            " , t.type" +
            " , (select name from t_dm_code where code = t.type and use_yn = 'Y') as typeName" +
            " from t_dm_sensor t" +
            " where case when :division = 'all' then t.division <> :division" +
            "            else t.division = :division" +
            "       end"
            , nativeQuery = true)
    List<Map<String, Object>> findAllByDivisionWithNativeQuery(@Param("division") String division);


}
