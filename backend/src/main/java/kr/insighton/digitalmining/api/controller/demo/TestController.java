/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.api.controller.demo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import kr.insighton.digitalmining.api.service.demo.TestService;
import kr.insighton.digitalmining.api.vo.demo.TestVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/*
 *
 * title : test Controller
 * author : 이형범
 * date : 2020.08.05
 *
 * */

@Api(tags = "데모-테스트")
@RestController
@RequestMapping("/api/demo")
public class TestController {

    @Autowired
    private TestService testService;


    @ApiOperation(value = "전체조회"
            , notes =
                "Request : {}\n" +
                "Response :{\n" +
                "    data: [" +
                "       {\n" +
                "           name: 홍길동1,\n" +
                "           test: 테스트1\n" +
                "       },\n" +
                "       {\n" +
                "           name: 홍길동2,\n" +
                "           test: 테스트2\n" +
                "       },\n" +
                "       {\n" +
                "           name: 홍길동3,\n" +
                "           test: 테스트3\n" +
                "       }\n" +
                "}"
    )
    @GetMapping(value = "/test", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<List<TestVo>> apiFindAll() {
        return new ResponseEntity<>(testService.findAll(), HttpStatus.OK);
    }

    @ApiOperation(value = "건수조회"
            , notes =
                "Request : {}\n" +
                "Response :{\n" +
                "    data: 10\n" +
                "}"
    )
    @GetMapping(value = "/test/count", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Long> apiCount() {
        return new ResponseEntity<>(testService.numberOfTest(), HttpStatus.OK);
    }

    @ApiOperation(value = "이름조회"
            , notes =
                "Request : {\n" +
                "    name: '홍길동'\n" +
                "\n" +
                "Response :{\n" +
                "    data: [" +
                "       {\n" +
                "           name: 홍길동1,\n" +
                "           test: 테스트1\n" +
                "       },\n" +
                "       {\n" +
                "           name: 홍길동2,\n" +
                "           test: 테스트2\n" +
                "       },\n" +
                "       {\n" +
                "           name: 홍길동3,\n" +
                "           test: 테스트3\n" +
                "       }\n" +
                    "]\n" +
                "}"
    )
    @GetMapping(value = "/test/find", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<List<TestVo>> apiFind(@RequestParam(name = "name") String name) {
        return new ResponseEntity<>(testService.findByNameIgnoreCaseContaining(name), HttpStatus.OK);
    }

    @ApiOperation(value = "등록"
            , notes =
                "Request : {\n" +
                "    name: '홍길동4',\n" +
                "    text: '테스트4'\n" +
                "}\n" +
                "Response :{" +
                "    status : 'OK'\n" +
                "}"
    )
    @PostMapping(value = "/test/create", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<TestVo> apiSave(@RequestBody TestVo testVo) {
        if(testService.findByName(testVo.getName()) != null) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        return new ResponseEntity<>(testService.saveTest(testVo), HttpStatus.CREATED);
    }

    @ApiOperation(value = "수정"
            , notes =
                "Request: {\n" +
                "    name: '홍길동4',\n" +
                "    text: '테스트4'\n" +
                "}\n" +
                "Response: {" +
                "    status : 'OK'\n" +
                "}"
    )
    @PutMapping(value = "/test/update", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<TestVo> apiUpdate(@RequestBody TestVo testVo) {
        if(testService.findByName(testVo.getName()) == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(testService.updateTest(testVo), HttpStatus.OK);
    }

    @ApiOperation(value = "삭제"
            , notes =
                "Request : {\n" +
                "    name: '홍길동'\n" +
                "}" +
                "Response: {" +
                "    status : 'OK'\n" +
                "}"
    )
    @PostMapping(value = "/test/delete", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<TestVo> apiDelete(@RequestBody TestVo testVo) {
        if(testService.findByName(testVo.getName()) == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        testService.deleteTest(testVo.getName());
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
