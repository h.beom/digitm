/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.api.controller.production;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import kr.insighton.digitalmining.api.service.DmGradeControlLogService;
import kr.insighton.digitalmining.common.util.CommonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/*
 *
 * title : GradeControl Controller
 * author : 윤영수
 * date : 2020.09.18
 *
 * */

@Api(tags = "광산운영 & 생산 - Grade Control")
@RestController
public class GradeControlController {

    @Autowired
    private DmGradeControlLogService dmGradeControlLogService;

    @ApiOperation(value = "실시간 품위 분석 - 차수순번"
            , notes =
            "Request: {}\n" +
                    "Response: {\n" +
                    "    data: {\n" +
                    "        versions: [\n" +
                    "            {id: 2, name: 2},\n" +
                    "            {id: 3, name: 3}\n" +
                    "        ]\n" +
                    "    }\n" +
                    "}"
    )
    @GetMapping(value = "/api/production/grade-control-realtime/number", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> apiProductionGradeControlRealtimeNumber() {
        return new ResponseEntity<>(dmGradeControlLogService.findProductionGradeControlRealtimeNumber(), HttpStatus.OK);
    }

    @ApiOperation(value = "실시간 품위 분석 - 조회"
            , notes =
            "Request: {\n" +
                    "    number: 2\n" +
                    "}\n" +
                    "Response: {\n" +
                    "    data: [\n" +
                    "        date: '6/19',\n" +
                    "        time: '08:30',\n" +
                    "        inOuter: 0,\n" +
                    "        inInner: 200,\n" +
                    "        note: '12(6)/6(6)',\n" +
                    "        input: 200,\n" +
                    "        inputAcc: 200,\n" +
                    "        number: 1,\n" +
                    "        sio2: 9.42,\n" +
                    "        al2o3: 2.97,\n" +
                    "        fe2o3: 1.10,\n" +
                    "        cao: 43.61,\n" +
                    "        mgo: 2.43,\n" +
                    "        so3: 0.17,\n" +
                    "        k2o: 0.87,\n" +
                    "        na2o: 0.05,\n" +
                    "        sum: 60.62,\n" +
                    "        sio2Acc: 9.42,\n" +
                    "        al2o3Acc: 2.97,\n" +
                    "        fe2o3Acc: 1.10,\n" +
                    "        caoAcc: 43.61,\n" +
                    "        mgoAcc: 2.43,\n" +
                    "        so3Acc: 0.17,\n" +
                    "        k2oAcc: 0.87,\n" +
                    "        na2oAcc: 0.05,\n" +
                    "        sumAcc: 59.70\n" +
                    "    ]\n" +
                    "}"
    )
    @GetMapping(value = "/api/production/grade-control-realtime", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> apiProductionGradeControlRealtime(@RequestParam("number") Integer number) {
        // 요청 파라미터 검증
        if (!CommonUtil.isValidNumber(number)) {
            return new ResponseEntity<>(CommonUtil.setError("요청 파라미터의 데이터가 숫자형식이 아닙니다."), HttpStatus.OK);
        }

        return new ResponseEntity<>(dmGradeControlLogService.findProductionGradeControlRealtime(number), HttpStatus.OK);
    }

    @ApiOperation(value = "실시간 품위 분석 - 차트"
            , notes =
            "Request: {\n" +
                    "    number: 2\n" +
                    "}\n" +
                    "Response: {\n" +
                    "    data: {\n" +
                    "        chemical: [\n" +
                    "            'cao',\n" +
                    "            'caoAcc',\n" +
                    "            'sio2',\n" +
                    "            'sio2Acc',\n" +
                    "            'al2o3',\n" +
                    "            'al2o3Acc',\n" +
                    "            'mgo',\n" +
                    "            'mgoAcc',\n" +
                    "            'fe2o3',\n" +
                    "            'fe2o3Acc',\n" +
                    "            'k2o',\n" +
                    "            'k2oAcc',\n" +
                    "            'so3',\n" +
                    "            'so3Acc',\n" +
                    "            'na2o',\n" +
                    "            'na2oAcc'\n" +
                    "        ],\n" +
                    "        average: [\n" +
                    "            43.61,\n" +
                    "            43.61,\n" +
                    "            9.42,\n" +
                    "            9.42,\n" +
                    "            2.97,\n" +
                    "            2.97,\n" +
                    "            2.43,\n" +
                    "            2.43,\n" +
                    "            1.1,\n" +
                    "            1.1,\n" +
                    "            0.87,\n" +
                    "            0.87,\n" +
                    "            0.17,\n" +
                    "            0.17,\n" +
                    "            0.05,\n" +
                    "            0.05'\n" +
                    "        ]\n" +
                    "    }\n" +
                    "}"
    )
    @GetMapping(value = "/api/production/grade-control-realtime/chart", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> apiProductionGradeControlRealtimeChart(@RequestParam("number") Integer number) {
        // 요청 파라미터 검증
        if (!CommonUtil.isValidNumber(number)) {
            return new ResponseEntity<>(CommonUtil.setError("요청 파라미터의 데이터가 숫자형식이 아닙니다."), HttpStatus.OK);
        }

        return new ResponseEntity<>(dmGradeControlLogService.findProductionGradeControlRealtimeChart(number), HttpStatus.OK);
    }

    @ApiOperation(value = "일자별 품위 분석 - 조회"
            , notes =
            "Request: {\n" +
                    "    date: '2020-07-13'\n" +
                    "}\n" +
                    "Response: {\n" +
                    "    data: {\n" +
                    "        list: [\n" +
                    "            time: '08:30',\n" +
                    "            input: 200,\n" +
                    "            number: 1,\n" +
                    "            sio2: 9.42,\n" +
                    "            al2o3: 2.97,\n" +
                    "            fe2o3: 1.10,\n" +
                    "            cao: 43.61,\n" +
                    "            mgo: 2.43,\n" +
                    "            so3: 0.17,\n" +
                    "            k2o: 0.87,\n" +
                    "            na2o: 0.05,\n" +
                    "            sum: 60.62,\n" +
                    "            sio2Acc: 9.42,\n" +
                    "            al2o3Acc: 2.97,\n" +
                    "            fe2o3Acc: 1.10,\n" +
                    "            caoAcc: 43.61,\n" +
                    "            mgoAcc: 2.43,\n" +
                    "            so3Acc: 0.17,\n" +
                    "            k2oAcc: 0.87,\n" +
                    "            na2oAcc: 0.05,\n" +
                    "            sumAcc: 59.70\n" +
                    "        ],\n" +
                    "        ........" +
                    "    }\n" +
                    "}"
    )
    @GetMapping(value = "/api/production/grade-control-analysis", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> apiProductionGradeControlAnalysis(@RequestParam("date") String date) {
        // 요청 파라미터 검증
        if (date == null || (date !=null && date.equals("") || !CommonUtil.isValidDate(date, 10))) {
            return new ResponseEntity<>(CommonUtil.setError("요청 파라미터의 데이터가 날짜형식이 아닙니다."), HttpStatus.OK);
        }

        return new ResponseEntity<>(dmGradeControlLogService.findProductionGradeControlAnalysis(date), HttpStatus.OK);
    }

    @ApiOperation(value = "일자별 품위 분석 - 차트"
            , notes =
            "Request: {\n" +
                    "    date: '2020-07-13',\n" +
                    "    chemical: 'cao',\n" +
                    "}\n" +
                    "Response: {\n" +
                    "    data: [\n" +
                    "        {\n" +
                    "            time: '08:09',\n" +
                    "            category: 'chemical',\n" +
                    "            value: 4.5\n" +
                    "        },\n" +
                    "        {\n" +
                    "            time: '08:09',\n" +
                    "            category: 'chemical',\n" +
                    "            value: 5.5\n" +
                    "        },\n" +
                    "        {\n" +
                    "            time: '08:09',\n" +
                    "            category: 'chemical',\n" +
                    "            value: 28.9\n" +
                    "        },\n" +
                    "        {\n" +
                    "            time: '08:09',\n" +
                    "            category: 'chemical',\n" +
                    "            value: 11.9\n" +
                    "        },\n" +
                    "        {\n" +
                    "            time: '08:09',\n" +
                    "            category: 'chemical',\n" +
                    "            value: 15.9\n" +
                    "        },\n" +
                    "        {\n" +
                    "            time: '08:09',\n" +
                    "            category: 'chemical',\n" +
                    "            value: 21.9\n" +
                    "        },\n" +
                    "        {\n" +
                    "            time: '08:09',\n" +
                    "            category: 'chemical',\n" +
                    "            value: 18.9\n" +
                    "        },\n" +
                    "        {\n" +
                    "            time: '08:09',\n" +
                    "            category: 'chemical',\n" +
                    "            value: 60.9\n" +
                    "        },\n" +
                    "        {\n" +
                    "            time: '08:09',\n" +
                    "            category: 'chemicalAcc',\n" +
                    "            value: 8.5\n" +
                    "        },\n" +
                    "        {\n" +
                    "            time: '08:09',\n" +
                    "            category: 'chemicalAcc',\n" +
                    "            value: 5.5\n" +
                    "        },\n" +
                    "        {\n" +
                    "            time: '08:09',\n" +
                    "            category: 'chemicalAcc',\n" +
                    "            value: 28.9\n" +
                    "        },\n" +
                    "        {\n" +
                    "            time: '08:09',\n" +
                    "            category: 'chemicalAcc',\n" +
                    "            value: 11.9\n" +
                    "        },\n" +
                    "        {\n" +
                    "            time: '08:09',\n" +
                    "            category: 'chemicalAcc',\n" +
                    "            value: 11.9\n" +
                    "        },\n" +
                    "        {\n" +
                    "            time: '08:09',\n" +
                    "            category: 'chemicalAcc',\n" +
                    "            value: 31.9\n" +
                    "        },\n" +
                    "        {\n" +
                    "            time: '08:09',\n" +
                    "            category: 'chemicalAcc',\n" +
                    "            value: 14.9\n" +
                    "        },\n" +
                    "        {\n" +
                    "            time: '08:09',\n" +
                    "            category: 'chemicalAcc',\n" +
                    "            value: 11.9\n" +
                    "        },\n" +
                    "    ]\n" +
                    "}"
    )
    @GetMapping(value = "/api/production/grade-control-analysis/chart", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> apiProductionGradeControlAnalysisChart(@RequestParam("date") String date, @RequestParam("chemical") String chemical) {
        // 요청 파라미터 검증
        if (date == null || (date !=null && date.equals("") || !CommonUtil.isValidDate(date, 10))) {
            return new ResponseEntity<>(CommonUtil.setError("요청 파라미터의 데이터가 날짜형식이 아닙니다."), HttpStatus.OK);
        }
        if (chemical == null || (chemical !=null && chemical.equals(""))) {
            return new ResponseEntity<>(CommonUtil.setError("요청 파라미터의 데이터가 날짜형식이 아닙니다."), HttpStatus.OK);
        }

        return new ResponseEntity<>(dmGradeControlLogService.findProductionGradeControlAnalysisChart(date, chemical), HttpStatus.OK);
    }

}
