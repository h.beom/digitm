/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.api.service;

import kr.insighton.digitalmining.api.repository.DmProductionPlanMasterRepository;
import kr.insighton.digitalmining.api.vo.DmProductionPlanMasterVo;
import kr.insighton.digitalmining.common.util.CommonUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;

/*
 *
 * title : DmProductionPlanMaster Service
 * author : 이형범
 * date : 2020.08.19
 *
 * */

@Service
@Transactional
public class DmProductionPlanMasterService {

    @Resource
    private DmProductionPlanMasterRepository dmProductionPlanMasterRepository;

    public DmProductionPlanMasterVo save(String versionName) {
        DmProductionPlanMasterVo dmProductionPlanMasterVo = new DmProductionPlanMasterVo();
        dmProductionPlanMasterVo.setPlanName(versionName);
        return dmProductionPlanMasterRepository.save(dmProductionPlanMasterVo);
    }

    public Map<String, Object> findVersionList() {
        Map<String, Object> data = new HashMap<>();
        List<Map<String, Object>> versionList = dmProductionPlanMasterRepository.findVersionListWithNativeQuery();
        if (versionList != null && versionList.size() > 0) {
            List<LinkedHashMap<String, Object>> versions = new ArrayList<>();
            for (int i = 0, len = versionList.size(); i < len; i++) {
                LinkedHashMap<String, Object> version = new LinkedHashMap<>();
                version.put("id", versionList.get(i).get("id"));
                version.put("name", versionList.get(i).get("name"));
                versions.add(version);
            }
            data.put("versions", versions);
        } else {
            data.put("versions", new ArrayList<>());
        }
        return CommonUtil.setResult(data);
    }
}
