package kr.insighton.digitalmining.api.controller.environment;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import kr.insighton.digitalmining.api.service.DmDpmRealtimeService;
import kr.insighton.digitalmining.common.util.CommonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@Api(tags = "채광환경 제어 - DPM 실시간 모니터링")
@RestController
public class DpmRealtimeController {

    @Autowired
    private DmDpmRealtimeService dmDpmRealtimeService;

    @ApiOperation(value = "DPM - 실시간 로그")
    @GetMapping(value = "/api/environment/dpm-realtime")
    public ResponseEntity<?> apiEnvironmentDpmRealtime(@RequestParam("sensorId") Integer sensorId) {
        // 요청 파라미터 검증
        if (!CommonUtil.isValidNumber(sensorId)) {
            return new ResponseEntity<>(CommonUtil.setError("요청 파라미터 'sensorId' 숫자형식이 아닙니다."), HttpStatus.OK);
        }

        return new ResponseEntity<>(dmDpmRealtimeService.getBySensorId(sensorId), HttpStatus.OK);
    }
}
