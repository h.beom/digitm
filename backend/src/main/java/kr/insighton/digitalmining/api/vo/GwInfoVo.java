/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.api.vo;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/*
 *
 * title : GwInfo Vo
 * author : 윤영수
 * date : 2020.08.05
 *
 * */

@Getter
@Setter
@Entity
@Table(name = "t_gw_info")
public class GwInfoVo implements Serializable {

    private static final long serialVersionUID = -4265641245155948539L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Integer idx;

    @Column(name = "gw_name", length = 45)
    private String gwName;

    @Column(name = "gw_ip", length = 45)
    private String gwIp;

    @Column(name = "gw_point", length = 45)
    private String gwPoint;

    @Column(name = "gw_type")
    private Integer gwType;

    @Column(name = "gw_connection")
    private Integer gwConnection;

    @Column(name = "gw_user_idx")
    private Integer gwUserIdx;

    @Column(name = "reg_date")
    private Timestamp regDate;

}
