/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.api.controller.common;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import kr.insighton.digitalmining.api.service.UserDetailsServiceImpl;
import kr.insighton.digitalmining.api.vo.UserDetailsVo;
import kr.insighton.digitalmining.api.vo.UserVo;
import kr.insighton.digitalmining.common.util.TokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/*
 *
 * title : User Controller
 * author : 윤영수
 * date : 2020.09.04
 *
 * */

@Api(tags = "사용자")
@RestController
public class UserController {

    @Value("${jwt.token.header}")
    private String tokenHeader;

    @Autowired
    private TokenUtil tokenUtil;

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @ApiOperation(value = "사용자 API"
            , notes = "{\n" +
            "    Request: {\n" +
            "        header: {\n" +
            "            authorization: 'Bearer ${JWT TOKEN}'\n" +
            "        }," +
            "        body: {}\n" +
            "    }\n" +
            "    Response: {\n" +
            "        header: {\n" +
            "            Authorization: 'Bearer ${JWT TOKEN}'\n" +
            "        }," +
            "        body: {\n" +
            "            id : 1,\n" +
            "            username : 'user',\n" +
            "            password : null,\n" +
            "            name : '사용자',\n" +
            "            team : '디지털마이닝',\n" +
            "            role: 'USER'\n" +
            "        }\n" +
            "    }\n" +
            "}\n"
    )
    @ApiImplicitParam(paramType = "header", name = "authorization", value = "jwt token", required = true, example = "Bearer {jwt token}")
    @GetMapping(value = "/api/user", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> apiUser(HttpServletRequest request, HttpServletResponse response) {
        if (request.getHeader("authorization") != null) {
            String token = tokenUtil.getTokenFromHeader(request.getHeader("authorization"));
            String id = tokenUtil.getUserIdFromToken(token);
            UserDetailsVo userDetailsVo = userDetailsService.loadUserByUsername(id);

            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            UserVo userVO = userDetailsVo.getUserVo();
            Map<String, Object> user = new HashMap<>();
            user.put("id", userVO.getIdx());
            user.put("username", userVO.getId());
            user.put("password", null);
            user.put("name", userVO.getName());
            user.put("team", userVO.getTeam());
            user.put("role", userVO.getRole());
            user.put("token", token);

            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.set(tokenHeader, "Bearer " + token);
            return new ResponseEntity<>(user, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
