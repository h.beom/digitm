/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.api.controller.production;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import kr.insighton.digitalmining.api.service.DmProductionPlanMasterService;
import kr.insighton.digitalmining.api.service.DmProductionPlanService;
import kr.insighton.digitalmining.api.service.DmWeighbridgeLogService;
import kr.insighton.digitalmining.common.util.CommonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/*
 *
 * title : WeighBridge Controller
 * author : 윤영수
 * date : 2020.09.18
 *
 * */

@Api(tags = "광산운영 & 생산 - Weighbridge")
@RestController
public class WeighbridgeController {

    @Autowired
    private DmProductionPlanMasterService dmProductionPlanMasterService;

    @Autowired
    private DmProductionPlanService dmProductionPlanService;

    @Autowired
    private DmWeighbridgeLogService dmWeighbridgeLogService;


    @ApiOperation(value = "생산 계획 관리 - 차수순번"
            , notes =
            "Request: {}\n" +
                    "Response: {\n" +
                    "    data: {\n" +
                    "        versions: [\n" +
                    "            {id: 2, name: 'Ver. 2020-04'},\n" +
                    "            {id: 2, name: 'Ver. 2020-04'}\n" +
                    "        ]\n" +
                    "    }\n" +
                    "}"
    )
    @GetMapping(value = "/api/production/planning/version", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> apiProductionPlanningVersion() {
        return new ResponseEntity<>(dmProductionPlanMasterService.findVersionList(), HttpStatus.OK);
    }

    @ApiOperation(value = "생산 계획 관리 - 생산량"
            , notes =
            "Request: {\n" +
                    "    id: 2,\n" +
                    "    date: '2020-06'\n" +
                    "}\n" +
                    "Response: {\n" +
                    "    data: {\n" +
                    "        version: { id: 2, name: 'Ver. 2020-04'},\n" +
                    "        date: '2020-06',\n" +
                    "        list: [\n" +
                    "            {\n" +
                    "                date: '2020-06-01',\n" +
                    "                amount: 4000,\n" +
                    "                acc: 4000,\n" +
                    "            },\n" +
                    "            {\n" +
                    "                date: '2020-06-02',\n" +
                    "                amount: 4000,\n" +
                    "                acc: 8000,\n" +
                    "            },\n" +
                    "            {\n" +
                    "                date: '2020-06-03',\n" +
                    "                amount: 4000,\n" +
                    "                acc: 1200,\n" +
                    "            },\n" +
                    "            {\n" +
                    "                date: '2020-06-04',\n" +
                    "                amount: 4000,\n" +
                    "                acc: 16000,\n" +
                    "            },\n" +
                    "            {\n" +
                    "                date: '2020-06-05',\n" +
                    "                amount: 4000,\n" +
                    "                acc: 20000,\n" +
                    "            }\n" +
                    "        ],\n" +
                    "        total: 120000,\n" +
                    "        average: 4000,\n" +
                    "    }\n" +
                    "}"
    )
    @GetMapping(value = "/api/production/planning", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> apiProductionPlanning(@RequestParam("id") Integer id, @RequestParam("date") String date) {
        // 요청 파라미터 검증
        if (date == null || (date !=null && date.equals("") || !CommonUtil.isValidDate(date, 7))) {
            return new ResponseEntity<>(CommonUtil.setError("요청 파라미터의 데이터가 날짜형식이 아닙니다."), HttpStatus.OK);
        }
        if (!CommonUtil.isValidNumber(id)) {
            return new ResponseEntity<>(CommonUtil.setError("요청 파라미터의 데이터가 숫자형식이 아닙니다."), HttpStatus.OK);
        }
        return new ResponseEntity<>(dmProductionPlanService.findByVersionAndDate(id, date), HttpStatus.OK);
    }

    @ApiOperation(value = "생산 계획 관리 - 생산 계획 업로드"
            , notes =
            "Request: {\n" +
                    "    file: FILE,\n" +
                    "    version-name: 'text'\n" +
                    "}\n" +
                    "Response: {\n" +
                    "    data: {\n" +
                    "        file: '생산계획관리(양식)',\n" +
                    "        extension: 'xlsx',\n" +
                    "        count: 999,\n" +
                    "        id: '98faa94f-30b9-44e3-b52e-ec4d263d6a5a',\n" +
                    "        versionName: 'Ver. 2020'\n" +
                    "    }\n" +
                    "}"
    )
    @PostMapping(value = "/api/production/planning/excel", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> apiProductionPlanningExcel(@RequestParam("version-name") String versionName, @RequestParam("file") MultipartFile file) {
        // 요청 파라미터 검증
        if (!CommonUtil.isValidFile(file, "excel")) {
            return new ResponseEntity<>(CommonUtil.setError("요청 파라미터 중 업로드 파일이 엑셀형식의 파일이 아닙니다."), HttpStatus.OK);
        }
        if (versionName == null || (versionName != null && versionName.equals(""))) {
            return new ResponseEntity<>(CommonUtil.setError("요청 파라미터 중 계획차수명에 값이 없습니다."), HttpStatus.OK);
        }

        return new ResponseEntity<>(dmProductionPlanService.save(versionName, file), HttpStatus.OK);
    }

    @ApiOperation(value = "실시간 생산 & 구매 실적 - 조회"
            , notes =
            "Request: {\n" +
                    "}\n" +
                    "Response: {\n" +
                    "    data: {\n" +
                    "        datetime: '2020-07-13 13:35',\n" +
                    "        production: {\n" +
                    "            current: 4500, -- 일생산량\n" +
                    "            month: 70000, -- 월 누적 생산량\n" +
                    "            year: 800000, -- 연 누적 생산량\n" +
                    "            currentOut: 3000,\n" +
                    "            monthOut: 70000,\n" +
                    "            yearOut: 800000\n" +
                    "        },\n" +
                    "        procurement: {\n" +
                    "            current: 200,\n" +
                    "            month: 6000,\n" +
                    "            year: 20000,\n" +
                    "            currentOut: 200,\n" +
                    "            monthOut: 6000,\n" +
                    "            yearOut: 20000\n" +
                    "        },\n" +
                    "        lastNumber: 30\n" +
                    "    }\n" +
                    "}"
    )
    @GetMapping(value = "/api/production/weighbridge-realtime", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> apiProductionWeighbridgeRealtime() {
        return new ResponseEntity<>(dmWeighbridgeLogService.findProductionWeighbridgeRealtime(), HttpStatus.OK);
    }

    @ApiOperation(value = "실시간 생산 & 구매 실적 - 상세조회"
            , notes =
            "Request: {\n" +
                    "    number: 5\n" +
                    "}\n" +
                    "Response: {\n" +
                    "    data: {\n" +
                    "        list: [\n" +
                    "            number: 1,\n" +
                    "            time: '09:10',\n" +
                    "            id: '9242911',\n" +
                    "            source: '갱내파쇄',\n" +
                    "            productName: '건동석회석',\n" +
                    "            weight: 100,\n" +
                    "            type: '입고'\n" +
                    "        ]," +
                    "        production: {" +
                    "            input: 40,\n" +
                    "            output: 0,\n" +
                    "            change: 40,\n" +
                    "            preAmount: 102200,\n" +
                    "            postAmount: 102240\n" +
                    "        },\n" +
                    "        procurement: {" +
                    "            input: 40,\n" +
                    "            output: 0,\n" +
                    "            change: 40,\n" +
                    "            preAmount: 102200,\n" +
                    "            postAmount: 102240\n" +
                    "        }\n" +
                    "    }\n" +
                    "}"
    )
    @GetMapping(value = "/api/production/weighbridge-realtime/detail", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> apiProductionWeighbridgeRealtimeDetail(@RequestParam("number") Integer number) {
        // 요청 파라미터 검증
        if (!CommonUtil.isValidNumber(number)) {
            return new ResponseEntity<>(CommonUtil.setError("요청 파라미터의 'number' 데이터가 숫자형식이 아닙니다."), HttpStatus.OK);
        }

        return new ResponseEntity<>(dmWeighbridgeLogService.findProductionWeighbridgeRealtimeDetail(number), HttpStatus.OK);
    }

    @ApiOperation(value = "일자별 생산 & 구매 실적 - 조회"
            , notes =
            "Request: {\n" +
                    "    date: '2020-07-13'\n" +
                    "}\n" +
                    "Response: {\n" +
                    "    data: {\n" +
                    "        accCount: 30,\n" +
                    "        accAmount: 4000,\n" +
                    "        list: [\n" +
                    "            {\n" +
                    "                number: 1,\n" +
                    "                time: '09:10',\n" +
                    "                source: '갱내파쇄',\n" +
                    "                productName: '건동석회석',\n" +
                    "                weight: 100,\n" +
                    "                type: '입고'\n" +
                    "            },\n" +
                    "            {\n" +
                    "                number: 2,\n" +
                    "                time: '09:50',\n" +
                    "                source: '고품위석회석',\n" +
                    "                productName: '건동석회석',\n" +
                    "                weight: 30,\n" +
                    "                type: '입고'\n" +
                    "            },\n" +
                    "            {\n" +
                    "                number: 3,\n" +
                    "                time: '10:50',\n" +
                    "                source: '원석야적',\n" +
                    "                productName: '건동석회석',\n" +
                    "                weight: 100,\n" +
                    "                type: '출고'\n" +
                    "            },\n" +
                    "            {\n" +
                    "                number: 4,\n" +
                    "                time: '13:30',\n" +
                    "                source: '갱내파쇄',\n" +
                    "                productName: '원석야적',\n" +
                    "                weight: 40,\n" +
                    "                type: '입고'\n" +
                    "            }\n" +
                    "        ]\n" +
                    "    }\n" +
                    "}\n"
    )
    @GetMapping(value = "/api/production/weighbridge-analysis", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> apiProductionWeighbridgeAnalysis(@RequestParam("date") String date) {
        // 요청 파라미터 검증
        if (date == null || (date !=null && date.equals("") || !CommonUtil.isValidDate(date, 10))) {
            return new ResponseEntity<>(CommonUtil.setError("요청 파라미터의 데이터가 날짜형식이 아닙니다."), HttpStatus.OK);
        }

        return new ResponseEntity<>(dmWeighbridgeLogService.findProductionWeighbridgeAnalysis(date), HttpStatus.OK);
    }

    @ApiOperation(value = "일자별 생산 & 구매 실적 - 차트"
            , notes =
            "Request: {\n" +
                    "    date: '2020-07-13'\n" +
                    "}\n" +
                    "Response: {\n" +
                    "    data: [\n" +
                    "        {name: 'prodMove', date: '7/7', value: 4.3},\n" +
                    "        {name: 'prodAcc', date: '7/7', value: 2.4},\n" +
                    "        {name: 'purMove', date: '7/7', value: 2},\n" +
                    "        {name: 'purAcc', date: '7/7', value: 1},\n" +
                    "        {name: 'prodMove', date: '7/8', value: 2.5},\n" +
                    "        {name: 'prodAcc', date: '7/8', value: 4.4},\n" +
                    "        {name: 'purMove', date: '7/8', value: 2},\n" +
                    "        {name: 'purAcc', date: '7/8', value: 2},\n" +
                    "        {name: 'prodMove', date: '7/9', value: 3.5},\n" +
                    "        {name: 'prodAcc', date: '7/9', value: 1.8},\n" +
                    "        {name: 'purMove', date: '7/9', value: 3},\n" +
                    "        {name: 'purAcc', date: '7/9', value: 3},\n" +
                    "        {name: 'prodMove', date: '7/10', value: 4.5},\n" +
                    "        {name: 'prodAcc', date: '7/10', value: 2.8},\n" +
                    "        {name: 'purMove', date: '7/10', value: 5},\n" +
                    "        {name: 'purAcc', date: '7/10', value: 4},\n" +
                    "        {name: 'prodMove', date: '7/11', value: 2.5},\n" +
                    "        {name: 'prodAcc', date: '7/11', value: 2.8},\n" +
                    "        {name: 'purMove', date: '7/11', value: 5},\n" +
                    "        {name: 'purAcc', date: '7/11', value: 4},\n" +
                    "        {name: 'prodMove', date: '7/12', value: 1.5},\n" +
                    "        {name: 'prodAcc', date: '7/12', value: 2.8},\n" +
                    "        {name: 'purMove', date: '7/12', value: 5},\n" +
                    "        {name: 'purAcc', date: '7/12', value: 4},\n" +
                    "        {name: 'prodMove', date: '7/13', value: 4.5},\n" +
                    "        {name: 'prodAcc', date: '7/13', value: 2.8},\n" +
                    "        {name: 'purMove', date: '7/13', value: 5},\n" +
                    "        {name: 'purAcc', date: '7/13', value: 4}\n" +
                    "    ]\n" +
                    "}"
    )
    @GetMapping(value = "/api/production/weighbridge-analysis/chart", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> apiProductionWeighbridgeAnalysisChart(@RequestParam("date") String date) {
        // 요청 파라미터 검증
        if (date == null || (date !=null && date.equals("") || !CommonUtil.isValidDate(date, 10))) {
            return new ResponseEntity<>(CommonUtil.setError("요청 파라미터의 데이터가 날짜형식이 아닙니다."), HttpStatus.OK);
        }

        return new ResponseEntity<>(dmWeighbridgeLogService.findProductionWeighbridgeAnalysisChart(date), HttpStatus.OK);
    }

}
