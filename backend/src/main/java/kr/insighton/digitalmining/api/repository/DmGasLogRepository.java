/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.api.repository;

import kr.insighton.digitalmining.api.vo.DmGasLogVo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/*
 *
 * title : DmGasLog Repository
 * author : 이형범
 * date : 2020.09.11
 *
 * */

@Repository
public interface DmGasLogRepository extends JpaRepository<DmGasLogVo, Integer> {

    @Query(value = "" +
            " select t.idx as id" +
            " , t.sensor_idx as sensorId" +
            " , cast(date_format(t.date, '%Y-%m-%d') as char(10)) as 'date'" +
            " , t.time as 'time'" +
            " , truncate(t.temp, 1) as temp" +
            " , truncate(t.humidity, 1) as humidity" +
            " , truncate(t.co2, 1) as co2" +
            " , truncate(t.co, 1) as co" +
            " , truncate(t.no, 1) as 'no'" +
            " , truncate(t.no2, 2) as no2" +
            " , truncate(t.so2, 2) as so2" +
            " from t_dm_gas_log t" +
            " where t.sensor_idx = :sensorId" +
            " and date_format(concat(date, ' ', time), '%Y-%m-%d %H:%i:%s') < now()" +
            " and date_format(concat(date, ' ', time), '%Y-%m-%d %H:%i:%s') > date_add(date_format(now(), '%Y-%m-%d %H:%i:%s'), interval -2 hour)"
            , nativeQuery = true)
    List<Map<String, Object>> findAllByNowAndSensorIdWithNativeQuery(@Param("sensorId") Integer sensorId);

    @Query(value = "" +
            " select t.idx as id" +
            " , t.sensor_idx as sensorId" +
            " , cast(date_format(t.date, '%Y-%m-%d') as char(10)) as 'date'" +
            " , t.time as 'time'" +
            " , truncate(t.temp, 1) as temp" +
            " , truncate(t.humidity, 1) as humidity" +
            " , truncate(t.co2, 1) as co2" +
            " , truncate(t.co, 1) as co" +
            " , truncate(t.no, 1) as 'no'" +
            " , truncate(t.no2, 2) as no2" +
            " , truncate(t.so2, 2) as so2" +
            " from t_dm_gas_log t" +
            " where t.date = date_format(:date, '%Y-%m-%d')" +
            " and t.sensor_idx = :sensorId"
            , nativeQuery = true)
    List<Map<String, Object>> findAllByDateAndSensorIdWithNativeQuery(@Param("sensorId") Integer sensorId, @Param("date") String date);
}
