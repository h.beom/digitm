/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.api.controller.admin;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import kr.insighton.digitalmining.api.service.CodeManageService;
import kr.insighton.digitalmining.common.util.CommonUtil;


/*
 *
 * title : 공통코드 관리 Controller
 * author : 이동현
 * date : 2020.09.09
 *
 * */


@Api(tags = "마스터정보관리 - Admin")
@RestController
@RequestMapping(value = "/api/admin")
public class CodeManageController {
    
    @Autowired
    private CodeManageService codeManageService;

    @ApiOperation(value = "공통코드관리 - 코드목록"
            , notes =
                "Request: {\n" +
                "    id:12\n" +
                "}\n" +
                "Response: {\n" +
                "    data: {\n" +
                "        list: [\n" +
                "            {idx: 1, code: 1,  name: 권한,   grp_code: null, sort: null, reg_date: 2020-08-12 17:29:15.0, item_1: null, item_2: null, item_3: null, item_4: null, item_5: null},\n" +
                "            {idx: 2, code: 2,  name: 구분,   grp_code: null, sort: null, reg_date: 2020-08-12 17:29:15.0, item_1: null, item_2: null, item_3: null, item_4: null, item_5: null},\n" +
                "            {idx: 6, code: 5,  name: 사용자, grp_code: 1,    sort: 1,    reg_date: 2020-08-12 17:29:15.0, item_1: null, item_2: null, item_3: null, item_4: null, item_5: null}\n" +
                "        ]\n" +
                "    }\n" +
                "}"
    )
    @GetMapping(value = "/code", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> apiAdminCodeManage(@RequestParam("codeGroup") String codeGroup, @RequestParam("codeName") String codeName) {
        return new ResponseEntity<>(codeManageService.getCodeList(codeGroup, codeName), HttpStatus.OK);
    }
}
