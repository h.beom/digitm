/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.api.service;

import kr.insighton.digitalmining.api.repository.DmGasLogRepository;
import kr.insighton.digitalmining.common.util.CommonUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 *
 * title : DmGasLog Service
 * author : 윤영수
 * date : 2020.09.11
 *
 * */

@Service
@Transactional
public class DmGasLogService {

    @Resource
    private DmGasLogRepository dmGasLogRepository;

    /*
    * 지반변위 실시간 로그
    * */
    public Map<String, Object> findAllByNowAndSensorId(Integer sensorId) {
        Map<String, Object> data = new HashMap<>();
        List<Map<String, Object>> logs = dmGasLogRepository.findAllByNowAndSensorIdWithNativeQuery(sensorId);
        if (logs != null && logs.size() > 0) {
            data.put("logs", logs);
        } else {
            data.put("logs", new ArrayList<>());
        }

        return CommonUtil.setResult(data);
    }

    /*
    * 지반변위 실시간 로그 차트
    * */
    public Map<String, Object> findAllChartByNowAndSensorId(Integer sensorId) {
        Map<String, Object> data = new HashMap<>();
        List<Map<String, Object>> logs = dmGasLogRepository.findAllByNowAndSensorIdWithNativeQuery(sensorId);
        if (logs != null && logs.size() > 0) {
            data.put("charts", convertChart(logs));
        } else {
            data.put("charts", new ArrayList<>());
        }

        return CommonUtil.setResult(data);
    }

    /*
    * 지반변위 일자 범위 로그
    * */
    public Map<String, Object> findAllByDateAndSensorId(Integer sensorId, String date) {
        Map<String, Object> data = new HashMap<>();
        List<Map<String, Object>> logs = dmGasLogRepository.findAllByDateAndSensorIdWithNativeQuery(sensorId, date);
        if (logs != null && logs.size() > 0) {
            data.put("logs", logs);
        } else {
            data.put("logs", new ArrayList<>());
        }

        return CommonUtil.setResult(data);
    }

    /*
    * 지반변위 일자 범위 로그 차트
    * */
    public Map<String, Object> findAllChartByDateAndSensorId(Integer sensorId, String date) {
        Map<String, Object> data = new HashMap<>();
        List<Map<String, Object>> logs = dmGasLogRepository.findAllByDateAndSensorIdWithNativeQuery(sensorId, date);
        if (logs != null && logs.size() > 0) {
            data.put("charts", convertChart(logs));
        } else {
            data.put("charts", new ArrayList<>());
        }

        return CommonUtil.setResult(data);
    }

    private List<Map<String, Object>> convertChart(List<Map<String, Object>> logs) {
        List<Map<String, Object>> charts = new ArrayList<>();
        if (logs != null && logs.size() > 0) {
            for (int i = 0; i < logs.size(); i++) {
                Map<String, Object> co = new HashMap<>();
                co.put("date", logs.get(i).get("time").toString().substring(0, 5));
                co.put("type", "Co");
                co.put("value", logs.get(i).get("co"));
                charts.add(co);

                Map<String, Object> no = new HashMap<>();
                no.put("date", logs.get(i).get("time").toString().substring(0, 5));
                no.put("type", "No");
                no.put("value", logs.get(i).get("no"));
                charts.add(no);

                Map<String, Object> no2 = new HashMap<>();
                no2.put("date", logs.get(i).get("time").toString().substring(0, 5));
                no2.put("type", "No2");
                no2.put("value", logs.get(i).get("no2"));
                charts.add(no2);

                Map<String, Object> so2 = new HashMap<>();
                so2.put("date", logs.get(i).get("time").toString().substring(0, 5));
                so2.put("type", "So2");
                so2.put("value", logs.get(i).get("so2"));
                charts.add(so2);
            }
        }

        return charts;
    }

}
