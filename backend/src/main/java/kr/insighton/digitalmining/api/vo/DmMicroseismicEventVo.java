/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.api.vo;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/*
 *
 * title : DmMicroseismicEvent Vo
 * author : 윤영수
 * date : 2020.09.08
 *
 * */

@Getter
@Setter
@Entity
@Table(name = "t_dm_microseismic_event")
public class DmMicroseismicEventVo implements Serializable {

    private static final long serialVersionUID = 2561573226596246770L;

    @Id
    @Column(name = "idx")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "number")
    private Integer number;

    @Column(name = "datetime")
    private Timestamp datetime;

    @Column(name = "category")
    private String category;

    @Column(name = "type")
    private String type;

    @Column(name = "radius")
    private Integer radius;

    @Column(name = "x")
    private Integer x;

    @Column(name = "y")
    private Integer y;

    @Column(name = "z")
    private Integer z;

}
