/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.api.vo;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;

/*
 *
 * title : DmGradeControlLog Vo
 * author : 윤영수
 * date : 2020.08.05
 *
 * */

@Getter
@Setter
@Entity
@Table(name = "t_dm_grade_control_log")
public class DmGradeControlLogVo implements Serializable {

    private static final long serialVersionUID = -8141288388783617485L;

    @Id
    @Column(name = "idx")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idx;

    @Column(name = "date")
    private Date date;

    @Column(name = "time")
    private Time time;

    @Column(name = "input_outer")
    private Integer inputOuter;

    @Column(name = "input_inner")
    private Integer inputInner;

    @Column(name = "note")
    private String note;

    @Column(name = "input")
    private Integer input;

    @Column(name = "input_cumulative")
    private Integer inputCumulative;

    @Column(name = "number")
    private Integer number;

    @Column(name = "sio2")
    private Double sio2;

    @Column(name = "al2o3")
    private Double al2o3;

    @Column(name = "fe2o3")
    private Double fe2o3;

    @Column(name = "cao")
    private Double cao;

    @Column(name = "mgo")
    private Double mgo;

    @Column(name = "so3")
    private Double so3;

    @Column(name = "k2o")
    private Double k2o;

    @Column(name = "na2o")
    private Double na2o;

    @Column(name = "sum")
    private Double sum;

    @Column(name = "sio2_cumulative")
    private Double sio2Cumulative;

    @Column(name = "al2o3_cumulative")
    private Double al2o3Cumulative;

    @Column(name = "fe2o3_cumulative")
    private Double fe2o3Cumulative;

    @Column(name = "cao_cumulative")
    private Double caoCumulative;

    @Column(name = "mgo_cumulative")
    private Double mgoCumulative;

    @Column(name = "so3_cumulative")
    private Double so3Cumulative;

    @Column(name = "k2o_cumulative")
    private Double k2oCumulative;

    @Column(name = "na2o_cumulative")
    private Double na2oCumulative;

    @Column(name = "sum_cumulative")
    private Double sumCumulative;

}
