/**
 * DIGITAL MINING
 */
package kr.insighton.digitalmining.api.service;

import kr.insighton.digitalmining.api.repository.DmProductionPlanMasterRepository;
import kr.insighton.digitalmining.api.repository.DmProductionPlanRepository;
import kr.insighton.digitalmining.api.vo.DmProductionPlanMasterVo;
import kr.insighton.digitalmining.api.vo.DmProductionPlanVo;
import kr.insighton.digitalmining.common.service.FileService;
import kr.insighton.digitalmining.common.util.CommonUtil;
import kr.insighton.digitalmining.common.vo.FileVo;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 *
 * title : DmProductionPlan Service
 * author : 윤영수
 * date : 2020.08.19
 *
 * */

@Service
@Transactional
public class DmProductionPlanService {

    @Resource
    private FileService fileService;

    @Resource
    private DmProductionPlanMasterRepository dmProductionPlanMasterRepository;

    @Resource
    private DmProductionPlanRepository dmProductionPlanRepository;

    public Map<String, Object> findByVersionAndDate(Integer id, String date) {
        Map<String, Object> data = new HashMap<>();
        Map<String, Object> version = new HashMap<>();

        DmProductionPlanMasterVo dmProductionPlanMasterVo = dmProductionPlanMasterRepository.findByPlanId(id);

        // 요청한 차수가 존재하지 않는 경우
        if (dmProductionPlanMasterVo == null) {
            return CommonUtil.setError("계획차수 조회 중 오류가 발생했습니다.");
        } else {
            version.put("id", dmProductionPlanMasterVo.getPlanId());
            version.put("name", dmProductionPlanMasterVo.getPlanName());
            data.put("date", date);
        }

        data.put("version", version);

        // 마지막 차수에 해당하는 월별 생산계획 리스트
        List<Map<String, Object>> list = dmProductionPlanRepository.findByVersionAndDateWithNativeQuery(id, date);
        // 일자별로 1일부터 누적생산량 계산을 위해 쿼리 대신 반복문으로 처리
        List<Map<String, Object>> cloneList = new ArrayList<>();
        if (list != null && list.size() > 0) {
            for (int i = 0, len = list.size(); i < len; i++) {
                Map<String, Object> item = new HashMap<>();
                item.put("date", list.get(i).get("date"));
                item.put("amount", list.get(i).get("amount"));

                String startDate = list.get(i).get("date").toString().substring(0, 7) + "-01";
                String endDate = list.get(i).get("date").toString();
                Map<String, Object> acc = dmProductionPlanRepository.findGroupByDateWithNativeQuery(id, startDate, endDate);
                item.put("acc", acc.get("total"));

                cloneList.add(item);
            }
            data.put("list", cloneList);
        } else {
            data.put("list", new ArrayList<>());
        }
        // 요청한 월에 일평균, 총 계획 생산량
        Map<String, Object> monthAllAmount = dmProductionPlanRepository.findGroupByVersionAndDateWithNativeQuery(id, date);
        if (CommonUtil.isNullMap(monthAllAmount, "average")) {
            data.put("average", 0);
        } else {
            data.put("average", monthAllAmount.get("average"));
        }
        if (CommonUtil.isNullMap(monthAllAmount, "total")) {
            data.put("total", 0);
        } else {
            data.put("total", monthAllAmount.get("total"));
        }

        return CommonUtil.setResult(data);
    }

    public Map<String, Object> save(String versionName, MultipartFile file) {
        Map<String, Object> data = new HashMap<>();
        Workbook workbook = null;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

        // 엑셀 파일 검증
        try {
            String extension = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".") + 1);
            if (extension.equals("xlsx")) {
                workbook = new XSSFWorkbook(file.getInputStream());
            } else if (extension.equals("xls")) {
                workbook = new HSSFWorkbook(file.getInputStream());
            }

            if (workbook == null) {
                return CommonUtil.setError("엑셀 데이터 처리중 오류가 발생했습니다.");
            }
        } catch (Exception ex) {
            return CommonUtil.setError("엑셀 데이터 처리중 오류가 발생했습니다.");
        }

        // 엑셀 데이터 검증
        Sheet sheet = workbook.getSheetAt(0);
        for (int i = 1; i < sheet.getPhysicalNumberOfRows(); i++) {
            Row row = sheet.getRow(i);
            String planDate = simpleDateFormat.format(row.getCell(0).getDateCellValue());
            if (!CommonUtil.isValidDate(planDate, 10)) {
                return CommonUtil.setError("엑셀 데이터 저장중 " + (i + 1) + "행의 계획일자가 날짜형식이 아닙니다.");
            }
            String planAmount = row.getCell(1).getNumericCellValue() + "";
            if (!planAmount.replace(".", "").matches("[0-9]+")) {
                return CommonUtil.setError("엑셀 데이터 저장중 " + (i + 1) + "행의 계획생산량이 숫자형식이 아닙니다.");
            }
        }

        try {
            FileVo fileVo = fileService.saveFile(file, "생산계획");
            // 마스터생성
            DmProductionPlanMasterVo dmProductionPlanMasterVo = new DmProductionPlanMasterVo();
            dmProductionPlanMasterVo.setPlanName(versionName);
            dmProductionPlanMasterRepository.save(dmProductionPlanMasterVo);

            for (int i = 1; i < sheet.getPhysicalNumberOfRows(); i++) {
                Row row = sheet.getRow(i);
                DmProductionPlanVo dmProductionPlanVo = new DmProductionPlanVo();
                dmProductionPlanVo.setPlanId(dmProductionPlanMasterVo.getPlanId());
                dmProductionPlanVo.setPlanDate(Date.valueOf(simpleDateFormat.format(row.getCell(0).getDateCellValue())));
                dmProductionPlanVo.setPlanAmount(Double.valueOf(row.getCell(1).getNumericCellValue()).intValue());

                dmProductionPlanRepository.save(dmProductionPlanVo);
            }

            data.put("id", fileVo.getName());
            data.put("file", fileVo.getOriginName());
            data.put("extention", fileVo.getExtension());
            data.put("versionName", versionName);
            data.put("count", sheet.getPhysicalNumberOfRows() - 1);

            return CommonUtil.setResult(data);
        } catch (Exception ex) {
            throw new RuntimeException();
        }
    }
}
