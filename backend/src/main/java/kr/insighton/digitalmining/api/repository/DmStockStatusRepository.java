/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.api.repository;

import kr.insighton.digitalmining.api.vo.DmStockStatusVo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Map;

/*
 *
 * title : DmStockStatus Repository
 * author : 윤영수
 * date : 2020.08.31
 *
 * */

@Repository
public interface DmStockStatusRepository extends JpaRepository<DmStockStatusVo, Integer> {

    /*
     * 생산모니터링 재고데이터
     * */
    @Query(value = "" +
            " select" +
            "   round(t.production_input / 1000) as input" +
            " , round(t.production_output / 1000) as output" +
            " , round(t.production_change / 1000) as 'change'" +
            " , round(t.production_pre_amount / 1000) as preAmount" +
            " , round(t.production_post_amount / 1000) as postAmount" +
            " from t_dm_stock_status t" +
            " where t.weighbridge_log_idx = :number"
            , nativeQuery = true)
    Map<String, Object> findByWeighbridgeLogIdxProductionWithNativeQuery(@Param("number") Integer number);

    /*
     * 구매모니터링 재고데이터
     * */
    @Query(value = "" +
            " select" +
            "   round(t.procurement_input / 1000) as input" +
            " , round(t.procurement_output / 1000) as output" +
            " , round(t.procurement_change / 1000) as 'change'" +
            " , round(t.procurement_pre_amount / 1000) as preAmount" +
            " , round(t.procurement_post_amount / 1000) as postAmount" +
            " from t_dm_stock_status t" +
            " where t.weighbridge_log_idx = :number"
            , nativeQuery = true)
    Map<String, Object> findByWeighbridgeLogIdxProcurementWithNativeQuery(@Param("number") Integer number);
}
