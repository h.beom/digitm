/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.api.vo;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/*
 *
 * title : User Vo
 * author : 윤영수
 * date : 2020.08.05
 *
 * */

@Getter
@Setter
@Entity
@Table(name = "t_dm_user")
public class UserVo implements Serializable {

    private static final long serialVersionUID = -8612667073743788917L;

    @Id
    @Column(name = "idx")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idx;

    @Column(name = "u_id", length = 45)
    private String id;

    @Column(name = "u_password", length = 45)
    private String password;

    @Column(name = "u_name", length = 45)
    private String name;

    @Column(name = "u_tel", length = 45)
    private String tel;

    @Column(name = "u_info", length = 45)
    private String info;

    @Column(name = "u_authority")
    private Integer authority;

    @Column(name = "u_ip", length = 45)
    private String ip;

    @Column(name = "reg_date")
    private Timestamp regDate;

    @Column(name = "u_email", length = 45)
    private String email;

    @Transient
    private String team = "디지털마이닝"; // 팀생성전 임시 사용

    @Transient
    private String role;

    public void setRole() {
        if(this.authority != null && this.authority == 0) {
            this.role = "ADMIN";
        } else {
            this.role = "USER";
        }
    }

    public String getRole() {
        if(this.authority != null && this.authority == 0) {
            return "ADMIN";
        } else {
            return "USER";
        }
    }
}
