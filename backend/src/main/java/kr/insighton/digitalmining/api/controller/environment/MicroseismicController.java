/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.api.controller.environment;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import kr.insighton.digitalmining.api.service.DmMicroseismicEventService;
import kr.insighton.digitalmining.common.util.CommonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/*
 *
 * title : Microseismic Controller
 * author : 윤영수
 * date : 2020.09.18
 *
 * */

@Api(tags = "채광환경 제어 - 미소진동")
@RestController
public class MicroseismicController {

    @Autowired
    private DmMicroseismicEventService dmMicroseismicEventService;

    @ApiOperation(value = "미소진동 - 일자 마지막 이벤트"
            , notes = "" +
            "Request: {\n" +
            "    date: '2020-09-04'\n" +
            "}\n" +
            "Response: {\n" +
            "    data: {" +
            "        id: 3,\n" +
            "        number: 1,\n" +
            "        datetime: '2020-09-04 07:30:00',\n" +
            "        category: 1,\n" +
            "        categoryName: '천공',\n" +
            "        type: 1,\n" +
            "        typeName: '점',\n" +
            "        radius: 0,\n" +
            "        x: 178893,\n" +
            "        y: 197596,\n" +
            "        z: 95,\n" +
            "        sensors: [\n" +
            "            {\n" +
            "                id: 3,\n" +
            "                eventId: 233,\n" +
            "                sensorId: 1,\n" +
            "                sensorName: 'Microseismic  G-7-1',\n" +
            "                imgLink: 'https://example.com/abc1.png'\n" +
            "            },\n" +
            "            {\n" +
            "                id: 3,\n" +
            "                eventId: 233,\n" +
            "                sensorId: 2,\n" +
            "                sensorName: 'Microseismic  G-7-2',\n" +
            "                imgLink: 'https://example.com/abc2.png'\n" +
            "            },\n" +
            "            {\n" +
            "                id: 3,\n" +
            "                eventId: 233,\n" +
            "                sensorId: 3,\n" +
            "                sensorName: 'Microseismic  G-7-3',\n" +
            "                imgLink: 'https://example.com/abc3.png'\n" +
            "            }\n" +
            "        ]\n" +
            "    }\n" +
            "}"
    )
    @GetMapping(value = "/api/environment/microseismic/lastest-event", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> apiEnvironmentMicroseismicLastestEvent(@RequestParam("date") String date) {
        // 요청 파라미터 검증
        if (date == null || (date !=null && date.equals("") || !CommonUtil.isValidDate(date, 10))) {
            return new ResponseEntity<>(CommonUtil.setError("요청 파라미터의 데이터가 날짜형식이 아닙니다."), HttpStatus.OK);
        }
        return new ResponseEntity<>(dmMicroseismicEventService.findAllByDateAndNumberMax(date), HttpStatus.OK);
    }

    @ApiOperation(value = "미소진동 - 일자, 카테고리 조회"
            , notes =
            "Request: {\n" +
                    "    date: '2020-09-04',\n" +
                    "    category: 1" +
                    "}" +
                    "Response: {\n" +
                    "    data: {\n" +
                    "        events: [\n" +
                    "            {\n" +
                    "                id: 3,\n" +
                    "                number: 1,\n" +
                    "                datetime: '2020-09-04 07:30:00',\n" +
                    "                category: 1,\n" +
                    "                categoryName: '천공',\n" +
                    "                type: 1,\n" +
                    "                typeName: '점',\n" +
                    "                radius: 0,\n" +
                    "                x: 178893,\n" +
                    "                y: 197596,\n" +
                    "                z: 95,\n" +
                    "                sensors: [\n" +
                    "                    {\n" +
                    "                        id: 344,\n" +
                    "                        eventId: 3,\n" +
                    "                        sensorId: 1,\n" +
                    "                        sensorName: 'Microseismic  G-7-1',\n" +
                    "                        imgLink: 'https://example.com/abc1.png'\n" +
                    "                    },\n" +
                    "                    {\n" +
                    "                        id: 345,\n" +
                    "                        eventId: 3,\n" +
                    "                        sensorId: 2,\n" +
                    "                        sensorName: 'Microseismic  G-7-2',\n" +
                    "                        imgLink: 'https://example.com/abc2.png'\n" +
                    "                    },\n" +
                    "                    {\n" +
                    "                        id: 346,\n" +
                    "                        eventId: 3,\n" +
                    "                        sensorId: 3,\n" +
                    "                        sensorName: 'Microseismic  G-7-3',\n" +
                    "                        imgLink: 'https://example.com/abc3.png'\n" +
                    "                    }\n" +
                    "                ]\n" +
                    "            },\n" +
                    "            {\n" +
                    "                id: 4,\n" +
                    "                number: 2,\n" +
                    "                datetime: '2020-09-04 08:00:00',\n" +
                    "                category: 2,\n" +
                    "                categoryName: '발파',\n" +
                    "                type: 2,\n" +
                    "                typeName: '영역',\n" +
                    "                radius: 30,\n" +
                    "                x: 178976,\n" +
                    "                y: 197542,\n" +
                    "                z: 95,\n" +
                    "                sensors: [\n" +
                    "                    {\n" +
                    "                        id: 414,\n" +
                    "                        eventId: 4,\n" +
                    "                        sensorId: 1,\n" +
                    "                        sensorName: 'Microseismic  G-7-1',\n" +
                    "                        imgLink: 'https://example.com/abc1.png'\n" +
                    "                    },\n" +
                    "                    {\n" +
                    "                        id: 415,\n" +
                    "                        eventId: 4,\n" +
                    "                        sensorId: 3,\n" +
                    "                        sensorName: 'Microseismic  G-7-3',\n" +
                    "                        imgLink: 'https://example.com/abc3.png'\n" +
                    "                    }\n" +
                    "                ]\n" +
                    "            }\n" +
                    "        ]\n" +
                    "    }\n" +
                    "}"
    )
    @GetMapping(value = "/api/environment/microseismic/events", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> apiEnvironmentMicroseismicEvents(@RequestParam("date") String date, @RequestParam("category") String category) {
        // 요청 파라미터 검증
        if (date == null || (date !=null && date.equals("") || !CommonUtil.isValidDate(date, 10))) {
            return new ResponseEntity<>(CommonUtil.setError("요청 파라미터 'date' 날짜형식이 아닙니다."), HttpStatus.OK);
        }

        return new ResponseEntity<>(dmMicroseismicEventService.findAllByDateAndCategory(date, category), HttpStatus.OK);
    }

    @ApiOperation(value = "미소진동 - 이벤트 수정"
            , notes =
            "Request: {\n" +
                    "    category: 2" +
                    "}\n" +
                    "Response: {\n" +
                    "    data: null,\n" +
                    "    status: 'OK',\n" +
                    "    message: ''\n" +
                    "}"
    )
    @PutMapping(value = "/api/environment/microseismic/events/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> apiEnvironmentMicroseismicEvents(@PathVariable("id") Integer id, @RequestParam("category") String category) {
        // 요청 파라미터 검증
        if (!CommonUtil.isValidNumber(id)) {
            return new ResponseEntity<>(CommonUtil.setError("요청 URL ID의 값 '" + id + "' 숫자형식이 아닙니다."), HttpStatus.OK);
        }

        return new ResponseEntity<>(dmMicroseismicEventService.updateCategoryById(id, category), HttpStatus.OK);
    }

    @ApiOperation(value = "미소진동 - 이벤트 삭제"
            , notes =
            "Request: {}\n" +
                    "Response: {\n" +
                    "    data: null,\n" +
                    "    status: 'OK',\n" +
                    "    message: ''\n" +
                    "}"
    )
    @DeleteMapping(value = "/api/environment/microseismic/events/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> apiEnvironmentMicroseismicEvents(@PathVariable("id") Integer id) {
        // 요청 파라미터 검증
        if (!CommonUtil.isValidNumber(id)) {
            return new ResponseEntity<>(CommonUtil.setError("요청 URL '" + id + "' 숫자형식이 아닙니다."), HttpStatus.OK);
        }
        return new ResponseEntity<>(dmMicroseismicEventService.deleteById(id), HttpStatus.OK);
    }

    @ApiOperation(value = "미소진동 - 일자(3개월단위) 차트"
            , notes =
            "Request: {" +
                    "    date: '2020-09-10'" +
                    "}\n" +
                    "Response: {\n" +
                    "    data: [\n" +
                    "        {date: '2020-07-10', category: '천공', count: 5},\n" +
                    "        {date: '2020-07-15', category: '발파', count: 14},\n" +
                    "        {date: '2020-07-20', category: '암반파괴', count: 7},\n" +
                    "        {date: '2020-07-25', category: '부석제거', count: 4},\n" +
                    "        {date: '2020-07-30', category: '천공', count: 7},\n" +
                    "        {date: '2020-08-05', category: '발파', count: 9},\n" +
                    "        {date: '2020-08-10', category: '천공', count: 11},\n" +
                    "        {date: '2020-08-15', category: '암반파괴', count: 3},\n" +
                    "        {date: '2020-08-20', category: '천공', count: 22},\n" +
                    "        {date: '2020-08-25', category: '부석제거', count: 8},\n" +
                    "        {date: '2020-08-30', category: '천공', count: 15},\n" +
                    "        {date: '2020-09-05', category: '발파', count: 13},\n" +
                    "        {date: '2020-09-10', category: '발파', count: 18}\n" +
                    "    ]\n" +
                    "    status: 'OK',\n" +
                    "    message: ''\n" +
                    "}"
    )
    @GetMapping(value = "/api/environment/microseismic/events-occurences", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> apiEnvironmentMicroseismicEventsOccurences(@RequestParam("date") String date) {
        // 요청 파라미터 검증
        if (date == null || (date !=null && date.equals("") || !CommonUtil.isValidDate(date, 10))) {
            return new ResponseEntity<>(CommonUtil.setError("요청 파라미터 'date' 날짜형식이 아닙니다."), HttpStatus.OK);
        }
        return new ResponseEntity<>(dmMicroseismicEventService.findCountCategoryGroupByDate(date), HttpStatus.OK);
    }

}
