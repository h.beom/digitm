/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.api.repository.demo;

import kr.insighton.digitalmining.api.vo.demo.TestVo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/*
 *
 * title : test Repository
 * author : 윤영수
 * date : 2020.08.05
 *
 * */

@Repository
public interface TestRepository extends JpaRepository<TestVo, String> {

    Optional<TestVo> findByName(String name);

    List<TestVo> findByNameIgnoreCaseContaining(String keyword);
}
