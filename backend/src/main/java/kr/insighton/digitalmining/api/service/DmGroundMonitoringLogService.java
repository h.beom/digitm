/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.api.service;

import kr.insighton.digitalmining.api.repository.DmGroundMonitoringLogRepository;
import kr.insighton.digitalmining.common.util.CommonUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;

/*
 *
 * title : DmGroundMonitoringLog Service
 * author : 윤영수
 * date : 2020.09.10
 *
 * */

@Service
@Transactional
public class DmGroundMonitoringLogService {

    @Resource
    private DmGroundMonitoringLogRepository dmGroundMonitoringLogRepository;

    /*
    * 지반변위 실시간 모니터링
    * */
    public Map<String, Object> findAllByNowAndSensorId(Integer sensorId) {
        Map<String, Object> data = new HashMap<>();
        List<Map<String, Object>> logs = dmGroundMonitoringLogRepository.findAllByNowAndSensorIdWithNativeQuery(sensorId);
        if (logs != null && logs.size() > 0) {
            data.put("logs", logs);
        } else {
            data.put("logs", new ArrayList<>());
        }

        return CommonUtil.setResult(data);
    }

    /*
     * 지반변위 실시간 모니터링 차트
     * */
    public Map<String, Object> findAllChartByNowAndSensorId(Integer sensorId) {
        Map<String, Object> data = new HashMap<>();
        List<Map<String, Object>> logs = dmGroundMonitoringLogRepository.findAllByNowAndSensorIdWithNativeQuery(sensorId);
        data.put("charts", convertChart(logs, true));

        return CommonUtil.setResult(data);
    }

    /*
    * 지반변위 일자 범위 로그
    * */
    public Map<String, Object> findAllByDateFromToAndSensorId(Integer sensorId, String from, String to) {
        Map<String, Object> data = new HashMap<>();
        List<Map<String, Object>> logs = dmGroundMonitoringLogRepository.findAllByDateFromToAndSensorIdWithNativeQuery(sensorId, from, to);
        if (logs != null && logs.size() > 0) {
            data.put("logs", logs);
        } else {
            data.put("logs", new ArrayList<>());
        }

        return CommonUtil.setResult(data);
    }

    /*
    * 지반변위 일자 범위 로그
    * */
    public Map<String, Object> findAllChartByDateFromToAndSensorId(Integer sensorId, String from, String to) {
        Map<String, Object> data = new HashMap<>();
        List<Map<String, Object>> logs = dmGroundMonitoringLogRepository.findAllByDateFromToAndSensorIdWithNativeQuery(sensorId, from, to);
        data.put("charts", convertChart(logs, false));

        return CommonUtil.setResult(data);
    }

    private List<Map<String, Object>> convertChart(List<Map<String, Object>> logs, boolean realtimeYn) {
        List<Map<String, Object>> charts = new ArrayList<>();
        if (logs != null && logs.size() > 0) {
            for (int i = 0; i < logs.size(); i++) {
                String date = "";
                if (realtimeYn){
                    date = logs.get(i).get("time").toString().substring(0,5);
                } else {
                    date = logs.get(i).get("date").toString().substring(5,10) + " " + logs.get(i).get("time").toString().substring(0,5);
                }
                Map<String, Object> xDegree = new HashMap<>();

                xDegree.put("date", date);
                xDegree.put("type", "지반변위(수평)");
                xDegree.put("value", logs.get(i).get("xDegree"));
                charts.add(xDegree);

                Map<String, Object> xDegreeDisplacement = new HashMap<>();
                xDegreeDisplacement.put("date", date);
                xDegreeDisplacement.put("type", "지반변위 변화율(수평)");
                xDegreeDisplacement.put("value", logs.get(i).get("xDegreeDisplacement"));
                charts.add(xDegreeDisplacement);

                Map<String, Object> yDegree = new HashMap<>();
                yDegree.put("date", date);
                yDegree.put("type", "지반변위(수직)");
                yDegree.put("value", logs.get(i).get("yDegree"));
                charts.add(yDegree);

                Map<String, Object> yDegreeDisplacement = new HashMap<>();
                yDegreeDisplacement.put("date", date);
                yDegreeDisplacement.put("type", "지반변위 변화율(수직)");
                yDegreeDisplacement.put("value", logs.get(i).get("yDegreeDisplacement"));
                charts.add(yDegreeDisplacement);
            }
        }

        return charts;
    }

}
