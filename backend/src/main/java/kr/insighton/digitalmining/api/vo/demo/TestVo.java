/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.api.vo.demo;

import io.swagger.annotations.ApiParam;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/*
 *
 * title : test Vo
 * author : 이형범
 * date : 2020.08.05
 *
 * */

@Getter
@Setter
@Entity
@Table(name = "test")
public class TestVo {

    @Id
    @Column(name = "name", length=100)
    @ApiParam(value = "test : name", required = true)
    private String name;

    @Column(name = "text", length=100)
    @ApiParam(value = "test : text")
    private String text;

}
