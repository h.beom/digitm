/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.api.vo;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/*
 *
 * title : DmStockStatus Vo
 * author : 윤영수
 * date : 2020.08.31
 *
 * */

@Getter
@Setter
@Entity
@Table(name = "t_dm_stock_status")
public class DmStockStatusVo implements Serializable {

    private static final long serialVersionUID = 7756370899177651028L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Integer idx;

    @Column(name = "weighbridge_log_idx")
    private Integer weighbridgeLogIdx;

    @Column(name = "production_input")
    private Integer productionInput;

    @Column(name = "production_output")
    private Integer productionOutput;

    @Column(name = "production_change")
    private Integer productionChange;

    @Column(name = "production_pre_amount")
    private Integer productionPreAmount;

    @Column(name = "production_post_amount")
    private Integer productionPostAmount;

    @Column(name = "procurement_input")
    private Integer procurementInput;

    @Column(name = "procurement_output")
    private Integer procurementOutput;

    @Column(name = "procurement_change")
    private Integer procurementChange;

    @Column(name = "procurement_pre_amount")
    private Integer procurementPreAmount;

    @Column(name = "procurement_post_amount")
    private Integer procurementPostAmount;

}
