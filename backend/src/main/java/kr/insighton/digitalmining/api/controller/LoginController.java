/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.api.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import kr.insighton.digitalmining.api.service.UserDetailsServiceImpl;
import kr.insighton.digitalmining.api.vo.UserDetailsVo;
import kr.insighton.digitalmining.api.vo.UserVo;
import kr.insighton.digitalmining.common.util.TokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;
import java.util.HashMap;
import java.util.Map;

/*
 *
 * title : 로그인 Controller
 * author : 윤영수
 * date : 2020.08.05
 *
 * */

@Api(tags = "로그인")
@RestController
public class LoginController {

    @Value("${jwt.token.header}")
    private String tokenHeader;

    @Autowired
    private TokenUtil tokenUtil;

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @ApiOperation(value = "로그인 API"
            , notes = "{\n" +
            "    Request: {\n" +
            "        header: {\n" +
            "            authorization: 'Bacic ${Base64 Encode Value}'\n" +
            "        }," +
            "        body: {}\n" +
            "    }\n" +
            "    Response: {\n" +
            "        header: {\n" +
            "            Authorization: 'Bearer ${JWT TOKEN}'\n" +
            "        }," +
            "        body: {\n" +
            "            id : 1,\n" +
            "            username : 'user',\n" +
            "            password : null,\n" +
            "            name : '사용자',\n" +
            "            team : '디지털마이닝',\n" +
            "            role: 'USER'\n" +
            "        }\n" +
            "    }\n" +
            "}\n"
    )
    @ApiImplicitParam(paramType = "header", name = "authorization", value = "id, password ", required = true, example = "Basic {base64 Encode}")
    @GetMapping(value = "/api/login", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> apiLogin(HttpServletRequest request, HttpServletResponse response) throws Exception {
        if (request.getHeader("authorization") != null) {
            String authorization = request.getHeader("authorization").substring(6);
            byte[] decode = DatatypeConverter.parseBase64Binary(authorization);
            String convert = new String(decode);
            String id = convert.split(":")[0];
            String uPassword = convert.split(":")[1];

            UserDetailsVo userDetailsVo = userDetailsService.loadUserByUsername(id);
            if (!userDetailsVo.getPassword().equals(uPassword)) {
                return new ResponseEntity<>(HttpStatus.CONFLICT);
            }

            String token = tokenUtil.generateJwtToken(userDetailsVo.getUserVo());

            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            UserVo userVO = userDetailsVo.getUserVo();
            Map<String, Object> user = new HashMap<>();
            user.put("id", userVO.getIdx());
            user.put("username", userVO.getId());
            user.put("password", null);
            user.put("name", userVO.getName());
            user.put("team", userVO.getTeam());
            user.put("role", userVO.getRole());
            user.put("token", token);

            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.set(tokenHeader, "Bearer " + token);

            return new ResponseEntity<>(user, HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
