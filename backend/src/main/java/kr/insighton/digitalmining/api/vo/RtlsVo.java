/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.api.vo;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;

/*
 *
 * title : Rtls Vo
 * author : 윤영수
 * date : 2020.08.05
 *
 * */

@Getter
@Setter
@Entity
@Table(name = "t_rtls")
public class RtlsVo implements Serializable {

    private static final long serialVersionUID = 3167000348882058167L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Integer idx;

    @Column(name = "is_moving")
    @ColumnDefault("0")
    private Integer isMoving;

    @Column(name = "tag_uid", length = 45)
    private String tagUid;

    @Column(name = "rtls_name", length = 45)
    private String rtlsName;

    @Column(name = "ues_type")
    @ColumnDefault("0")
    private Integer uesType;

    @Column(name = "read_time")
    private Date readTime;

    @Column(name = "count")
    @ColumnDefault("0")
    private Integer count;

    @Column(name = "point_h")
    private Integer pointH;

    @Column(name = "point_w")
    private Integer pointW;

    @Column(name = "sensor_idx")
    private Integer sensorIdx;

    @Column(name = "r_type", length = 45)
    @ColumnDefault("사람")
    private String rType;

    @Column(name = "memo", length = 200)
    private String memo;

    @Column(name = "reg_date", length = 200)
    private String regDate;

    @Column(name = "x_point")
    private Double xPoint;

    @Column(name = "y_point")
    private Double yPoint;

    @Column(name = "point_text", length = 200)
    private String pointText;

    @Column(name = "phone", length = 45)
    @ColumnDefault("")
    private String phone;

    @Column(name = "department", length = 45)
    @ColumnDefault("")
    private String department;

}
