package kr.insighton.digitalmining.api.repository;

import kr.insighton.digitalmining.api.vo.DmDpmLogVo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Map;

public interface DmDpmRealtimeRepository extends JpaRepository<DmDpmLogVo, Integer> {
    @Query(value = "" +
            "     SELECT T.IDX 					 AS id                                                                 " +
            "          , T.SENSOR_IDX 			 AS sensorId                                                           " +
            "          , CAST(DATE_FORMAT(T.DATE, '%Y-%M-%D') AS CHAR(10))                                            " +
            "    								 AS 'date'                                                             " +
            "          , T.TIME 				 AS 'time'                                                         " +
            "          , TRUNCATE(T.TEMP, 1) 	 AS temp                                                               " +
            "          , TRUNCATE(T.HUMIDITY, 1) AS humidity                                                          " +
            "          , TRUNCATE(T.CO, 1) 		 AS co                                                                 " +
            "          , TRUNCATE(T.NO, 1) 		 AS no                                                                 " +
            "          , TRUNCATE(T.DPM_10M, 2)  AS dpm10m                                                         " +
            "          , TRUNCATE(T.DPM_30M, 2)  AS dpm30m                                                         " +
            "       FROM T_DM_DPM_LOG T                                                                               " +
            "      WHERE 1=1                                                                                          " +
            "        AND T.SENSOR_IDX = :sensorId                                                                     " +
            "        AND CONCAT(DATE, ' ', TIME) < NOW()                                                              " +
            "        AND CONCAT(DATE, ' ', TIME) > DATE_ADD(DATE_FORMAT(NOW(), '%Y-%m-%d %H:%i:%s'), INTERVAL -2 HOUR)" +
            "   ORDER BY  TIME DESC                                                                                       "
            , nativeQuery = true)
    List<Map<String, Object>> getBySEnsorId(@Param("sensorId") Integer sensorId);
}
