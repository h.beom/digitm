/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.common.service;

import kr.insighton.digitalmining.common.repository.FileRepository;
import kr.insighton.digitalmining.common.vo.FileVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/*
 *
 * title : File Service
 * author : 이형범
 * date : 2020.08.25
 *
 * */

@Slf4j
@Service
@Transactional
public class FileService {

    @Value("${digitalmining.file.upload.enable}")
    private boolean isEnable;

    @Value("${digitalmining.file.upload.path}")
    private String location;

    @Value("${spring.servlet.multipart.location}")
    private String tmpLocation;

    @Resource
    private FileRepository fileRepository;

    @Resource
    private CodeService codeService;

    public List<FileVo> findAll() {
        return fileRepository.findAll();
    }

    public FileVo saveFile(MultipartFile file, String group) {
        if (isEnable) {
            FileVo fileVo = new FileVo();
            String fileName = StringUtils.cleanPath(file.getOriginalFilename());
            String originName = fileName.substring(0, fileName.lastIndexOf("."));
            String extension = fileName.substring(fileName.lastIndexOf(".") + 1);
            fileVo.setName(UUID.randomUUID().toString());
            fileVo.setOriginName(originName);
            fileVo.setExtension(extension);
            if (group.equals("생산계획")) {
                fileVo.setGroup(codeService.findByName("생산계획", "파일그룹"));
            }
            fileVo.setRegDate(new Timestamp(new Date().getTime()));
            fileRepository.save(fileVo);

            try (InputStream inputStream = file.getInputStream()) {
                Path uploadPath = Paths.get(location);
                Files.copy(inputStream, uploadPath.resolve(fileVo.getName()), StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException ex) {
                throw new RuntimeException();
            }

            return fileVo;
        }

        return new FileVo();
    }
    public FileVo updateFile(FileVo fileVo) {
        return fileRepository.save(fileVo);
    }

    public void deleteFile(Integer id) {
        fileRepository.deleteById(id);
    }

}
