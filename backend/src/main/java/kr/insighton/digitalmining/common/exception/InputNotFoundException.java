package kr.insighton.digitalmining.common.exception;

public class InputNotFoundException extends RuntimeException {

    public InputNotFoundException(){
        super("입력하신 항목을 찾을 수 없습니다.");
    }

}
