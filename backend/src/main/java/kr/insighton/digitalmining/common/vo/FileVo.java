/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.common.vo;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/*
 *
 * title : File Vo
 * author : 이형범
 * date : 2020.08.25
 *
 * */

@Getter
@Setter
@Entity
@Table(name = "t_dm_file")
public class FileVo implements Serializable {

    private static final long serialVersionUID = 6515620721871157685L;

    @Id
    @Column(name = "idx")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idx;

    @Column(name = "f_name")
    private String name;

    @Column(name = "f_origin_name")
    private String originName;

    @Column(name = "f_extension")
    private String extension;

    @Column(name = "f_group")
    private String group;

    @Column(name = "f_reg_date")
    private Timestamp regDate;
}
