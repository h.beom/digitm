package kr.insighton.digitalmining.common.exception;

public class BaseMsgException extends RuntimeException {

    public BaseMsgException(String msg) {
        super(msg);
    }
}
