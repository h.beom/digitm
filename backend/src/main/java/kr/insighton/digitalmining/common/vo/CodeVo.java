/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.common.vo;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/*
 *
 * title : Code Vo
 * author : 이형범
 * date : 2020.08.26
 *
 * */

@Getter
@Setter
@Entity
@Table(name = "t_dm_code")
public class CodeVo implements Serializable {

    private static final long serialVersionUID = -4595445011630213561L;

    @Id
    @Column(name = "idx")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idx;

    @Column(name = "code")
    private String code;

    @Column(name = "name")
    private String name;

    @Column(name = "grp_code")
    private String group;

    @Column(name = "sort")
    private Integer sort;

    @Column(name= "reg_date")
    private Timestamp regDate;

    @Column(name = "item_1")
    private String item1;

    @Column(name = "item_2")
    private String item2;

    @Column(name = "item_3")
    private String item3;

    @Column(name = "item_4")
    private String item4;

    @Column(name = "item_5")
    private String item5;

}
