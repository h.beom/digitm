/**
 * DIGITAL MINING
 *
 *
 * */

package kr.insighton.digitalmining.common.util;

import io.jsonwebtoken.*;
import kr.insighton.digitalmining.api.service.UserDetailsServiceImpl;
import kr.insighton.digitalmining.api.vo.UserVo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.security.Key;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

/*
 *
 * title : TokenUtil Util
 * author : 이형범
 * date : 2020.08.06
 *
 * */

@Slf4j
@Component
@RequiredArgsConstructor
public class TokenUtil {

    @Value("${jwt.token.secret.key}")
    private String secretKey;

    @Resource
    private UserDetailsServiceImpl userDetailsService;

    public String generateJwtToken(UserVo userVo) {
        JwtBuilder builder = Jwts.builder()
                .setSubject(userVo.getId())
                .setHeader(createHeader())
                .setClaims(createClaims(userVo))
                .setExpiration(createExpireDateForOneYear())
                .signWith(SignatureAlgorithm.HS256, createSigningKey())
        ;

        return builder.compact();
    }

    // 스프링 시큐리티 필터에서 모든 요청마다 토큰확인을 하기 때문에 이건 무의미하지만 작성만 함
    public boolean isValidToken(String token) {
        try {
            Jws<Claims> claims = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token);
//            return !claims.getBody().getExpiration().before(new Date());
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public Authentication createAuthenticationFromToken(String token) {
        UserDetails userDetails = userDetailsService.loadUserByUsername(getUserIdFromToken(token));
        // it is rather safe to return Authentication with NULL credentials if you do not require to use user credentials after successful authentication.
        return new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
    }

    public String getTokenFromHeader(String header) {
        return header.split(" ")[1];
    }

    public String getIdFromToken(String token) {
        return getClaimFromToken(token, Claims::getSubject);
    }

    public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = getAllClaimsFromToken(token);
        return claimsResolver.apply(claims);
    }

    private Claims getAllClaimsFromToken(String token) {
        return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody();
    }

    private Map<String, Object> createHeader() {
        Map<String, Object> header = new HashMap<>();

        header.put("typ", "JWT");
        header.put("regDate", System.currentTimeMillis());

        return header;
    }

    private Map<String, Object> createClaims(UserVo userVO) {
        // 비공개 클레임으로 사용자의 이름과 이메일을 설정, 세션 처럼 정보를 넣고 빼서 쓸 수 있다.
        Map<String, Object> claims = new HashMap<>();

        claims.put("id", userVO.getId());
        claims.put("name", userVO.getName());
        claims.put("team", userVO.getTeam());
        claims.put("role", userVO.getRole());

        return claims;
    }

    private Date createExpireDateForOneYear() {
        // 토큰 만료시간은 1년으로 설정
        Calendar c= Calendar.getInstance();
        c.add(Calendar.DATE, 365);
        return c.getTime();
    }

    private Key createSigningKey() {
        byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(secretKey);
        return new SecretKeySpec(apiKeySecretBytes, SignatureAlgorithm.HS256.getJcaName());
    }

    public Claims getClaimsFromToken(String token) {
        return Jwts.parser().setSigningKey(DatatypeConverter.parseBase64Binary(secretKey))
                .parseClaimsJws(token).getBody();
    }

    public String getUserIdFromToken(String token) {
        Claims claims = getClaimsFromToken(token);
        return (String) claims.get("id");
    }
}
