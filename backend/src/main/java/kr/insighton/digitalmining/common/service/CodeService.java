/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.common.service;

import kr.insighton.digitalmining.common.repository.CodeRepository;
import kr.insighton.digitalmining.common.util.CommonUtil;
import kr.insighton.digitalmining.common.vo.CodeVo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Map;

/*
 *
 * title : Code Repository
 * author : 이형범
 * date : 2020.08.26
 *
 * */

@Service
@Transactional
public class CodeService {

    @Resource
    private CodeRepository codeRepository;

    public String findByName(String name, String grpName) {
        String group = codeRepository.findByGrpCodeWithNativeQuery(grpName);

        if(group != null) {
            CodeVo codeVo = codeRepository.findByNameAndGroup(name, group);
            if (codeVo == null) {
                return null;
            } else {
                return codeVo.getCode();
            }
        } else {
            return null;
        }
    }

    public Map<String, Object> findAllByGroup(String group) {
        return CommonUtil.setResultList(codeRepository.findAllByGroupWithNativeQuery(group));
    }

    public Map<String, Object> findAllByGroupAndItem(String group, Integer item) {
        return CommonUtil.setResultList(codeRepository.findAllByGroupAndItemWithNativeQuery(group, item));
    }
    public Map<String, Object> findAllGroup() {
        return CommonUtil.setResultList(codeRepository.findAllGroupWithNativeQuery());
    }

}
