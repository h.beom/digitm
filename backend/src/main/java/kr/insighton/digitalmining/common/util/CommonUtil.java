/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.common.util;

import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/*
 *
 * title : Common Util
 * author : 이형범
 * date : 2020.08.27
 *
 * */

public class CommonUtil {

    public static String getToday() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        return dateFormat.format(date);
    }

    public static Integer getTodayDate() {
        DateFormat dateFormat = new SimpleDateFormat("d");
        Date date = new Date();
        return Integer.parseInt(dateFormat.format(date));
    }

    public static String getTodayMonthDate() {
        DateFormat dateFormat = new SimpleDateFormat("M-d");
        Date date = new Date();
        return dateFormat.format(date);
    }

    public static String getTodayMonthDate(String format) {
        DateFormat dateFormat = new SimpleDateFormat("M" + format + "d");
        Date date = new Date();
        return dateFormat.format(date);
    }

    public static String getDayMonthDate(String date, int plusOrMinus) {
        DateFormat dateFormat = new SimpleDateFormat("M-d");
        Calendar calendar = Calendar.getInstance();
        String[] dateSplit = date.split("-");
        calendar.set(Calendar.YEAR, Integer.valueOf(dateSplit[0]));
        calendar.set(Calendar.MONTH, Integer.valueOf(dateSplit[1]) - 1);
        calendar.set(Calendar.DATE, Integer.valueOf(dateSplit[2]));
        if (plusOrMinus != 0) {
            calendar.add(Calendar.DATE, plusOrMinus);
        }
        return dateFormat.format(calendar.getTime());
    }

    public static String getDayMonthDate(String date, String format, int plusOrMinus) {
        DateFormat dateFormat = new SimpleDateFormat("M" + format + "d");
        Calendar calendar = Calendar.getInstance();
        String[] dateSplit = date.split("-");
        calendar.set(Calendar.YEAR, Integer.valueOf(dateSplit[0]));
        calendar.set(Calendar.MONTH, Integer.valueOf(dateSplit[1]) - 1);
        calendar.set(Calendar.DATE, Integer.valueOf(dateSplit[2]));
        if (plusOrMinus != 0) {
            calendar.add(Calendar.DATE, plusOrMinus);
        }
        return dateFormat.format(calendar.getTime());
    }
    public static String getBeforeOrAfterDay(String date, int plusOrMinus) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        String[] dateSplit = date.split("-");
        calendar.set(Calendar.YEAR, Integer.valueOf(dateSplit[0]));
        calendar.set(Calendar.MONTH, Integer.valueOf(dateSplit[1]) - 1);
        calendar.set(Calendar.DATE, Integer.valueOf(dateSplit[2]));
        calendar.add(Calendar.DATE, plusOrMinus);
        return dateFormat.format(calendar.getTime());
    }

    public static String getBeforeOrAfterToday(int plusOrMinus) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, plusOrMinus);
        return dateFormat.format(calendar.getTime());
    }

    public static String getTodayYear() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy");
        Date date = new Date();
        return dateFormat.format(date);
    }

    public static String getTodayYearMonth() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM");
        Date date = new Date();
        return dateFormat.format(date);
    }

    public static String getTodayTimeAll() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);
    }

    public static String getTodayTimeOnly() {
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);
    }

    /*
    * JPA 쿼리 결과 체크
    * */
    public static boolean isNullMap(Map<String, Object> map, String key) {
        if (map == null) {
            return true;
        } else {
            if (map.get(key) == null) {
                return true;
            } else if (map.get(key) == "") {
                return true;
            }
        }

        return false;
    }

    /*
    * Response data 정상처리
    * */
    public static Map<String, Object> setResult(Map<String, Object> data) {
        Map<String, Object> result = new HashMap<>();
        result.put("status", "OK");
        result.put("message", "");
        result.put("data", data);
        return result;
    }

    /*
    * Response list 정상처리
    * */
    public static Map<String, Object> setResultList(List<Map<String, Object>> data) {
        Map<String, Object> result = new HashMap<>();
        result.put("status", "OK");
        result.put("message", "");
        result.put("data", data);
        return result;
    }

    /*
    * Response data 에러처리
    * */
    public static Map<String, Object> setError(String message) {
        Map<String, Object> result = new HashMap<>();
        result.put("status", "ERROR");
        result.put("message", message);
        result.put("data", null);
        return result;

    }

    /*
    * 날짜타입인지 체크
    * */
    public static boolean isValidDate(String date, Integer length) {
        SimpleDateFormat dateFormat;
        try {
            if(length == 19) {
                dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.KOREA);
                dateFormat.setLenient(false);
                dateFormat.parse(date);
            } else if(length == 10) {
                dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.KOREA);
                dateFormat.setLenient(false);
                dateFormat.parse(date);
            } else if(length == 7) {
                dateFormat = new SimpleDateFormat("yyyy-MM", Locale.KOREA);
                dateFormat.setLenient(false);
                dateFormat.parse(date);
            } else if(length == 5) {
                dateFormat = new SimpleDateFormat("MM-dd", Locale.KOREA);
                dateFormat.setLenient(false);
                dateFormat.parse(date);
            } else if(length == 4) {
                dateFormat = new SimpleDateFormat("yyyy", Locale.KOREA);
                dateFormat.setLenient(false);
                dateFormat.parse(date);
            }
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    /*
    * 숫자타입인지 체크
    * */
    public static boolean isValidNumber(Integer number) {
        try {
            if (number == null) {
                return false;
            }
            Double test = number.doubleValue();
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    /*
    * 파일타입 체크
    * default : 엑셀, 이미지
    * */
    public static boolean isValidFile(MultipartFile file, String extension) {
        try {
            String fileName = StringUtils.cleanPath(file.getOriginalFilename());
            String fileExtension = fileName.substring(fileName.lastIndexOf(".") + 1);
            if (file.isEmpty()) {
                return false;
            }
            if (fileName.contains("..")) {
                return false;
            }
            if (extension != null) {
                // 엑셀 파일
                if (extension.equals("excel")) {
                    if (!fileExtension.toLowerCase().equals("xls") && !fileExtension.toLowerCase().equals("xlsx")) {
                        return false;
                    }
                }

                // 이미지 파일
                if (extension.equals("image")) {
                    if (!fileExtension.toLowerCase().equals("jpg")
                            && !fileExtension.toLowerCase().equals("jpeg")
                            && !fileExtension.toLowerCase().equals("bmp")
                            && !fileExtension.toLowerCase().equals("png")
                            && !fileExtension.toLowerCase().equals("gif")
                            && !fileExtension.toLowerCase().equals("tiff")
                            && !fileExtension.toLowerCase().equals("tif")
                            && !fileExtension.toLowerCase().equals("raw")
                            && !fileExtension.toLowerCase().equals("jpeg")
                    ) {
                        return false;
                    }
                }
            }
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

}
