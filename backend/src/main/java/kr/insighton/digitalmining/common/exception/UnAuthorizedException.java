package kr.insighton.digitalmining.common.exception;

public class UnAuthorizedException extends RuntimeException {

    public UnAuthorizedException(){
        super("관리자 권한만 접근 가능합니다.");
    }

}
