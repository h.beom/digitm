package kr.insighton.digitalmining.common.exception;

public class ExpiredException extends RuntimeException {

    public ExpiredException(){
        super("로그인 인증이 만료되었습니다.");
    }

}
