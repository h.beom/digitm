package kr.insighton.digitalmining.common.schedule;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Slf4j
@Component
public class Scheduler {

    @Value("${digitalmining.file.upload.enable}")
    private boolean isEnable;

    @Value("${digitalmining.file.upload.path}")
    private String location;

    @Value("${spring.servlet.multipart.location}")
    private String tmpLocation;

    /*
    * 파일업로드로 생성된 임시파일 매년 12월31일 23시에 비우기
    * */
    @Scheduled(cron = "0 0 23 31 12 ?")
    public void cronJobCleanTmpFile() {
        log.error("임시파일 비우기 시작");
        try {
            if (isEnable) {
                File tmpDir = new File(tmpLocation);
                if (tmpDir.isDirectory()) {
                    File[] files = tmpDir.listFiles();
                    if (files != null && files.length > 0) {
                        for(File file : files) {
                            Path fileLocation = Paths.get(file.toURI());
                            Files.delete(fileLocation);
                        }
                    }
                } else {
                    log.error("임시폴더 경로 : " + tmpLocation + "=> 오류 : 폴더가 아니거나 존재하지 않습니다.");
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            log.error("임시폴더 삭제중 오류(IOException) 발생 : " + ex.getMessage());
        } catch (Exception ex) {
            ex.printStackTrace();
            log.error("임시폴더 삭제중 오류(Exception) 발생 : " + ex.getMessage());
        }
        log.error("임시파일 비우기 종료");
    }
}
