/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.common.repository;

import kr.insighton.digitalmining.common.vo.CodeVo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/*
 *
 * title : Code Repository
 * author : 이형범
 * date : 2020.08.26
 *
 * */

@Repository
public interface CodeRepository extends JpaRepository<CodeVo, Integer> {

    @Query(value = "" +
            " select" +
            "   t.code" +
            " from t_dm_code t" +
            " where t.grp_code is null" +
            " and t.name = :name" +
            " and t.use_yn = 'Y'"
            , nativeQuery = true)
    String findByGrpCodeWithNativeQuery(@Param("name") String name);

    CodeVo findByNameAndGroup(String name, String group);

    @Query(value = "" +
            " select" +
            "   t.code" +
            " , t.name" +
            " from t_dm_code t" +
            " where t.grp_code = :group" +
            " and t.use_yn = 'Y'" +
            " order by t.sort"
            , nativeQuery = true)
    List<Map<String, Object>> findAllByGroupWithNativeQuery(@Param("group")String group);

    @Query(value = "" +
            " select" +
            "   case " +
            "       when :item = 1 then t.item_1" +
            "       when :item = 2 then t.item_2" +
            "       when :item = 3 then t.item_3" +
            "       when :item = 4 then t.item_4" +
            "       when :item = 5 then t.item_5" +
            "       else t.code" +
            "   end as item" +
            " , t.name" +
            " from t_dm_code t" +
            " where t.grp_code = :group" +
            " and t.use_yn = 'Y'" +
            " order by t.sort"
            , nativeQuery = true)
    List<Map<String, Object>> findAllByGroupAndItemWithNativeQuery(@Param("group")String group, @Param("item")Integer item);

    @Query(value = "" +
            " select" +
            "   t.code as id" +
            " , t.name" +
            " from t_dm_code t" +
            " where t.grp_code is null" +
            " and t.use_yn = 'Y'"
            , nativeQuery = true)
    List<Map<String, Object>> findAllGroupWithNativeQuery();
}
