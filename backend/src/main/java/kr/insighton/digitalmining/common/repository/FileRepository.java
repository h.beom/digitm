/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.common.repository;

import kr.insighton.digitalmining.common.vo.FileVo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/*
 *
 * title : File Repository
 * author : 이형범
 * date : 2020.08.25
 *
 * */

@Repository
public interface FileRepository extends JpaRepository<FileVo, Integer> {

}
