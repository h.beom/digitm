package kr.insighton.digitalmining.common.exception;

public class UserNotFoundException extends RuntimeException {

    public UserNotFoundException(String id){
        super(id + " -> 존재하지 않는 사용자입니다.");
    }

}
