/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/*
 *
 * title : Application
 * author : 이형범
 * date : 2020.08.05
 *
 * */

@SpringBootApplication
@EnableScheduling
public class DigitalminingApplication {

    public static void main(String[] args) {
        SpringApplication.run(DigitalminingApplication.class, args);
    }

}
