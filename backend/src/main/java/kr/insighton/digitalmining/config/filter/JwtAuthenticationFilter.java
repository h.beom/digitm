/**
 * DIGITAL MINING
 *
 *
 * */

package kr.insighton.digitalmining.config.filter;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import kr.insighton.digitalmining.api.service.UserDetailsServiceImpl;
import kr.insighton.digitalmining.api.vo.UserDetailsVo;
import kr.insighton.digitalmining.common.exception.BaseMsgException;
import kr.insighton.digitalmining.common.exception.UnAuthorizedException;
import kr.insighton.digitalmining.common.exception.UserNotFoundException;
import kr.insighton.digitalmining.common.util.TokenUtil;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.annotation.Resource;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/*
 *
 * title : AuthenticationFilter Filter
 * author : 이형범
 * date : 2020.08.06
 *
 * */

@Slf4j
@Component
public class JwtAuthenticationFilter extends OncePerRequestFilter {

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @Autowired
    private TokenUtil tokenUtil;

    @Value("${jwt.token.header}")
    private String tokenHeader;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        // JWT 토큰 인증 패스할 URL
        if(request.getRequestURI().contains("/api/login")
                || request.getRequestURI().contains("/api/logout")
                || request.getRequestURI().contains("/api/temp")
                || request.getRequestURI().contains("/v2/api-docs")
                || request.getRequestURI().contains("/configuration/ui")
                || request.getRequestURI().contains("/swagger-resources")
                || request.getRequestURI().contains("/configuration/security")
                || request.getRequestURI().contains("/swagger-ui.html")
                || request.getRequestURI().contains("/webjars")
                || request.getRequestURI().contains("/csrf")
                || request.getRequestURI().contains("/swagger")) {
        } else {

            String requestTokenHeader = request.getHeader(tokenHeader);

            String id = null;
            String role;
            String token;

            if (requestTokenHeader != null && requestTokenHeader.startsWith("Bearer ")) {
                token = tokenUtil.getTokenFromHeader(requestTokenHeader);
                if (tokenUtil.isValidToken(token)) {
                    // 토큰정보확인
                    try {
                        Claims claims = tokenUtil.getClaimsFromToken(token);
                        id = (String)claims.get("id");
                        role = (String)claims.get("role");

                        // 사용자확인
                        // 사용자가 존재하지 않으면 UserNotFoundException 발생
                        UserDetailsVo userDetailsVo = userDetailsService.loadUserByUsername(id);

                        // 롤확인(ADMIN 권한이면 모두 허용)
                        // 1. token의 claims에 role의 값이 uservo role이 같은지 확인(token 훼손 및 사용자의 롤이 변경되었을 가능성)
                        if(!userDetailsVo.getRole().equals(role)) {
                            throw new UnAuthorizedException();
                        }
                        // 2. user 권한이면 admin api 요청만 확인
                        if(role.equals("USER") && request.getRequestURI().contains("/api/admin")) {
                            throw new UnAuthorizedException();
                        }

                        SecurityContextHolder.getContext().setAuthentication(tokenUtil.createAuthenticationFromToken(token));
                    } catch (IllegalArgumentException e) {
                        log.error("JWT_TOKEN_UNABLE", e);
                        throw new UserNotFoundException(id);
                    } catch (ExpiredJwtException e) {
                        log.warn("JWT_TOKEN_EXPIRED", e);
                        // 토큰 만료시간 무제한
//                    throw new ExpiredException();
                    }
                }
            } else {
                throw new BaseMsgException("JWT_TOKEN_NOT_FOUND");
            }

            log.debug("JWT_TOKEN_USER_ID_VALUE '{}'", id);

        }

        chain.doFilter(request, response);
    }
}
