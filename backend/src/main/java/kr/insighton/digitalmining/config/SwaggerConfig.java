/**
 * DIGITAL MINING
 *
 *
 * */
package kr.insighton.digitalmining.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/*
 *
 * title : SwaggerConfig Config
 * author : 이형범
 * date : 2020.08.08
 *
 * */

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Value("${spring.profile}")
    private String profile;

    @Value("${jwt.token.header}")
    private String tokenHeader;

    @Value("${jwt.token.swagger.only.token}")
    private String swaggerOnlyToken;

    @Bean
    public Docket api() {
        boolean swaggerEnabled = true;
        // 운영에서는 swagger 사용 안함
        if(profile.equals("prod")) {
            swaggerEnabled = false;
        }
        return new Docket(DocumentationType.SWAGGER_2)
                .enable(swaggerEnabled)
                .globalOperationParameters(setAuthorizationHeader())
                .consumes(getConsumeContentTypes())
                .produces(getProduceContentTypes())
                .apiInfo(getApiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("kr.insighton.digitalmining.api.controller"))
                .paths(PathSelectors.ant("/api/**"))
                .build()
                .pathMapping("/");
    }

    private List<Parameter> setAuthorizationHeader() {
        ParameterBuilder parameterBuilder = new ParameterBuilder();
        parameterBuilder.name(tokenHeader)
                .description("Access Token")
                .modelRef(new ModelRef("string"))
                .parameterType("header")
                .required(false)
                .defaultValue(swaggerOnlyToken)
                .build();
        List<Parameter> parameters = new ArrayList<>();
        parameters.add(parameterBuilder.build());

        return parameters;
    }
    private Set<String> getConsumeContentTypes() {
        Set<String> consumes = new HashSet<>();
        consumes.add("application/json;charset=UTF-8");
        consumes.add("application/x-www-form-urlencoded");
        return consumes;
    }

    private Set<String> getProduceContentTypes() {
        Set<String> produces = new HashSet<>();
        produces.add("application/json;charset=UTF-8");
        return produces;
    }

    private ApiInfo getApiInfo() {
        return new ApiInfoBuilder()
                .title("API")
                .description("[DIGITAL MINING] API")
                .contact(new Contact("Digital Mining Swagger", "http://digitalmining.insighton.kr/", "shinho.kang@insighton.kr"))
                .version("0.0.1")
                .build();
    }
}
