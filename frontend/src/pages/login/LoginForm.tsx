import React, {Component} from 'react';
import 'antd/dist/antd.css';
import {observer} from "mobx-react";
import {Button, Card, Menu, Table} from "antd";
import {Credentials, LoginButton, LoginInput} from "../../type/UserStyled";
import Service from "../../common/UserService";
import {User} from "../../type/user";
import autobind from "autobind-decorator";
import Icon from "antd/es/icon";




@observer
class LoginForm extends Component<any, any> {

    constructor(props: any) {
        super(props);

        // redirect to home if already logged in
        if (Service.currentUserValue) {
            this.props.history.push('/');
        }

        this.state = {
            user: new User('',''),
            submitted: false,
            loading: false,
            errorMessage: ''
        };
    }

    handleChange(e: { target: { name: any; value: any; }; }){
        var { name, value } = e.target;
        var user = this.state.user;
        user[name] = value;
        this.setState({ user: user });
    }

    @autobind
    handleLogin(e: any) {
        e.preventDefault();

        this.setState({ submitted: true });
        const { user } = this.state;

        // stop here if form is invalid
        if (!(user.username && user.password)) {
            return;
        }

        this.setState({ loading: true });
        Service.login(user)
            .then(
                data => {
                    this.props.history.push("/digitalmining/dashboard/production");
                },
                error => {
                    console.log(error);
                    alert('사용자이름 또는 비밀번호가 일치하지 않습니다!!!');
                    this.setState({ errorMessage: "사용자이름 또는 비밀번호가 일치하지 않습니다!!!.", loading: false });
                }
            );
    }
    render() {
        const { user, submitted, loading, errorMessage } = this.state;


        return (
            <div className="login">
                <div className="login-child">
                    <Card>
                        <h1 className="mb-2vw"><img src={require('../../resources/login_logo.png')} alt="KOREA RESOURCES CORPORATION" /></h1>
                        <h2>Digital Mining Solutions</h2>
                        <div className={'form-group' + (submitted && !user.username ? ' has-error' : '')}>
                            <label>ID</label>
                            <LoginInput type="text" className="form-control" name="username" value={user.username} onChange={(e) => this.handleChange(e)} placeholder="Enter your username" />
                            {submitted && !user.username &&
                            <div className="help-block">Username is required</div>
                            }
                        </div>
                        <div className={'form-group' + (submitted && !user.password ? ' has-error' : '')}>
                            <label>PW</label>
                            <LoginInput type="password" className="form-control" name="password" value={user.password} onChange={(e) => this.handleChange(e)} placeholder="Enter your password"/>
                            {submitted && !user.password &&
                            <div className="help-block">Password is required</div>
                            }
                        </div>
                        {errorMessage &&
                        <div className="alert alert-danger" role="alert">
                            {errorMessage}
                        </div>
                        }
                        <LoginButton type={'primary'} className="btn_login" disabled={loading} onClick={this.handleLogin}>Login</LoginButton>
                    </Card>
                </div>
                <h2 className="f_logo">(주)고려시멘트</h2>
            </div>
        );
    };
}


export default LoginForm;
