import React, {Component, RefObject} from 'react';
import {
    BoxGeometry,
    DoubleSide,
    Group,
    HemisphereLight,
    Mesh,
    MeshBasicMaterial,
    Object3D,
    PerspectiveCamera,
    PlaneGeometry,
    Raycaster,
    RingGeometry,
    Scene,
    Sprite,
    SpriteMaterial,
    Texture,
    TextureLoader,
    Vector3,
    WebGLRenderer
} from 'three';
import {OrbitControls} from "three/examples/jsm/controls/OrbitControls";
import {GLTFLoader} from "three/examples/jsm/loaders/GLTFLoader";
import {Button, Card, Select} from "antd";
import {inject, observer} from "mobx-react";
import autobind from "autobind-decorator";
import StringUtil from "../../util/StringUtil";
// @ts-ignore
import Icon from "antd/es/icon";
import {bsApi} from "../../common/Api";
import Model3DObject from "../../util/threeService/Model3DObject";
import {Link} from "react-router-dom";

let INTERSECTED: any = null;

const iconMap = new Map();
iconMap.set('C009-002', require("../../resources/map/icon_dpm.png"))
iconMap.set('C009-003', require("../../resources/map/icon_gas.png"))
iconMap.set('C009-004', require("../../resources/map/icon_ground.png"))


@inject('EnvironmentStore')
@observer
class MonitoringSenserMap extends Component<any, any> {

    // @description 버튼 클릭시 컨텐츠 화면 풀스크린 모드
    @autobind
    private onClickEventFullScreen() {
        const type: boolean = !this.state.fullscreenType;
        const callBack: Function = this.state.renderEvent;
        this.setState({ fullscreenType: type }, function () {
            if (callBack != undefined) {
                callBack(type);
            }
        });
    }


    private _mount: RefObject<HTMLDivElement> | null | undefined;
    private scene: Scene = new Scene();
    private camera: PerspectiveCamera | null = null;
    private renderer: WebGLRenderer | null = null;
    private controls: OrbitControls | null = null;
    private frameId: number = 0;
    private modelItem: Object3D | null = null;
    private mouse = { x: 0, y: 0 };
    private targetList: any = []

    constructor(props: any) {
        super(props);
        this.state = {
            mode: 0,
            mode2: 0,
            selectDate: StringUtil.formatDate(),
            visible: false,
            chartVisible: true,
            loading: true,
            selectCode:'all',
            sensortype : props.match.params.sensortype,
            setUrl : '',
            setUrl2 : ''
        };
        this._mount = React.createRef();
        this.start = this.start.bind(this);
        this.animate = this.animate.bind(this);
        this.stop = this.stop.bind(this);
    }


    /**
     * @descriptiong 캔버스 리사이징 처리 (offset 수치 수정)
     */
    @autobind
    public handleResize() {
        if (this.camera != null) {
            this.camera.aspect = window.innerWidth / window.innerHeight;
            this.camera.updateProjectionMatrix()

        }
        if (this.renderer != null) {
            this.renderer.setSize(window.innerWidth * 0.735, window.innerHeight * 0.630);
        }

    }


    public updateDimensions() {
        let mount: RefObject<HTMLDivElement> | null | undefined = this._mount;
        if (mount == null && mount == undefined) {
            return;
        }
        if (mount.current == null && mount.current == undefined) {
            return;
        }
        this.handleResize();
    };


    @autobind
    async load3D(value: string, num?: number) {
        const {EnvironmentStore} = this.props;
        this.targetList = [];

        //이벤트 객체 호출
        const resultData = await bsApi.get('/environment/sensors', {
            params: {
                division: value
            }
        })


        let mount: RefObject<HTMLDivElement> | null | undefined = this._mount;
        if (mount == null && mount == undefined) {
            return;
        }
        if (mount.current == null && mount.current == undefined) {
            return;
        }


        let width: number = mount.current.clientWidth - 2;
        let height: number = mount.current.clientHeight - 7;

        const scene: Scene = this.scene;

        const camera: PerspectiveCamera = new PerspectiveCamera(
            50,
            width / height,
            1,
            10000
        );

        //배경 컬러
        const renderer: WebGLRenderer = new WebGLRenderer();
        renderer.setSize(width, height);
        renderer.setClearColor(0x202223, 1);

        this.controls = new OrbitControls(camera, renderer.domElement);

        this.controls.enableRotate = false;
        this.controls.minDistance = 10;
        this.controls.maxDistance = 1000;
        this.controls.maxPolarAngle = Math.PI * 2;



        // let loadTextURL: string = require("./map01.jpg");
        let loader: TextureLoader = new TextureLoader();



        let planeGeometry: PlaneGeometry = new PlaneGeometry(1500, 700, 100, 70);
        let planeMaterial: MeshBasicMaterial = new MeshBasicMaterial({ wireframe: true, opacity: 0.5 });

        let grid = new Mesh(planeGeometry, planeMaterial);
        grid.position.set(0, 0, 0);

        grid.rotation.x = -0.5 * Math.PI;
        // scene.add(grid);


        // ====================================카메라설정===========================================================================

        // 카메라 위치 설정
        camera.position.set(1000, 800, 0);

        this.scene = scene;
        this.camera = camera;
        this.renderer = renderer;

        if (num !== 1) {
            mount.current.appendChild(this.renderer.domElement);
        }

        // this.stats.showPanel(1); // 0: fps, 1: ms, 2: mb, 3+: custom
        // let stateHtml: HTMLElement = this.stats.dom;
        // stateHtml.setAttribute("style", "position: absolute; top: 274px; cursor: pointer; opacity: 0.8; z-index: 1000;");
        // mount.appendChild(stateHtml);


        this.handleResize()
        // ====================================구역나누기===========================================================================


        const geometry = new BoxGeometry(10, 10, 10);

        const loader2 = new TextureLoader();


      const restData = resultData.data.data.sensors.map((value: any, index: any) => {
            if(value.division == 'C009-002' || value.division == 'C009-003' || value.division == 'C009-004'){
                return value
            }
        })
        const filterData = restData.filter((value: any) => value !== undefined);
        EnvironmentStore.setSensorFilterData(filterData);

        //데이터베이스에 대한 로드
        filterData.map((value: any, index: any) => {
            const ras = iconMap.get(value.division)
            const material = new MeshBasicMaterial({
                map: loader2.load(ras),
                transparent: true, opacity: 1
            });


            const cube = new Mesh(geometry, material);
            let object3D:Model3DObject = new Model3DObject();
            object3D.init({obj:cube , postition:value})


            cube.name = value.id
            this.targetList.push(cube)
            scene.add(object3D.getGroup());


            if (value.type === 'C007-002') {
                var geometry2 = new RingGeometry(value.radius, value.radius + 1.5, 32, 1);
                var material2 = new MeshBasicMaterial({ color: 0xffff00, side: DoubleSide });
                var mesh = new Mesh(geometry2, material2);
                mesh.rotateX(0.5 * Math.PI)
                mesh.position.x = value.x - 178600 -450;
                mesh.position.y = value.z - 75;
                mesh.position.z = value.y - 197045 - 200;
                scene.add(mesh);
            }

            // let text = value.id
            //
            // let spritey = this.makeTextSprite( text, { fontsize: 32, fontface: "Georgia", borderColor: {r:0, g:0, b:0} } );
            // spritey.name = value.id
            // spritey.position.set(value.x - 178600 -420,value.z - 75, value.y - 197045 - 235);
            // scene.add( spritey );

            function render(time: any) {
                time *= 0.003;  // convert time to seconds
                // cube.rotation.x = time;
                cube.rotation.y = time;

                renderer.render(scene, camera);
                requestAnimationFrame(render);
            }
            requestAnimationFrame(render);
        })

        console.log('이벤트 횟수');

        if(num !== 1){
            window.addEventListener('resize', this.updateDimensions.bind(this), false);
            document.removeEventListener('mousedown', this.onDocumentMouseDown.bind(this), false);
            document.removeEventListener('mousemove', this.onDocumentMouseMove.bind(this), false);
            document.addEventListener('mousedown', this.onDocumentMouseDown.bind(this), false);
            document.addEventListener('mousemove', this.onDocumentMouseMove.bind(this), false);
        }
    }

    @autobind
    public makeTextSprite( message: string, parameters: { [x: string]: any; hasOwnProperty?: any; } | undefined ) {
        if ( parameters === undefined ) parameters = {};

        var fontface = parameters.hasOwnProperty("fontface") ?
            parameters["fontface"] : "Arial";

        var fontsize = parameters.hasOwnProperty("fontsize") ?
            parameters["fontsize"] : 18;

        var borderThickness = parameters.hasOwnProperty("borderThickness") ?
            parameters["borderThickness"] : 4;

        var borderColor = parameters.hasOwnProperty("borderColor") ?
            parameters["borderColor"] : { r:0, g:0, b:0, a:1.0 };

        var backgroundColor = parameters.hasOwnProperty("backgroundColor") ?
            parameters["backgroundColor"] : { r:255, g:255, b:255, a:1.0 };

        // var spriteAlignment = SpriteAlignment.topLeft;

        var canvas = document.createElement('canvas');
        let context = canvas.getContext('2d');
        // @ts-ignore
        context.font = "Bold " + fontsize + "px " + fontface;

        // get size data (height depends only on font size)
        // @ts-ignore
        var metrics = context.measureText( message );
        var textWidth = metrics.width;

        // background color
        // @ts-ignore
        context.fillStyle   = "rgba(" + backgroundColor.r + "," + backgroundColor.g + ","
            + backgroundColor.b + "," + backgroundColor.a + ")";
        // border color
        // @ts-ignore
        context.strokeStyle = "rgba(" + borderColor.r + "," + borderColor.g + ","
            + borderColor.b + "," + borderColor.a + ")";
        // @ts-ignore
        context.lineWidth = borderThickness;
        this.roundRect(context, borderThickness/2, borderThickness/2, textWidth + borderThickness, fontsize * 1.4 + borderThickness, 6);
        // 1.4 is extra height factor for text below baseline: g,j,p,q.

        // text color
        // @ts-ignore
        context.fillStyle = "rgba(0, 0, 0, 1.0)";

        // @ts-ignore
        context.fillText( message, borderThickness, fontsize + borderThickness);

        // canvas contents will be used for a texture
        var texture = new Texture(canvas)
        texture.needsUpdate = true;

        var spriteMaterial = new SpriteMaterial(
            { map: texture } );
        var sprite = new Sprite( spriteMaterial );
        sprite.scale.set(100,50,1.0);
        return sprite;
    }

    @autobind
    public roundRect(ctx: CanvasRenderingContext2D | null, x: number, y: number, w: any, h: any, r: number) {
        // @ts-ignore
        ctx.beginPath();
        // @ts-ignore
        ctx.moveTo(x+r, y);
        // @ts-ignore
        ctx.lineTo(x+w-r, y);
        // @ts-ignore
        ctx.quadraticCurveTo(x+w, y, x+w, y+r);
        // @ts-ignore
        ctx.lineTo(x+w, y+h-r);
        // @ts-ignore
        ctx.quadraticCurveTo(x+w, y+h, x+w-r, y+h);
        // @ts-ignore
        ctx.lineTo(x+r, y+h);
        // @ts-ignore
        ctx.quadraticCurveTo(x, y+h, x, y+h-r);
        // @ts-ignore
        ctx.lineTo(x, y+r);
        // @ts-ignore
        ctx.quadraticCurveTo(x, y, x+r, y);
        // @ts-ignore
        ctx.closePath();
        // @ts-ignore
        ctx.fill();
        // @ts-ignore
        ctx.stroke();
    }



    async componentDidMount() {
        const {EnvironmentStore} = this.props
        const {sensortype} = this.state
        console.log(window.location.hash,'1111');
        if( window.location.hash == '#dpm'){
            EnvironmentStore.setSelectData('C009-002')
            this.load3D('C009-002');
        }else if( window.location.hash == '#gas'){
            EnvironmentStore.setSelectData('C009-003')
            this.load3D('C009-003');
        }else if( window.location.hash == '#ground-control'){
            EnvironmentStore.setSelectData('C009-004')
            this.load3D('C009-004');
        }else{
            this.load3D('all');
        }

        /**
         * @description 지도 로드
         */
        let glbURL2: string = require("../../resources/threejs/krcmine/Lv_07.glb");
        let gbxLoader = new GLTFLoader();
        let group: Group = new Group();

        gbxLoader.load(glbURL2, (object) => {
            let obj: any = object.scene;
            obj.children[0].geometry.computeBoundingBox();
            let bbox = obj.children[0].geometry.boundingBox.clone();

            var XCt = 0.5 * (bbox.min.x + bbox.max.x);
            var YCt = 0.5 * (bbox.min.y + bbox.max.y);
            var ZCt = 0.5 * (bbox.min.z + bbox.max.z);


            obj.children[0].material.transparent = true
            obj.children[0].material.opacity = 0.5
            obj.children[0].material.color.set(0xFF0000);
            group.add(obj);
            group.name = 'map'
            this.scene.add(group);
            // group.rotation.y = 0.5 * Math.PI
            // obj.children[0].rotation._y = 0.5 * Math.PI
            obj.position.x = -XCt;
            obj.position.y = -YCt;
            obj.position.z = -ZCt;

        })
        let light: HemisphereLight = new HemisphereLight(0xffffff, 0x444444);
        light.position.set(0, 100, 0);
        light.name = 'light'
        this.scene.add(light);
        this.start();
        this.setState({
            loading: false
        })

    }


    /**
     * @description 마우스 오버일때 해당 오브젝트의 색상을 변화시킨다
     * @param event
     */
    public onDocumentMouseMove(event: any) {
        let gapX: number = event.clientX - event.offsetX;
        let gapY: number = event.clientY - event.offsetY;


        this.mouse.x = ((event.clientX - gapX) / (window.innerWidth * 0.815)) * 2 - 1;
        this.mouse.y = -((event.clientY - gapY) / (window.innerHeight * 0.618)) * 2 + 1;
        const vector = new Vector3(this.mouse.x, this.mouse.y, 1);
        // @ts-ignore
        vector.unproject(this.camera);
        // @ts-ignore
        const ray = new Raycaster(this.camera.position, vector.sub(this.camera.position).normalize());
        // @ts-ignore
        const intersects: [] = ray.intersectObjects(this.targetList, true); // 신에 추가한 대상들이 포함되어 있는 애들
        if (intersects.length > 0) {
            {
                // @ts-ignore
                if (intersects[0].object != INTERSECTED) {
                    if (INTERSECTED)
                        INTERSECTED.material.color = INTERSECTED.currentHex;
                    // @ts-ignore
                    INTERSECTED = intersects[0].object;
                    INTERSECTED.currentHex = INTERSECTED.material.color;
                    INTERSECTED.material.color = { r: 255, g: 255, b: 0 }
                }
            }
        } else {
            if (INTERSECTED)
                INTERSECTED.material.color = INTERSECTED.currentHex;
            INTERSECTED = null;
        }

    }


    /**
     * @description 해당 오브젝트를 클릭시 클릭이벤트가 진행(driildown으로 해당 맵 이동)
     * @param event
     */
    public onDocumentMouseDown(event: any) {
        console.log('test');
        let gapX: number = event.clientX - event.offsetX;
        let gapY: number = event.clientY - event.offsetY;

        this.mouse.x = ((event.clientX - gapX) / (window.innerWidth * 0.815)) * 2 - 1;
        this.mouse.y = -((event.clientY - gapY) / (window.innerHeight * 0.618)) * 2 + 1;

        const vector = new Vector3(this.mouse.x, this.mouse.y, 1);
        // @ts-ignore
        vector.unproject(this.camera);

        // @ts-ignore
        const ray = new Raycaster(this.camera.position, vector.sub(this.camera.position).normalize());

        // @ts-ignore
        const intersects: [] = ray.intersectObjects(this.targetList, true); // 신에 추가한 대상들이 포함되어 있는 애들
        if (intersects.length > 0) {
            this.props.EnvironmentStore.getSensorFilterData.map((value: any) => {
                // @ts-ignore
                if (value.id == intersects[0].object.name) {
                    this.props.EnvironmentStore.selectSensor(value.id);
                    this.setState({
                        visible: true
                    })
                    this.setUrl()

                }else{

                }
            });
        }
    }

    start(): void {
        this.animate();
    }

    animate = () => {
        window.requestAnimationFrame(this.animate);


        this.renderScene();
    };

    componentWillUnmount() {
        let mount: RefObject<HTMLDivElement> | null | undefined = this._mount;
        if (mount == null && mount == undefined) {
            return;
        }
        if (mount.current == null && mount.current == undefined) {
            return;
        }

        if (this.renderer == null) {
            return;
        }
        this.stop();

        // mount.current.removeChild(this.renderer.domElement);
        window.removeEventListener('resize', this.updateDimensions);
        this.props.EnvironmentStore.setSensorInfo()
    }

    stop = () => {
        cancelAnimationFrame(this.frameId)
    };


    renderScene() {
        if (this.renderer == null || this.scene == null || this.camera == null) {
            return;
        }

        if (this.controls != null) {
            this.controls.update();
        }
        this.renderer.render(this.scene, this.camera);
    }

    // =======================================================================================================

    @autobind
    public codeChange(value:string){


        this.setState({
            selectCode:value
        })

        let test = this.scene.children.filter((value: any) => value.name == 'map' || value.name == 'light' );
        this.scene.children = test
        this.camera = null;
        this.renderer = null;
        this.controls = null;
        // this._mount = null;

    }


    @autobind
    async changeSensor(value: string) {
        const {EnvironmentStore} = this.props;
        EnvironmentStore.setSelectData(value)
        let test = this.scene.children.filter((value: any) => value.name == 'map' || value.name == 'light' );
        this.scene.children = test
        // this.camera = null;
        this.renderer = null;
        this.controls = null;
        // this._mount = null;
        this.load3D(value, 1);
        EnvironmentStore.setSensorInfo()
    }

    @autobind
    async setUrl() {
        const {EnvironmentStore} = this.props;
        if(EnvironmentStore.getSensorInfo.division == 'C009-002'){
            this.setState({
                setUrl: '/environment/dpm-analysis',
                setUrl2: '/environment/dpm-realtime'
            })
        }else if(EnvironmentStore.getSensorInfo.division == 'C009-003'){
            this.setState({
                setUrl: '/environment/gas-analysis',
                setUrl2: '/environment/gas-realtime'
            })
        }else if(EnvironmentStore.getSensorInfo.division == 'C009-004'){
            this.setState({
                setUrl: '/environment/ground-analysis',
                setUrl2: '/environment/ground-realtime'
            })
        }
    }

    render() {
        const { EnvironmentStore } = this.props;
        const { Option } = Select;


        const visibleConfirm = this.state.chartVisible;
        const dateFormat = 'YYYY-MM-DD';
        return (
            <>
                <div id="container" className={(this.state.fullscreenType == true) ? 'full' : 'origin'}>
                    <div>
                        <h3>Sensor Map(지반변위,DPM,Gas 공통)</h3>
                        <Button type={"primary"} className="btn_full" onClick={this.onClickEventFullScreen}>
                            {/*@ts-ignore*/}
                            <Icon type='fullscreen'/>{(this.state.fullscreenType == true) ? "OriginScreen" : "FullScreen"}
                        </Button>
                        <ul className="condition">
                            <li>
                                <Select value={EnvironmentStore.getSelectData} onChange={this.changeSensor}>
                                    <Option value={'all'}>전체</Option>
                                    <Option value={'C009-002'}>DPM센서</Option>
                                    <Option value={'C009-003'}>가스센서</Option>
                                    <Option value={'C009-004'}>지반변위</Option>
                                </Select>
                            </li>
                        </ul>

                        <Card className="sensor_map mt-1vw">
                            <div ref={this._mount}
                            >
                            </div>
                            <ul className="legend">
                                <li className="dpm">DPM</li>
                                <li className="gro">지반변위</li>
                                <li className="gas">Gas</li>
                            </ul>
                        </Card>

                    </div>
                    {(EnvironmentStore.getSensorInfo.length === 0) ? null : <Card
                        title={EnvironmentStore.getSensorInfo.sensorId + 'where?'}
                    >
                        <p>종류 : {EnvironmentStore.getSensorInfo.sensorName}</p>
                        <p>위치</p>
                        <p>x:{EnvironmentStore.getSensorInfo.x}</p>
                        <p>y:{EnvironmentStore.getSensorInfo.y}</p>
                        <p>z:{EnvironmentStore.getSensorInfo.z}</p>
                        <p>최종 계측 시각 : where?</p>

                        <Link to={this.state.setUrl2}><Button>실시간 모니터링</Button></Link>
                        <Link to={this.state.setUrl}><Button>계측기록 확인</Button></Link>
                    </Card> }
                </div>
            </>
        )
    }
}

export default MonitoringSenserMap;
