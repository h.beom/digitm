import React, {Component} from 'react';
import {Button, Card, DatePicker, Input, message, Select, Statistic, Table} from "antd";
import autobind from "autobind-decorator";
import Icon from "antd/es/icon";
import {inject, observer} from "mobx-react";

import {CaretLeftOutlined, CaretRightOutlined} from "@ant-design/icons/lib";
import moment from "moment";
import {LineChart, StateManagerProvider} from "@opd/g2plot-react";
import {EnvironmentSensorColumn} from "../../components/tableGrid/Columns";
import StringUtil from "../../util/StringUtil";
import {bsApi} from "../../common/Api";
import EnvironmentStore from "../../stores/EnvironmentStore";

@inject('EnvironmentStore')
@observer
class GasAnalysis extends Component<any, any> {
    constructor(props: any) {
        super(props);
        this.state = {
            loading: true,
            selectDate: StringUtil.formatDate(),
            selectCurrentTimeIndex: 0,
            selectedRowKeys: [],
            fullscreenType: false,
            interactions: [],
            pointVisible: false,
        }
    }

    async componentDidMount() {
        const {EnvironmentStore} = this.props;
        const {selectDate} = this.state;

        // 공통코드 지반변위센서 코드 -> C009-003
        let sensorData = await bsApi.get('/environment/sensors', {
            params: {
                division: 'C009-003'
            }
        });

        if (sensorData.data.data === null || sensorData.data.status === 'ERROR') {
            message.warning(sensorData.data.message);
        } else {
            EnvironmentStore.setGaSensorTable(sensorData.data.data.sensors);
            if (sensorData.data.data.sensors.length > 0) {
                EnvironmentStore.setGaSensorId(sensorData.data.data.sensors[0].id);
                this.setState({
                    selectedRowKeys: [sensorData.data.data.sensors[0].id]
                });

                // 최근 한달 데이터 가져오기
                let analysisData = await bsApi.get('/environment/gas-analysis', {
                    params: {
                        sensorId: EnvironmentStore.getGaSensorId,
                        date: selectDate
                    }
                });

                if (analysisData.data.data === null || analysisData.data.status === 'ERROR') {
                    message.warning(analysisData.data.message);
                } else {
                    EnvironmentStore.setGaLogs(analysisData.data.data.logs);
                    if (analysisData.data.data.logs.length > 0) {
                        // 마지막 목록의 계측 시간
                        let lastIndex: number = analysisData.data.data.logs.length - 1;
                        EnvironmentStore.setGaLogsCard(lastIndex);
                        EnvironmentStore.setGaLogsCurrentTimeIndex(lastIndex);

                        this.setState({
                            selectCurrentTimeIndex: EnvironmentStore.getGaLogsCurrentTimeIndex
                        });

                        // 차트 목록
                        let chartData = await bsApi.get('/environment/gas-analysis/chart', {
                            params: {
                                sensorId: EnvironmentStore.getGaSensorId,
                                date: selectDate
                            }
                        });

                        if (chartData.data.data.charts.length > 1000) {
                            this.interactionConfig(true);
                        } else {
                            this.interactionConfig(false);
                        }

                        if (chartData.data.data.charts.length > 100) {
                            this.pointVisibleConfig(false);
                        } else {
                            this.pointVisibleConfig(true);
                        }

                        if (chartData.data.data.charts.length > 0) {
                            EnvironmentStore.setGaLogsChart([]);
                            EnvironmentStore.setGaLogsChart(chartData.data.data.charts);
                        }
                    }
                }
            }
        }

        this.setState({
            loading: false
        });
    }

    @autobind
    async selectSensorChange(sensorId: number) {
        const {EnvironmentStore} = this.props;
        const {selectDate} = this.state;

        this.setState({
            loading: true,
            selectedRowKeys: [sensorId]
        });

        EnvironmentStore.setGaSensorId(sensorId);

        // 최근 한달 데이터 가져오기
        let analysisData = await bsApi.get('/environment/gas-analysis', {
            params: {
                sensorId: EnvironmentStore.getGaSensorId,
                date: selectDate
            }
        });

        if (analysisData.data.data === null || analysisData.data.status === 'ERROR') {
            message.warning(analysisData.data.message);
        } else {
            EnvironmentStore.setGaLogs(analysisData.data.data.logs);
            if (analysisData.data.data.logs.length > 0) {
                // 마지막 목록의 계측 시간
                let lastIndex: number = analysisData.data.data.logs.length - 1;
                EnvironmentStore.setGaLogsCard(lastIndex);
                EnvironmentStore.setGaLogsCurrentTimeIndex(lastIndex);

                this.setState({
                    selectCurrentTimeIndex: EnvironmentStore.getGaLogsCurrentTimeIndex
                });

                // 차트 목록
                let chartData = await bsApi.get('/environment/gas-analysis/chart', {
                    params: {
                        sensorId: EnvironmentStore.getGaSensorId,
                        date: selectDate
                    }
                });

                if (chartData.data.data.charts.length > 1000) {
                    this.interactionConfig(true);
                } else {
                    this.interactionConfig(false);
                }

                if (chartData.data.data.charts.length > 100) {
                    this.pointVisibleConfig(false);
                } else {
                    this.pointVisibleConfig(true);
                }

                if (chartData.data.data.charts.length > 0) {
                    EnvironmentStore.setGaLogsChart([]);
                    EnvironmentStore.setGaLogsChart(chartData.data.data.charts);
                }
            }
        }

        this.setState({
            loading: false
        });
    }
    /**
     * @description 날짜 이벤트핸들링 메서드
     **/
    @autobind
    async dateSelect(date: moment.Moment | null, dateString: string) {
        const {EnvironmentStore} = this.props;
        const {selectDate} = this.state;

        this.setState({
            selectDate: dateString,
            loading:true
        })

        let analysisData = await bsApi.get('/environment/gas-analysis', {
            params: {
                sensorId: EnvironmentStore.getGaSensorId,
                date: dateString
            }
        });

        if (analysisData.data.data === null || analysisData.data.status === 'ERROR') {
            message.warning(analysisData.data.message);
        } else {
            EnvironmentStore.setGaLogs(analysisData.data.data.logs);
            if (analysisData.data.data.logs.length > 0) {
                // 마지막 목록의 계측 시간
                let lastIndex: number = analysisData.data.data.logs.length - 1;
                EnvironmentStore.setGaLogsCard(lastIndex);
                EnvironmentStore.setGaLogsCurrentTimeIndex(lastIndex);

                this.setState({
                    selectCurrentTimeIndex: EnvironmentStore.getGaLogsCurrentTimeIndex
                });

                // 차트 목록
                let chartData = await bsApi.get('/environment/gas-analysis/chart', {
                    params: {
                        sensorId: EnvironmentStore.getGaSensorId,
                        date: selectDate
                    }
                });

                if (chartData.data.data.charts.length > 1000) {
                    this.interactionConfig(true);
                } else {
                    this.interactionConfig(false);
                }

                if (chartData.data.data.charts.length > 100) {
                    this.pointVisibleConfig(false);
                } else {
                    this.pointVisibleConfig(true);
                }

                if (chartData.data.data.charts.length > 0) {
                    EnvironmentStore.setGaLogsChart([]);
                    EnvironmentStore.setGaLogsChart(chartData.data.data.charts);
                }
            }
        }

        this.setState({
            loading:false
        })
    }

    /**
     * @description 오늘날짜 이후로는 선택 불가한 메서드
     * @param current
     */
    @autobind
    disabledDate(current: any) {
        return current && current > moment().endOf('day');
    }

    /**
     * @description 날짜 좌, 우 이벤트핸들링 메서드
     **/
    @autobind
    async arrowDateCtr(param: string) {
        const {EnvironmentStore} = this.props;
        const {selectDate} = this.state;

        this.setState({
            loading: true
        });


        let arrowDate: string = selectDate;

        if (param === 'left') {
            arrowDate = StringUtil.setDay(selectDate, -1);
            this.setState({
                selectDate: arrowDate
            });
        }

        if (param === 'right') {
            arrowDate = StringUtil.setDay(selectDate, 1);
            this.setState({
                selectDate: arrowDate
            });
        }

        let analysisData = await bsApi.get('/environment/gas-analysis', {
            params: {
                sensorId: EnvironmentStore.getGaSensorId,
                date: arrowDate
            }
        });

        if (analysisData.data.data === null || analysisData.data.status === 'ERROR') {
            message.warning(analysisData.data.message);
        } else {
            EnvironmentStore.setGaLogs(analysisData.data.data.logs);
            if (analysisData.data.data.logs.length > 0) {
                // 마지막 목록의 계측 시간
                let lastIndex: number = analysisData.data.data.logs.length - 1;
                EnvironmentStore.setGaLogsCard(lastIndex);
                EnvironmentStore.setGaLogsCurrentTimeIndex(lastIndex);

                this.setState({
                    selectCurrentTimeIndex: EnvironmentStore.getGaLogsCurrentTimeIndex
                });

                // 차트 목록
                let chartData = await bsApi.get('/environment/gas-analysis/chart', {
                    params: {
                        sensorId: EnvironmentStore.getGaSensorId,
                        date: selectDate
                    }
                });

                if (chartData.data.data.charts.length > 1000) {
                    this.interactionConfig(true);
                } else {
                    this.interactionConfig(false);
                }

                if (chartData.data.data.charts.length > 100) {
                    this.pointVisibleConfig(false);
                } else {
                    this.pointVisibleConfig(true);
                }

                if (chartData.data.data.charts.length > 0) {
                    EnvironmentStore.setGaLogsChart(chartData.data.data.charts);
                }
            }
        }

        this.setState({
            loading: false
        });
    }

    /**
     * @description 계측시간 좌, 우 이벤트핸들링 메서드
     **/
    @autobind
    async arrowCtr(param: string) {
        const {EnvironmentStore} = this.props;

        if (param === 'left') {
            if (EnvironmentStore.getGaLogsCurrentTimeIndex != 0) {
                let leftIndex = EnvironmentStore.getGaLogsCurrentTimeIndex - 1;
                EnvironmentStore.setGaLogsCard(leftIndex);
                EnvironmentStore.setGaLogsCurrentTimeIndex(leftIndex);

                this.setState({
                    selectCurrentTimeIndex: leftIndex
                });
            }
        }
        if (param === 'right') {
            if (EnvironmentStore.getGaLogsCurrentTimeIndex != EnvironmentStore.getGaLogs.length - 1) {
                let rightIndex = EnvironmentStore.getGaLogsCurrentTimeIndex + 1;
                EnvironmentStore.setGaLogsCard(rightIndex);
                EnvironmentStore.setGaLogsCurrentTimeIndex(rightIndex);

                this.setState({
                    selectCurrentTimeIndex: rightIndex
                });
            }
        }
    }

    @autobind
    async selectCurrentTimeIndex(value: number) {
        const {EnvironmentStore} = this.props;

        EnvironmentStore.setGaLogsCurrentTimeIndex(value);
        EnvironmentStore.setGaLogsCard(value);

        this.setState({
            selectCurrentTimeIndex: value
        });
    }

    @autobind
    async selectChartCurrentTimeIndex(date: string) {
        const {EnvironmentStore} = this.props;
        for (let i = 0; i < EnvironmentStore.getGaLogs.length; i++) {
            let item = EnvironmentStore.getGaLogs[i];
            let compare = item.date.substr(5,5) + ' ' + item.time.substr(0,5)
            if (compare === date) {
                this.selectCurrentTimeIndex(i);
            }
        }
    }

    /**
     * @description 버튼 클릭시 컨텐츠 화면 풀스크린 모드
     */
    @autobind
    private onClickEventFullScreen() {
        const type: boolean = !this.state.fullscreenType;
        const callBack: Function = this.state.renderEvent;
        this.setState({fullscreenType: type}, function () {
            if (callBack != undefined) {
                callBack(type);
            }
        });
    }

    interactionConfig(use: boolean) {
        if (use) {
            this.setState({
                interactions: [
                    {
                        type: 'slider',
                        cfg: {
                            start: 0.0,
                            end: 1.0,
                        },
                    }
                ]
            })
        } else {
            this.setState({
                interactions: []
            });
        }
    }

    pointVisibleConfig(use: boolean) {
        if (use) {
            this.setState({
                pointVisible: true,
            })
        } else {
            this.setState({
                pointVisible: false,
            })
        }
    }

    render() {
        const {EnvironmentStore} = this.props;
        const {loading, selectDate, selectedRowKeys, selectCurrentTimeIndex, interactions, pointVisible} = this.state;
        const dateFormat = 'YYYY-MM-DD';
        const {Option} = Select;
        const config: any = {
            padding: 'auto',
            forceFit: true,
            data: EnvironmentStore.getGaLogsChart,
            xField: 'date',
            yField: 'value',
            yAxis: {
                title: {
                    visible: false,
                },
                label: {
                    style: {
                        fill: '#fff',
                        fillOpacity: 0.3,
                        fontSize: 14,
                    },
                },

            },
            xAxis: {
                title: {
                    visible: false,
                },
                label: {
                    style: {
                        fill: '#fff',
                        fillOpacity: 0.3,
                        fontSize: 14,
                    },
                },
            },
            legend: {
                visible: false,
                flipPage: false,
                position: 'bottom-right',
                text: {
                    style: {
                        fill: '#fff',
                        fontSize: 15,
                    },
                },
            },
            tooltip: {
                visible: true,
            },
            color: ['#ff9845', '#d9bb45', '#5ebf3','#5ad8a6', '#1e9493', '#4fc6de'],
            point: {
                visible: pointVisible,
                size: 5,
                fill: ['#ff9845', '#d9bb45', '#5ebf3','#5ad8a6', '#1e9493', '#4fc6de'],
                shape: 'circle',
                style: {
                  stroke: false,
                },
            },
            label: {
                visible: false,
                style: {
                    fill: '#fff',
                    stroke: false,
                    fontSize: 15,
                },
            },
            seriesField: 'type',
            responsive: true,
            interactions: interactions,
        };

        const rowSelection = {
            selectedRowKeys: selectedRowKeys,
            onChange: (selectedRowKeys: any, selectedRows: any) => {
                this.selectSensorChange(selectedRowKeys[0]);
            }
        }

        const stateManager = {
            setState: [
                {
                    event: 'point:click',
                    state: (e: any, index: number) => {
                        const origion = e.target.get('origin').data
                        const state = { name : 'date', exp: origion.date}
                        this.selectChartCurrentTimeIndex(origion.date);
                        return state
                    }
                }
            ],
            onStateChange: [
                {
                    name: 'date',
                    callback: (d: any, plot: any) => {}
                }
            ]
        }

        // @ts-ignore
        return (

            <div id="container" className={(this.state.fullscreenType == true) ? 'full' : 'origin'}>
                <div>
                    <h3>일자별 유해가스 분석</h3>
                    <Button type={"primary"} className="btn_full" onClick={this.onClickEventFullScreen}>
                        {/*@ts-ignore*/}
                        <Icon type='fullscreen'/>{(this.state.fullscreenType == true) ? "OriginScreen" : "FullScreen"}
                    </Button>
                    <div className="input_arrow">
                        {/*@ts-ignore*/}
                        <CaretLeftOutlined  onClick={() => this.arrowCtr('left')}/><DatePicker value={moment(selectDate)} format={dateFormat} onChange={this.dateSelect}  disabledDate={this.disabledDate} allowClear={false}/><CaretRightOutlined onClick={() => this.arrowCtr('right')}/>
                    </div>
                    <Card>
                    <Table className="mt-1vw"
                        rowSelection={{
                            type: 'radio',
                            ...rowSelection
                        }}
                        columns={EnvironmentSensorColumn}
                        dataSource={EnvironmentStore.getGaSensorTable}
                        onRow={(record, rowIndex) => ({
                            onDoubleClick: () => {
                                if (record.key !== selectedRowKeys[0]) {
                                    this.selectSensorChange(record.key)
                                }
                            }
                        })}
                        pagination={false}/>
                    </Card>

                    <ul className="condition mt-1vw">
                        <li className="ant-col-24">
                            <label>계측 기록</label>
                            <div className="input_arrow">
                                {/*@ts-ignore*/}
                                <CaretLeftOutlined onClick={() => this.arrowCtr('left')}/>
                                <Select value={selectCurrentTimeIndex} onChange={this.selectCurrentTimeIndex}>
                                    {
                                        // @ts-ignore
                                        EnvironmentStore.getGaLogs.map((value: any, index: number) => {
                                            return <Option value={index}>{value.time}</Option>
                                        })}
                                </Select>
                                {/*@ts-ignore*/}
                                <CaretRightOutlined onClick={() => this.arrowCtr('right')}/>
                            </div>
                        </li>
                        <li className="ant-col-24">
                            <div className="overview">

                                <Statistic className="step1" //step1: 파란색, step2: 주황색, step3: 빨간색 
                                    title="온도"
                                    value={EnvironmentStore.getGaLogsCard.temp}
                                    suffix="C"
                                />
                                <Statistic className="step2"
                                    title="습도"
                                    value={EnvironmentStore.getGaLogsCard.humidity}
                                    suffix="%"
                                />
                                <Statistic className="step3"
                                    title="co2"
                                    value={EnvironmentStore.getGaLogsCard.co2}
                                    suffix="ppm"
                                />
                                <Statistic
                                    title="co"
                                    value={EnvironmentStore.getGaLogsCard.co}
                                    suffix="ppm"
                                />
                                <Statistic
                                    title="NO"
                                    value={EnvironmentStore.getGaLogsCard.no}
                                    suffix="ppm"
                                />
                                <Statistic
                                    title="NO2"
                                    value={EnvironmentStore.getGaLogsCard.no2}
                                    suffix="ppm"
                                />
                                <Statistic 
                                    title="SO2"
                                    value={EnvironmentStore.getGaLogsCard.so2}
                                    suffix="ppm"
                                />
                            </div>
                        </li>
                    </ul>
                    <Card className="mt-2vw" loading={loading}>
                        <label>유해가스 그래프</label>
                        <StateManagerProvider>
                            <LineChart {...config} className="chart" stateManager={stateManager} />
                        </StateManagerProvider>
                    </Card>
                </div>
            </div>
        );
    };
}

export default GasAnalysis;
