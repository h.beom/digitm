import React, {Component} from 'react';
import {Button, Card, DatePicker, Input, message, Select, Statistic, Table} from "antd";
import autobind from "autobind-decorator";
import Icon from "antd/es/icon";
import {inject, observer} from "mobx-react";

import {CaretLeftOutlined, CaretRightOutlined} from "@ant-design/icons/lib";
import moment from "moment";
import {LineChart, StateManagerProvider} from "@opd/g2plot-react";
import {EnvironmentSensorColumn} from "../../components/tableGrid/Columns";
import StringUtil from "../../util/StringUtil";
import {bsApi} from "../../common/Api";
import EnvironmentStore from "../../stores/EnvironmentStore";

@inject('EnvironmentStore')
@observer
class GasRealtime extends Component<any, any> {
    constructor(props: any) {
        super(props);
        this.state = {
            loading: true,
            selectDate: StringUtil.formatDate(),
            selectCurrentTimeIndex: 0,
            selectedRowKeys: [],
            fullscreenType: false,
            intervalId: null,
            interactions: [],
            pointVisible: false,
        }
    }

    async componentDidMount() {
        const {EnvironmentStore} = this.props;

        // 공통코드 지반변위센서 코드 -> C009-003
        let sensorData = await bsApi.get('/environment/sensors', {
            params: {
                division: 'C009-003'
            }
        });

        if (sensorData.data.data === null || sensorData.data.status === 'ERROR') {
            message.warning(sensorData.data.message);
        } else {
            EnvironmentStore.setGrSensorTable(sensorData.data.data.sensors);
            if (sensorData.data.data.sensors.length > 0) {
                EnvironmentStore.setGrSensorId(sensorData.data.data.sensors[0].id);
                this.setState({
                    selectedRowKeys: [sensorData.data.data.sensors[0].id]
                });
                
                // 최근 24시간 데이터 가져오기
                let realtimeData = await bsApi.get('/environment/gas-realtime', {
                    params: {
                        sensorId: EnvironmentStore.getGrSensorId
                    }
                });

                if (realtimeData.data.data === null || realtimeData.data.status === 'ERROR') {
                    message.warning(realtimeData.data.message);
                } else {
                    EnvironmentStore.setGrLogs(realtimeData.data.data.logs);
                    if (realtimeData.data.data.logs.length > 0) {
                        // 마지막 목록의 계측 시간
                        let lastIndex: number = realtimeData.data.data.logs.length - 1;
                        EnvironmentStore.setGrLogsCard(lastIndex);
                        EnvironmentStore.setGrLogsCurrentTimeIndex(lastIndex);

                        this.setState({
                            selectCurrentTimeIndex: EnvironmentStore.getGrLogsCurrentTimeIndex
                        });

                        // 차트 목록
                        let chartData = await bsApi.get('/environment/gas-realtime/chart', {
                            params: {
                                sensorId: EnvironmentStore.getGrSensorId
                            }
                        });

                        if (chartData.data.data.charts.length > 1000) {
                            this.interactionConfig(true);
                        } else {
                            this.interactionConfig(false);
                        }

                        if (chartData.data.data.charts.length > 100) {
                            this.pointVisibleConfig(false);
                        } else {
                            this.pointVisibleConfig(true);
                        }

                        if (chartData.data.data.charts.length > 0) {
                            EnvironmentStore.setGrLogsChart(chartData.data.data.charts);
                        }
                    }
                }
            }
        }

        const interval = setInterval(async () => {
            this.setState({
                loading: true
            });

            // 최근 24시간 데이터 가져오기
            let realtimeData = await bsApi.get('/environment/gas-realtime', {
                params: {
                    sensorId: EnvironmentStore.getGrSensorId
                }
            });

            if (realtimeData.data.data === null || realtimeData.data.status === 'ERROR') {
                message.warning(realtimeData.data.message);
            } else {
                EnvironmentStore.setGrLogs(realtimeData.data.data.logs);
                if (realtimeData.data.data.logs.length > 0) {
                    // 마지막 목록의 계측 시간
                    let lastIndex: number = realtimeData.data.data.logs.length - 1;
                    EnvironmentStore.setGrLogsCard(lastIndex);
                    EnvironmentStore.setGrLogsCurrentTimeIndex(lastIndex);

                    this.setState({
                        selectCurrentTimeIndex: EnvironmentStore.getGrLogsCurrentTimeIndex
                    });

                    // 차트 목록
                    let chartData = await bsApi.get('/environment/gas-realtime/chart', {
                        params: {
                            sensorId: EnvironmentStore.getGrSensorId
                        }
                    });

                    if (chartData.data.data.charts.length > 1000) {
                        this.interactionConfig(true);
                    } else {
                        this.interactionConfig(false);
                    }

                    if (chartData.data.data.charts.length > 100) {
                        this.pointVisibleConfig(false);
                    } else {
                        this.pointVisibleConfig(true);
                    }

                    if (chartData.data.data.charts.length > 0) {
                        EnvironmentStore.setGrLogsChart(chartData.data.data.charts);
                    }
                }
            }

            this.setState({
                loading: false
            });
        }, 60000);

        this.setState({
            loading: false,
            intervalId: interval
        });
    }

    @autobind
    async selectSensorChange(sensorId: number) {
        const {EnvironmentStore} = this.props;

        this.setState({
            loading: true,
            selectedRowKeys: [sensorId]
        });

        // 지반변위 센서 목록 가져오기
        EnvironmentStore.setGrSensorId(sensorId);

        // 최근 24시간 데이터 가져오기
        let realtimeData = await bsApi.get('/environment/gas-realtime', {
            params: {
                sensorId: EnvironmentStore.getGrSensorId
            }
        });

        if (realtimeData.data.data === null || realtimeData.data.status === 'ERROR') {
            message.warning(realtimeData.data.message);
        } else {
            EnvironmentStore.setGrLogs(realtimeData.data.data.logs);
            if (realtimeData.data.data.logs.length > 0) {
                // 마지막 목록의 계측 시간
                let lastIndex: number = realtimeData.data.data.logs.length - 1;
                EnvironmentStore.setGrLogsCard(lastIndex);
                EnvironmentStore.setGrLogsCurrentTimeIndex(lastIndex);

                this.setState({
                    selectCurrentTimeIndex: EnvironmentStore.getGrLogsCurrentTimeIndex
                });

                // 차트 목록
                let chartData = await bsApi.get('/environment/gas-realtime/chart', {
                    params: {
                        sensorId: EnvironmentStore.getGrSensorId
                    }
                });

                if (chartData.data.data.charts.length > 1000) {
                    this.interactionConfig(true);
                } else {
                    this.interactionConfig(false);
                }

                if (chartData.data.data.charts.length > 100) {
                    this.pointVisibleConfig(false);
                } else {
                    this.pointVisibleConfig(true);
                }

                if (chartData.data.data.charts.length > 0) {
                    EnvironmentStore.setGrLogsChart(chartData.data.data.charts);
                }
            }
        }

        this.setState({
            loading: false
        });
    }
    @autobind
    async arrowCtr(param: string) {
        const {EnvironmentStore} = this.props;

        if (param === 'left') {
            if (EnvironmentStore.getGrLogsCurrentTimeIndex != 0) {
                let leftIndex = EnvironmentStore.getGrLogsCurrentTimeIndex - 1;
                EnvironmentStore.setGrLogsCard(leftIndex);
                EnvironmentStore.setGrLogsCurrentTimeIndex(leftIndex);

                this.setState({
                    selectCurrentTimeIndex: leftIndex
                });
            } else {
                message.warning('첫번째 계측기록 입니다.');
            }
        }
        if (param === 'right') {
            if (EnvironmentStore.getGrLogsCurrentTimeIndex != EnvironmentStore.getGrLogs.length - 1) {
                let rightIndex = EnvironmentStore.getGrLogsCurrentTimeIndex + 1;
                EnvironmentStore.setGrLogsCard(rightIndex);
                EnvironmentStore.setGrLogsCurrentTimeIndex(rightIndex);

                this.setState({
                    selectCurrentTimeIndex: rightIndex
                });
            } else {
                message.warning('마지막 계측기록 입니다.');
            }
        }
    }

    @autobind
    async selectCurrentTimeIndex(value: number) {
        const {EnvironmentStore} = this.props;

        EnvironmentStore.setGrLogsCurrentTimeIndex(value);
        EnvironmentStore.setGrLogsCard(value);

        this.setState({
            selectCurrentTimeIndex: value
        });
    }

    @autobind
    async selectChartCurrentTimeIndex(date: string) {
        const {EnvironmentStore} = this.props;
        for (let i = 0; i < EnvironmentStore.getGrLogs.length; i++) {
            let item = EnvironmentStore.getGrLogs[i];
            let compare = item.time.substr(0,5)
            if (compare === date) {
                this.selectCurrentTimeIndex(i);
            }
        }
    }

    /**
     * @description 버튼 클릭시 컨텐츠 화면 풀스크린 모드
     */
    @autobind
    private onClickEventFullScreen() {
        const type: boolean = !this.state.fullscreenType;
        const callBack: Function = this.state.renderEvent;
        this.setState({fullscreenType: type}, function () {
            if (callBack != undefined) {
                callBack(type);
            }
        });
    }

    componentWillUnmount() {
        // @ts-ignore
        clearInterval(this.state.intervalId);
    }

    interactionConfig(use: boolean) {
        if (use) {
            this.setState({
                interactions: [
                    {
                        type: 'slider',
                        cfg: {
                            start: 0.0,
                            end: 1.0,
                        },
                    }
                ]
            })
        } else {
            this.setState({
                interactions: []
            });
        }
    }

    pointVisibleConfig(use: boolean) {
        if (use) {
            this.setState({
                pointVisible: true,
            })
        } else {
            this.setState({
                pointVisible: false,
            })
        }
    }

    render() {
        const {EnvironmentStore} = this.props;
        const {loading, selectDate, selectedRowKeys, selectCurrentTimeIndex, interactions, pointVisible} = this.state;
        const {Option} = Select;
        const config: any = {
            padding: 'auto',
            forceFit: true,
            data: EnvironmentStore.getGrLogsChart,
            xField: 'date',
            yField: 'value',
            yAxis: {
                title: {
                    visible: false,
                },
                label: {
                    style: {
                        fill: '#fff',
                        fillOpacity: 0.3,
                        fontSize: 14,
                    },
                },

            },
            xAxis: {
                title: {
                    visible: false,
                },
                label: {
                    style: {
                        fill: '#fff',
                        fillOpacity: 0.3,
                        fontSize: 14,
                    },
                },
            },
            legend: {
                visible: false,
                flipPage: false,
                position: 'bottom-right',
                text: {
                    style: {
                        fill: '#fff',
                        fontSize: 15,
                    },
                },
            },
            tooltip: {
                visible: true,
            },
            color: ['#ff9845', '#d9bb45', '#5ebf3','#5ad8a6', '#1e9493', '#4fc6de'],
            point: {
                visible: pointVisible,
                size: 5,
                fill: ['#ff9845', '#d9bb45', '#5ebf3','#5ad8a6', '#1e9493', '#4fc6de'],
                shape: 'circle',
                style: {
                  stroke: false,
                },
            },
            label: {
                visible: false,
                style: {
                    fill: '#fff',
                    stroke: false,
                    fontSize: 15,
                },
            },
            seriesField: 'type',
            responsive: true,
            interactions: interactions,
        };

        const rowSelection = {
            selectedRowKeys: selectedRowKeys,
            onChange: (selectedRowKeys: any, selectedRows: any) => {
                this.selectSensorChange(selectedRowKeys[0]);
            }
        }

        const stateManager = {
            setState: [
                {
                    event: 'point:click',
                    state: (e: any, index: number) => {
                        const origion = e.target.get('origin').data
                        const state = { name : 'date', exp: origion.date}
                        this.selectChartCurrentTimeIndex(origion.date);
                        return state
                    }
                }
            ],
            onStateChange: [
                {
                    name: 'date',
                    callback: (d: any, plot: any) => {}
                }
            ]
        }

        // @ts-ignore
        return (

            <div id="container" className={(this.state.fullscreenType == true) ? 'full' : 'origin'}>
                <div>
                    <h3>실시간 유해가스 모니터링</h3>
                    <Button type={"primary"} className="btn_full" onClick={this.onClickEventFullScreen}>
                        {/*@ts-ignore*/}
                        <Icon type='fullscreen'/>{(this.state.fullscreenType == true) ? "OriginScreen" : "FullScreen"}
                    </Button>
                    <div className="input_arrow">
                        <Input value={selectDate}/>
                    </div>
                    <Card>
                    <Table className="mt-1vw"
                        rowSelection={{
                            type: 'radio',
                            ...rowSelection
                        }}
                        columns={EnvironmentSensorColumn}
                        dataSource={EnvironmentStore.getGrSensorTable}
                        onRow={(record, rowIndex) => ({
                            onDoubleClick: () => {
                                if (record.key !== selectedRowKeys[0]) {
                                    this.selectSensorChange(record.key)
                                }
                            }
                        })}
                        pagination={false}/>
                    </Card>

                    <ul className="condition mt-1vw">
                        <li className="ant-col-24">
                            <label>실시간 계측 기록</label>
                            <div className="input_arrow">
                                {/*@ts-ignore*/}
                                <CaretLeftOutlined onClick={() => this.arrowCtr('left')}/>
                                <Select value={selectCurrentTimeIndex} onChange={this.selectCurrentTimeIndex}>
                                    {
                                        // @ts-ignore
                                        EnvironmentStore.getGrLogs.map((value: any, index: number) => {
                                            return <Option value={index}>{value.time}</Option>
                                        })}
                                </Select>
                                {/*@ts-ignore*/}
                                <CaretRightOutlined onClick={() => this.arrowCtr('right')}/>
                            </div>
                        </li>
                        <li className="ant-col-24">
                            <div className="overview">

                                <Statistic className="step1" //step1: 파란색, step2: 주황색, step3: 빨간색(기준점을 넘어가면 경고)
                                    title="온도"
                                    value={EnvironmentStore.getGrLogsCard.temp}
                                    suffix="C"
                                />
                                <Statistic className="step2"
                                    title="습도"
                                    value={EnvironmentStore.getGrLogsCard.humidity}
                                    suffix="%"
                                />
                                <Statistic className="step3"
                                    title="co2"
                                    value={EnvironmentStore.getGrLogsCard.co2}
                                    suffix="ppm"
                                />
                                <Statistic
                                    title="co"
                                    value={EnvironmentStore.getGrLogsCard.co}
                                    suffix="ppm"
                                />
                                <Statistic
                                    title="NO"
                                    value={EnvironmentStore.getGrLogsCard.no}
                                    suffix="ppm"
                                />
                                <Statistic
                                    title="NO2"
                                    value={EnvironmentStore.getGrLogsCard.no2}
                                    suffix="ppm"
                                />
                                <Statistic
                                    title="SO2"
                                    value={EnvironmentStore.getGrLogsCard.so2}
                                    suffix="ppm"
                                />
                            </div>
                        </li>
                    </ul>
                    <Card className="mt-2vw" loading={loading}>
                        <label>실시간 유해가스 그래프</label>
                        <StateManagerProvider>
                            <LineChart {...config} className="chart" stateManager={stateManager} />
                        </StateManagerProvider>
                    </Card>
                </div>
            </div>
        );
    };
}

export default GasRealtime;
