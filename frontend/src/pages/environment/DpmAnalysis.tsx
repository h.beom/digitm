import React, {Component} from 'react';
import {Button, Card, DatePicker, message, Select, Statistic, Table} from "antd";
import {bsApi} from "../../common/Api";
import autobind from "autobind-decorator";
import Icon from "antd/es/icon";
import StringUtil from "../../util/StringUtil";
import {observer} from "mobx-react";


import {CaretLeftOutlined, CaretRightOutlined} from "@ant-design/icons/lib";
import moment from "moment";
import {LineChart} from "@opd/g2plot-react";

@observer
class DpmAnalysis extends Component<any, any> {

    //# region == 생성자 ==
    constructor(props: any) {
        super(props);
        this.state = {
            sensorData: [],
            logData: [],
            chartData: [],
            indicationRecordTimeData: [],
            selectedDate: StringUtil.formatDate(),
            selectedTime: "",
            selectedTableRowKeys: [],
            fullscreenType: false,
            temp:0,
            humidity:0,
            dpm10m:0,
            dpm30m:0,
            DpmAnalysisColumn: [
                {
                    title: '좌표 x',
                    dataIndex: 'x',
                    key: 'x'
                }, {
                    title: '좌표 y',
                    dataIndex: 'y',
                    key: 'y'
                }, {
                    title: '좌표 z',
                    dataIndex: 'z',
                    key: 'z'
                }, {
                    title: '센서 ID',
                    dataIndex: 'sensorId',
                    key: 'sensorId'
                }
            ],
        }
    }
    //# endregion

    //# region == componentDidMount event ==
    async componentDidMount() {
        this.setSensorTable();
    }
    //# endregion

    //# region == sensor table events ==
    @autobind
    private onSensorTableRadioSelectChanged(selectedRowKey:any, selectedRows:any) {
        this.setRadio(selectedRowKey);
    }

    @autobind
    private onSensorTableRowDoubleClick(record:any, index:any) {
        this.setRadio(record.key);
    }
    //# endregion

    //# region == 계측 기록 combo events ==
    @autobind
    private onSelectedTimeChanged(time:string) {
        this.setIndicationRecord(time);
    }
    @autobind
    private arrowCtr(param: string) {

        this.setState({
            loading: true
        });

        const {selectedTime, indicationRecordTimeData} = this.state;

        let currentIndex = 0;

        indicationRecordTimeData.filter((e:any) => e.time == selectedTime)
            .map((item: any, index: number) => {
                currentIndex = item.index;
            });

        if (param == 'left') {
            if (currentIndex < indicationRecordTimeData.length) {
                let prevTime = "";

                indicationRecordTimeData.filter((e:any) => e.index == (currentIndex + 1))
                    .map((item: any, index: number) => {
                        prevTime = item.time;
                    });

                this.setIndicationRecord(prevTime);
            }
        }
        if (param == 'right') {
            if (currentIndex != 0) {
                let nextTime = "";

                indicationRecordTimeData.filter((e:any) => e.index == (currentIndex - 1))
                    .map((item: any, index: number) => {
                        nextTime = item.time;
                    });

                this.setIndicationRecord(nextTime);
            }
        }

        this.setState({
            loading: false
        });
    }
    //# endregion

    //# region == 날짜 컨트롤 events ==
    @autobind
    private onSelectedDateChanged(selectedDate:string) {
        this.setRadio(0);
    }
    //# endregion

    //# region == 버튼 클릭시 컨텐츠 화면 풀스크린 모드 ==
    /**
     * @description 버튼 클릭시 컨텐츠 화면 풀스크린 모드
     */
    @autobind
    private onClickEventFullScreen() {
        const type: boolean = !this.state.fullscreenType;
        const callBack: Function = this.state.renderEvent;
        this.setState({fullscreenType: type}, function () {
            if (callBack != undefined) {
                callBack(type);
            }
        });
    }
    //# endregion

    //# region == function ==
    @autobind
    private setIndicationRecord(time:string) {

        let {logData} = this.state;
        let temp = 0;
        let humidity = 0;
        let dpm10m = 0;
        let dpm30m = 0;

        logData.filter((e:any) => e.time == time).map((record:any, index:number) => {
            temp = record.temp;
            humidity = record.humidity;
            dpm10m = record.dpm10m;
            dpm30m = record.dpm30m;
        });

        this.setState({
            selectedTime:time,
            temp:temp,
            humidity:humidity,
            dpm10m:dpm10m,
            dpm30m:dpm30m
        });
    }

    @autobind
    private setSensorTable() {

        let param = {
            params: {
                division: 'C009-002'
            }
        };

        bsApi.get('/environment/sensors', param)
            .then((resultData) => {
                if (resultData.data.data === null || resultData.data.status === 'ERROR') {
                    message.warning(resultData.data.message);
                } else {
                    let sensorDataTemp: Array<any> = [];

                    resultData.data.data.sensors.map((item: any, index: number) => {
                        sensorDataTemp.push({
                            key: index,
                            ...item
                        })
                    })

                    this.setState({sensorData: sensorDataTemp});
                    this.setRadio(0);
                }
            })
            .catch((error) => {
                message.error(error);
            });
    }

    @autobind
    private setRadio(rowKey:number) {
        let {selectedTableRowKeys} = this.state;
        selectedTableRowKeys = [];
        selectedTableRowKeys.push(rowKey);
        this.setState({selectedTableRowKeys:selectedTableRowKeys});
        this.initLogData(rowKey)
    }

    @autobind
    private initLogData(rowKey:number) {
        let {sensorData, selectedDate} = this.state;
        let sensorId = "";

        sensorData.filter((e:any) => e.key == rowKey).map((record:any, index:number) => {
            sensorId = record.id;
        });

        let param = {
            params: {
                sensorId: sensorId,
                date: selectedDate
            }
        };

        bsApi.get('/environment/dpm-analysis', param)
            .then((resultData) => {
                if (resultData.data.data === null || resultData.data.status === 'ERROR') {
                    message.warning(resultData.data.errorMessage);
                } else {

                    let timeTemp = Array<any>();
                    let chartTemp = Array<any>();
                    let selectedTimeTemp = "";

                    resultData.data.data.logs.map((record:any, index:number) => {
                        let date: string = record.date + ' ' + record.time.substr(0, 5);

                        chartTemp.push({
                            key:index + "dpm10",
                            index:index,
                            date: date,
                            type: '디젤미세먼지(10분)',
                            value: record.dpm10m
                        });

                        chartTemp.push({
                            key:index + "dpm30",
                            index:index,
                            date: date,
                            type: '디젤미세먼지(30분)',
                            value: record.dpm30m
                        });

                        // 계측 기록 Time 데이터 가공
                        timeTemp.push({
                            index: index,
                            time: record.time
                        })

                        if(index == 0){
                            selectedTimeTemp = record.time;
                        }
                    });

                    this.setState({logData: resultData.data.data.logs,
                        chartData: chartTemp,
                        selectedTime:selectedTimeTemp,
                        indicationRecordTimeData: timeTemp
                    });

                    this.setIndicationRecord(selectedTimeTemp);
                }
            })
            .catch((error) => {
                message.warning(error);
            });

    }
    //# endregion

    //# region == render ==
    render() {

        //# region * const *
        const {
            DpmAnalysisColumn,
            sensorData,
            indicationRecordTimeData,
            chartData,
            selectedDate,
            selectedTime,
            selectedTableRowKeys,
            temp,
            humidity,
            dpm10m,
            dpm30m
        } = this.state;

        const dateFormat = 'YYYY-MM-DD';
        const {Option} = Select;
        const chartConfig: any = {
            padding: 'auto',
            forceFit: true,
            data: chartData,
            xField: 'date',
            yField: 'value',
            yAxis: {
                title: {
                    visible: false,
                },
                label: {
                    style: {
                        fill: '#fff',
                        fillOpacity: 0.3,
                        fontSize: 14,
                    },
                },

            },
            xAxis: {
                title: {
                    visible: false,
                },
                label: {
                    style: {
                        fill: '#fff',
                        fillOpacity: 0.3,
                        fontSize: 14,
                    },
                },
            },
            legend: {
                visible: false,
                flipPage: false,
                position: 'bottom-right',
                text: {
                    style: {
                        fill: '#fff',
                        fontSize: 15,
                    },
                },
            },
            tooltip: {
                visible: false,
            },
            color: ['#ff9845', '#d9bb45', '#5ebf3','#5ad8a6', '#1e9493', '#4fc6de'],
            point: {
                visible: true,
                size: 7,
                fill: ['#ff9845', '#d9bb45', '#5ebf3','#5ad8a6', '#1e9493', '#4fc6de'],
                shape: 'circle',
                style: {
                  stroke: false,
                },
            },
            label: {
                visible: true,
                style: {
                    fill: '#fff',
                    stroke: false,
                    fontSize: 15,
                },
            },
            seriesField: 'type',
            responsive: true,
        };
        //# endregion

        // @ts-ignore
        return (
            <div id="container" className={(this.state.fullscreenType == true) ? 'full' : 'origin'}>
                <div>
                    <h3>일자별 DPM 분석</h3>
                    <Button type={"primary"} className="btn_full" onClick={this.onClickEventFullScreen}>
                        {/*@ts-ignore*/}
                        <Icon type='fullscreen'/>{(this.state.fullscreenType == true) ? "OriginScreen" : "FullScreen"}
                    </Button>
                    <div className="input_arrow">
                        {/*@ts-ignore*/}
                        <CaretLeftOutlined  onClick={() => this.arrowCtr('left')}/><DatePicker value={moment(selectedDate)} format={dateFormat} onChange={this.onSelectedDateChanged}  disabledDate={this.disabledDate} allowClear={false}/><CaretRightOutlined onClick={() => this.arrowCtr('right')}/>
                    </div>
                    <Card>
                    <Table className="mt-1vw"
                        rowSelection={{
                            selectedRowKeys:selectedTableRowKeys,
                            type:'radio',
                            onChange: (selectedRowKeys, selectedRows) => {
                                this.onSensorTableRadioSelectChanged(selectedRowKeys[0], selectedRows[0]);
                            },
                        }}
                        columns={DpmAnalysisColumn}
                        dataSource={sensorData}
                        pagination={false}

                        onRow = {(record, index): any => {
                             return {
                                onDoubleClick: () => this.onSensorTableRowDoubleClick(record, index)
                            }}
                        }
                    />
                    </Card>

                    <ul className="condition mt-1vw">
                        <li className="ant-col-24">
                            <label>계측 기록</label>
                            <div className="input_arrow">
                                {/*@ts-ignore*/}
                                <CaretLeftOutlined onClick = {() => this.arrowCtr('left')} />
                                <Select value={selectedTime} onChange={this.onSelectedTimeChanged}>
                                    {
                                        indicationRecordTimeData.map((record:any, index:number) => {
                                            return <Option value={record.time}>{record.time}</Option>
                                        })
                                    }
                                </Select>
                                {/*@ts-ignore*/}
                                <CaretRightOutlined onClick={() => this.arrowCtr('right')} />
                            </div>
                        </li>
                        <li className="ant-col-24">
                            <div className="overview">
                                <Statistic
                                    title="온도" className="step3" //step1: 파란색, step2: 주황색, step3: 빨간색 
                                    value={temp}
                                    suffix="C"
                                />
                                <Statistic
                                    title="습도"
                                    value={humidity}
                                    suffix="%"
                                />
                                <Statistic
                                    title="디젤 미세먼지(10분)"
                                    value={dpm10m}
                                    suffix="uh/m3"
                                />
                                <Statistic
                                    title="디젤 미세먼지(30분)"
                                    value={dpm30m}
                                    suffix="uh/m3"
                                />

                            </div>
                        </li>
                    </ul>
                    <Card className="mt-2vw">
                        <label>DPM 그래프</label>
                        <LineChart {...chartConfig} className="chart" />
                    </Card>
                </div>
            </div>
        );
    };
    //# endregion
}

export default DpmAnalysis;
