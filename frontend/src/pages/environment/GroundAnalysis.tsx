import React, {Component} from 'react';
import {Button, Card, DatePicker, Input, message, Select, Statistic, Table} from "antd";
import autobind from "autobind-decorator";
import Icon from "antd/es/icon";
import {inject, observer} from "mobx-react";

import {CaretLeftOutlined, CaretRightOutlined} from "@ant-design/icons/lib";
import moment from "moment";
import {LineChart, StateManagerProvider} from "@opd/g2plot-react";
import {EnvironmentSensorColumn} from "../../components/tableGrid/Columns";
import StringUtil from "../../util/StringUtil";
import {bsApi} from "../../common/Api";
import EnvironmentStore from "../../stores/EnvironmentStore";

@inject('EnvironmentStore')
@observer
class GroundAnalysis extends Component<any, any> {
    constructor(props: any) {
        super(props);
        this.state = {
            loading: true,
            selectLeftDate: StringUtil.setMonth(StringUtil.formatDate(), -1),
            selectRightDate: StringUtil.formatDate(),
            selectCurrentTimeIndex: 0,
            selectedRowKeys: [],
            fullscreenType: false,
            interactions: [],
            pointVisible: false,
        }
    }

    async componentDidMount() {
        const {EnvironmentStore} = this.props;
        const {selectLeftDate, selectRightDate} = this.state;

        // 공통코드 지반변위센서 코드 -> C009-004
        let sensorData = await bsApi.get('/environment/sensors', {
            params: {
                division: 'C009-004'
            }
        });

        if (sensorData.data.data === null || sensorData.data.status === 'ERROR') {
            message.warning(sensorData.data.message);
        } else {
            EnvironmentStore.setGmaSensorTable(sensorData.data.data.sensors);
            if (sensorData.data.data.sensors.length > 0) {
                EnvironmentStore.setGmaSensorId(sensorData.data.data.sensors[0].id);
                this.setState({
                    selectedRowKeys: [sensorData.data.data.sensors[0].id]
                });

                // 차트 목록
                let chartData = await bsApi.get('/environment/ground-monitoring-analysis/chart', {
                    params: {
                        sensorId: EnvironmentStore.getGmaSensorId,
                        from: selectLeftDate,
                        to: selectRightDate
                    }
                });

                if (chartData.data.data.charts.length > 1000) {
                    this.interactionConfig(true);
                } else {
                    this.interactionConfig(false);
                }

                if (chartData.data.data.charts.length > 100) {
                    this.pointVisibleConfig(false);
                } else {
                    this.pointVisibleConfig(true);
                }

                if (chartData.data.data.charts.length > 10000) {
                    message.warning("조회하신 기간에 데이터가 표현할 수 있는 범위를 초과하였습니다. 기간을 줄여주세요.");
                } else {
                    // 최근 한달 데이터 가져오기
                    let analysisData = await bsApi.get('/environment/ground-monitoring-analysis', {
                        params: {
                            sensorId: EnvironmentStore.getGmaSensorId,
                            from: selectLeftDate,
                            to: selectRightDate
                        }
                    });

                    if (analysisData.data.data === null || analysisData.data.status === 'ERROR') {
                        message.warning(analysisData.data.message);
                    } else {
                        EnvironmentStore.setGmaLogs(analysisData.data.data.logs);
                        if (analysisData.data.data.logs.length > 0) {
                            // 마지막 목록의 계측 시간
                            let lastIndex: number = analysisData.data.data.logs.length - 1;
                            EnvironmentStore.setGmaLogsCard(lastIndex);
                            EnvironmentStore.setGmaLogsCurrentTimeIndex(lastIndex);

                            this.setState({
                                selectCurrentTimeIndex: EnvironmentStore.getGmaLogsCurrentTimeIndex
                            });

                            // 차트 목록
                            if (chartData.data.data.charts.length > 0) {
                                EnvironmentStore.setGmaLogsChart(chartData.data.data.charts);
                            }
                        }
                    }
                }
            }
        }

        this.setState({
            loading: false
        });
    }

    @autobind
    async selectSensorChange(sensorId: number) {
        const {EnvironmentStore} = this.props;
        const {selectLeftDate, selectRightDate} = this.state;

        this.setState({
            loading: true
        });

        // 차트 목록
        let chartData = await bsApi.get('/environment/ground-monitoring-analysis/chart', {
            params: {
                sensorId: sensorId,
                from: selectLeftDate,
                to: selectRightDate
            }
        });

        if (chartData.data.data.charts.length > 1000) {
            this.interactionConfig(true);
        } else {
            this.interactionConfig(false);
        }

        if (chartData.data.data.charts.length > 100) {
            this.pointVisibleConfig(false);
        } else {
            this.pointVisibleConfig(true);
        }

        if (chartData.data.data.charts.length > 10000) {
            message.warning("조회하신 기간에 데이터가 표현할 수 있는 범위를 초과하였습니다. 기간을 줄여주세요.");
        } else {
            this.setState({
                selectedRowKeys: [sensorId]
            });

            EnvironmentStore.setGmaSensorId(sensorId);

            // 최근 한달 데이터 가져오기
            let analysisData = await bsApi.get('/environment/ground-monitoring-analysis', {
                params: {
                    sensorId: sensorId,
                    from: selectLeftDate,
                    to: selectRightDate
                }
            });

            if (analysisData.data.data === null || analysisData.data.status === 'ERROR') {
                message.warning(analysisData.data.message);
            } else {
                EnvironmentStore.setGmaLogs(analysisData.data.data.logs);
                if (analysisData.data.data.logs.length > 0) {
                    // 마지막 목록의 계측 시간
                    let lastIndex: number = analysisData.data.data.logs.length - 1;
                    EnvironmentStore.setGmaLogsCard(lastIndex);
                    EnvironmentStore.setGmaLogsCurrentTimeIndex(lastIndex);

                    this.setState({
                        selectCurrentTimeIndex: EnvironmentStore.getGmaLogsCurrentTimeIndex
                    });

                    // 차트 목록
                    if (chartData.data.data.charts.length > 0) {
                        EnvironmentStore.setGmaLogsChart(chartData.data.data.charts);
                    }
                }
            }
        }

        this.setState({
            loading: false
        });
    }

    /**
     * @description 날짜 좌 이벤트핸들링 메서드
     **/
    @autobind
    async dateLeftSelect(date: moment.Moment | null, dateString: string) {
        const {EnvironmentStore} = this.props;
        const {selectLeftDate, selectRightDate} = this.state;
        let enabled: boolean = true;
        let enabledChart: boolean = true;

        let monthSixTerm = new Date(StringUtil.setMonth(selectRightDate, -6));
        let selectDate = new Date(dateString);
        let today = new Date(StringUtil.formatDate());

        if (selectDate < monthSixTerm) {
            enabled = false;
            message.warning('기간을 6개월이내로 선택하세요.');
        }

        if (selectDate > today || selectDate > new Date(selectRightDate)) {
            enabled = false;
            message.warning('선택할 수 없는 범위입니다.');
        }

        // 차트 목록
        let chartData = await bsApi.get('/environment/ground-monitoring-analysis/chart', {
            params: {
                sensorId: EnvironmentStore.getGmaSensorId,
                from: dateString,
                to: selectRightDate
            }
        });

        if (chartData.data.data.charts.length > 1000) {
            this.interactionConfig(true);
        } else {
            this.interactionConfig(false);
        }

        if (chartData.data.data.charts.length > 100) {
            this.pointVisibleConfig(false);
        } else {
            this.pointVisibleConfig(true);
        }

        if (chartData.data.data.charts.length > 10000) {
            message.warning("조회하신 기간에 데이터가 표현할 수 있는 범위를 초과하였습니다. 기간을 줄여주세요.");
            enabledChart = false;
        }

        if (enabled && enabledChart) {
            this.setState({
                loading:true,
                selectLeftDate: dateString
            })

            let analysisData = await bsApi.get('/environment/ground-monitoring-analysis', {
                params: {
                    sensorId: EnvironmentStore.getGmaSensorId,
                    from: dateString,
                    to: selectRightDate
                }
            });

            if (analysisData.data.data === null || analysisData.data.status === 'ERROR') {
                message.warning(analysisData.data.message);
            } else {
                EnvironmentStore.setGmaLogs(analysisData.data.data.logs);
                if (analysisData.data.data.logs.length > 0) {
                    // 마지막 목록의 계측 시간
                    let lastIndex: number = analysisData.data.data.logs.length - 1;
                    EnvironmentStore.setGmaLogsCard(lastIndex);
                    EnvironmentStore.setGmaLogsCurrentTimeIndex(lastIndex);

                    this.setState({
                        selectCurrentTimeIndex: EnvironmentStore.getGmaLogsCurrentTimeIndex
                    });

                    if (chartData.data.data.charts.length > 0) {
                        EnvironmentStore.setGmaLogsChart(chartData.data.data.charts);
                    }
                }
            }

            this.setState({
                loading:false
            })
        }
    }

    /**
     * @description 날짜 우 이벤트핸들링 메서드
     **/
    @autobind
    async dateRightSelect(date: moment.Moment | null, dateString: string) {
        const {EnvironmentStore} = this.props;
        const {selectLeftDate, selectRightDate} = this.state;
        let enabled:boolean = true;
        let enabledChart: boolean = true;

        let monthSixTerm = new Date(StringUtil.setMonth(selectLeftDate, 6));
        let selectDate = new Date(dateString);
        let today = new Date(StringUtil.formatDate());

        if (selectDate > monthSixTerm) {
            enabled = false;
            message.warning('기간을 6개월이내로 선택하세요.');
        }

        if (selectDate > today || selectDate < new Date(selectLeftDate)) {
            enabled = false;
            message.warning('선택할 수 없는 범위입니다.');
        }

        // 차트 목록
        let chartData = await bsApi.get('/environment/ground-monitoring-analysis/chart', {
            params: {
                sensorId: EnvironmentStore.getGmaSensorId,
                from: selectLeftDate,
                to: dateString
            }
        });

        if (chartData.data.data.charts.length > 1000) {
            this.interactionConfig(true);
        } else {
            this.interactionConfig(false);
        }

        if (chartData.data.data.charts.length > 100) {
            this.pointVisibleConfig(false);
        } else {
            this.pointVisibleConfig(true);
        }

        if (chartData.data.data.charts.length > 10000) {
            message.warning("조회하신 기간에 데이터가 표현할 수 있는 범위를 초과하였습니다. 기간을 줄여주세요.");
            enabledChart = false;
        }

        if (enabled && enabledChart) {
            this.setState({
                selectRightDate: dateString,
                loading:true
            })

            let analysisData = await bsApi.get('/environment/ground-monitoring-analysis', {
                params: {
                    sensorId: EnvironmentStore.getGmaSensorId,
                    from: selectLeftDate,
                    to: dateString
                }
            });

            if (analysisData.data.data === null || analysisData.data.status === 'ERROR') {
                message.warning(analysisData.data.message);
            } else {
                EnvironmentStore.setGmaLogs(analysisData.data.data.logs);
                if (analysisData.data.data.logs.length > 0) {
                    // 마지막 목록의 계측 시간
                    let lastIndex: number = analysisData.data.data.logs.length - 1;
                    EnvironmentStore.setGmaLogsCard(lastIndex);
                    EnvironmentStore.setGmaLogsCurrentTimeIndex(lastIndex);

                    this.setState({
                        selectCurrentTimeIndex: EnvironmentStore.getGmaLogsCurrentTimeIndex
                    });


                    if (chartData.data.data.charts.length > 0) {
                        EnvironmentStore.setGmaLogsChart(chartData.data.data.charts);
                    }
                }
            }

            this.setState({
                loading:false
            })
        }
    }

    /**
     * @description 오늘날짜 이후로는 선택 불가한 메서드
     * @param current
     */
    @autobind
    disabledDate(current: any) {
        return current && current > moment().endOf('day');
    }

    /**
     * @description 좌측날짜 좌, 우 이벤트핸들링 메서드
     **/
    @autobind
    async arrowDate1Ctr(param: string) {
        const {EnvironmentStore} = this.props;
        const {selectLeftDate, selectRightDate} = this.state;
        let enabled: boolean = true;
        let enabledChart: boolean = true;

        let leftDate: string = StringUtil.setDay(selectLeftDate, -1);
        let rightDate: string = StringUtil.setDay(selectLeftDate, 1);

        let from: string = selectLeftDate;

        if (param === 'left') {
            let monthSixTerm = new Date(StringUtil.setMonth(selectRightDate, -6));

            if (new Date(leftDate) < monthSixTerm) {
                enabled = false;
                message.warning('기간을 6개월이내로 선택하세요.');
            }
            from = leftDate;
        }

        if (param === 'right') {
            let today = new Date(StringUtil.formatDate());

            if (new Date(rightDate) > today || new Date(rightDate) > new Date(selectRightDate)) {
                enabled = false;
                message.warning('선택할 수 없는 범위입니다.');
            }
            from = rightDate;
        }

        // 차트 목록
        let chartData = await bsApi.get('/environment/ground-monitoring-analysis/chart', {
            params: {
                sensorId: EnvironmentStore.getGmaSensorId,
                from: from,
                to: selectRightDate
            }
        });

        if (chartData.data.data.charts.length > 1000) {
            this.interactionConfig(true);
        } else {
            this.interactionConfig(false);
        }

        if (chartData.data.data.charts.length > 100) {
            this.pointVisibleConfig(false);
        } else {
            this.pointVisibleConfig(true);
        }

        if (chartData.data.data.charts.length > 10000) {
            message.warning("조회하신 기간에 데이터가 표현할 수 있는 범위를 초과하였습니다. 기간을 줄여주세요.");
            enabledChart = false;
        }

        if (enabled && enabledChart) {
            this.setState({
                loading: true
            });

            if (param === 'left') {
                this.setState({
                    selectLeftDate: leftDate
                });
            }

            if (param === 'right') {
                this.setState({
                    selectLeftDate: rightDate
                });
            }

            let analysisData = await bsApi.get('/environment/ground-monitoring-analysis', {
                params: {
                    sensorId: EnvironmentStore.getGmaSensorId,
                    from: from,
                    to: selectRightDate
                }
            });

            if (analysisData.data.data === null || analysisData.data.status === 'ERROR') {
                message.warning(analysisData.data.message);
            } else {
                EnvironmentStore.setGmaLogs(analysisData.data.data.logs);
                if (analysisData.data.data.logs.length > 0) {
                    // 마지막 목록의 계측 시간
                    let lastIndex: number = analysisData.data.data.logs.length - 1;
                    EnvironmentStore.setGmaLogsCard(lastIndex);
                    EnvironmentStore.setGmaLogsCurrentTimeIndex(lastIndex);

                    this.setState({
                        selectCurrentTimeIndex: EnvironmentStore.getGmaLogsCurrentTimeIndex
                    });

                    // 차트 목록
                    if (chartData.data.data.charts.length > 0) {
                        EnvironmentStore.setGmaLogsChart(chartData.data.data.charts);
                    }
                }
            }
        }

        this.setState({
            loading: false
        });
    }

    /**
     * @description 우측날짜 이벤트핸들링 메서드
     **/
    @autobind
    async arrowDate2Ctr(param: string) {
        const {EnvironmentStore} = this.props;
        const {selectLeftDate, selectRightDate} = this.state;
        let enabled: boolean = true;
        let enabledChart: boolean = true;

        let leftDate: string = StringUtil.setDay(selectRightDate, -1);
        let rightDate: string = StringUtil.setDay(selectRightDate, 1);

        let to: string = selectRightDate;

        if (param === 'left') {
            if (new Date(leftDate) < new Date(selectLeftDate)) {
                enabled = false;
                message.warning('선택할 수 없는 범위입니다.');
            }
            to = leftDate;
        }

        if (param === 'right') {
            let monthSixTerm = new Date(StringUtil.setMonth(selectLeftDate, 6));

            if (new Date(rightDate) > monthSixTerm) {
                enabled = false;
                message.warning('기간을 6개월이내로 선택하세요.');
            }

            let today = new Date(StringUtil.formatDate());

            // 현재일자 이후 날짜 체크
            if (new Date(rightDate) > today) {
                enabled = false;
                message.warning('선택할 수 없는 범위입니다.');
            }
            to = rightDate;
        }

        // 차트 목록
        let chartData = await bsApi.get('/environment/ground-monitoring-analysis/chart', {
            params: {
                sensorId: EnvironmentStore.getGmaSensorId,
                from: selectLeftDate,
                to: to
            }
        });

        if (chartData.data.data.charts.length > 1000) {
            this.interactionConfig(true);
        } else {
            this.interactionConfig(false);
        }

        if (chartData.data.data.charts.length > 100) {
            this.pointVisibleConfig(false);
        } else {
            this.pointVisibleConfig(true);
        }

        if (chartData.data.data.charts.length > 10000) {
            message.warning("조회하신 기간에 데이터가 표현할 수 있는 범위를 초과하였습니다. 기간을 줄여주세요.");
            enabledChart = false;
        }

        if (enabled && enabledChart) {
            this.setState({
                loading: true
            });

            if (param === 'left') {
                this.setState({
                    selectRightDate: leftDate
                });
            }

            if (param === 'right') {
                this.setState({
                    selectRightDate: rightDate
                });
            }

            // 차트 목록
            let chartData = await bsApi.get('/environment/ground-monitoring-analysis/chart', {
                params: {
                    sensorId: EnvironmentStore.getGmaSensorId,
                    from: selectLeftDate,
                    to: to
                }
            });

            if (chartData.data.data.charts.length > 10000) {
                message.warning("조회하신 기간에 데이터가 표현할 수 있는 범위를 초과하였습니다. 기간을 줄여주세요.");
            } else {
                let analysisData = await bsApi.get('/environment/ground-monitoring-analysis', {
                    params: {
                        sensorId: EnvironmentStore.getGmaSensorId,
                        from: selectLeftDate,
                        to: to
                    }
                });

                if (analysisData.data.data === null || analysisData.data.status === 'ERROR') {
                    message.warning(analysisData.data.message);
                } else {
                    EnvironmentStore.setGmaLogs(analysisData.data.data.logs);
                    if (analysisData.data.data.logs.length > 0) {
                        // 마지막 목록의 계측 시간
                        let lastIndex: number = analysisData.data.data.logs.length - 1;
                        EnvironmentStore.setGmaLogsCard(lastIndex);
                        EnvironmentStore.setGmaLogsCurrentTimeIndex(lastIndex);

                        this.setState({
                            selectCurrentTimeIndex: EnvironmentStore.getGmaLogsCurrentTimeIndex
                        });

                        if (chartData.data.data.charts.length > 0) {
                            EnvironmentStore.setGmaLogsChart(chartData.data.data.charts);
                        }
                    }
                }
            }
        }

        this.setState({
            loading: false
        });
    }

    /**
     * @description 계측시간 좌, 우 이벤트핸들링 메서드
     **/
    @autobind
    async arrowCtr(param: string) {
        const {EnvironmentStore} = this.props;

        if (param === 'left') {
            if (EnvironmentStore.getGmaLogsCurrentTimeIndex != 0) {
                let leftIndex = EnvironmentStore.getGmaLogsCurrentTimeIndex - 1;
                EnvironmentStore.setGmaLogsCard(leftIndex);
                EnvironmentStore.setGmaLogsCurrentTimeIndex(leftIndex);

                this.setState({
                    selectCurrentTimeIndex: leftIndex
                });
            } else {
                message.warning('첫번째 계측기록 입니다.');
            }
        }
        if (param === 'right') {
            if (EnvironmentStore.getGmaLogsCurrentTimeIndex != EnvironmentStore.getGmaLogs.length - 1) {
                let rightIndex = EnvironmentStore.getGmaLogsCurrentTimeIndex + 1;
                EnvironmentStore.setGmaLogsCard(rightIndex);
                EnvironmentStore.setGmaLogsCurrentTimeIndex(rightIndex);

                this.setState({
                    selectCurrentTimeIndex: rightIndex
                });
            } else {
                message.warning('마지막 계측기록 입니다.');
            }
        }
    }

    @autobind
    async selectCurrentTimeIndex(value: number) {
        const {EnvironmentStore} = this.props;

        EnvironmentStore.setGmaLogsCurrentTimeIndex(value);
        EnvironmentStore.setGmaLogsCard(value);

        this.setState({
            selectCurrentTimeIndex: value
        });
    }

    @autobind
    async selectChartCurrentTimeIndex(date: string) {
        const {EnvironmentStore} = this.props;
        for (let i = 0; i < EnvironmentStore.getGmaLogs.length; i++) {
            let item = EnvironmentStore.getGmaLogs[i];
            let compare = item.date.substr(5,5) + ' ' + item.time.substr(0,5)
            if (compare === date) {
                this.selectCurrentTimeIndex(i);
            }
        }
    }

    /**
     * @description 버튼 클릭시 컨텐츠 화면 풀스크린 모드
     */
    @autobind
    private onClickEventFullScreen() {
        const type: boolean = !this.state.fullscreenType;
        const callBack: Function = this.state.renderEvent;
        this.setState({fullscreenType: type}, function () {
            if (callBack != undefined) {
                callBack(type);
            }
        });
    }

    interactionConfig(use: boolean) {
        if (use) {
            this.setState({
                interactions: [
                    {
                        type: 'slider',
                        cfg: {
                            start: 0.0,
                            end: 1.0,
                        },
                    }
                ]
            })
        } else {
            this.setState({
                interactions: []
            });
        }
    }

    pointVisibleConfig(use: boolean) {
        if (use) {
            this.setState({
                pointVisible: true,
            })
        } else {
            this.setState({
                pointVisible: false,
            })
        }
    }

    render() {
        const {EnvironmentStore} = this.props;
        const {loading, selectLeftDate, selectRightDate, selectedRowKeys, selectCurrentTimeIndex, interactions, pointVisible} = this.state;
        const dateFormat = 'YYYY-MM-DD';
        const {Option} = Select;
        const config: any = {
            padding: 'auto',
            forceFit: true,
            data: EnvironmentStore.getGmaLogsChart,
            xField: 'date',
            yField: 'value',

            yAxis: {
                title: {
                    visible: false,
                },
                label: {
                    style: {
                        fill: '#fff',
                        fillOpacity: 0.3,
                        fontSize: 14,
                    },
                },

            },
            xAxis: {
                title: {
                    visible: false,
                },
                label: {
                    style: {
                        fill: '#fff',
                        fillOpacity: 0.3,
                        fontSize: 14,
                    },
                },
            },
            legend: {
                visible: false,
                flipPage: false,
                position: 'bottom-right',
                text: {
                    style: {
                        fill: '#fff',
                        fontSize: 15,
                    },
                },
            },
            tooltip: {
                visible: true
            },
            color: ['#ff9845', '#d9bb45', '#5ebf3','#5ad8a6', '#1e9493', '#4fc6de'],
            point: {
                visible: pointVisible,
                size: 5,
                fill: ['#ff9845', '#d9bb45', '#5ebf3','#5ad8a6', '#1e9493', '#4fc6de'],
                shape: 'circle',
                style: {
                  stroke: false,
                },
            },
            label: {
                visible: false,
                style: {
                    fill: '#fff',
                    stroke: false,
                    fontSize: 15,
                },
            },
            seriesField: 'type',
            responsive: true,
            interactions: interactions,
        };

        const rowSelection = {
            selectedRowKeys: selectedRowKeys,
            onChange: (selectedRowKeys: any, selectedRows: any) => {
                this.selectSensorChange(selectedRowKeys[0]);
            }
        }

        const stateManager = {
            setState: [
                {
                    event: 'point:click',
                    state: (e: any, index: number) => {
                        const origion = e.target.get('origin').data
                        const state = { name : 'date', exp: origion.date}
                        this.selectChartCurrentTimeIndex(origion.date);
                        return state
                    }
                }
            ],
            onStateChange: [
                {
                    name: 'date',
                    callback: (d: any, plot: any) => {}
                }
            ]
        }

        // @ts-ignore
        return (

            <div id="container" className={(this.state.fullscreenType == true) ? 'full' : 'origin'}>
                <div>
                    <h3>기간별 지반변위 분석</h3>
                    <Button type={"primary"} className="btn_full" onClick={this.onClickEventFullScreen}>
                        {/*@ts-ignore*/}
                        <Icon type='fullscreen'/>{(this.state.fullscreenType == true) ? "OriginScreen" : "FullScreen"}
                    </Button>
                    <div className="input_arrow">
                        {/*@ts-ignore*/}
                        <CaretLeftOutlined  onClick={() => this.arrowDate1Ctr('left')}/><DatePicker value={moment(selectLeftDate)} format={dateFormat} onChange={this.dateLeftSelect}  disabledDate={this.disabledDate} allowClear={false}/><CaretRightOutlined onClick={() => this.arrowDate1Ctr('right')}/>
                    </div>
                    <div className="input_arrow">
                        {/*@ts-ignore*/}
                        <CaretLeftOutlined  onClick={() => this.arrowDate2Ctr('left')}/><DatePicker value={moment(selectRightDate)} format={dateFormat} onChange={this.dateRightSelect}  disabledDate={this.disabledDate} allowClear={false}/><CaretRightOutlined onClick={() => this.arrowDate2Ctr('right')}/>
                    </div>
                    <Card>
                    <Table className="mt-1vw"
                        rowSelection={{
                            type: 'radio',
                            ...rowSelection
                        }}
                        columns={EnvironmentSensorColumn}
                        dataSource={EnvironmentStore.getGmaSensorTable}
                        onRow={(record, rowIndex) => ({
                            onDoubleClick: () => {
                                if (record.key !== selectedRowKeys[0]) {
                                    this.selectSensorChange(record.key)
                                }
                            }
                        })}
                        pagination={false}/>
                    </Card>

                    <ul className="condition mt-1vw">
                        <li className="ant-col-24">
                            <label>계측 기록</label>
                            <div className="input_arrow">
                                {/*@ts-ignore*/}
                                <CaretLeftOutlined onClick={() => this.arrowCtr('left')}/>
                                <Select value={selectCurrentTimeIndex} onChange={this.selectCurrentTimeIndex}>
                                    {
                                        // @ts-ignore
                                        EnvironmentStore.getGmaLogs.map((value: any, index: number) => {
                                            return <Option value={index}>{value.date + ' ' + value.time}</Option>
                                        })}
                                </Select>
                                {/*@ts-ignore*/}
                                <CaretRightOutlined onClick={() => this.arrowCtr('right')}/>
                            </div>
                        </li>
                        <li className="ant-col-24">
                            <div className="overview">
                                <Statistic className="step3" //step1: 파란색, step2: 주황색, step3: 빨간색
                                    title="지반변위(수평)"
                                    value={EnvironmentStore.getGmaLogsCard.xDegree}
                                    suffix="degree"
                                />
                                <Statistic
                                    title="지반변위 변화율(수평)"
                                    value={EnvironmentStore.getGmaLogsCard.xDegreeDisplacement}
                                    suffix="deg./hr"
                                />
                                <Statistic
                                    title="지반변위(수직)"
                                    value={EnvironmentStore.getGmaLogsCard.yDegree}
                                    suffix="degree"
                                />
                                <Statistic
                                    title="지반변위 변화율(수직)"
                                    value={EnvironmentStore.getGmaLogsCard.yDegreeDisplacement}
                                    suffix="deg./hr"
                                />
                            </div>
                        </li>
                    </ul>
                    <Card className="mt-2vw" loading={loading}>
                        <label>지반변위 그래프</label>
                        <StateManagerProvider>
                            <LineChart {...config} className="chart" stateManager={stateManager} />
                        </StateManagerProvider>
                    </Card>
                </div>
            </div>
        );
    };
}

export default GroundAnalysis;
