import React, {Component} from 'react';
import {Button, Card, message, Statistic} from "antd";
import '../../styles/css/layout.css'
import '../../styles/component/TestComent.less';
import {inject, observer} from "mobx-react";
import {bsApi} from "../../common/Api";
import MoneyConvert from "../../util/MoneyConvert";
import {BulletChart, ColumnLineChart} from "@opd/g2plot-react";
import Icon from "antd/es/icon";
import autobind from "autobind-decorator";


@inject('ProductStore')
@observer
class ProductionPanel extends Component<any, any> {

    state = {
        visible: true,
        fullscreenType: false,
        left1: true,
        left2: true,
        right1: true,
        right2: true
    }

    async componentDidMount() {
        const {ProductStore} = this.props;

        const resultData = await bsApi.get('/dashboard/production/amount')
        if(resultData.data.data === null){
            message.warning(resultData.data.message);
        }
        ProductStore.setProductDashAmount(resultData.data.data)
        const chartData = await bsApi.get('/dashboard/production/chart')
        ProductStore.setProductDashChart(chartData.data.data);
        this.setState({
            visible:false
        })
    }

    /**
     * @description 버튼 클릭시 컨텐츠 화면 풀스크린 모드
     */
    @autobind
    private onClickEventFullScreen() {
        const type: boolean = !this.state.fullscreenType;
        this.setState({fullscreenType: type});
    }

    render() {
        const {ProductStore} = this.props;

        const config: any = {
            data: [ProductStore.getProductDashChartAcc,ProductStore.getProductDashChartDaily],
            xField: 'date',
            yField: ['acc', 'daily'],
            forceFit: true,
            xAxis: {
                title: {
                    visible: false,
                    style: {
                        fill: '#fff',
                        fillOpacity: 0.3,
                        fontSize: 14,
                    },
                },
                label: {
                    style: {
                        fill: '#fff',
                        fillOpacity: 0.3,
                        fontSize: 14,
                    },
                },
            },
            legend: {
                visible: true,
                flipPage: true,
                position: 'bottom-left',
                text: {
                    //formatter: (text: string) => text === "acc" ? "누적생산량" : "일자별",
                    formatter: (text: string) => {
                        if(text === 'acc'){
                            return "누적생산량";
                        }
                        else if(text === 'daily'){
                            return "일자별";
                        }
                    },
                    style: {
                        fill: '#fff',
                        fontSize: 15,
                    },
                },
            },
            tooltip: {
                visible: false,
            },
            columnConfig: {
                color: 'l(90) 0:#eca04c 1:#f3c669',
                /*colorField: 'date',
                color: ['l(90) 0:#ff9845 1:#ffb77d', 'l(90) 0:#d9bb45 1:#e4d07d', 'l(90) 0:#5ebf3c 1:#8fd277',
                'l(90) 0:#5ad8a6 1:#8ce4c1', 'l(90) 0:#1e9493 1:#62b4b4', 'l(90) 0:#4fc6de 1:#83d7e8',
                'l(90) 0:#5b8ff9 1:#8db1fb', 'l(90) 0:#945fb9 1:#b48fce', 'l(90) 0:#d54f97 1:#e284b6',
                'l(90) 0:#ff99c3 1:#ffb8d5', 'l(90) 0:#e86452 1:#ef9386'],*/
                legend: {
                    visible: false,
                },
                label: {
                    visible: true,
                    position: 'middle',
                    adjustColor: false,
                    style: {
                        fill: '#fff',
                        fontSize: '15',
                        stroke: '#cd883b',
                    },
                },
                yAxis: {
                    title: {
                        visible: false,
                    },
                    label: {
                        style: {
                            fill: '#fff',
                            fillOpacity: 0.3,
                            fontSize: 14,
                        },
                    },
                },
            },
            lineConfig: {
                color: '#fc4303',
                lineStyle: {
                    stroke: '#fc4303',
                    lineDash: [5, 5],
                    lineWidth: 2,
                  },
                point: {
                    visible: true,
                    size: 7,
                    style: {
                        fill: '#fc4303',
                        stroke: false,//'#fc4303',
                        lineWidth: 0,//lineWidth: 2,
                        //lineDash: [3, 3],
                    },
                  },
                label: {
                    visible: true,
                    style: {
                        fill: '#fff',
                        stroke: false,
                        fontSize: 15,
                    },
                },
                yAxis: {
                    title: {
                        visible: false,
                    },
                    position:'right',
                    label: {
                        style: {
                            fill: '#fff',
                            fillOpacity: 0.3,
                            fontSize: 14,
                        },
                    },
                },
            },

        }

        const plan = ProductStore.getProductDashAmount.plan;
        const actual = ProductStore.getProductDashAmount.currentProd;

        const rangeMax = plan *1.2;
        const limitedActual = Math.floor( Math.min(actual, rangeMax));
        const config2: any = {
            data: [
                {
                    measures: [limitedActual], // 여기가 현재치 2948037
                    targets: [plan],  // 여기가 계획치 4058
                },
            ],
            title:{
                visible:false
            },
            rangeMax: rangeMax,
            label: {
                visible: false,
            },
            xAxis: {//왼쪽 숫자
                visible: false,
            },
            color: '#eca04c',//그래프 색상
            markerColors: ['#fff'],//한계선 색상
            rangeColors:['#ccc'],//max그래프 색상
        }

        const plan2 = ProductStore.getProductDashAmount.accPlan;
        const actual2 = ProductStore.getProductDashAmount.accProd;

        const rangeMax2 = plan2 *1.2;
        const limitedActual2 = Math.floor( Math.min(actual2, rangeMax2));
        const config3: any = {
            data: [
                {
                    title: ' ',
                    measures: [limitedActual2], // 여기가 현재치 2948037
                    targets: [plan2],  // 여기가 계획치 4058
                },
            ],
            title:{
                visible:false,
            },
            rangeMax: rangeMax2,
            label: {
                visible: false,
            },
            xAxis: {
                visible: false,
            },
            color: '#eca04c',
            markerColors: ['#fff'],
            rangeColors:['#ccc'],
        }
        const plan3 = ProductStore.getProductDashAmount.plan + ProductStore.getProductDashAmount.accPlan;
        const actual3 = ProductStore.getProductDashAmount.currentProd + ProductStore.getProductDashAmount.accProd;

        const rangeMax3 = plan3 *1.2;
        const limitedActual3 = Math.floor( Math.min(actual3, rangeMax3));
        const config4: any = {
            data: [
                {
                    title: ' ',
                    measures: [limitedActual3],
                    targets: [plan3],
                },
            ],
            title:{
                visible:false
            },
            rangeMax: rangeMax3,
            label: {
                visible: false,
            },
            xAxis: {
                visible: false,
            },
            color: '#eca04c',
            markerColors: ['#fff'],
            rangeColors:['#ccc'],
        }
        // @ts-ignore
        return (
            <Card loading={this.state.visible}>
                <div id="container" className={(this.state.fullscreenType == true) ? 'full' : 'origin'}>
                    {/*@ts-ignore*/}
                    <div>
                    <h3>광석생산관리</h3>
                        {/*@ts-ignore*/}
                    <Button type={"primary"} className="btn_full" onClick={this.onClickEventFullScreen}>
                        {/*@ts-ignore*/}
                        <Icon type='fullscreen'/>{(this.state.fullscreenType == true) ? "OriginScreen" : "FullScreen"}
                    </Button>
                    <div className="sum">
                        <Card>
                            <h5>일 생산량<em></em></h5>
                            <Statistic value={ProductStore.getProductDashAmount.currentProd} suffix="t"/>
                            <BulletChart {...config2} className="bulletchat_box mt-1vw"/>
                            <p><span className="head">계획</span> {MoneyConvert.commas(ProductStore.getProductDashAmount.plan)} t</p>
                            <p>초과 &nbsp;{(Math.floor(ProductStore.getProductDashAmount.currentProd/ ProductStore.getProductDashAmount.plan) * 100)}% 달성<span className="over">▲</span></p>
                        </Card>
                        <Card>
                            <h5>누적 생산량<em></em></h5>
                            <Statistic value={ProductStore.getProductDashAmount.accProd} suffix="t"/>
                            <BulletChart {...config3} className="bulletchat_box mt-1vw"/>
                            <p><span className="head">계획</span> {MoneyConvert.commas(ProductStore.getProductDashAmount.accPlan)} t</p>
                            <p>초과 &nbsp;{(Math.floor(ProductStore.getProductDashAmount.accProd/ ProductStore.getProductDashAmount.accPlan) * 100)}% 달성<span className="over">▲</span></p>
                        </Card>
                        <Card>
                            <h5>총 생산량<em></em></h5>
                            <Statistic
                                value={ProductStore.getProductDashAmount.currentProd + ProductStore.getProductDashAmount.accProd}
                                suffix="t"/>
                            <BulletChart {...config4} className="bulletchat_box mt-1vw"/>
                            <p><span className="head">계획</span> {MoneyConvert.commas(ProductStore.getProductDashAmount.plan + ProductStore.getProductDashAmount.accPlan)} t</p>
                            <p>초과&nbsp; {(Math.floor(ProductStore.getProductDashAmount.accProd/ ProductStore.getProductDashAmount.accPlan) * 100)}% 달성<span className="over">▲</span></p>
                        </Card>
                    </div>
                    <div className="tit"><h4><span>광석 생산량</span></h4></div>

                    <ColumnLineChart {...config} className="chart" />
                </div>
                </div>
            </Card>
        );
    };
}

export default ProductionPanel;
