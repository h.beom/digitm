import React, {Component} from 'react';
import {Button, Card, Input, message, Select, Statistic, Table} from "antd";
import autobind from "autobind-decorator";
import Icon from "antd/es/icon";
import {weighbridgeColumns} from "../../../components/tableGrid/Columns";
import {inject, observer} from "mobx-react";
import {bsApi} from "../../../common/Api";
import StringUtil from "../../../util/StringUtil";
import {CaretLeftOutlined, CaretRightOutlined} from "@ant-design/icons/lib";
import MoneyConvert from "../../../util/MoneyConvert";

@inject('ProductStore')
@observer
class WeighbridgeRealtime extends Component<any, any> {
    constructor(props: any) {
        super(props);
        this.state = {
            loading: true,
            selectDate: StringUtil.formatDate(),
            intervalId: null,
            maxNumber: 0,
            fullscreenType: false,
            left1: true,
            right1: true,
        };
    }

    async componentDidMount() {
        const {ProductStore} = this.props;
        const {maxNumber, selectDate} = this.state;

        const resultData = await bsApi.get('/production/weighbridge-realtime');
        if(resultData.data.data === null){
            message.warning(resultData.data.message);
        }
        ProductStore.setWeighbridgeRealCardData(resultData.data.data);
        const lastnumber = resultData.data.data.lastNumber

        // 파라미터 바뀔 예정

        const resultDetailData = await bsApi.get('/production/weighbridge-realtime/detail', {
            params: {
                date: StringUtil.formatDate(),
                number: lastnumber
            }
        });


        ProductStore.setWeighbridgeRealTable(resultDetailData.data.data);

        ProductStore.setLastNumber(lastnumber);

        const interval = setInterval(async () => {
            const resultData = await bsApi.get('/production/weighbridge-realtime');
            ProductStore.setWeighbridgeRealCardData(resultData.data.data);

            const lastnumber = resultData.data.data.lastNumber

            // 파라미터 바뀔 예정
            // const resultDetailData = await bsApi.get('/production/weighbridge-realtime/detail', {params: {date:selectDate ,number: lastnumber}});
            const resultDetailData = await bsApi.get('/production/weighbridge-realtime/detail', {
                params: {
                    date: StringUtil.formatDate(),
                    number: ProductStore.getLastNumber
                }
            });


            ProductStore.setWeighbridgeRealTable(resultDetailData.data.data);
            // ProductStore.setLastNumber(lastnumber);
            this.setState({
                lastNumber: lastnumber,
            })
        }, 60000)


        let str = [];
        for (let i = 0; i < lastnumber; i++) {
            str.push(i)
        }
        ProductStore.setWeighbridgeRealList(str.reverse())
// expected output: "012345678"


        this.setState({
            loading: false,
            maxNumber: lastnumber
            // intervalId: interval
        })
    }

    @autobind
    showModal() {
        this.setState({
            visible: true,
        });
    };

    @autobind
    handleOk() {
        this.setState({
            visible: false,
        });
    };

    @autobind
    handleCancel() {
        this.setState({
            visible: false,
        });
    };

    /**
     * @description 계근회차 data 변경
     **/
    @autobind
    async arrowCtr(param: string) {

        const {ProductStore} = this.props
        const {maxNumber} = this.state
        this.setState({
            loading: true
        })
        let numberState = ProductStore.getLastNumber;
        // const last = resultData.filter((value: any) => value.id === nowversion);
        //
        if (param === 'left') {
            if (ProductStore.getLastNumber == 1) {
                message.warning('마지막 계근회차 입니다.');
            } else {
                const resultDetailData = await bsApi.get('/production/weighbridge-realtime/detail', {
                    params: {
                        date: StringUtil.formatDate(),
                        number: numberState - 1
                    }
                });
                ProductStore.setLastNumber(numberState - 1)
                this.props.ProductStore.setWeighbridgeRealTable(resultDetailData.data.data);
            }
        }
        if (param === 'right') {
            if (ProductStore.getLastNumber == maxNumber) {
                message.warning('마지막 계근회차 입니다.');
            } else {
                // 파뀔 파라미터
                const resultDetailData = await bsApi.get('/production/weighbridge-realtime/detail', {
                    params: {
                        date: StringUtil.formatDate(),
                        number: numberState + 1
                    }
                });
                ProductStore.setLastNumber(numberState + 1)
                this.props.ProductStore.setWeighbridgeRealTable(resultDetailData.data.data);
            }
        }
        this.setState({
            loading: false
        })
    }

    @autobind
    async selectLastNumber(value: number) {
        this.setState({
            loading: true
        })
        this.props.ProductStore.setLastNumber(value)
        //파라미터 바뀔예정
        // const resultDetailData = await bsApi.get('/production/weighbridge-realtime/detail', {params: {date:value + 1 ,number: this.state.selectDate}});
        const resultDetailData = await bsApi.get('/production/weighbridge-realtime/detail', {
            params: {
                date: StringUtil.formatDate(),
                number: value
            }
        });
        this.props.ProductStore.setWeighbridgeRealTable(resultDetailData.data.data);
        this.setState({
            loading: false
        })
    }

    /**
     * @description 버튼 클릭시 컨텐츠 화면 풀스크린 모드
     */
    @autobind
    private onClickEventFullScreen() {
        const type: boolean = !this.state.fullscreenType;
        const callBack: Function = this.state.renderEvent;
        this.setState({fullscreenType: type}, function () {
            if (callBack != undefined) {
                callBack(type);
            }
        });
    }

    componentWillUnmount() {
        // @ts-ignore
        clearInterval(this.state.intervalId)
    }

    render() {
        const {ProductStore} = this.props;
        const {selectDate, maxNumber} = this.state;
        const {Option} = Select;
        return (

            <div id="container" className={(this.state.fullscreenType == true) ? 'full' : 'origin'}>
                <div>
                    <h3>실시간 생산 &amp; 구매실적</h3>
                    <Button type={"primary"} className="btn_full" onClick={this.onClickEventFullScreen}>
                        {/*@ts-ignore*/}
                        <Icon type='fullscreen'/>{(this.state.fullscreenType == true) ? "OriginScreen" : "FullScreen"}
                    </Button>
                    <ul className="condition">
                        <li className="ant-col-24">
                            <label>일자</label>
                            <div className="input_arrow">
                                <Input value={selectDate}/>
                            </div>
                        </li>
                        <li>
                            <div className="overview">
                                <label>생산실적</label>
                                <div className="overview_child">
                                    <Statistic
                                        title="입고량"
                                        value={ProductStore.getProductCurrent2}
                                        suffix="t"
                                    />
                                    <Statistic
                                        title="입고(월누계)"
                                        value={ProductStore.getProductMonth2}
                                        suffix="t"
                                    />
                                    <Statistic
                                        title="입고(연누계)"
                                        value={ProductStore.getProductYear2}
                                        suffix="t"
                                    />
                                    <Statistic
                                        title="출고량"
                                        value={ProductStore.getProductCurrentOut2}
                                        suffix="t"
                                    />
                                    <Statistic
                                        title="출고(월누계)"
                                        value={ProductStore.getProductMonthOut2}
                                        suffix="t"
                                    />
                                    <Statistic
                                        title="출고(연누계)"
                                        value={ProductStore.getProductYearOut2}
                                        suffix="t"
                                    />
                                </div>
                            </div>
                        </li>
                        <li>
                            <div className="overview mb-2vw">
                                <label>구매실적</label>
                                <div className="overview_child">
                                    <Statistic
                                        title="입고량"
                                        value={ProductStore.getProductCurrent}
                                        suffix="t"
                                    />
                                    <Statistic
                                        title="입고(월누계)"
                                        value={ProductStore.getProductMonth}
                                        suffix="t"
                                    />
                                    <Statistic
                                        title="입고(연누계)"
                                        value={ProductStore.getProductYear}
                                        suffix="t"
                                    />
                                    <Statistic
                                        title="출고량"
                                        value={ProductStore.getProductCurrentOut}
                                        suffix="t"
                                    />
                                    <Statistic
                                        title="출고(월누계)"
                                        value={ProductStore.getProductMonthOut}
                                        suffix="t"
                                    />
                                    <Statistic
                                        title="출고(연누계)"
                                        value={ProductStore.getProductYearOut}
                                        suffix="t"
                                    />
                                </div>
                            </div>
                        </li>
                    </ul>


                    <ul className="condition">
                        <li className="ant-col-24">
                            <label>계근회차 별 재고 변동량</label>
                            <div className="input_arrow">
                                {/*@ts-ignore*/}
                                <CaretLeftOutlined onClick={() => this.arrowCtr('left')}/>
                                <Select value={ProductStore.getLastNumber} onChange={this.selectLastNumber}>
                                    {
                                        // @ts-ignore
                                        ProductStore.getWeighbridgeRealList.map((value: any, index: number) => {
                                            return <Option value={value + 1}>{value + 1}</Option>
                                        })}
                                </Select>
                                {/*@ts-ignore*/}
                                {/*<CaretRightOutlined style={{float: "right"}} onClick={() => this.arrowCtr('right')}/>*/}
                                {/*@ts-ignore*/}
                                <CaretRightOutlined style={{float: "right"}} onClick={() => this.arrowCtr('right')}/>
                            </div>
                        </li>
                    </ul>
                    <Card loading={this.state.loading}>
                        <Table
                            columns={weighbridgeColumns}
                            dataSource={ProductStore.getWeighbridgeRealTable.list}
                            pagination={false}/>

                        <div className="semicircle">
                            <div className="ant-col-12">
                                <label>생산 모니터링</label>
                                <div className="graph_semicircle step1">{/*step1~3 : 반원 그래프 색상 변경*/}
                                    <Statistic
                                        title="재고 변동량"
                                        value={MoneyConvert.commas(ProductStore.getTest123.change)}
                                    />
                                    <div className="graph_semicircle_child">
                                        <Statistic
                                            title="출고량"
                                            value={ProductStore.getTest123.output}
                                        />
                                        <p>현재재고 : {MoneyConvert.commas(ProductStore.getTest123.postAmount)}t</p>
                                    </div>
                                    <div className="graph_semicircle_child">
                                        <Statistic
                                            title="생산량"
                                            value={MoneyConvert.commas(ProductStore.getTest123.input)}
                                        />
                                        <p>이전재고 : {MoneyConvert.commas(ProductStore.getTest123.preAmount)}t</p>
                                    </div>
                                </div>
                            </div>
                            <div className="ant-col-12">
                                <label>구매 모니터링</label>
                                <div className="graph_semicircle step1">{/*step1~3 : 반원 그래프 색상 변경*/}
                                    <Statistic
                                        title="재고 변동량"
                                        value={MoneyConvert.commas(ProductStore.getTest456.change)}
                                    />
                                    <div className="graph_semicircle_child">
                                        <Statistic
                                            title="출고량"
                                            value={MoneyConvert.commas(ProductStore.getTest456.output)}
                                        />
                                        <p>현재재고 : {MoneyConvert.commas(ProductStore.getTest456.postAmount)}t</p>

                                    </div>
                                    <div className="graph_semicircle_child">
                                        <Statistic
                                            title="입고량"
                                            value={ProductStore.getTest456.input}
                                        />
                                        <p>이전재고 : {MoneyConvert.commas(ProductStore.getTest456.preAmount)}t</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Card>
                </div>
            </div>
        );
    };
}

export default WeighbridgeRealtime;
