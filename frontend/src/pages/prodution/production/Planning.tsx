import React, { Component, CSSProperties } from "react";
import {
  Alert,
  Button,
  Card,
  DatePicker,
  Input,
  message,
  Modal,
  Select,
  Statistic,
  Table,
} from "antd";
import autobind from "autobind-decorator";
import StringUtil from "../../../util/StringUtil";
import { inject, observer } from "mobx-react";
import { PlanningColumn } from "../../../components/tableGrid/Columns";
import { bsApi } from "../../../common/Api";
import MoneyConvert from "../../../util/MoneyConvert";
import moment from "moment";
import Icon from "antd/es/icon";
import { CaretLeftOutlined, CaretRightOutlined } from "@ant-design/icons/lib";
import axios from "axios";

@inject("ProductStore")
@observer
class Planning extends Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      loading: true,
      loading2: false,
      modalText: "",
      visible: false,
      fullscreenType: false,
      selectDate: StringUtil.formatDate2(),
      file: "",
      imagePreviewUrl: "",
      fileName: "",
      left1: true,
      left2: true,
      right1: false,
      right2: true,
    };
  }

  async componentDidMount() {
    const { ProductStore } = this.props;
    const versionData = await bsApi.get("/production/planning/version");
    ProductStore.setVersionData(versionData.data.data.versions);
    ProductStore.setNowVersion(versionData.data.data.versions[0].id);
    let test = {
      params: {
        date: StringUtil.formatDate2(),
        id: versionData.data.data.versions[0].id,
      },
    };
    const resultData = await bsApi.get("/production/planning", test);
    ProductStore.setPlanningData(resultData.data.data);

    this.setState({
      loading: false,
    });
  }

  /**
   * @description 버튼 클릭시 컨텐츠 화면 풀스크린 모드
   */
  @autobind
  private onClickEventFullScreen() {
    const type: boolean = !this.state.fullscreenType;
    const callBack: Function = this.state.renderEvent;
    this.setState({ fullscreenType: type }, function () {
      if (callBack != undefined) {
        callBack(type);
      }
    });
  }

  /**
   * @description 생산계획
   */
  @autobind
  public modalText(e: React.FormEvent<HTMLInputElement>) {
    this.setState({
      modalText: e.currentTarget.value,
    });
  }

  @autobind
  showModal() {
    this.setState({
      visible: true,
    });
  }

  @autobind
  async handleOk() {
    const { file, modalText } = this.state;
    if (modalText == "") {
      alert("계획 차수명을 입력해 주세요");
    } else if (file == null) {
      alert("파일을 선택해주세요");
    }

    let formdata = new FormData();
    formdata.append("file", file);
    formdata.append("name", modalText);

    let test = {
      params: {
        "version-name": modalText,
      },
    };

    const repeatData = await bsApi.post(
      "/production/planning/excel",
      formdata,
      test
    );
    if (repeatData.data.data == null) {
      alert(repeatData.data.message);
    } else {
      this.setState({
        visible: false,
        modalText: "",
        file: "",
      });
    }
    const versionData = await bsApi.get("/production/planning/version");
    this.props.ProductStore.setVersionData(versionData.data.data.versions);
    this.props.ProductStore.setNowVersion(versionData.data.data.versions[0].id);
    let test1 = {
      params: {
        date: StringUtil.formatDate2(),
        id: versionData.data.data.versions[0].id,
      },
    };
    const resultData = await bsApi.get("/production/planning", test1);
    this.props.ProductStore.setPlanningData(resultData.data.data);
  }

  @autobind
  handleCancel() {
    this.setState({
      visible: false,
    });
  }

  /**
   * @description 달력 선택에따른 날짜변경
   * @param date
   * @param dateString ex) 2020-08 (format YYYY-MM)
   */
  @autobind
  private changeDate(date: moment.Moment | null, dateString: string) {
    // this.props.user.setSignBirthday(dateString);
    const getId = this.props.ProductStore.getNowVersion;
    this.resetData(getId, dateString);
    this.setState({
      selectDate: dateString,
    });
  }

  @autobind
  handleChange(value: any) {
    const nowversion = this.props.ProductStore.getNowVersion;
    const resultData = this.props.ProductStore.getVersionData.map(
      (param: any, index: number) => {
        return { id: param.id, name: param.name, index: index };
      }
    );
    const last = resultData.filter((value: any) => value.id === nowversion);
    if (last[0].index == 0) {
      this.setState({
        right1: true,
      });
    } else if (last[0].index == resultData.length) {
      message.warning("첫번째 계획차수 입니다1.");
    } else {
      this.setState({
        right1: false,
      });
    }

    const thisDate = this.state.selectDate;
    this.props.ProductStore.setNowVersion(value);
    this.resetData(value, thisDate);
  }

  /**
   * @description 날자 좌우 클릭시 date 변경
   **/
  @autobind
  public arrowCtr(param: string) {
    let nowdate = this.state.selectDate;
    const version = this.props.ProductStore.getNowVersion;
    if (param === "left") {
      this.setState({
        selectDate: StringUtil._setDate2(nowdate, -1, 0),
      });
      this.resetData(version, StringUtil._setDate2(nowdate, -1, 0));
    } else if (param === "right") {
      this.setState({
        selectDate: StringUtil._setDate2(nowdate, 1, 0),
      });
      this.resetData(version, StringUtil._setDate2(nowdate, 1, 0));
    }
  }

  /**
   * @description 계획차수 data 변경
   **/
  @autobind
  public arrowCtr2(param: string) {
    const nowversion = this.props.ProductStore.getNowVersion;
    const thisDate = this.state.selectDate;
    const resultData = this.props.ProductStore.getVersionData.map(
      (param: any, index: number) => {
        return { id: param.id, name: param.name, index: index };
      }
    );
    const last = resultData.filter((value: any) => value.id === nowversion);

    if (param === "right") {
      if (last[0].index == 0) {
        message.warning("마지막 계획차수 입니다.");
      } else {
        this.props.ProductStore.setNowVersion(resultData[last[0].index - 1].id);
        this.resetData(resultData[last[0].index - 1].id, thisDate);
      }
    } else if (param === "left") {
      if (last[0].index == resultData.length - 1) {
        message.warning("첫번째 계획차수 입니다.");
      } else {
        this.props.ProductStore.setNowVersion(resultData[last[0].index + 1].id);
        this.resetData(resultData[last[0].index + 1].id, thisDate);
      }
    }
  }

  /**
   * @description 계획차수 data 변경시 api 데이터 호출
   **/
  async resetData(value: number, date: any) {
    this.setState({
      loading2: true,
    });
    let test = {
      params: {
        date: date,
        id: value,
      },
    };
    const resultData = await bsApi.get("/production/planning", test);
    this.props.ProductStore.setPlanningData(resultData.data.data);

    this.setState({
      loading2: false,
    });
  }

  /**
   * @description 파일 다운로드
   */
  // @autobind
  // private download() {
  //     function onStartedDownload(id: any) {
  //         console.log(`Started downloading: ${id}`);
  //     }
  //
  //     function onFailed(error: any) {
  //         console.log(`Download failed: ${error}`);
  //     }
  //
  //     var downloadUrl = "https://example.org/image.png";
  //
  //     var downloading = browser.downloads.download({
  //         url : downloadUrl,
  //         filename : 'my-image-again.png',
  //         conflictAction : 'uniquify'
  //     });
  //     downloading.then(onStartedDownload, onFailed);
  // }

  @autobind
  private handleFile() {
    // @ts-ignore
    const checkFile = document.getElementById("fileElem").files;
    if (checkFile[0].name == "3.png") {
      alert("3.png은 안되!!");
    } else {
      this.setState({
        file: checkFile[0],
        fileName: checkFile[0].name,
      });
    }
  }

  render() {
    const { ProductStore } = this.props;
    const { modalText, selectDate, loading, loading2 } = this.state;
    const dateFormat = "YYYY-MM";
    const { Option } = Select;

    // @ts-ignore
    return (
      <Card loading={loading}>
        <div
          id="container"
          className={this.state.fullscreenType == true ? "full" : "origin"}
        >
          <div>
            <h3>생산 계획 관리</h3>
            <Button
              type={"primary"}
              className="btn_full"
              onClick={this.onClickEventFullScreen}
            >
              {/*@ts-ignore*/}
              <Icon type="fullscreen" />
              {this.state.fullscreenType == true
                ? "OriginScreen"
                : "FullScreen"}
            </Button>
            <ul className="condition">
              <li className="ant-col-24">
                <label>계획차수</label>
                <div className="input_arrow">
                  {/*@ts-ignore*/}
                  <CaretLeftOutlined
                    onClick={() => this.arrowCtr2("left")}
                    className={this.state.left1 == true ? "test1" : "test2"}
                  />
                  <Select
                    value={ProductStore.getNowVersion}
                    size={"small"}
                    style={{ float: "left", width: 100 }}
                    onChange={this.handleChange}
                  >
                    {ProductStore.getVersionData.map(
                      (param: any, index: number) => {
                        return <Option value={param.id}>{param.name}</Option>;
                      }
                    )}
                  </Select>
                  {/*@ts-ignore*/}
                  <CaretRightOutlined
                    onClick={() => this.arrowCtr2("right")}
                    style={{ fontStyle: "blue" }}
                    className={this.state.right1 == true ? "test1" : "test2"}
                  />
                </div>
                <div className="input_arrow">
                  {/*@ts-ignore*/}
                  <CaretLeftOutlined onClick={() => this.arrowCtr("left")} />
                  <DatePicker
                    picker="month"
                    value={moment(selectDate)}
                    format={dateFormat}
                    onChange={this.changeDate}
                    allowClear={false}
                  />
                  {/*@ts-ignore*/}
                  <CaretRightOutlined onClick={() => this.arrowCtr("right")} />
                </div>

                <div className="output">
                  <Statistic
                    title="월간 총 계획 생산량"
                    value={ProductStore.getPlanningData.total}
                    suffix={"t"}
                  />
                  <Statistic
                    title="일 평균 계획 생산량"
                    value={ProductStore.getPlanningData.average}
                    suffix="t"
                  />
                </div>
              </li>
            </ul>

            <div className="board_tit">
              <span className="total">단위 : Ton</span>
              <div className="btn_wrap">
                <Button type={"primary"} onClick={this.showModal}>
                  생산 계획 업로드
                </Button>
              </div>
            </div>

            <Card loading={loading2} className="load_layer">
              <Table
                columns={PlanningColumn}
                dataSource={ProductStore.getPlanningData.list}
                scroll={{ y: 450 }}
                pagination={false}
              />

              <table className="ant-table table_footer">
                <tfoot>
                  <tr>
                    <th>Total</th>
                    <th>
                      {MoneyConvert.commas(ProductStore.getPlanningData.total)}
                    </th>
                  </tr>
                </tfoot>
              </table>
            </Card>

            {/*::::::::::::::::::::::::모달컨트롤::::::::::::::::::::::::::::*/}
            <Modal
              title="생산계획 업로드"
              style={{ textAlign: "center" }}
              visible={this.state.visible}
              onOk={this.handleOk}
              onCancel={this.handleCancel}
              okText={"저장"}
              cancelText={"취소"}
            >
              <div>
                <ul className="condition">
                  <li className="ant-col-24">
                    <label>계획 차수명</label>
                    <Input
                      placeholder={"해당 차수명"}
                      value={modalText}
                      onChange={this.modalText}
                    />
                  </li>
                </ul>
                <div className="btn_file">
                  <div>
                    {/*<input type="file" onChange={this._handleImageChange}/>*/}
                    {/*<button name="virtu_btn" className="ant-btn"*/}
                    {/*        onClick="clickFile(); return false;">파일선택*/}
                    {/*</button>*/}
                    {/*<Input name="virtu_text" readOnly={true} placeholder={'파일을 선택하세요'}/>*/}

                    <input
                      type="file"
                      id="fileElem"
                      accept=".xlsx, .xls, .csv"
                      className="visually-hidden"
                      onChange={this.handleFile}
                    />
                    <button name="virtu_btn" className="ant-btn">
                      <label htmlFor="fileElem">
                        <a>파일선택</a>
                      </label>
                    </button>
                    {this.state.fileName === "" ? (
                      <Input
                        name="virtu_text"
                        readOnly={true}
                        placeholder={"파일을 선택하세요"}
                      />
                    ) : (
                      <p>{this.state.fileName}</p>
                    )}
                  </div>
                  <div>
                    <Button type={"primary"}>
                      {/*@ts-ignore*/}
                      <Icon type={"download"} />
                      <a href="/생산계획관리_양식.xls" download>
                        양식 다운로드
                      </a>
                    </Button>
                  </div>
                </div>
              </div>
            </Modal>
          </div>
        </div>
      </Card>
    );
  }
}

export default Planning;
