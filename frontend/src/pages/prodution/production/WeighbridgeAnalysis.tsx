import React, {Component} from 'react';
import {Button, Card, DatePicker, message, Modal, PageHeader, Statistic, Table} from "antd";
import autobind from "autobind-decorator";
import Test from '../../../components/chart/Test'
import Icon from "antd/es/icon";
import axios from "axios";
import {inject, observer} from "mobx-react";
import ProductStore from "../../../stores/ProductionStore";
import {weighbridgeColumns} from "../../../components/tableGrid/Columns";
import {bsApi} from "../../../common/Api";
import StringUtil from "../../../util/StringUtil";
import moment from "moment";
import {CaretLeftOutlined, CaretRightOutlined} from "@ant-design/icons/lib";
import {GroupedColumnChart} from '@opd/g2plot-react'

@inject('ProductStore')
@observer
class WeighbridgeAnalysis extends Component<any, any> {


    state = {
        visible: true,
        selectDate: StringUtil._setDate(StringUtil.formatDate(), -1,0,0),
        fullscreenType: false,
        left1: true,
        right1: true,
    }

    async componentDidMount() {
        const {ProductStore} = this.props;

        const resultData = await bsApi.get('/production/weighbridge-analysis', {
            params: {
                date: this.state.selectDate
            }
        });
        ProductStore.setWeighbridgeAnalysisData(resultData.data.data);

        const resultChartData = await bsApi.get('/production/weighbridge-analysis/chart', {
            params: {
                date: this.state.selectDate
            }
        });
        ProductStore.setWeighbridgeAnalysisChart(resultChartData.data.data)
        this.setState({
            loading: false
        })
    }

    /**
     * @description 날짜 바뀌면 해당날짜 데이터 불러오기1
     * @param prevProps 현재 날짜와 이전 날짜의 비교할 값
     */
    componentDidUpdate(prevProps: { selectDate: string; }) {
        if (this.state.selectDate !== prevProps.selectDate) {
        }
    }

    /**
     * @description 날자 좌우 클릭시 date 변경
     **/
    @autobind
    async arrowCtr(param: string) {
        let nowdate = this.state.selectDate;
        const {ProductStore} = this.props;
        this.setState({
            loading: true
        })
        if (param === 'left') {
            this.setState({
                selectDate: StringUtil._setDate(nowdate, -1, 0, 0)
            })
            const resultData = await bsApi.get('/production/weighbridge-analysis', {
                params: {
                    date: StringUtil._setDate(nowdate, -1, 0, 0)
                }
            });
            ProductStore.setWeighbridgeAnalysisData(resultData.data.data);
            const resultChartData = await bsApi.get('/production/weighbridge-analysis/chart', {
                params: {
                    date: this.state.selectDate
                }
            });
            ProductStore.setWeighbridgeAnalysisChart(resultChartData.data.data)
        } else if (param === 'right') {
            if (nowdate === StringUtil._setDate(StringUtil.formatDate(), -1,0,0)) {
                message.warning('마지막 조회일자 입니다.');
            } else {
                this.setState({
                    selectDate: StringUtil._setDate(nowdate, 1, 0, 0)
                })
                const resultData = await bsApi.get('/production/weighbridge-analysis', {
                    params: {
                        date: StringUtil._setDate(nowdate, 1, 0, 0)
                    }
                });

                this.props.ProductStore.setWeighbridgeAnalysisData(resultData.data.data);
                const resultChartData = await bsApi.get('/production/weighbridge-analysis/chart', {
                    params: {
                        date: this.state.selectDate
                    }
                });
                ProductStore.setWeighbridgeAnalysisChart(resultChartData.data.data)
            }
        }
        this.setState({
            loading: false
        })
    }


    /**
     * @description 오늘날짜 이후로는 선택 불가한 메서드
     * @param current
     */
    @autobind
    disabledDate(current: any) {
        let today = moment();
        const day = today.add(-1, "days");
        const endOfDay = day.endOf('day')
        return current && current > endOfDay;
    }

    @autobind
    async onChange(date: moment.Moment | null, dateString: string) {
        this.setState({
            loading: true,
            selectDate: dateString,
        })
        const resultData = await bsApi.get('/production/weighbridge-analysis', {
            params: {
                date: dateString
            }
        });
        this.props.ProductStore.setWeighbridgeAnalysisData(resultData.data.data);
        const resultChartData = await bsApi.get('/production/weighbridge-analysis/chart', {
            params: {
                date: dateString
            }
        });
        this.props.ProductStore.setWeighbridgeAnalysisChart(resultChartData.data.data)
        this.setState({
            loading: false
        })
    }

    @autobind
    public cardVisible() {
        if (this.state.visible == true) {
            this.setState({
                visible: false
            })
        } else {
            this.setState({
                visible: true
            })
        }
    }


    /**
     * @description 버튼 클릭시 컨텐츠 화면 풀스크린 모드
     */
    @autobind
    private onClickEventFullScreen() {
        const type: boolean = !this.state.fullscreenType;
        this.setState({fullscreenType: type});
    }

    render() {
        const dateFormat = 'YYYY-MM-DD';
        const visibleConfirm = this.state.visible;
        const {ProductStore} = this.props;
        const {selectDate} = this.state;

        const config: any = {
            forceFit: true,
            data: ProductStore.getWeighbridgeAnalysisChart,
            xField: 'date',
            yField: 'value',
            legend: {
                visible: true,
                flipPage: false,
                padding: 10,
                position: 'bottom-right',
                text: {//prodMove, purMove, prodAcc, purAcc
                    formatter: (text: string) => {
                        if(text === 'prodMove'){
                            return "생산 운반량";
                        }
                        else if(text === 'purMove'){
                            return "생산 비축량";
                        }
                        else if(text === 'prodAcc'){
                            return "구매 운반량";
                        }
                        else if(text === 'purAcc'){
                            return "구매 비축량";
                        }
                    },
                    style: {
                        fill: '#fff',
                        fontSize: 15,
                    },
                },
            },
            tooltip: {
                visible: false,
            },
            groupField: 'name',
            color: ['l(90) 0:#ff9845 1:#ffb77d', 'l(90) 0:#d9bb45 1:#e4d07d', 'l(90) 0:#5ebf3c 1:#8fd277', 
            'l(90) 0:#5ad8a6 1:#8ce4c1', 'l(90) 0:#1e9493 1:#62b4b4', 'l(90) 0:#4fc6de 1:#83d7e8'],
            label: {
                visible: true,
                style: {
                    fill: '#fff',
                    stroke: false,
                    fontSize: 15,
                },
            },
            yAxis: {
                min: 0,
                title: {
                    visible: false,
                },
                //position: 'right',
                label: {
                    style: {
                        fill: '#fff',
                        fillOpacity: 0.3,
                        fontSize: 14,
                    },
                },

            },
            xAxis: {
                title: {
                    visible: false,
                },
                label: {
                    style: {
                        fill: '#fff',
                        fillOpacity: 0.3,
                        fontSize: 14,
                    },
                },
            },
        };
        return (
            <>
                <div id="container" className={(this.state.fullscreenType == true) ? 'full' : 'origin'}>
                    <div>
                        <h3>일자별 생산 &amp; 구매실적</h3>
                        <Button type={"primary"} className="btn_full" onClick={this.onClickEventFullScreen}>
                            {/*@ts-ignore*/}
                            <Icon type='fullscreen'/>{(this.state.fullscreenType == true) ? "OriginScreen" : "FullScreen"}
                        </Button>
                        <ul className="condition">
                            <li className="ant-col-24">
                                <label>일자</label>
                                <div className="input_arrow">
                                    {/*@ts-ignore*/}
                                    <CaretLeftOutlined onClick={() => this.arrowCtr('left')}/>
                                    <DatePicker value={moment(selectDate)} format={dateFormat} onChange={this.onChange}
                                                disabledDate={this.disabledDate}  allowClear={false}/>
                                    {/*@ts-ignore*/}
                                    <CaretRightOutlined onClick={() => this.arrowCtr('right')}/>
                                </div>
                            </li>
                            <li>
                                <div className="overview">
                                    <label>생산실적</label>
                                    <div className="overview_child">
                                        <Statistic title="누적 운반 횟수"
                                                   value={ProductStore.getAccCount}
                                                   suffix={'회'}/>
                                        <Statistic title="누적 운반량"
                                                   value={ProductStore.getAccAmount} suffix="t"/>
                                    </div>
                                </div>
                            </li>
                        </ul>

                        <Table
                            columns={weighbridgeColumns}
                            dataSource={ProductStore.getWeighbridgeAnalysisData.list}
                            pagination={false}/>

                        <ul className="condition mt-1vw">
                            <li className="ant-col-24">
                                <label>최근 7일 생산실적</label>
                                {/*@ts-ignore*/}
                                <Button type={'primary'} className="btn_open" onClick={this.cardVisible}> {(visibleConfirm == true) ? '펼치기' : '접기'} {(visibleConfirm == true) ? <Icon type={'down'}/> : <Icon type={'up'}/>} </Button>
                            </li>
                        </ul>

                        {(visibleConfirm == false) ? <GroupedColumnChart {...config} className="chart" /> : null}
                    </div>
                </div>
            </>
        );
    };
}

export default WeighbridgeAnalysis;
