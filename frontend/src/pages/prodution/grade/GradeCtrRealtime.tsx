import React, {Component} from 'react';
import {Button, Card, Input, message, PageHeader, Select, Table} from "antd";
import {inject, observer} from "mobx-react";
import Icon from "antd/es/icon";
import {GradeRealColumn} from "../../../components/tableGrid/Columns";
import {bsApi} from "../../../common/Api";
import GradeChartLine from "../../../components/chart/GradeChartLine";
import StringUtil from "../../../util/StringUtil";
import moment from "moment";
import GradeChartLine2 from "../../../components/chart/GradeChartLine2";
import autobind from "autobind-decorator";
import {CaretLeftOutlined, CaretRightOutlined} from "@ant-design/icons/lib";
import {ColumnChart} from "@opd/g2plot-react";

@inject('ProductStore')
@observer
class GradeCtrRealtime extends Component<any, any> {

    state = {
        selectDate: StringUtil.formatDate(),
        loading: true,
        intervalId : null,
        fullscreenType: false,
        left1: true,
        right1: true,
    }

    async componentDidMount() {
        const {ProductStore} = this.props;
        let lastVersion = null;

        try {
            const resultNumberData = await bsApi.get('/production/grade-control-realtime/number');
            ProductStore.setGradeCtrRealVersion(resultNumberData.data.data.numbers);

            lastVersion = ProductStore.getLastVersion;
            if (lastVersion != null && lastVersion != '') {
                const resultData = await bsApi.get('/production/grade-control-realtime', {
                    params: {
                        number: lastVersion
                    }
                });
                ProductStore.setGradeCtrRealData(resultData.data.data);

                const resultChartData = await bsApi.get('/production/grade-control-realtime/chart', {
                    params: {
                        number: lastVersion
                    }
                });
                ProductStore.setGradeCtrRealChart(resultChartData.data.data);
            } else {
                message.warning("기록된 품위분석 데이터가 없습니다.");
            }
        }catch(e) {
            console.log(e.error)
        }

       // 인터벌 구간
        const interval = setInterval(async ()=>{
            const lastVersion = ProductStore.getLastVersion;
            if (lastVersion != null && lastVersion != '') {
                const resultData = await bsApi.get('/production/grade-control-realtime', {
                    params: {
                        number: lastVersion
                    }
                });
                ProductStore.setGradeCtrRealData(resultData.data.data);
                const resultChartData = await bsApi.get('/production/grade-control-realtime/chart', {
                    params: {
                        number: lastVersion
                    }
                });
                ProductStore.setGradeCtrRealChart(resultChartData.data.data);
            }
        },60000)


        this.setState({
            loading:false,
            // intervalId: interval
        })
    }

    /**
     * @description 분석회차 이벤트핸들링
     */
    @autobind
    handleChange(value: any) {
        const thisDate = this.state.selectDate
        this.props.ProductStore.setLastVersion(value)
        this.resetData(value)
    }

    /**
     * @description 계획차수 data 변경시 api 데이터 호출
     **/
    async resetData(value: number) {
        const {ProductStore} = this.props;
        this.setState({
            loading: true
        })
        let test = {
            params: {
                number: value
            }
        };
        const resultData = await bsApi.get('/production/grade-control-realtime', test);
        ProductStore.setGradeCtrRealData(resultData.data.data);
        const resultChartData = await bsApi.get('/production/grade-control-realtime/chart', {
            params: {
                number: value
            }
        });
        ProductStore.setGradeCtrRealChart(resultChartData.data.data);

        this.setState({
            loading: false
        })
    }


    /**
     * @description 계획차수 data 변경
     **/
    @autobind
    public arrowCtr2(param: string) {
        const nowversion = this.props.ProductStore.getLastVersion
        const resultData = this.props.ProductStore.getGradeCtrRealVersion.map((param: any, index: number) => {
            return {id: param.number, text: param.text, index: index}
        })
        const last = resultData.filter((value: any) => value.id === nowversion);
        if (nowversion != null && nowversion != '') {
            if (param === 'right') {
                if (last[0].index == 0) {
                    message.warning('마지막 시간기록 입니다.');
                } else {
                    this.props.ProductStore.setLastVersion(resultData[last[0].index - 1].id)
                    this.resetData(resultData[last[0].index - 1].id)
                }
            } else if (param === 'left') {
                if (last[0].index == resultData.length - 1) {
                    message.warning('첫번째 시간기록 입니다.');
                } else {
                    this.props.ProductStore.setLastVersion(resultData[last[0].index + 1].id)
                    this.resetData(resultData[last[0].index + 1].id)
                }
            }
        } else {
            message.warning('기록된 시간이 없습니다.');
        }
    }




    /**
     * @description 버튼 클릭시 컨텐츠 화면 풀스크린 모드
     */
    @autobind
    private onClickEventFullScreen() {
        const type: boolean = !this.state.fullscreenType;
        this.setState({fullscreenType: type});
    }



    componentWillUnmount(){
        // @ts-ignore
        clearInterval(this.state.intervalId)
    }

    render() {
        const {ProductStore} = this.props;
        const {selectDate} = this.state;
        const {Option} = Select;
        const config: any = {
            data:ProductStore.getGradeCtrRealChart1,
            xField: 'category',
            yField: 'value',
            colorField: 'category',
            color: ['l(90) 0:#ff9845 1:#ffb77d', 'l(90) 0:#d9bb45 1:#e4d07d'],
            legend: {
                visible: false,
                flipPage: true,
                position: 'bottom-right',
                text: {
                    style: {
                        fill: '#fff',
                        fontSize: 15,
                    },
                },
            },
            tooltip: {
                visible: false,
            },
            label: {
                visible: true,
                style: {
                    fill: '#fff',
                    stroke: false,
                    fontSize: 15,
                },
            },
            yAxis: {
                min: 0,
                title: {
                    visible: false,
                },
                //position: 'right',
                label: {
                    style: {
                        fill: '#fff',
                        fillOpacity: 0.3,
                        fontSize: 14,
                    },
                },

            },
            xAxis: {
                title: {
                    visible: false,
                },
                label: {
                    style: {
                        fill: '#fff',
                        fillOpacity: 0.3,
                        fontSize: 14,
                    },
                },
            },
            guideLine: [
                {
                    start: [0, 42.6],
                    end: [100, 42.6],
                    text: {
                        position: 'start',
                        content: '품위한계선 42.6%',
                        style: {
                            fill: '#fc4303',
                            stroke: false,
                            fontSize: 13,
                          },
                    },
                    lineStyle: {
                        stroke: '#fc4303',
                        lineDash: [4, 2],
                    },
                },
            ],
        }
        const config2: any = {
            data:ProductStore.getGradeCtrRealChart2,
            xField: 'category',
            yField: 'value',
            colorField: 'category',
            color: ['l(90) 0:#5ebf3c 1:#8fd277',
            'l(90) 0:#5ad8a6 1:#8ce4c1', 'l(90) 0:#1e9493 1:#62b4b4', 'l(90) 0:#4fc6de 1:#83d7e8',
            'l(90) 0:#5b8ff9 1:#8db1fb', 'l(90) 0:#945fb9 1:#b48fce', 'l(90) 0:#d54f97 1:#e284b6',
            'l(90) 0:#ff99c3 1:#ffb8d5', 'l(90) 0:#e86452 1:#ef9386', 'l(90) 0:#ff9845 1:#ffb77d', 'l(90) 0:#d9bb45 1:#e4d07d'],
            legend: {
                visible: false,
                flipPage: true,
                position: 'bottom-right',
                text: {
                    style: {
                        fill: '#fff',
                        fontSize: 15,
                    },
                },
            },
            tooltip: {
                visible: false,
            },
            label: {
                visible: true,
                style: {
                    fill: '#fff',
                    stroke: false,
                    fontSize: 15,
                },
            },
            yAxis: {
                min: 0,
                title: {
                    visible: false,
                },
                //position: 'right',
                label: {
                    style: {
                        fill: '#fff',
                        fillOpacity: 0.3,
                        fontSize: 14,
                    },
                },

            },
            xAxis: {
                title: {
                    visible: false,
                },
                label: {
                    style: {
                        fill: '#fff',
                        fillOpacity: 0.3,
                        fontSize: 14,
                    },
                },
            },
            guideLine: [
                {
                    start: [0, 2.6], // ['1991', 30],
                    end: [100, 2.6], // ['1999', 30],
                    text: {
                        position: 'start',
                        content: '패널티 한계선 2.6%',
                        style: {
                            fill: '#fc4303',
                            stroke: false,
                            fontSize: 13,
                          },
                    },
                    lineStyle: {
                        stroke: '#fc4303',
                        lineDash: [4, 2],
                    },
                },
            ],
        }

        return (
            <>
            <div id="container" className={(this.state.fullscreenType == true) ? 'full' : 'origin'}>
                <div>
                <h3>실시간 품위 분석</h3>
                <Button type={"primary"} className="btn_full" onClick={this.onClickEventFullScreen}>
                    {/*@ts-ignore*/}
                    <Icon type='fullscreen'/>{(this.state.fullscreenType == true) ? "OriginScreen" : "FullScreen"}
                </Button>
                <Card loading={this.state.loading}>
                    <ul className="condition">
                        <li className="ant-col-24">
                            <label>일자</label>
                            <Input value={selectDate}/>
                        </li>
                        <li className="ant-col-24">
                            <label>실시간 품위분석 기록</label>
                            <div className="input_arrow">
                                {/*@ts-ignore*/}
                                <CaretLeftOutlined onClick={() => this.arrowCtr2('left')}/>
                                <Select value={ProductStore.getLastVersion}
                                        onChange={this.handleChange}
                                >
                                    {ProductStore.getGradeCtrRealVersion.map((param: any, index: number) => {
                                        return <Option value={param.number}>{param.text}</Option>
                                    })}
                                </Select>
                                {/*@ts-ignore*/}
                                <CaretRightOutlined onClick={() => this.arrowCtr2('right')}/>
                            </div>
                        </li>
                    </ul>

                    <Table
                        columns={GradeRealColumn}
                        dataSource={ProductStore.getGradeCtrRealData}
                        pagination={false}
                        bordered
                        rowKey={record => record.key}
                    />

                    <label className="mt-2vw">실시간 품위분석 그래프</label>
                    <Card>
                        <ColumnChart {...config} className="chart inline" />
                        <ColumnChart {...config2} className="chart inline" />
                    </Card>

                </Card>
            </div>
            </div>
            </>
        );
    };
}

export default GradeCtrRealtime;
