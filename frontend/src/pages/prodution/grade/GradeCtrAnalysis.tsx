import React, {Component} from 'react';
import {Button, Card, DatePicker, message, Select, Table} from "antd";
import autobind from "autobind-decorator";

import moment from "moment";
import {inject, observer} from "mobx-react";
import Icon from "antd/es/icon";
import {bsApi} from "../../../common/Api";
import Test3 from "../../../components/chart/Test3";
import StringUtil from "../../../util/StringUtil";
import {GradeTable} from "../../../components/tableGrid/Columns";
import {CaretLeftOutlined, CaretRightOutlined} from "@ant-design/icons/lib";
import {GroupedColumnChart} from "@opd/g2plot-react";


const {Option} = Select;

@inject('ProductStore')
@observer
class GradeCtrAnalysis extends Component<any, any> {

    state = {
        loading:true,
        visible: true,
        chemiSelect: 'cao',
        chemiSelect2: 'cao',
        selectDate: StringUtil._setDate(StringUtil.formatDate(), -1,0,0),
        fullscreenType: false,
        left1: true,
        right1: true,
    }

    async componentDidMount() {
        const {ProductStore} = this.props;
        const {selectDate} = this.state;
        const resultData = await bsApi.get('/production/grade-control-analysis', {
            params: {
                date: selectDate
            }
        })
        ProductStore.setGradeCtrAnalysisData(resultData.data.data.list);


        const resultChartData = await bsApi.get('/production/grade-control-analysis/chart', {
                    params: {
                        date: selectDate,
                        chemical: 'cao'
                    }
                })

        ProductStore.setGradeCtrAnalyChartData(resultChartData.data.data)
        ProductStore.setGradeCtrAnalyChartData2(resultChartData.data.data)

        this.setState({
            loading:false
        })
    }

    /**
     * @description 날짜 바뀌면 해당날짜 데이터 불러오기
     * @param prevProps 현재 날짜와 이전 날짜의 비교할 값
     */
    componentDidUpdate(prevProps: { selectDate: string; }) {
        if (this.state.selectDate !== prevProps.selectDate) {
        }
    }

    /**
     * @description 분석항목 선택 카테고리
     **/
    @autobind
    async selectList(value: string) {
        const {selectDate} = this.state;
        this.setState({
            chemiSelect: value
        });
        const resultChartData = await bsApi.get('/production/grade-control-analysis/chart', {
            params: {
                date: selectDate,
                chemical: value
            }
        })

        this.props.ProductStore.setGradeCtrAnalyChartData(resultChartData.data.data)
    }

    @autobind
    async selectList2(value: string) {
        const {selectDate} = this.state;
        this.setState({
            chemiSelect2: value
        });
        const resultChartData = await bsApi.get('/production/grade-control-analysis/chart', {
            params: {
                date: selectDate,
                chemical: value
            }
        })

        this.props.ProductStore.setGradeCtrAnalyChartData2(resultChartData.data.data)
    }

    /**
     * @description 날짜 이벤트핸들링 메서드
     **/
    @autobind
    async dateSelect(date: moment.Moment | null, dateString: string) {
        this.setState({
            selectDate: dateString,
            loading:true
        })
        const resultData = await bsApi.get('/production/grade-control-analysis', {
            params: {
                date: dateString
            }
        })
        this.props.ProductStore.setGradeCtrAnalysisData(resultData.data.data.list);



        const resultChartData = await bsApi.get('/production/grade-control-analysis/chart', {
            params: {
                date: dateString,
                chemical: this.state.chemiSelect2
            }
        })
        this.props.ProductStore.setGradeCtrAnalyChartData2(resultChartData.data.data)

        const resultChartData2 = await bsApi.get('/production/grade-control-analysis/chart', {
            params: {
                date: dateString,
                chemical: this.state.chemiSelect
            }
        })
        this.props.ProductStore.setGradeCtrAnalyChartData(resultChartData2.data.data)


        this.setState({
            loading:false
        })
    }

    /**
     * @description 날자 좌우 클릭시 date 변경
     **/
    @autobind
    async arrowCtr(param: string) {
        const {ProductStore} = this.props;
        const {loading, selectDate} = this.state;
        this.setState({
            loading:true
        })
        let nowdate = selectDate;
        if (param === 'left') {
            const resultData = await bsApi.get('/production/grade-control-analysis', {
                params: {
                    date: StringUtil._setDate(nowdate, -1, 0, 0)
                }
            })
            ProductStore.setGradeCtrAnalysisData(resultData.data.data.list);
            this.setState({
                selectDate: StringUtil._setDate(nowdate, -1, 0, 0)
            })

            const resultChartData = await bsApi.get('/production/grade-control-analysis/chart', {
                params: {
                    date: StringUtil._setDate(nowdate, -1, 0, 0),
                    chemical: this.state.chemiSelect2
                }
            })
            this.props.ProductStore.setGradeCtrAnalyChartData2(resultChartData.data.data)

            const resultChartData2 = await bsApi.get('/production/grade-control-analysis/chart', {
                params: {
                    date: StringUtil._setDate(nowdate, -1, 0, 0),
                    chemical: this.state.chemiSelect
                }
            })
            this.props.ProductStore.setGradeCtrAnalyChartData(resultChartData2.data.data)

        } else if (param === 'right') {

            if(nowdate === StringUtil._setDate(StringUtil.formatDate(), -1,0,0)){
                message.warning('마지막 조회일자 입니다.');
            }else {

                const resultData = await bsApi.get('/production/grade-control-analysis', {
                    params: {
                        date: StringUtil._setDate(nowdate, 1, 0, 0)
                    }
                })
                ProductStore.setGradeCtrAnalysisData(resultData.data.data.list);
                this.setState({
                    selectDate: StringUtil._setDate(nowdate, 1, 0, 0)
                })

                const resultChartData = await bsApi.get('/production/grade-control-analysis/chart', {
                    params: {
                        date: StringUtil._setDate(nowdate, 1, 0, 0),
                        chemical: this.state.chemiSelect2
                    }
                })
                this.props.ProductStore.setGradeCtrAnalyChartData2(resultChartData.data.data)

                const resultChartData2 = await bsApi.get('/production/grade-control-analysis/chart', {
                    params: {
                        date: StringUtil._setDate(nowdate, 1, 0, 0),
                        chemical: this.state.chemiSelect
                    }
                })
                this.props.ProductStore.setGradeCtrAnalyChartData(resultChartData2.data.data)
            }
        }
        this.setState({
            loading:false
        })
    }

    /**
     * @description 차트 데이터 접었다 펴기
     */
    @autobind
    public cardVisible() {
        if (this.state.visible == true) {
            this.setState({
                visible: false
            })
        } else {
            this.setState({
                visible: true
            })
        }
    }

    /**
     * @description 오늘날짜 이후로는 선택 불가한 메서드
     * @param current
     */
    @autobind
    disabledDate(current:any) {
        let today = moment();
        const day = today.add(-1, "days");
        const endOfDay = day.endOf('day')
        return current && current > endOfDay;
    }

    /**
     * @description 버튼 클릭시 컨텐츠 화면 풀스크린 모드
     */
    @autobind
    private onClickEventFullScreen() {
        const type: boolean = !this.state.fullscreenType;
        this.setState({fullscreenType: type});
    }
    render() {
        const {ProductStore} = this.props;
        const {chemiSelect, chemiSelect2, selectDate} = this.state;
        const dateFormat = 'YYYY-MM-DD';
        const visibleConfirm = this.state.visible;
        const config: any = {
            // title: {
            //     visible: true,
            //     text: 'name',
            // },
            forceFit: true,
            data:ProductStore.getGradeCtrAnalyChartData,
            xField: 'date',
            yField: 'value',
            color: ['l(90) 0:#ff9845 1:#ffb77d', 'l(90) 0:#d9bb45 1:#e4d07d', 'l(90) 0:#5ebf3c 1:#8fd277', 
            'l(90) 0:#5ad8a6 1:#8ce4c1', 'l(90) 0:#1e9493 1:#62b4b4', 'l(90) 0:#4fc6de 1:#83d7e8',
            'l(90) 0:#5b8ff9 1:#8db1fb', 'l(90) 0:#945fb9 1:#b48fce', 'l(90) 0:#d54f97 1:#e284b6',
            'l(90) 0:#ff99c3 1:#ffb8d5', 'l(90) 0:#e86452 1:#ef9386'], 
            yAxis: {
                min: 0,
                title: {
                    visible: false,
                },
                //position: 'right',
                label: {
                    style: {
                        fill: '#fff',
                        fillOpacity: 0.3,
                        fontSize: 14,
                    },
                },

            },
            xAxis: {
                title: {
                    visible: false,
                },
                label: {
                    style: {
                        fill: '#fff',
                        fillOpacity: 0.3,
                        fontSize: 14,
                    },
                },
            },
            legend: {
                visible: false,
                flipPage: false,
                position: 'bottom',
                text: {
                    style: {
                        fill: '#fff',
                        fontSize: 15,
                    },
                },
            },
            tooltip: {
                visible: false,
            },
            label: {
                visible: true,
                style: {
                    fill: '#fff',
                    stroke: false,
                    fontSize: 15,
                },
            },
            groupField: 'time',
            //색깔 지정, 정하지않으면 각각 램던으로 다른 색상을 적용하게 됨
            // color: ['#1ca9e6'],
        };
        const config2: any = {
            forceFit: true,
            data:ProductStore.getGradeCtrAnalyChartData2,
            xField: 'date',
            yField: 'value',
            color: ['l(90) 0:#ff9845 1:#ffb77d', 'l(90) 0:#d9bb45 1:#e4d07d', 'l(90) 0:#5ebf3c 1:#8fd277', 
            'l(90) 0:#5ad8a6 1:#8ce4c1', 'l(90) 0:#1e9493 1:#62b4b4', 'l(90) 0:#4fc6de 1:#83d7e8',
            'l(90) 0:#5b8ff9 1:#8db1fb', 'l(90) 0:#945fb9 1:#b48fce', 'l(90) 0:#d54f97 1:#e284b6',
            'l(90) 0:#ff99c3 1:#ffb8d5', 'l(90) 0:#e86452 1:#ef9386'], 
            yAxis: {
                min: 0,
                title: {
                    visible: false,
                },
                //position: 'right',
                label: {
                    style: {
                        fill: '#fff',
                        fillOpacity: 0.3,
                        fontSize: 14,
                    },
                },

            },
            xAxis: {
                title: {
                    visible: false,
                },
                label: {
                    style: {
                        fill: '#fff',
                        fillOpacity: 0.3,
                        fontSize: 14,
                    },
                },
            },
            legend: {
                visible: false,
                flipPage: false,
                position: 'bottom',
                text: {
                    style: {
                        fill: '#fff',
                        fontSize: 15,
                    },
                },
            },
            tooltip: {
                visible: false,
            },
            label: {
                visible: true,
                style: {
                    fill: '#fff',
                    stroke: false,
                    fontSize: 15,
                },
            },

            groupField: 'time',
            //색깔 지정, 정하지않으면 각각 램던으로 다른 색상을 적용하게 됨
            // color: ['#1ca9e6'],
        };
        return (
            <>
            <div id="container" className={(this.state.fullscreenType == true) ? 'full' : 'origin'}>
                <div>
                <h3>일자별 품위 분석 로그</h3>
                <Button type={"primary"} className="btn_full" onClick={this.onClickEventFullScreen}>
                    {/*@ts-ignore*/}
                    <Icon type='fullscreen'/>{(this.state.fullscreenType == true) ? "OriginScreen" : "FullScreen"}
                </Button>
                <ul className="condition">
                    <li className="ant-col-24">
                        <label>일자</label>
                        <div className="input_arrow">
                            {/*@ts-ignore*/}
                            <CaretLeftOutlined  onClick={() => this.arrowCtr('left')}/><DatePicker value={moment(selectDate)} format={dateFormat} onChange={this.dateSelect}  disabledDate={this.disabledDate} allowClear={false}/><CaretRightOutlined onClick={() => this.arrowCtr('right')}/>
                        </div>
                    </li>
                </ul>

                <Card loading={this.state.loading}>
                    <Table
                        columns={GradeTable}
                        dataSource={ProductStore.getGradeCtrAnalysisData}
                        bordered
                        pagination={false}
                    />
                        {/*@ts-ignore*/}
                    <Button type={'primary'} className="btn_open mt-2vw" onClick={this.cardVisible}> {(visibleConfirm == true) ? '펼치기' : '접기'} {(visibleConfirm == true) ? <Icon type={'down'}/> : <Icon type={'up'}/>} </Button>

                    <div className="open_graph mt-2vw">
                        {(visibleConfirm == false) ? <>
                        <Card title={<div><label>분석항목 선택1</label>
                            <Select value={chemiSelect} onChange={this.selectList}>
                                <Option value="cao">CaO</Option>
                                <Option value="sio2">SiO<sub>2</sub></Option>
                                <Option value="al2o3">Al<sub>2</sub>O<sub>3</sub></Option>
                                <Option value="mgo">MgO</Option>
                                <Option value="fe2o3">Fe<sub>2</sub>O<sub>3</sub></Option>
                                <Option value="k2o">K<sub>2</sub>O</Option>
                                <Option value="so3">SO<sub>3</sub></Option>
                                <Option value="na2o">Na<sub>2</sub>O</Option>
                            </Select>

                        </div>}>

                            <GroupedColumnChart {...config} className="chart" />
                        </Card>
                        <Card title={<div><label>분석항목 선택</label>
                            <Select value={chemiSelect2} onChange={this.selectList2}>
                                <Option value="cao">CaO</Option>
                                <Option value="sio2">SiO<sub>2</sub></Option>
                                <Option value="al2o3">Al<sub>2</sub>O<sub>3</sub></Option>
                                <Option value="mgo">MgO</Option>
                                <Option value="fe2o3">Fe<sub>2</sub>O<sub>3</sub></Option>
                                <Option value="k2o">K<sub>2</sub>O</Option>
                                <Option value="so3">SO<sub>3</sub></Option>
                                <Option value="na2o">Na<sub>2</sub>O</Option>
                            </Select>
                        </div>}>
                            <GroupedColumnChart {...config2} className="chart" />
                        </Card> </> : null}
                    </div>
                </Card>
            </div>
            </div>
            </>
        );
    };
}

export default GradeCtrAnalysis;
