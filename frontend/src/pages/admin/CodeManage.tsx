import React, {Component, CSSProperties, useContext, useState} from 'react';
import {Button, Card, Input, InputNumber, Form, Table, Popconfirm} from "antd";
import autobind from "autobind-decorator";
import {inject, observer} from "mobx-react";
import {bsApi} from "../../common/Api";
import Icon from "antd/es/icon";
import Item from 'antd/es/list/Item';

// @ts-ignore
const EditableContext = React.createContext<any>();

interface Item {
    key:string;
    idx:string;
    code:string;
    name:string;
    grpCode:string;
    sort:string;
    regDate:string;
    item1:string;
    item2:string;
    item3:string;
    item4:string;
    item5:string;
}

interface EditableCellProps extends React.HTMLAttributes<any> {
    editing: boolean;
    dataIndex: string;
    title: any;
    inputType: 'number' | 'text';
    record: Item;
    index: number;
    children: React.ReactNode;
}

const EditableCell: React.FC<any> = ({
    editing,
    dataIndex,
    title,
    inputType,
    record,
    index,
    children,
    ...restProps
  }) => {
    const inputNode = inputType == 'number' ? <InputNumber /> : <Input />;

    return (
      <td {...restProps}>
        {editing ? (
          <Form.Item
            name={dataIndex}
            style={{ margin: 0 }}
            rules={[
              {
                required: true,
                message: `Please Input ${title}!`,
              },
            ]}
          >
            {inputNode}
          </Form.Item>
        ) : (
          children
        )}
      </td>
    );
};


 
@inject('CodeStore')
@observer
class CodeManage extends Component<any, any> {

    private offFullScreen: CSSProperties = {height: "calc(100% - 154px)"};

    private onFullScreen: CSSProperties = {
        position: "fixed",
        top: "0px",
        right: "0px",
        bottom: "0px",
        left: "0px",
        zIndex: 999
    };

    constructor(props: any) {
        super(props);
        this.state = {
                loading: true,
                loading2:true,
                codeGroup:'',
                codeName:'',
                dataSource:null,
            };
    };
    
    /**
     * @description 버튼 클릭시 컨텐츠 화면 풀스크린 모드
     */
    @autobind
    private onClickEventFullScreen() {
        const type: boolean = !this.state.fullscreenType;
        const callBack: Function = this.state.renderEvent;

        this.setState({fullscreenType: type}, function () {
            if (callBack != undefined) {
                callBack(type);
            }
        });
    }
    
    @autobind
    private handleChange(e: React.ChangeEvent<HTMLInputElement>) {
        if(e.currentTarget.name == "txtCodeName")
        {
            this.setState({ codeName: e.currentTarget.value })
        }
        else if(e.currentTarget.name == "txtCodeGroup")
        {
            this.setState({ codeGroup: e.currentTarget.value })
        }
    }

    isEditing = (record: Item) => record.idx == this.state.editingKey;

    
    @autobind
    public cancel() {
        this.setState({editingKey:''});
    };
    
    @autobind
    async save(key: React.Key) {
        const [form] = Form.useForm();
        const [data, setData] = useState(this.state.dataSource);
        this.setState({editingKey:''});
        
        try {
          const row = (await form.validateFields()) as Item;
    
          const newData = [...data];
          const index = newData.findIndex(item => key === item.key);
          if (index > -1) {
            const item = newData[index];
            newData.splice(index, 1, {
              ...item,
              ...row,
            });
            setData(newData);
            this.setState({editingKey:''});
          } else {
            newData.push(row);
            setData(newData);
            this.setState({editingKey:''});
          }
        } catch (errInfo) {
          console.log('Validate Failed:', errInfo);
        }
    }

    @autobind
    private async onSearch() {  
        
        let test = {
            params: {
                codeGroup: this.state.codeGroup,
                codeName: this.state.codeName
            }
        };  

        const resultData = await bsApi.get('/admin/code', test);

        let Item:any[] = [];

        for(let i=0; i < resultData.data.data.list.length; i++)
        {
            let row = resultData.data.data.list[i];
            Item.push({
                key: row.idx,
                ...row
            })
        }

        this.setState({dataSource:Item});
    }
        
    async componentDidMount() {        
        await this.onSearch();

        this.setState({
            loading: false,
            loading2:false
        })
    }
    
    @autobind
    private handleSearch() {
        this.onSearch();
    }

    render() {
        const components = {
            body: {
                cell: EditableCell
            }
        };
        const {loading, loading2} = this.state;

        const columns:any = [
                {
                    key:'idx',
                    title: 'idx',
                    dataIndex: 'idx',
                },
                {
                    key:'code',
                    title: '코드',
                    dataIndex: 'code',
                    editable:true
                },
                {
                    key:'name',
                    title: '코드명',
                    dataIndex: 'name',
                    editable:true
                },
                {
                    key:'group',
                    title: '그룹코드',
                    dataIndex: 'group',
                    editable:true
                },
                {
                    key:'idx',
                    title: '정렬',
                    dataIndex: 'sort',
                    editable:true
                },
                {
                    key:'idx',
                    title: '등록일자',
                    dataIndex: 'regDate',
                },
                {
                    key:'idx',
                    title: '지정값_1',
                    dataIndex: 'item1',
                    editable:true
                },
                {
                    key:'idx',
                    title: '지정값_2',
                    dataIndex: 'item2',
                    editable:true
                },
                {
                    key:'idx',
                    title: '지정값_3',
                    dataIndex: 'item3',
                    editable:true
                },
                {
                    key:'idx',
                    title: '지정값_4',
                    dataIndex: 'item4',
                    editable:true
                },
                {
                    key:'idx',
                    title: '지정값_5',
                    dataIndex: 'item_5',
                    editable:true
                },
                {
                    title: 'operation',
                    dataIndex: 'operation',
                    render: (_: any, record: Item) => {
                        const editable = this.isEditing(record);
                        return (
                            <React.Fragment>
                                {editable ? (
                                    <React.Fragment>
                                        <EditableContext.Consumer>
                                            {() => (
                                                <a onClick={() => this.save(record.key)}>저장</a>
                                            )}
                                        </EditableContext.Consumer>
                                        {/*<a onClick={() => this.edit(record)}>취소</a>*/}
                                        <a onClick={(e) => {
                                            const [form] = Form.useForm();
                                            form.setFieldsValue({...record });
                                            this.setState({editingKey:""});
                                        }}>취소</a>
                                    </React.Fragment>
                                ) : (
                                    <React.Fragment>
                                        <a onClick={(e) => {
                                            const [form] = Form.useForm();
                                            form.setFieldsValue({...record });

                                            this.setState({editingKey:record.key});
                                        }}>수정</a>
                                    </React.Fragment>
                                )}
                            </React.Fragment>
                        )
                    },
                },
            ];

        const mergedColumns = columns.map((col:any) => {
            if (!col.editable) {
                return col;
            }
            return {
                ...col,
                onCell: (record: Item) => ({
                    record,
                    inputType: col.dataIndex == 'age' ? 'number' : 'text',
                    dataIndex: col.dataIndex,
                    title: col.title,
                    editing: this.isEditing(record),
                }),
            };
        });

        return (
            <Card loading={loading}>
                <div id="container">
                    <div className={(this.state.fullscreenType == true) ? 'full' : 'origin'}>
                        <h3>공통코드 관리</h3>
                        <Button type={"primary"} className="btn_full" onClick={this.onClickEventFullScreen}>
                            {/*@ts-ignore*/}
                            <Icon type='fullscreen'/>{(this.state.fullscreenType == true) ? "OriginScreen" : "FullScreen"}
                        </Button>
                        <ul className="condition">
                            <li className="ant-col-24">
                                <label>코드그룹</label>
                                <div className="input_arrow">
                                    <Input name="txtCodeGroup" value={this.state.codeGroup} onChange={this.handleChange} />
                                </div>
                                <label>코드명</label>
                                <div className="input_arrow">
                                    <Input name="txtCodeName" value={this.state.codeName} onChange={this.handleChange} />
                                </div>
                                
                                <Button style={{float: "right", width: 100}} >추가</Button>
                                <Button type={'primary'} style={{float: "right", width: 100}} onClick={this.handleSearch}>조회</Button>
                            </li>
                        </ul>
                        <Card loading={loading2}>
                            <Table 
                                components={components}
                                columns={mergedColumns}
                                dataSource={this.state.dataSource}
                                scroll={{y: 800}}
                                pagination={false}
                            />
                        </Card>
                    </div>
                </div>
            </Card>            
        );
    };
}

export default CodeManage;