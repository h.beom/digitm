import React, {Component} from 'react';
import autobind from "autobind-decorator";

class IntroPage extends Component<any, any> {

    @autobind
    test(){
        location.href= process.env.REACT_APP_HREFURL as string
    }

    render() {
        return (
            <>
            <div className="img_bg"  onClick={this.test}>
                <h1 className="logo"></h1>
                <div className="img_main"><img src={require('../../resources/img_main.gif')} alt="" /></div>
                <div className="img_intro">
                    <h2><em>D</em>IGITAL <em>M</em>INING <em>S</em>OLUTIONS</h2>
                    <ul>
                        <li><h5>위치 추적 &amp; 통신</h5><p>Tracking &amp; Communications</p></li>
                        <li><h5>채광환경 제어</h5><p>Mining Environment</p></li>
                        <li><h5>광산운영 &amp; 생산</h5><p>Operations &amp; Production</p></li>
                        <li><h5>통합분석 &amp; 성과</h5><p>Analytics &amp; KPI</p></li>
                        <li><h5>통신 네트워크 관리</h5><p>Network Management</p></li>
                    </ul>
                </div>
                <div className="footer">ⓒ <strong>KORES Digital Mining Co.</strong></div>
            </div>
            </>
        );
    };
}

export default IntroPage;
