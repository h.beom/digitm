import axios from 'axios';

const API_URL = process.env.REACT_APP_PROXY as string + 'api/';
export const bsApi = axios.create({
    baseURL: API_URL,
});


export const authApi = axios.create({
    baseURL: API_URL,
});

export const setToken = (cookies: { accessToken: string }) => {
    const { accessToken } = cookies;
    bsApi.defaults.headers.common['Authorization'] = `Bearer ${accessToken}`;
};
