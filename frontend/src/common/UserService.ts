//import Collator = Intl.Collator;
import axios, {AxiosRequestConfig} from "axios";
import {BehaviorSubject} from 'rxjs';
import {authApi, setToken} from "./Api";
import nookies from "nookies";

const currentUserSubject = new BehaviorSubject(JSON.parse(<string>localStorage.getItem('currentUser')));
const API_URL = process.env.REACT_APP_PROXY as string + 'api/';

/**
 * @description axios(http 클라이언트 라이브러리--typeScript지원) (Searvice.ts)
 */
class UserService {
    get currentUserValue() {
        return currentUserSubject.value;
    }

    get currentUser() {
        return currentUserSubject.asObservable();
    }


    /**
     * description 로그인 메서드
     * @param user
     */
    async login(user: { username: string; password: string; }) {
        const headers = {
            authorization: 'Basic ' + btoa(user.username + ':' + user.password)
        };

        const {data} = await authApi.get('/login', {headers: headers});
        if (data.token) {
            setToken({
                accessToken:data.token,
            });
            localStorage.setItem('currentUser', JSON.stringify(data));
            localStorage.setItem('refresh', JSON.stringify(data.token));
            currentUserSubject.next(data);
        }
        return data
    }

    /**
     * description 로그아웃 메서드
     * @param user
     */
    logOut() {
        const token = this.currentUserValue.token;
        localStorage.removeItem('currentUser');
        localStorage.removeItem('refresh');
        const headers = {
            authorization: `Bearer ${token}`
        };
        return axios.post(API_URL + "logout", {}, {headers: headers})
            .then(response => {
                localStorage.removeItem('currentUser');
                localStorage.removeItem('refresh');
                currentUserSubject.next(null);
            });
    }
}

export default new UserService();
