import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';
import stores from "./stores/stores";

import 'antd/dist/antd.css';
import {Provider} from "mobx-react";
import {BrowserRouter, Route, Switch} from "react-router-dom";

import AuthGuard from "./components/AuthGuard";
import LoginForm from "./pages/login/LoginForm";
import {Role} from "./type/Role";
import IntroPage from "./pages/mainPage/IntroPage";

const store: object = {...stores};


const Root = (
    <BrowserRouter>
        <Provider {...store}>
            <Switch>
                <Route path="/" component={IntroPage} exact={true}/>
                {(process.env.REACT_APP_PUBLISH as string == 'PUB')? <Route path="/digitalMining" component={App}/> : <AuthGuard path="/digitalMining" roles={[Role.ADMIN, Role.USER]} component={App}/>}
                <Route exact path="/login" component={LoginForm}/>
            </Switch>
        </Provider>
    </BrowserRouter>
);

ReactDOM.render(Root, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
