import {CanvasTexture, ClampToEdgeWrapping, Group, LinearFilter, Object3D, Sprite, SpriteMaterial} from 'three';


export default class Model3DObject{

    private _axis:Group = new Group();

    constructor(){}

    public init(data:{obj:Object3D, postition:number, text?:string } ){
        //red bar ..
        this.addModel( data.obj, data.postition, data.text);
    }

    public getGroup():Group{
        return this._axis;
    }

    private addModel( obj:Object3D, postition:any, text?:string ):void{
        obj.position.x = postition.x - 178600 -450;
        obj.position.y = postition.z - 75;
        obj.position.z = postition.y - 197045 - 200;

        this._axis.add(obj);



        if(text != undefined){
            const canvas:HTMLCanvasElement | null = this.makeLabelCanvas(20, 10, text );

            if(canvas == null){
                return;
            }
            const texture:CanvasTexture = new CanvasTexture(canvas);
            // because our canvas is likely not a power of 2
            // in both dimensions set the filtering appropriately.
            texture.minFilter = LinearFilter;
            texture.wrapS = ClampToEdgeWrapping;
            texture.wrapT = ClampToEdgeWrapping;

            const labelMaterial = new SpriteMaterial({
                map: texture,
                transparent: true,
            });
            const labelBaseScale = 0.51;

            const label:Sprite = new Sprite(labelMaterial);
            label.scale.x = canvas.width  * labelBaseScale;
            label.scale.y = canvas.height * labelBaseScale;

            // label.position.x = modelItem.stencPosx -  ( mapRangeLenx / 2 );
            // label.position.y = 25;
            // label.position.z = modelItem.stencPosy - ( mapRangeLeny / 2.5 ) ;

            label.position.x = postition.x - 178600 -450;
            label.position.y = postition.z - 65;
            label.position.z = postition.y - 197045 - 200;

            this._axis.add(label);
        }


    }

    // https://threejsfundamentals.org/threejs/lessons/threejs-billboards.html 참조
    private makeLabelCanvas(baseWidth:number, size:number, name:string):HTMLCanvasElement | null{
        const ctx:CanvasRenderingContext2D | null = document.createElement('canvas').getContext('2d');
        if(ctx == null){
            return null;
        }

        const fontWidth : number = name.length * 5.2;
        if(baseWidth < fontWidth){
            baseWidth = fontWidth;
        }
        const borderSize:number = 2;
        const font:string =  `${size}px bold sans-serif`;
        const textWidth:number = ctx.measureText(name).width;

        const doubleBorderSize:number = borderSize * 2;
        const width:number = baseWidth + doubleBorderSize;
        const height:number = size + doubleBorderSize;

        ctx.font = font;
        ctx.canvas.width = width;
        ctx.canvas.height = height;

        // need to set font again after resizing canvas
        ctx.font = font;
        ctx.textBaseline = 'middle';
        ctx.textAlign = 'center';

        // ctx.strokeRect(-10,-10,width,height);
        ctx.fillStyle = 'white';
        ctx.fillRect(0, 0, width, height);

        ctx.fillStyle = 'black';
        ctx.strokeRect(0, 0, width, height);

        // scale to fit but don't stretch
        const scaleFactor = Math.min(1, baseWidth / textWidth);
        ctx.translate(width / 2, height / 2);
        ctx.scale(scaleFactor, 1);
        ctx.fillStyle = 'black';
        ctx.fillText(name, 0, 0);

        return ctx.canvas;
    }

}
