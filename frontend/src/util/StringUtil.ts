//import Collator = Intl.Collator;

export default class StringUtil{

    /**
     * @type function
     * @param {Date} date 날짜함수
     * @returns "yyyy-mm-dd"날짜
     * @description 날짜 format 설정 함수
     */
    public static formatDate(date: Date = new Date()):string{
        const d:Date = date;
        const month:string = StringUtil.lpad((d.getMonth() + 1).toString() , 2 , "0");
        const day:string = StringUtil.lpad((d.getDate()).toString() , 2 , "0");
        const year:string = (d.getFullYear()).toString();
       return [year, month, day].join('-').toString();
    }
    public static formatDate2(date: Date = new Date()):string{
        const d:Date = date;
        const month:string = StringUtil.lpad((d.getMonth() + 1).toString() , 2 , "0");
        const year:string = (d.getFullYear()).toString();
        return [year, month].join('-').toString();
    }


    public static  setDay(dateStr:string , dd:number):string{
        return this._setDate(dateStr, dd);
    }

    public static  setMonth(dateStr:string , mm:number):string{
        return this._setDate(dateStr, undefined, mm);
    }

    public static setYear(dateStr:string , yy:number):string{
        return this._setDate(dateStr, undefined, undefined, yy);
    }
    /**
     * @type function
     * @param {Number} dd? 증가될 day
     * @param {Number} mm? 증가될 momth
     * @return "yyyy-mm-dd"날짜
     * @description Date 계산기
     */
    static _setDate(dateStr: string, dd?: number, mm?: number, yy?: number):string{
        const date:string[] = dateStr.split('-');
        const day:Date= new Date( Number( date[0] ) , (Number(date[1] ) - 1) , Number( date[2] ) );
        const resultYY:number = (yy === undefined) ? day.getFullYear() : Number(day.getFullYear() + yy);
        const resultMM:number= (mm === undefined) ? day.getMonth() : Number(day.getMonth() + mm );
        const resultDD:number = (dd === undefined) ? day.getDate() : Number(day.getDate() + dd);
        const resultDate = new Date(resultYY, resultMM , resultDD );
        return StringUtil.formatDate(resultDate);
    }
    static _setDate2(dateStr: string, mm?: number, yy?: number):string{
        const date:string[] = dateStr.split('-');
        const day:Date= new Date( Number( date[0] ) , (Number(date[1] ) - 1));
        const resultYY:number = (yy === undefined) ? day.getFullYear() : Number(day.getFullYear() + yy);
        const resultMM:number= (mm === undefined) ? day.getMonth() : Number(day.getMonth() + mm );
        const resultDate = new Date(resultYY, resultMM );
        return StringUtil.formatDate2(resultDate);
    }


    /*
    *  originalstr: lpad 할 text
    * length: lpad할 길이
    * strToPad: lpad 시킬 text
    */
    public static lpad(originalstr:string = "", length:number = 0 , strToPad:string = ""):string{
        while (originalstr.length < length){
            originalstr = strToPad + originalstr;
        }
        return originalstr;
    }

    private static _TODAY:Date = new Date();

    static get getDate():Date{
        return StringUtil._TODAY;
    }

    /**
     * @type function
     * @param {String} leftText
     * @param {String} rightText
     * @description text Sort 함수
     */
    public static getFullTextSort( leftText:string , rightText:string ){
        leftText = leftText.split(' ').join('').toLowerCase();
        rightText = rightText.split(' ').join('').toLowerCase();
        let collator = new Intl.Collator(undefined, {numeric: true, sensitivity: 'base'});
        return collator.compare( leftText , rightText );
    }

    /**
     * @type function
     * @param {Number} num float 데이터
     * @param {Number} digite 소수점 반올림 자리수
     * @description javascript 소수점 반올림하는 함수
     */
    public static setFloat( { num , digite=2 }: { num:number , digite?:number }){
        return parseFloat((num).toFixed(digite));
    }

    public static setSecondToHHMMSS(second:string):{houre:string, minutes:string , seconds:string}{
        let sec_num:number = parseInt(second, 10); // don't forget the second param
        let hoursNum:number   = Math.floor(sec_num / 3600);
        let minutesNum:number = Math.floor((sec_num - (hoursNum * 3600)) / 60);
        let secondsNum:number = sec_num - (hoursNum * 3600) - (minutesNum * 60);

        let houresResult:string   = '';
        let minutesResult:string   = '';
        let secondsResult:string   = '';

        if (hoursNum   < 10) {houresResult   = "0"+hoursNum.toString();}
        if (minutesNum < 10) {minutesResult = "0"+minutesNum.toString();}
        if (secondsNum < 10) {secondsResult = "0"+secondsNum.toString();}
        return {houre:houresResult, minutes:minutesResult , seconds:secondsResult};
    }

    public static setStringSecondToHHMMSS(second:string):string{
        const resultData:{houre:string, minutes:string , seconds:string} = this.setSecondToHHMMSS(second);
        const result:string =`${resultData.houre}:${resultData.minutes}:${resultData.seconds}`;
        return result;
    }

    public static setDigit2(num:number):string{

        let length:number = Math.log(num) * Math.LOG10E + 1 | 0;  // for positive integers

        if( length < 2 ) {
            return '0' + num.toString();
        } else {
            return num.toString();
        }
    }

    public static setTimerhhmmss(second:number):string{
        let hours:number   = Math.floor(second / 3600);
        let minutes:number = Math.floor((second - (hours * 3600)) / 60);
        let seconds:number = second - (hours * 3600) - (minutes * 60);
        seconds = Math.round(seconds);

        let houresStr:string    = (hours   < 10) ? "0"+hours.toString() : hours.toString();
        let minutesStr:string   = (minutes < 10) ? "0"+minutes.toString() : minutes.toString();
        let secondsStr:string   = (seconds < 10) ? "0"+seconds.toString() : seconds.toString();
        return houresStr+':'+minutesStr+':'+secondsStr;
    }


}
