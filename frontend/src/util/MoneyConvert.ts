export default class StringUtil {
    public static commas(number: number) {
        // @ts-ignore
        if (number == undefined) {
            return '0'
        }else{
            let regexp = /\B(?=(\d{3})+(?!\d))/g;
            return number.toString().replace(regexp, ',');
        }
    }
}
