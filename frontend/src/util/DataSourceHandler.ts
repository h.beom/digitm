
import StringUtile from "./StringUtil";

/**
 * 정렬을 위한 클래스
 */
export default class DataSourceHandler{
    /**
     *
     * @description 정렬속성들을 수집하여 UI에서 선택한 정렬타입으로 데이터를 재정렬한다.
     * @param chartdata 정렬을 배치하기위한 차트의 data information
     * @param sortType 정렬방법
     * @param sortName 정렬할 아이템대상
     * @param txtSortName 정렬속성이름
     */
    public static chartDataSort(chartdata:Array<any> ,sortType:string , sortName:string , txtSortName:string | undefined ):Array<any>{
        let chartData : Array<any> = Object.assign([] , chartdata );
        chartData = chartData.sort(function(leftItem:any , rightItem:any ){
            switch(sortType){
                case 'DESC' : {
                    return ( leftItem[sortName] < rightItem[sortName] ) ? 1 : -1;
                    break;
                }
                case 'ASC' : {
                    return ( leftItem[sortName] > rightItem[sortName] ) ? 1 : -1;
                    break;
                }
                default: {
                    if(txtSortName == undefined ){
                        txtSortName ="id";
                    }
                    return StringUtile.getFullTextSort(leftItem[txtSortName] , rightItem[txtSortName]);
                    break;
                }
            }
        });
        return chartData;
    }
}
