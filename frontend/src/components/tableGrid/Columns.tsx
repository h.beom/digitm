import React from 'react';
// Title밑에 width값 조절 가능  (sample site --> https://codesandbox.io/s/8zqf3
import MoneyConvert from "../../util/MoneyConvert";

// ::::::::::::::::::::::::::::::::::::::::::::::::광산운영 & 생산::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

export const GradeTable = [
    {
        title: '시간',
        dataIndex: 'time',
        key: 'time',
    },
    {
        title: '투입량(T)',
        dataIndex: 'input',
        key: 'input',
    },
    {
        title: 'NO',
        dataIndex: 'number',
        key: 'number',
    },
    {
        title: 'CHEMICAL COMPONENTS(%)',
        children: [
            {
                title: (<p>SiO<sub>2</sub></p>),
                dataIndex: 'sio2',
                key: 'sio2',
            }, {
                title: (<p>Al<sub>2</sub>O<sub>3</sub></p>),
                dataIndex: 'al2o3',
                key: 'al2o3',
            },{
                title: (<p>Fe<sub>2</sub>O<sub>3</sub></p>),
                dataIndex: 'fe2o3',
                key: 'fe2o3',
            }, {
                title: (<p>CaO</p>),
                dataIndex: 'cao',
                key: 'cao',

            },{
                title: (<p>MgO</p>),
                dataIndex: 'mgo',
                key: 'mgo',

            },{
                title: (<p>SO<sub>3</sub></p>),
                dataIndex: 'so3',
                key: 'so3',

            },
            {
                title: (<p>K<sub>2</sub>O</p>),
                dataIndex: 'k2o',
                key: 'k2o',
            }, {
                title: (<p>Na<sub>2</sub>O</p>),
                dataIndex: 'na2o',
                key: 'na2o',
            },
            {
                title: 'Sum',
                dataIndex: 'sum',
                key: 'sum',
            },
        ],
    },
    {
        title: '누적 CHEMICAL COMPONENTS(%)',
        children: [
            {
                title: (<p>SiO<sub>2</sub></p>),
                dataIndex: 'sio2Acc',
                key: 'sio2Acc',

            }, {
                title: (<p>Al<sub>2</sub>O<sub>3</sub></p>),
                dataIndex: 'al2o3Acc',
                key: 'al2o3Acc',
            },{
                title: (<p>Fe<sub>2</sub>O<sub>3</sub></p>),
                dataIndex: 'fe2o3Acc',
                key: 'fe2o3Acc',
            }, {
                title: (<p>CaO</p>),
                dataIndex: 'caoAcc',
                key: 'caoAcc',

            },{
                title: (<p>MgO</p>),
                dataIndex: 'mgoAcc',
                key: 'mgoAcc',

            },{
                title: (<p>SO<sub>3</sub></p>),
                dataIndex: 'so3Acc',
                key: 'so3Acc',

            },
            {
                title: (<p>K<sub>2</sub>O</p>),
                dataIndex: 'k2oAcc',
                key: 'k2oAcc',
            }, {
                title: (<p>Na<sub>2</sub>O</p>),
                dataIndex: 'na2oAcc',
                key: 'na2oAcc',
            },
            {
                title: 'Sum',
                dataIndex: 'sumAcc',
                key: 'sumAcc',
            },
        ],
    },
];

// ==========================================================================
export const GradeRealColumn = [
    {
        title: '구분',
        dataIndex: 'date',
        key: 'date',
    },
    {
        title: '시간',
        dataIndex: 'time',
        key: 'time',
    },
    {
        title: '외부',
        dataIndex: 'inOuter',
        key: 'inOuter',
    },
    {
        title: '건동(굴진)',
        dataIndex: 'inInner',
        key: 'inInner',
    },
    {
        title: '갱(막장)',
        dataIndex: 'note',
        key: 'note',
    },
    {
        title: '투입량(T)',
        dataIndex: 'input',
        key: 'input',
    },
    {
        title: '누적합계(T)',
        dataIndex: 'inputAcc',
        key: 'inputAcc',
    },
    {
        title: 'NO',
        dataIndex: 'number',
        key: 'number',
    },
    {
        title: 'CHEMICAL COMPONENTS(%)',
        children: [
            {
                title: (<p>SiO<sub>2</sub></p>),
                dataIndex: 'sio2',
                key: 'sio2',
            }, {
                title: (<p>Al<sub>2</sub>O<sub>3</sub></p>),
                dataIndex: 'al2o3',
                key: 'al2o3',
            },{
                title: (<p>Fe<sub>2</sub>O<sub>3</sub></p>),
                dataIndex: 'fe2o3',
                key: 'fe2o3',
            }, {
                title: (<p>CaO</p>),
                dataIndex: 'cao',
                key: 'cao',

            },{
                title: (<p>MgO</p>),
                dataIndex: 'mgo',
                key: 'mgo',

            },{
                title: (<p>SO<sub>3</sub></p>),
                dataIndex: 'so3',
                key: 'so3',

            },
            {
                title: (<p>K<sub>2</sub>O</p>),
                dataIndex: 'k2o',
                key: 'k2o',
            }, {
                title: (<p>Na<sub>2</sub>O</p>),
                dataIndex: 'na2o',
                key: 'na2o',
            },
            {
                title: 'Sum',
                dataIndex: 'sum',
                key: 'sum',
            },
        ],
    },
    {
        title: '누적 CHEMICAL COMPONENTS(%)',
        children: [
            {
                title: (<p>SiO<sub>2</sub></p>),
                dataIndex: 'sio2Acc',
                key: 'sio2Acc',

            }, {
                title: (<p>Al<sub>2</sub>O<sub>3</sub></p>),
                dataIndex: 'al2o3Acc',
                key: 'al2o3Acc',
            },{
                title: (<p>Fe<sub>2</sub>O<sub>3</sub></p>),
                dataIndex: 'fe2o3Acc',
                key: 'fe2o3Acc',
            }, {
                title: (<p>CaO</p>),
                dataIndex: 'caoAcc',
                key: 'caoAcc',

            },{
                title: (<p>MgO</p>),
                dataIndex: 'mgoAcc',
                key: 'mgoAcc',

            },{
                title: (<p>SO<sub>3</sub></p>),
                dataIndex: 'so3Acc',
                key: 'so3Acc',

            },
            {
                title: (<p>K<sub>2</sub>O</p>),
                dataIndex: 'k2oAcc',
                key: 'k2oAcc',
            }, {
                title: (<p>Na<sub>2</sub>O</p>),
                dataIndex: 'na2oAcc',
                key: 'na2oAcc',
            },
            {
                title: 'Sum',
                dataIndex: 'sumAcc',
                key: 'sumAcc',
            },
        ],
    },
];

/**
 * @description 일자별 생산&구매 실적 Grid Columns
 */
export const weighbridgeColumns = [
    {
        title: '회차',
        dataIndex: 'number',
    }, {
        title: '시간',
        dataIndex: 'time',
    }, {
        title: '거래처',
        dataIndex: 'source',
    }, {
        title: '품명',
        dataIndex: 'productName',
    }, {
        title: '실중량(KG)',
        dataIndex: 'weight',
    }, {
        title: '구분',
        dataIndex: 'type',

    },
];
export const PlanningColumn = [
    {
        title: '일자',
        dataIndex: 'date',
        key:'date'
    },
    {
        title: '계획생산량',
        dataIndex: 'amount',
        key:'amount',
        width: '40%',
        render:(text:number) =>{
            return MoneyConvert.commas(text);
        }
    },
    {
        title: '누적계획생산량',
        dataIndex: 'acc',
        key:'acc',
        width: '40%',
        render:(text:number) =>{
            return MoneyConvert.commas(text);
        }
    },
];

export const threeMonitor = [
    {
        title: '연변',
        dataIndex: 'number',
        key:'number'
    },{
        title: '기록시간',
        dataIndex: 'datetime',
        key:'datetime'
    },{
        title: '종류',
        dataIndex: 'categoryName',
        key:'categoryName'
    },{
        title: '속성',
        dataIndex: 'typeName',
        key:'typeName'
    },{
        title: '반지름',
        dataIndex: 'radius',
        key:'radius'
    },{
        title: '음원x좌표',
        dataIndex: 'x',
        key:'x'
    },{
        title: '음원y좌표',
        dataIndex: 'y',
        key:'y'
    },{
        title: '음원z좌표',
        dataIndex: 'z',
        key:'z'
    },

];

// ::::::::::::::::::::::::::::::::::::::::::::::::채광환경 제어::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

/**
 * @description ground 기간별 지반변위 분석 테이블 컬럼(센서목록을 공통으로 사용하면 되니 지워도 됨)
 */
export const GroundAnalysisColumn = [
    {
        title: '좌표 x',
        dataIndex: 'posX',
    }, {
        title: '좌표 y',
        dataIndex: 'posY',
    }, {
        title: '좌표 z',
        dataIndex: 'posZ',
    }, {
        title: '센서 ID',
        dataIndex: 'sensorId',
    }
];

/**
 * @description 센서 목록 테이블 컬럼
 */
export const EnvironmentSensorColumn = [
    {
        title: '좌표 x',
        dataIndex: 'x',
        key: 'x'
    }, {
        title: '좌표 y',
        dataIndex: 'y',
        key: 'y'
    }, {
        title: '좌표 z',
        dataIndex: 'z',
        key: 'z'
    }, {
        title: '센서 ID',
        dataIndex: 'sensorId',
        key: 'sensorId'
    }
];

/**
 * @description 센서 목록 테이블 컬럼
 */
