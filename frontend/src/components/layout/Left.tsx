import React, {Component, Fragment} from 'react';
import {inject, observer} from "mobx-react";
import {Layout, Menu} from "antd";
import {Link} from "react-router-dom";
import SubMenu from "antd/es/menu/SubMenu";
import {MenuItemList} from "../../type/IndexType";
import autobind from "autobind-decorator";
import Service from "../../common/UserService";
import {createBrowserHistory} from 'history';
import {Button} from "antd/es";
import Icon from "antd/es/icon";
import axios from "axios";

/**
 * @description 좌측메뉴의 메뉴리스트를 출력하는 클래스
 */
@inject('IndexStore', 'ProductStore', 'EnvironmentStore')
@observer
class Left extends Component<any, any> {
    constructor(props: any) {
        super(props);

        this.state = {
            history: createBrowserHistory(),
            currentUser: null,
            isAdmin: false,
            currentLocation: window.location.pathname
        };
    }


    @autobind
    private logout() {
        Service.logOut()
            .then(
                data => {
                    this.state.history.push('/');
                    // eslint-disable-next-line no-restricted-globals
                    location.href = "/";


                },
                error => {
                    this.setState({
                        errorMessage: "Unexpected error occurred"
                    });
                }
            );
    }

A
    private setTree(menuItem: MenuItemList[]): any {
        let jsxResult: any = menuItem.map((value: MenuItemList) => {

            // 자식이 더이상 없는 최하위 메뉴.
            if (value.depth === 3) {
                return <Menu.Item key={value.id}>
                    <Link to={value.url}>{value.text}</Link>
                </Menu.Item>
            } else {
                let textTag: any = value.text;
                if (value.icon !== null) {
                    textTag = <p>{value.text}<span>{value.subtext}</span></p>
                }
                // 대메뉴
                if (value.depth == 1) {
                    return <SubMenu key={value.id} title={textTag}>
                        {this.setTree(value.child)}
                    </SubMenu>
                    // 소메뉴와 대메뉴 사이의 메뉴(2번째의 개념과는 조금 다름)
                } else if (value.depth == 2) {
                    return <SubMenu key={value.id} title={textTag}>
                        {this.setTree(value.child)}
                    </SubMenu>
                }
            }
        });
        return jsxResult;
    }


    render() {
        const {IndexStore} = this.props;
        let jsxResult: any = this.setTree(IndexStore.getLeftMenuList);


        return (
            <>
                <h1 className="logo"><a href=""><img src={require('../../resources/logo.png')}/></a></h1>
                <div className="log_info">
                    <div className="log_info_child">
                        {/*@ts-ignore*/}
                        <p><Icon type={'user'}/>광물공(디지털마이닝팀)</p>
                        <Button id={'test'} className="btn_logout" onClick={this.logout}>Logout</Button>
                    </div>
                </div>
                <Menu
                    defaultSelectedKeys={['1']}
                    mode="inline"
                    theme="dark">

                    {jsxResult}

                </Menu>
                <p className="f_link"><a href="">마스터정보관리</a></p>
            </>
        );
    };
}


export default Left;
