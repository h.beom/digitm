import React, {Component} from 'react';
import {inject, observer} from "mobx-react";


/**
 * @description 헤더 메뉴리스트를 출력하는 클래스
 */
@inject('IndexStore')
@observer
class Top extends Component<any, any> {

    render() {
        return (
            <>
                <div>TopMenu Teest</div>
                <div>TopMenu TeestTopMenu TeestTopMenu TeestTopMenu TeestTopMenu TeestTopMenu Teest</div>
            </>
        );
    };
}


export default Top;
