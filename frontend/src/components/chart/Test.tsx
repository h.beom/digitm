import React from 'react'
// @ts-ignore
import {GroupedColumnChart} from '@opd/g2plot-react'


const data = [

            {
                "date": "8/25",
                "name": "prodMove",
                "value": 3076377
            },
            {
                "date": "8/25",
                "name": "purMove",
                "value": 0
            },
            {
                "date": "8/25",
                "name": "prodAcc",
                "value": 508990
            },
            {
                "date": "8/25",
                "name": "purAcc",
                "value": 0
            },
            {
                "date": "8/26",
                "name": "prodMove",
                "value": 3076377
            },
            {
                "date": "8/26",
                "name": "purMove",
                "value": 0
            },
            {
                "date": "8/26",
                "name": "prodAcc",
                "value": 508990
            },
            {
                "date": "8/26",
                "name": "purAcc",
                "value": 0
            },
            {
                "date": "8/27",
                "name": "prodMove",
                "value": 3076377
            },
            {
                "date": "8/27",
                "name": "purMove",
                "value": 0
            },
            {
                "date": "8/27",
                "name": "prodAcc",
                "value": 508990
            },
            {
                "date": "8/27",
                "name": "purAcc",
                "value": 0
            },
            {
                "date": "8/28",
                "name": "prodMove",
                "value": 3076377
            },
            {
                "date": "8/28",
                "name": "purMove",
                "value": 0
            },
            {
                "date": "8/28",
                "name": "prodAcc",
                "value": 508990
            },
            {
                "date": "8/28",
                "name": "purAcc",
                "value": 0
            },
            {
                "date": "8/29",
                "name": "prodMove",
                "value": 3076377
            },
            {
                "date": "8/29",
                "name": "purMove",
                "value": 0
            },
            {
                "date": "8/29",
                "name": "prodAcc",
                "value": 508990
            },
            {
                "date": "8/29",
                "name": "purAcc",
                "value": 0
            },
            {
                "date": "8/30",
                "name": "prodMove",
                "value": 3076377
            },
            {
                "date": "8/30",
                "name": "purMove",
                "value": 0
            },
            {
                "date": "8/30",
                "name": "prodAcc",
                "value": 508990
            },
            {
                "date": "8/30",
                "name": "purAcc",
                "value": 3076377
            },
            {
                "date": "8/31",
                "name": "prodMove",
                "value": 3076377
            },
            {
                "date": "8/31",
                "name": "purMove",
                "value": 3076377
            },
            {
                "date": "8/31",
                "name": "prodAcc",
                "value": 508990
            },
            {
                "date": "8/31",
                "name": "purAcc",
                "value": 3076377
            }

];


const config: any = {
    title: {
        visible: true,
        text: 'name',
    },
    forceFit: true,
    data,
    xField: 'date',
    yField: 'value',
    yAxis: {
        min: 0,
    },
    label: {
        visible: true,
    },
    groupField: 'name',
    color: ['#1ca9e6', '#f88c24' ,'#fffff'],
};


// @ts-ignore
export default () => <GroupedColumnChart {...config} style={{height: 400}}/>
