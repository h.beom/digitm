import React from 'react'
import {ColumnChart} from '@opd/g2plot-react'
import {ColorKeywords} from "three";

const data = [
    {
        "category": 'cao',
        "value": 43.61,
    }, {
        "category": 'caoAcc',
        "value": 43.61,
    }
];

const config: any = {
    // title: {
    //     visible: true,
    //     text: '自定义图表辅助线',
    // },
    // description: {
    //     visible: true,
    //     text: '通过start和end自定义辅助线，支持传入位置百分比或数据',
    // },
    data,
    xField: 'category',
    yField: 'value',
    label: {
        visible: true,
        style: {
            fill: '#0D0E68',
            fontSize: 12,
            fontWeight: 600,
            opacity: 0.6,
        },
    },
    yAxis: {
        min: 0,
        title: {
            visible: false,
        },
        position:'right'
    },

    xAxis: {
        title: {
            visible: false,
        },
    },
    guideLine: [
        {
            start: ['cao', 42.6], // ['1991', 30],
            end: ['caoAcc', 42.6], // ['1999', 30],
            text: {
                position: 'center',
                content: '품위한계선 42.6%',
            },
            lineStyle: {
                stroke: 'red',
                lineDash: [4, 2],
            },
        },
    ],
}


export default () => <ColumnChart {...config} style={{height: 340, width: '20%', float:"left"}}/>
