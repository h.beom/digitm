import React from 'react'
import {ColumnChart} from '@opd/g2plot-react'
const data = [
    {
        name:'test1',
        year: '1991',
        value: 31,
    },
    {
        name:'test1',
        year: '1992',
        value: 41,
    },
    {
        name:'test1',
        year: '1993',
        value: 35,
    },
    {
        name:'test1',
        year: '1994',
        value: 55,
    },
    {
        name:'test1',
        year: '1995',
        value: 49,
    },
    {
        name:'test1',
        year: '1996',
        value: 15,
    },
    {
        name:'test1',
        year: '1997',
        value: 17,
    },
    {
        name:'test1',
        year: '1998',
        value: 29,
    },
    {
        name:'test1',
        year: '1999',
        value: 33,
    },



    {
        name:'test2',
        year: '1991',
        value: 42,
    },
    {
        name:'test2',
        year: '1992',
        value: 31,
    },
    {
        name:'test2',
        year: '1993',
        value: 46,
    },
    {
        name:'test2',
        year: '1994',
        value: 35,
    },
    {
        name:'test2',
        year: '1995',
        value: 22,
    },
    {
        name:'test2',
        year: '1996',
        value: 14,
    },
    {
        name:'test2',
        year: '1997',
        value: 43,
    },
    {
        name:'test2',
        year: '1998',
        value: 53,
    },
    {
        name:'test2',
        year: '1999',
        value: 23,
    },
];
const config: any = {
    data,
    xField: 'year',
    yField: 'value',
    groupField: 'name',
    guideLine: [
        {
            type: 'max', // 'max' | 'min' | 'median' |  'mean'
            text: {
                position: 'end',
                content: 'endpoint',

            },
        },
    ],
    color: ['#1ca9e6', '#f88c24'],
};



export default () => <ColumnChart {...config} style={{ height:340, width:'100%'}}  />
