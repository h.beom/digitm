import React from 'react'
// @ts-ignore
import {GroupedColumnChart} from '@opd/g2plot-react'


const data = [
    {
        name: 'prodMove',
        date: '7/7',
        value: 4.5,
    },
    {
        name: 'purMove',
        date: '7/7',
        value: 5.5,
    },
    {
        name: 'prodAcc',
        date:'7/7',
        value: 28.9,
    },{
        name: 'purAcc',
        date:'7/7',
        value: 11.9,
    },

    {
        name: 'prodMove',
        date: '7/8',
        value: 4.5,
    },
    {
        name: 'purMove',
        date: '7/8',
        value: 5.5,
    },
    {
        name: 'prodAcc',
        date:'7/8',
        value: 28.9,
    },{
        name: 'purAcc',
        date:'7/8',
        value: 11.9,
    }
];


const config: any = {
    title: {
        visible: true,
        text: 'name',
    },
    forceFit: true,
    data,
    xField: 'date',
    yField: 'value',
    yAxis: {
        min: 0,
    },
    label: {
        visible: true,
    },
    groupField: 'name',
    color: ['#1ca9e6', '#f88c24' ,'#fffff'],
};


// @ts-ignore
export default () => <GroupedColumnChart {...config} style={{height: 400}}/>
