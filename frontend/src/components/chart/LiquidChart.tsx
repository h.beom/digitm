import React from 'react'
import {LiquidChart} from '@opd/g2plot-react'

const config: any = {
    min: 0,
    max: 102240,
    value: 82240
};

export default () => <LiquidChart {...config} style={{ height:200}}  />
