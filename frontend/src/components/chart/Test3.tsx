import React from 'react'
// @ts-ignore
import {GroupedColumnChart} from '@opd/g2plot-react'


const data = [
    {
        time: '08:09',
        date: 'chemical',
        value: 4.5,
    },
    {
        time: '09:24',
        date: 'chemical',
        value: 5.5,
    },
    {
        time: '09:06',
        date: 'chemical',
        value: 28.9,
    },{
        time: '09:06',
        date: 'chemical',
        value: 11.9,
    },
    {
        time: '10:01',
        date: 'chemical',
        value: 15.9,
    },
    {
        time: '10:21',
        date: 'chemical',
        value: 21.9,
    },{
        time: '11:04',
        date: 'chemical',
        value: 18.9,
    },{
        time: '11:21',
        date: 'chemical',
        value: 60.9,
    },

    {
        time: '11:09',
        date: 'chemical',
        value: 4.5,
    },
    {
        time: '11:24',
        date: 'chemical',
        value: 5.5,
    },
    {
        time: '12:06',
        date: 'chemical',
        value: 28.9,
    },{
        time: '12:07',
        date: 'chemical',
        value: 11.9,
    },
    {
        time: '13:01',
        date: 'chemical',
        value: 15.9,
    },
    {
        time: '13:21',
        date: 'chemical',
        value: 21.9,
    },{
        time: '14:04',
        date: 'chemical',
        value: 18.9,
    },{
        time: '14:21',
        date: 'chemical',
        value: 60.9,
    },



    //누적부분
    {
        time: '08:09',
        date: 'chemicalAcc',
        value: 8.5,
    },
    {
        time: '09:24',
        date: 'chemicalAcc',
        value: 5.5,
    },
    {
        time: '09:06',
        date: 'chemicalAcc',
        value: 28.9,
    },{
        time: '09:06',
        date: 'chemicalAcc',
        value: 11.9,
    },
    {
        time: '10:01',
        date: 'chemicalAcc',
        value: 11.9,
    },
    {
        time: '10:21',
        date: 'chemicalAcc',
        value: 31.9,
    },{
        time: '11:04',
        date: 'chemicalAcc',
        value: 14.9,
    },{
        time: '11:21',
        date: 'chemicalAcc',
        value: 11.9,
    },
    {
        time: '12:09',
        date: 'chemicalAcc',
        value: 8.5,
    },
    {
        time: '12:24',
        date: 'chemicalAcc',
        value: 5.5,
    },
    {
        time: '13:06',
        date: 'chemicalAcc',
        value: 28.9,
    },{
        time: '13:06',
        date: 'chemicalAcc',
        value: 11.9,
    },
    {
        time: '13:01',
        date: 'chemicalAcc',
        value: 11.9,
    },
    {
        time: '14:21',
        date: 'chemicalAcc',
        value: 31.9,
    },{
        time: '14:04',
        date: 'chemicalAcc',
        value: 14.9,
    },{
        time: '15:21',
        date: 'chemicalAcc',
        value: 11.9,
    },
];


const config: any = {
    // title: {
    //     visible: true,
    //     text: 'name',
    // },
    forceFit: true,
    data,
    xField: 'date',
    yField: 'value',
    yAxis: {
        min: 0,
    },
    label: {
        visible: true,
    },
    groupField: 'time',
  //색깔 지정, 정하지않으면 각각 램던으로 다른 색상을 적용하게 됨
    // color: ['#1ca9e6'],
};


// @ts-ignore
export default () => <GroupedColumnChart {...config} style={{height: 400}}/>
