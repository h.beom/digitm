import React, {Component} from 'react'
import {ColumnLineChart} from '@opd/g2plot-react'
import {inject, observer} from "mobx-react";


@inject('ProductStore')
@observer
class Line extends Component<any, any> {

    render() {
        const {chartData} = this.props;
        const acc = [
            {date: '8/3', 'acc': 378500},
            {date: '8/4', 'acc': 382700},
            {date: '8/5', 'acc': 386700},
            {date: '8/6', 'acc': 390800},
            {date: '8/7', 'acc': 382700},
            {date: '8/8', 'acc': 386700},
            {date: '8/9', 'acc': 390800}
        ]

        const daily = [
            {date: '8/3', 'daily': 4500},
            {date: '8/4', 'daily': 4200},
            {date: '8/5', 'daily': 4000},
            {date: '8/6', 'daily': 4100},
            {date: '8/7', 'daily': 3800},
            {date: '8/8', 'daily': 4200},
            {date: '8/9', 'daily': 4500}
        ]

        // let parameter ={
        //     acc : this.props.chartData
        // }

        const config: any = {
            data: [acc, daily],
            xField: 'date',
            yField: ['acc', 'daily'],

            columnConfig: {color: 'l(90) 0:#eca04c 1:#f3c669'},
            lineConfig: {color: '#fff', fill: 'red',},

            //대시보드는 안먹히는것 같아 테스트 더해봐야 할것 같음.
            label: {
                visible: true,
                style: {
                    fill: '#0D0E68',
                    fontSize: 12,
                    fontWeight: 600,
                    opacity: 0.6,
                },
            },
        }
        return (
            <>
                <ColumnLineChart {...config} style={{height: 440}}/>
            </>
        );
    }
}
export default Line;



