import React from 'react'
import {ColumnChart} from '@opd/g2plot-react'
import {ColorKeywords} from "three";

const data = [
    {
        "category": 'sio2',
        "value": 9.42,
    }, {
        "category": 'sio2Acc',
        "value": 9.42,
    }, {
        "category": 'al2o3',
        "value": 2.97,
    }, {
        "category": 'al2o3Acc',
        "value": 2.97,
    }, {
        "category": 'mgo',
        "value": 2.43,
    }, {
        "category": 'mgoAcc',
        "value": 2.43,
    }, {
        "category": 'fe2o3',
        "value": 1.1,
    }, {
        "category": 'fe2o3Acc',
        "value": 1.1,
    }, {
        "category": 'k2o',
        "value": 0.57,
    }, {
        "category": 'k2oAcc',
        "value": 0.57,
    }, {
        "category": 'so3',
        "value": 0.17,
    }, {
        "category": 'so3Acc',
        "value": 0.17,
    }, {
        "category": 'na2o',
        "value": 0.5,
    }, {
        "category": 'na2oAcc',
        "value": 0.5,
    }
];

const config: any = {
    // title: {
    //     visible: true,
    //     text: '自定义图表辅助线',
    // },
    // description: {
    //     visible: true,
    //     text: '通过start和end自定义辅助线，支持传入位置百分比或数据',
    // },
    data,
    xField: 'category',
    yField: 'value',
    label: {
        visible: true,
        style: {
            fill: '#0D0E68',
            fontSize: 12,
            fontWeight: 600,
            opacity: 0.6,
        },
    },
    yAxis: {
        min: 0,
        title: {
            visible: false,
        },
        position:'right'
    },

    xAxis: {
        title: {
            visible: false,
        },
    },
    guideLine: [
        {
            start: ['sio2', 42.6], // ['1991', 30],
            end: ['na2oAcc', 42.6], // ['1999', 30],
            text: {
                position: 'end',
                content: '품위한계선 42.6%',
            },
            lineStyle: {
                stroke: 'red',
                lineDash: [4, 2],
            },
        },
        {
            start: ['sio2', 2.6], // ['1991', 30],
            end: ['na2oAcc', 2.6], // ['1999', 30],
            text: {
                position: 'end',
                content: '패널티 한계 2.6%',
            },
            lineStyle: {
                stroke: 'red',
                lineDash: [4, 2],
            },
        }
    ],
}


export default () => <ColumnChart {...config} style={{height: 340, width: '80%', float:"right"}}/>
