import React, {Component} from 'react';
import {GLTF, GLTFLoader} from "three/examples/jsm/loaders/GLTFLoader";
import {
    AnimationMixer,
    Camera,
    Clock,
    HemisphereLight,
    Mesh,
    MeshBasicMaterial,
    Object3D,
    PerspectiveCamera,
    PlaneGeometry,
    Scene,
    TextureLoader,
    WebGLRenderer
} from 'three';
import {OrbitControls} from "three/examples/jsm/controls/OrbitControls";
import {FBXLoader} from "three/examples/jsm/loaders/FBXLoader";
import {OBJLoader} from "three/examples/jsm/loaders/OBJLoader";
import {Button, Card, Select} from "antd";
import Stats from "stats.js";
import {inject, observer} from "mobx-react";
import autobind from "autobind-decorator";

@inject('IndexStore')
@observer
class ThreeObject extends Component<any, any> {


    // 시간
    private _clock: Array<Clock> = [];
    // 애니메이션 플레이어
    private _workerAnimationList: Array<AnimationMixer> = [];
    private stats: Stats = new Stats();
    private mixers = [];
    private _mount: HTMLElement | null = null;
    private scene: Scene =  new Scene();
    private camera: Camera | null = null;
    private renderer: WebGLRenderer | null = null;
    private controls: OrbitControls | null = null;
    private frameId: number = 0;
    private modelItem: Object3D | null = null;


    constructor(props: any) {
        super(props);
        this.state = {
            mode: 1,
            x:0,
            y:0,
            z:0
        };
        this.start = this.start.bind(this);
        this.animate = this.animate.bind(this);
        this.stop = this.stop.bind(this);
    }


    @autobind
    public test(){
        alert('비상!');
        // let fov = this.scene.getObjectByName('fov');
        // // @ts-ignore
        // fov.position.x += 100;
    }

    private getData(scene: Scene) {

    }

    // @autobind
    // public addObject(scene: Scene){
    //     let fbxURL:string = require("../../resources/Iron_Man.FBX"); //fbx가 있는 파일위치
    //     let fbxLoader = new FBXLoader(); // 인스턴스 생성해주고
    //     fbxLoader.load(fbxURL,function(object:any){
    //         object.position.y = 0;
    //         object.position.z = 0;
    //         object.position.x = 0;
    //         if(scene !== null){
    //             object.name  = 'fov';
    //             scene.add(object);
    //         }
    //     });
    // }


    async componentDidMount() {

        const {mode} = this.state;

        if (this._mount === null) {
            return;
        }
        let width: number = this._mount.clientWidth - 28;
        let height: number = this._mount.clientHeight - 10;
        const scene: Scene = this.scene;

        const camera: Camera = new PerspectiveCamera(
            100,
            width / height,
            1,
            10000
        );

        const renderer: WebGLRenderer = new WebGLRenderer();
        renderer.setSize(width, height);
        renderer.setClearColor(0xcccccc, 1);


        let light: HemisphereLight = new HemisphereLight(0xffffff, 0x444444);

        light.position.set(0, 200, 0);

        scene.add(light);


        this.controls = new OrbitControls(camera, renderer.domElement);

        this.controls.enableDamping = true;
        this.controls.minDistance = 10;
        this.controls.maxDistance = 1000;
        this.controls.maxPolarAngle = Math.PI * 2;

        this.getData(scene);

        let gridTile: { width: number, height: number } = {width: 50, height: 100};
        let gridwidth: number = 1200;
        let gridheight: number = 800;

        // let loadTextURL: string = require("./map01.jpg");
        // let loader: TextureLoader = new TextureLoader();
        //
        // const texture = loader.load('https://threejsfundamentals.org/threejs/resources/images/wall.jpg');
        //
        // let planeGeometry: PlaneGeometry = new PlaneGeometry(1500, 700, 100, 70);
        // let planeMaterial: MeshBasicMaterial = new MeshBasicMaterial({map: texture,wireframe: true});
        //
        // let grid = new Mesh(planeGeometry, planeMaterial);
        // grid.position.set(0, 0, 0);
        //
        // grid.rotation.x = -0.5 * Math.PI;
        // scene.add(grid);

// ====================================구역나누기===========================================================================

        let fbxURL:string = require("../../resources/robot.fbx"); //fbx가 있는 파일위치
        let fbxLoader = new FBXLoader(); // 인스턴스 생성해주고

       fbxLoader.load(fbxURL,function(object:any){
           object.position.x = 0;
           object.position.y = 0;
           object.position.z = 0;
           object.scale.multiplyScalar(5)
           object.name = 'fov';
            scene.add(object);
        });

// let objURL:string = require("../../resources/notebook.obj"); //fbx가 있는 파일위치
//         let objLoader = new OBJLoader(); // 인스턴스 생성해주고
//         //fbx를 로더해보자
//         objLoader.load(objURL,function(object:any){
//             object.y = 1000;
//             object.z = 1000;
//             object.x = 1000;
//             scene.add(object);
//         });

// ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


        // 카메라 위치 설정
        camera.position.set(0, 1500, 0);
        this.scene = scene;
        this.camera = camera;
        this.renderer = renderer;
        this._mount.appendChild(this.renderer.domElement);

        // this.stats.showPanel(0); // 0: fps, 1: ms, 2: mb, 3+: custom
        //
        // let stateHtml: HTMLElement = this.stats.dom;
        // stateHtml.setAttribute("style", "position: absolute; top: 60px; cursor: pointer; opacity: 0.9; z-index: 1000;");
        // this._mount.appendChild(stateHtml);

        this.start();

    }

    start(): void {
        this.animate();
    }

    animate = () => {

        this.stats.begin();
        // this.stats.end();
        this.frameId = window.requestAnimationFrame(this.animate);
        //push호 mixers에 담아두었던 것을 반복문을 통해 전체 프레임을 실행시킨다.
        if (this.mixers.length > 0) {
            for (let i = 0; i < this.mixers.length; i++) {
                // @ts-ignore
                //Object for keeping track of time. This uses performance.now if it is available, otherwise it reverts to the less accurate Date.now.
                //시간을 추척하는 용도.
                this.mixers[i].update(this._clock.getDelta());
            }
        }

        //반복문을 돌려서 다음 프레임(1000ms)를 계속 돌린다.
        if (this._workerAnimationList.length > 0) {
            for (let i: number = 0; i < this._workerAnimationList.length; i++) {
                // this.workerAnimationList[i].update(time);
                this._workerAnimationList[i].update(this._clock[i].getDelta());
            }
        }


        this.renderScene();
    };

    componentWillUnmount() {
        if (this.renderer == null || this._mount == null) {
            return;
        }
        this.stop();
        this._mount.removeChild(this.renderer.domElement);
    }

    stop = () => {
        cancelAnimationFrame(this.frameId)
    };



    renderScene() {
        if (this.renderer == null || this.scene == null || this.camera == null) {
            return;
        }

        if (this.controls != null) {
            this.controls.update();
        }
        this.renderer.render(this.scene, this.camera);
    }

    render() {
        const { Option } = Select;
        return (
            <Card style={{    width: 400, height: 300, float:"right"}} title={'이녀석을 선택한 오브젝트를 이런식으로 보여줘도? '}>
                <Button onClick={()=>this.test}>머리 경고하자</Button>
                <div style={{
                    width: 400, height: 300, float:"right"
                }}
                     ref={(mount) => {
                         if (mount === null) {
                             return;
                         }
                         this._mount = mount
                     }}

                />
            </Card>
        )
    }
}

export default ThreeObject;
