import React, {Component} from 'react';
import {Redirect} from "react-router";
import Service from "../common/UserService";


class AuthGuard extends Component<any, any> {


    render() {
        const {component: Component, roles} = this.props;
        const currentUser = Service.currentUserValue;

        if(!currentUser){
            return (<Redirect to={{pathname: '/login'}}/>);
        }

        if(roles && roles.indexOf(currentUser.role) === -1){
            return (<Redirect to={{pathname: '/401'}}/>);
        }

        return (
            <>
                <Component/>
            </>
        );
    };
}

export default AuthGuard;
