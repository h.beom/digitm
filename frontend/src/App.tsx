import React, {Component} from 'react';
import {BrowserRouter, Link, Route, Switch} from "react-router-dom";
import Left from "./components/layout/Left";
import {Layout} from "antd";
import ProductionPanel from "./pages/dashboard/ProductionPanel";
import ProductManage from "./pages/prodution/production/Planning";
import Planning from "./pages/prodution/production/Planning";
import GradeCtrAnalysis from "./pages/prodution/grade/GradeCtrAnalysis";
import LoginForm from "./pages/login/LoginForm";
import WeighbridgeAnalysis from "./pages/prodution/production/WeighbridgeAnalysis";
import WeighbridgeRealtime from "./pages/prodution/production/WeighbridgeRealtime";
import GradeCtrRealtime from "./pages/prodution/grade/GradeCtrRealtime";
import {inject, observer} from "mobx-react";
import './styles/component/style.less';
import Clock from 'react-live-clock';
import {createBrowserHistory} from "history";
import {bsApi, setToken} from "./common/Api";
import GasAnalysis from "./pages/environment/GasAnalysis";
import GasRealtime from "./pages/environment/GasRealtime";
import MicroseismicAnalysis from "./pages/environment/MicroseismicAnalysis";
import MicroseismicRealtime from "./pages/environment/MicroseismicRealtime";
import DpmRealtime from "./pages/environment/DpmRealtime";
import DpmAnalysis from "./pages/environment/DpmAnalysis";
import GroundRealtime from "./pages/environment/GroundRealtime";
import GroundAnalysis from "./pages/environment/GroundAnalysis";
import MonitoringSenserMap from "./pages/environment/MonitoringSenserMap";
import CodeManage from "./pages/admin/CodeManage";
import * as queryString from "querystring";

const {Header, Content, Footer, Sider} = Layout;


if (bsApi.defaults.headers.common['Authorization'] == undefined && localStorage.getItem('refresh') == null) {
    // 명령 입력
} else {
    setToken({
        accessToken: localStorage.getItem('refresh') as string
    })
}

@inject('IndexStore')
@observer
class App extends Component<any, any> {

    state = {
        history: createBrowserHistory(),
    }


    componentDidMount() {

    }

    render() {
        const {IndexStore} = this.props;
        return (
            <>
                <Layout>
                    <BrowserRouter basename={'digitalMining'}>
                        <Sider>
                            <Left/>
                        </Sider>
                        <Layout className="site-layout">
                            <Header className="site-layout-background">
                                <div className="t_link">
                                    {IndexStore.getTopButton.map(function (this: App, value: any, idx: number) {
                                        return <Link to={value.url} key={idx}>
                                            {value.text}
                                        </Link>
                                    }, this)}
                                </div>
                            </Header>
                            <Content>
                                <Switch>
                                    <Route path="/LoginForm" component={LoginForm}/>
                                    <Route path="/dashboard/production" component={ProductionPanel}/>
                                    <Route path="/production/grade-control-analysis" component={GradeCtrAnalysis}/>
                                    <Route path="/production/weighbridge-realtime" component={WeighbridgeRealtime}/>
                                    <Route path="/production/weighbridge-analysis" component={WeighbridgeAnalysis}/>
                                    <Route path="/production/planning" component={Planning}/>
                                    <Route path="/production/grade-control-realtime" component={GradeCtrRealtime}/>
                                    <Route path="/ProductManage" component={ProductManage}/>
                                    <Route path="/environment/sensors-map" component={MonitoringSenserMap}/>
                                    <Route path="/environment/microseismic-realtime" component={MicroseismicRealtime}/>
                                    <Route path="/environment/microseismic-analysis" component={MicroseismicAnalysis}/>
                                    <Route path="/environment/ground-realtime" component={GroundRealtime}/>
                                    <Route path="/environment/ground-analysis" component={GroundAnalysis}/>
                                    <Route path="/environment/dpm-realtime" component={DpmRealtime}/>
                                    <Route path="/environment/dpm-analysis" component={DpmAnalysis}/>
                                    <Route path="/environment/gas-realtime" component={GasRealtime}/>
                                    <Route path="/environment/gas-analysis" component={GasAnalysis}/>
                                    <Route path="/admin/codemanage" component={CodeManage} />
                                </Switch>
                            </Content>
                            <Footer>
                                <p><span>DATE</span><Clock format={'YYYY 년 MM 월 DD 일 HH:mm:ss'} ticking={true}/></p>
                                <h2 className="f_logo">(주)고려시멘트</h2>
                            </Footer>
                        </Layout>
                    </BrowserRouter>
                </Layout>
            </>
        );
    };
}

export default App;
