/**
 * @description ENVIRONMENT DASH 타입정리
 */

// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
/**
 * @description 미소진동 실시간 모니터링 타입정리
 */

// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
/**
 * @description 미소진동 일자별 분석 타입정리
 */

// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
/**
 * @description 지반변위 실시간 모니터링 타입정리
 */
export interface GmrSensorTableType {
    key: number,
    x: number,
    y: number,
    z: number,
    sensorId: string
}

export interface GmrLogsCardType {
    xDegree: number,
    xDegreeDisplacement: number,
    yDegree: number,
    yDegreeDisplacement: number
}

export interface GmrLogsTableType {
    id: number,
    sensorId: number,
    date: string,
    time: string,
    xDegree: number,
    xDegreeDisplacement: number,
    yDegree: number,
    yDegreeDisplacement: number
}

export interface GmrLogsChartType {
    date: string,
    type: string,
    value: number
}

 // ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
/**
 * @description 지반변위 기간별 분석 타입정리
 */
export interface GmaSensorTableType {
    key: number,
    x: number,
    y: number,
    z: number,
    sensorId: string
}

export interface GmaLogsCardType {
    xDegree: number,
    xDegreeDisplacement: number,
    yDegree: number,
    yDegreeDisplacement: number
}

export interface GmaLogsTableType {
    id: number,
    sensorId: number,
    date: string,
    time: string,
    xDegree: number,
    xDegreeDisplacement: number,
    yDegree: number,
    yDegreeDisplacement: number
}

export interface GmaLogsChartType {
    date: string,
    type: string,
    value: number
}

// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
/**
 * @description DPM 실시간 모니터링 타입정리
 */

// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
/**
 * @description DPM 일자별 분석 타입정리
 */

// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
/**
 * @description 유해가스 실시간 모니터링 타입정리
 */
export interface GrSensorTableType {
    key: number,
    x: number,
    y: number,
    z: number,
    sensorId: string
}

export interface GrLogsCardType {
    temp: number,
    humidity: number,
    co2: number,
    co: number,
    no: number,
    no2: number,
    so2: number
}

export interface GrLogsTableType {
    id: number,
    sensorId: number,
    date: string,
    time: string,
    temp: number,
    humidity: number,
    co2: number,
    co: number,
    no: number,
    no2: number,
    so2: number
}

export interface GrLogsChartType {
    date: string,
    type: string,
    value: number
}

// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
/**
 * @description 유해가스 일자별 분석 타입정리
 */
export interface GaSensorTableType {
    key: number,
    x: number,
    y: number,
    z: number,
    sensorId: string
}

export interface GaLogsCardType {
    temp: number,
    humidity: number,
    co2: number,
    co: number,
    no: number,
    no2: number,
    so2: number
}

export interface GaLogsTableType {
    id: number,
    sensorId: number,
    date: string,
    time: string,
    temp: number,
    humidity: number,
    co2: number,
    co: number,
    no: number,
    no2: number,
    so2: number
}

export interface GaLogsChartType {
    date: string,
    type: string,
    value: number
}

// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒