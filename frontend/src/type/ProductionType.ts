/**
 * @description PRODUCTION DASH 타입정리
 */
export interface ProductionDash{
    acc: Array<number>,
    daily: Array<number>,
    date: Array<string>
}

// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
/**
 * @description PRODUCTION PANEL 타입정리
 */
export interface ProductionAmount{
    accPlan: number,
    accProd: number,
    currentProd: number,
    plan: number,
}

// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
/**
 * @description 생산계획관리 관련 타입정리
 */
export interface PlanningType{
    average: number,
    date: string
    list: Array<PlanningList>,
    total: number,
    version: Array<PlanningVersion>
}

export interface PlanningList{
    date: string,
    acc: number,
    amount: number
}
export interface PlanningVersion{
    name: string,
    id: number,
}

// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
/**
 * @description 실시간생산&구매실적 관련 타입정리
 */
export interface RealtimeType{
    datetime: string,
    lastNumber: number,
    procurement: Array<RealtimeCardType>,
    production: Array<RealtimeCardType>
}

export interface RealtimeCardType{
    current: number,
    currentOut: number,
    month: number,
    monthOut: number,
    year: number,
    yearOut: number,
}

export interface RealtimeTableType extends WeighbridgeDetailType{
    id: number,
    procurement:  Array<RealtimeDetailType>,
    production: Array<RealtimeDetailType>,
}

export interface RealtimeDetailType{
    change:number,
    input: number,
    output:number,
    postAmount: number,
    preAmount: number,
}

// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
/**
 * @description 실시간생산&구매실적 관련 타입정리
 */
export interface AnalysisType{
    accAmount: 4000
    accCount: 30
    list: Array<WeighbridgeDetailType>
}
export interface WeighbridgeDetailType{
    number: 4
    productName: "원석야적"
    source: "갱내파쇄"
    time: "13:30"
    type: "입고"
    weight: 40
}

// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
/**
 * @description 실시간 품위분석 관련 타입정리
 */
export interface GradeDetailType{
    al2o3: number,
    al2o3Acc:number,
    cao: number,
    caoAcc:number,
    date: string,
    fe2o3:number,
    fe2o3Acc: number,
    inInner: number,
    inOuter: string,
    input: number,
    inputAcc: number,
    k2o:number,
    k2oAcc:number,
    mgo: number,
    mgoAcc: number,
    na2o:number,
    na2oAcc: number,
    note: string,
    number:number,
    sio2: number,
    sio2Acc: number,
    so3: number,
    so3Acc: number,
    sum: number,
    sumAcc: number,
    time: string
}
