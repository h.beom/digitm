export interface MenuItemList{
    id: number,
    url:string,
    parentid:number,
    text:string,
    subtext:string,
    depth:number,
    icon?:any,
    visible:boolean,
    child:Array<MenuItemList>
};
