
/**
 * @description t_dm_sensor 테이블 타입 정의
 */

export interface SensorTableType {
    x: number,
    y: number,
    z: number,
    sensorId: string
}

// export interface SensorTableType {
//     idx:number,
//     x: number,
//     y: number,
//     z: number,
//     type:number,
//     sensorId: string,
//     sensorName:string
// }