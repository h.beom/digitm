/**
 * @description 생산계획관리 관련 타입정리
 */
export interface CodeType{
    idx: number,
    code: string,
    name: string,
    grp_code: string,
    sort: number,
    reg_date: Date,
    item_1: string,
    item_2: string,
    item_3: string,
    item_4: string,
    item_5: string
}