import {action, computed, observable} from 'mobx';
import StringUtil from "../util/StringUtil";
import {
    GmrSensorTableType,
    GmrLogsTableType,
    GmrLogsCardType,
    GmrLogsChartType,
    GmaSensorTableType,
    GmaLogsTableType,
    GmaLogsCardType,
    GmaLogsChartType,
    GrSensorTableType,
    GrLogsTableType,
    GrLogsCardType,
    GrLogsChartType,
    GaSensorTableType,
    GaLogsTableType,
    GaLogsCardType,
    GaLogsChartType
} from "../type/EnvironmentType";
import React from "react";
import {concat} from "@amcharts/amcharts4/.internal/core/utils/Iterator";


/**
 * @description Environment 카테고리의 MobxStore
 */

class EnvironmentStore {
    // ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒microseismic-realtime▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    // ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    /**
     * @description 실시간 미소진동 마지막 이벤트 정보저장
     */
    @observable lastEvent: Array<any> = [];
    @action setLastEvent(param: Array<any>) {
        this.lastEvent = param;
    }
    @computed get getLastEvent() {
        return this.lastEvent;
    }

    // ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒microseismic-analysis▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    // ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    @observable envAnalysisData: Array<any> = [];
    @observable envAnalysisFilterData: Array<any> = [];

    @action setEnvAnalysisData(param: Array<any>) {
        this.envAnalysisData = param;
    }
    @computed get getEnvAnalysisData() {
        return this.envAnalysisData;
    }
    /**
     * @description 선택한 오브젝트 적보 추출하기
     * @param param 해당 object id
     */
    @action selectObejct(param: number) {
        const objectInfo = this.envAnalysisData.filter((value: any) => value.id === param);
        this.envAnalysisFilterData = objectInfo[0]
    }
    @computed get getEnvAnalysisFilterData() {
        return this.envAnalysisFilterData;
    }
    /**
     * @description 차트데이터 정제
     */
    @observable chartData: Array<any> = []
    @action setChartData(param: any) {
        this.chartData = param ;
    }
    @computed get getChartData() {
        return this.chartData;
    }


    /**
     * @description sensorList
     */
    @observable sensorList: Array<any> = []
    @action setSensorList(param: any) {
        this.sensorList = param ;
    }
    @computed get getSensorList() {
        return this.sensorList;
    }

    /**
     * @description select Data
     */
    @observable selectData: string = 'all'
    @action setSelectData(param: string) {
        this.selectData = param ;
    }
    @computed get getSelectData() {
        return this.selectData;
    }


    /**
     * @description 코드 종류 리스트
     */
    @observable codeList: Array<any> = []
    @action setCodeList(param: Array<any>) {
        this.codeList = param ;
    }
    @computed get getCodeList() {
        return this.codeList;
    }


    @observable sensorFilterData: Array<any> = [];

    @action setSensorFilterData(param: Array<any>) {
        this.sensorFilterData = param;
    }
    @computed get getSensorFilterData() {
        return this.sensorFilterData;
    }

    /**
     * @description 선택한 오브젝트 적보 추출하기
     * @param param 해당 object id
     */
    @action selectSensor(param: number) {
        const objectInfo = this.sensorFilterData.filter((value: any) => value.id === param);
        this.sensorInfo = objectInfo[0]
        console.log(objectInfo[0],'objectInfo[0]')
    }

    @observable sensorInfo: Array<any> = [];
    @action setSensorInfo() {
        this.sensorInfo = []
    }
    @computed get getSensorInfo() {
        return this.sensorInfo;
    }

// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒ground-monitoring-realtime▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    @observable gmrSensorId: any = [];
    @observable gmrSensors: Array<GmrSensorTableType> = [];
    @observable gmrLogsCurrentTimeIndex: any = [];
    @observable gmrLogsCard: any = [];
    @observable gmrLogs: Array<GmrLogsTableType> = [];
    @observable gmrLogsChart: Array<GmrLogsChartType> = [];

    /**
     * @description ground-monitoring-realtime 센서목록
     */
    @action setGmrSensorId(param: number) {
        this.gmrSensorId = param;
    }

    @computed get getGmrSensorId() {
        return this.gmrSensorId;
    }

    @action setGmrSensorTable(param: Array<any>) {
        let sensors = param.map((value: any, index: number) => {
            return {
                key: value.id,
                x: value.x,
                y: value.y,
                z: value.z,
                sensorId: value.sensorId,
            }
        });
        this.gmrSensors = sensors;
    }

    @computed get getGmrSensorTable() {
        return this.gmrSensors;
    }

    /**
     * @description ground-monitoring-realtime 최근 24시간 계측목록
     */
    @action setGmrLogsCard(param: number) {
        this.gmrLogsCard = {
            xDegree: this.gmrLogs[param].xDegree,
            xDegreeDisplacement: this.gmrLogs[param].xDegreeDisplacement,
            yDegree: this.gmrLogs[param].yDegree,
            yDegreeDisplacement: this.gmrLogs[param].yDegreeDisplacement
        };
    }

    @computed get getGmrLogsCard() {
        return this.gmrLogsCard;
    }

    @action setGmrLogsCurrentTimeIndex(param: number) {
        this.gmrLogsCurrentTimeIndex = param;
    }

    @computed get getGmrLogsCurrentTimeIndex() {
        return this.gmrLogsCurrentTimeIndex;
    }

    @action setGmrLogs(param: Array<GmrLogsTableType>) {
        this.gmrLogs = param;
    }

    @computed get getGmrLogs() {
        return this.gmrLogs;
    }

    @action setGmrLogsChart(param: Array<GmrLogsChartType>) {
        this.gmrLogsChart = param;
    }

    @computed get getGmrLogsChart() {
        return this.gmrLogsChart;
    }

// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒ground-monitoring-analysis▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    @observable gmaSensorId: any = [];
    @observable gmaSensors: Array<GmaSensorTableType> = [];
    @observable gmaLogsCurrentTimeIndex: any = [];
    @observable gmaLogsCard: any = [];
    @observable gmaLogs: Array<GmaLogsTableType> = [];
    @observable gmaLogsChart: Array<GmaLogsChartType> = [];

    /**
     * @description ground-monitoring-analysis 센서목록
     */
    @action setGmaSensorId(param: number) {
        this.gmaSensorId = param;
    }

    @computed get getGmaSensorId() {
        return this.gmaSensorId;
    }

    @action setGmaSensorTable(param: Array<any>) {
        let sensors = param.map((value: any, index: number) => {
            return {
                key: value.id,
                x: value.x,
                y: value.y,
                z: value.z,
                sensorId: value.sensorId,
            }
        });
        this.gmaSensors = sensors;
    }

    @computed get getGmaSensorTable() {
        return this.gmaSensors;
    }

    /**
     * @description ground-monitoring-analysis 계측목록
     */
    @action setGmaLogsCard(param: number) {
        this.gmaLogsCard = {
            xDegree: this.gmaLogs[param].xDegree,
            xDegreeDisplacement: this.gmaLogs[param].xDegreeDisplacement,
            yDegree: this.gmaLogs[param].yDegree,
            yDegreeDisplacement: this.gmaLogs[param].yDegreeDisplacement
        };
    }

    @computed get getGmaLogsCard() {
        return this.gmaLogsCard;
    }

    @action setGmaLogsCurrentTimeIndex(param: number) {
        this.gmaLogsCurrentTimeIndex = param;
    }

    @computed get getGmaLogsCurrentTimeIndex() {
        return this.gmaLogsCurrentTimeIndex;
    }

    @action setGmaLogs(param: Array<GmaLogsTableType>) {
        this.gmaLogs = param;
    }

    @computed get getGmaLogs() {
        return this.gmaLogs;
    }

    @action setGmaLogsChart(param: Array<GmaLogsChartType>) {
        this.gmaLogsChart = param;
    }

    @computed get getGmaLogsChart() {
        return this.gmaLogsChart;
    }

// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒dpm-realtime▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒

// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒dpm-analysis▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒

// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒gas-realtime▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    @observable grSensorId: any = [];
    @observable grSensors: Array<GrSensorTableType> = [];
    @observable grLogsCurrentTimeIndex: any = [];
    @observable grLogsCard: any = [];
    @observable grLogs: Array<GrLogsTableType> = [];
    @observable grLogsChart: Array<GrLogsChartType> = [];

    /**
     * @description gas-realtime 센서목록
     */
    @action setGrSensorId(param: number) {
        this.grSensorId = param;
    }

    @computed get getGrSensorId() {
        return this.grSensorId;
    }

    @action setGrSensorTable(param: Array<any>) {
        let sensors = param.map((value: any, index: number) => {
            return {
                key: value.id,
                x: value.x,
                y: value.y,
                z: value.z,
                sensorId: value.sensorId,
            }
        });
        this.grSensors = sensors;
    }

    @computed get getGrSensorTable() {
        return this.grSensors;
    }

    /**
     * @description gas-realtime 최근 2시간 계측목록
     */
    @action setGrLogsCard(param: number) {
        this.grLogsCard = {
            temp: this.grLogs[param].temp,
            humidity: this.grLogs[param].humidity,
            co2: this.grLogs[param].co2,
            co: this.grLogs[param].co,
            no: this.grLogs[param].no,
            no2: this.grLogs[param].no2,
            so2: this.grLogs[param].so2
        };
    }

    @computed get getGrLogsCard() {
        return this.grLogsCard;
    }

    @action setGrLogsCurrentTimeIndex(param: number) {
        this.grLogsCurrentTimeIndex = param;
    }

    @computed get getGrLogsCurrentTimeIndex() {
        return this.grLogsCurrentTimeIndex;
    }

    @action setGrLogs(param: Array<GrLogsTableType>) {
        this.grLogs = param;
    }

    @computed get getGrLogs() {
        return this.grLogs;
    }

    @action setGrLogsChart(param: Array<GrLogsChartType>) {
        this.grLogsChart = param;
    }

    @computed get getGrLogsChart() {
        return this.grLogsChart;
    }

// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒gas-analysis▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    @observable gaSensorId: any = [];
    @observable gaSensors: Array<GaSensorTableType> = [];
    @observable gaLogsCurrentTimeIndex: any = [];
    @observable gaLogsCard: any = [];
    @observable gaLogs: Array<GaLogsTableType> = [];
    @observable gaLogsChart: Array<GaLogsChartType> = [];

    /**
     * @description gas-realtime 센서목록
     */
    @action setGaSensorId(param: number) {
        this.gaSensorId = param;
    }

    @computed get getGaSensorId() {
        return this.gaSensorId;
    }

    @action setGaSensorTable(param: Array<any>) {
        let sensors = param.map((value: any, index: number) => {
            return {
                key: value.id,
                x: value.x,
                y: value.y,
                z: value.z,
                sensorId: value.sensorId,
            }
        });
        this.gaSensors = sensors;
    }

    @computed get getGaSensorTable() {
        return this.gaSensors;
    }

    /**
     * @description gas-realtime 최근 2시간 계측목록
     */
    @action setGaLogsCard(param: number) {
        this.gaLogsCard = {
            temp: this.gaLogs[param].temp,
            humidity: this.gaLogs[param].humidity,
            co2: this.gaLogs[param].co2,
            co: this.gaLogs[param].co,
            no: this.gaLogs[param].no,
            no2: this.gaLogs[param].no2,
            so2: this.gaLogs[param].so2
        };
    }

    @computed get getGaLogsCard() {
        return this.gaLogsCard;
    }

    @action setGaLogsCurrentTimeIndex(param: number) {
        this.gaLogsCurrentTimeIndex = param;
    }

    @computed get getGaLogsCurrentTimeIndex() {
        return this.gaLogsCurrentTimeIndex;
    }

    @action setGaLogs(param: Array<GaLogsTableType>) {
        this.gaLogs = param;
    }

    @computed get getGaLogs() {
        return this.gaLogs;
    }

    // chart형으로 데이터 변환
    @action setGaLogsChart(param: Array<GaLogsChartType>) {
        this.gaLogsChart = param;
    }

    @computed get getGaLogsChart() {
        return this.gaLogsChart;
    }

}

export default EnvironmentStore;
