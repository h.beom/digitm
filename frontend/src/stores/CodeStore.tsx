
import React from "react";
import {action, computed, observable} from 'mobx';
import StringUtil from "../util/StringUtil";
import {
    CodeType
} from "../type/CodeType";

/**
 * @description 관리자 카테고리의 MobxStroe
 */

class CodeStore { 
    /**
    * @description grade-control-analysis 그리드 데이터
    */
   
    @observable codeData: Array<CodeType> = [];

    @computed get getCodeData() {
        return this.codeData;
    }

    @action setCodeData(param: Array<CodeType>) {
        this.codeData = param;
    }
}
export default CodeStore;