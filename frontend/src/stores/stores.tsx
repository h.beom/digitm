import IndexStore from "./IndexStore";
import ProductionStore from "./ProductionStore";
import EnvironmentStore from "./EnvironmentStore";
import CodeStore from "./CodeStore";

const stores = {
    IndexStore: new IndexStore(),
    ProductStore: new ProductionStore(),
    EnvironmentStore: new EnvironmentStore(),
    CodeStore: new CodeStore()
};

export default stores;
