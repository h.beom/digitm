import {action, computed, observable} from 'mobx';
import StringUtil from "../util/StringUtil";
import {
    AnalysisType, GradeDetailType,
    PlanningType,
    PlanningVersion,
    ProductionAmount, ProductionDash,
    RealtimeCardType,
    RealtimeTableType,
    RealtimeType
} from "../type/ProductionType";
import React from "react";


/**
 * @description Production 카테고리의 MobxStroe
 */

class ProductionStore {
// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒grade-control-analysis▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    /**
     * @description grade-control-analysis 그리드 데이터
     */
    @observable gradeCtrAnalysisData: Array<GradeDetailType> = [];

    @action setGradeCtrAnalysisData(param: Array<GradeDetailType>) {
        this.gradeCtrAnalysisData = param;
    }

    @computed get getGradeCtrAnalysisData() {
        return this.gradeCtrAnalysisData;
    }

    /**
     * @description grade-control-analysis 차트 데이터
     */
    @observable gradeCtrAnalyChartData: any = [];

    @action setGradeCtrAnalyChartData(param: any) {
        const ctrChart1: [] = param.map((param: any, index: number) => {
            return {date: param.category, time: param.time, value: param.value}
        })
        this.gradeCtrAnalyChartData = ctrChart1;
    }

    @computed get getGradeCtrAnalyChartData() {
        return this.gradeCtrAnalyChartData;
    }


    @observable gradeCtrAnalyChartData2: any = [];

    @action setGradeCtrAnalyChartData2(param: any) {
        const ctrChart2: [] = param.map((param: any, index: number) => {
            return {date: param.category, time: param.time, value: param.value}
        })
        this.gradeCtrAnalyChartData2 = ctrChart2;
    }

    @computed get getGradeCtrAnalyChartData2() {
        return this.gradeCtrAnalyChartData2;
    }

// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒grade-control-realtime▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    /**
     * @description grade-control-realtime 그리드 데이터
     */
    @observable gradeCtrRealData: Array<GradeDetailType> = [];

    @action setGradeCtrRealData(param: Array<GradeDetailType>) {
        this.gradeCtrRealData = param;
    }

    @computed get getGradeCtrRealData() {
        return this.gradeCtrRealData;
    }

    /**
     * @description grade-control-realtime 차수순번
     */
    @observable lastVersion: any = [];
    @observable gradeCtrRealVersion: Array<any> = [];

    @action setGradeCtrRealVersion(param: Array<any>) {
        //마지막 버전 추출
        if (param.length > 0) {
            this.lastVersion = param[0].number
            this.gradeCtrRealVersion = param;
        }
    }

    @computed get getGradeCtrRealVersion() {
        return this.gradeCtrRealVersion;
    }

    //배열 마지막(최신) 차수버전 출력
    @computed get getLastVersion() {
        return this.lastVersion;
    }

    @action setLastVersion(param: number) {
        //마지막 버전 추출
        this.lastVersion = param

    }

    @observable nowVersion2: any = 0;

    @action setNowVersion2(param: any) {
        this.nowVersion2 = param;
    }

    @computed get getNowVersion2() {
        return this.nowVersion2;
    }


    /**
     * @description 차트데이터 가공
     */
    @observable gradeCtrRealChart: Array<any> = [];

    @action setGradeCtrRealChart(param: Array<any>) {
        let sample = param.slice(0, 2).map((value: any, index: number) => {

            if (value.category == 'cao') {
                return {category: 'CaO' , value: value.value}
            } else if(value.category == 'caoAcc') {
                return {category: 'CaO(누적)', value: value.value}
            }
        })
        this.gradeCtrRealChart1 = sample;

        let sample2 = param.slice(2).map((value: any, index: number) => {
            if (value.category == 'sio2') {
                return {category: 'SIO\u2082', value: value.value}
            }else if(value.category == 'sio2Acc') {
                return {category: 'SIO\u2082(누적)', value: value.value}
            }else if(value.category == 'al2o3') {
                return {category: 'Al\u2082O\u2083', value: value.value}
            }else if(value.category == 'al2o3Acc') {
                return {category: 'Al\u2082O\u2083(누적)', value: value.value}
            }else if(value.category == 'mgo') {
                return {category: 'MgO', value: value.value}
            }else if(value.category == 'mgoAcc') {
                return {category: 'MgO(누적)', value: value.value}
            }else if(value.category == 'fe2o3') {
                return {category: 'Fe\u2082O\u2083', value: value.value}
            }else if(value.category == 'fe2o3Acc') {
                return {category: 'Fe\u2082O\u2083(누적)', value: value.value}
            }else if(value.category == 'k2o') {
                return {category: 'K\u2082O', value: value.value}
            }else if(value.category == 'k2oAcc') {
                return {category: 'K\u2082O(누적)', value: value.value}
            }else if(value.category == 'so3') {
                return {category: 'SO\u2083', value: value.value}
            }else if(value.category == 'so3Acc') {
                return {category: 'SO\u2083(누적)', value: value.value}
            }else if(value.category == 'na2o') {
                return {category: 'Na\u2082O', value: value.value}
            }else if(value.category == 'na2oAcc') {
                return {category: 'Na\u2082O(누적)', value: value.value}
            } else {
                return {category: value.category , value: value.value}
            }
        })


        this.gradeCtrRealChart2 = sample2;
    }

    @observable gradeCtrRealChart1: Array<any> = [];

    @computed get getGradeCtrRealChart1() {
        return this.gradeCtrRealChart1;
    }

    @observable gradeCtrRealChart2: Array<any> = [];

    @computed get getGradeCtrRealChart2() {
        return this.gradeCtrRealChart2;
    }

// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒weighbridge-analysis▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    /**
     * @description weighbridge-analysis 그리드데이터
     */
    @observable weighbridgeAnalysisData: Array<AnalysisType> = [];

    @action setWeighbridgeAnalysisData(param: Array<AnalysisType>) {
        this.weighbridgeAnalysisData = param;
    }

    @computed get getWeighbridgeAnalysisData() {
        return this.weighbridgeAnalysisData;
    }


    /**
     * @description weighbridge-analysis 차트데이터
     */
    @observable weighbridgeAnalysisChart: Array<any> = [];

    @action setWeighbridgeAnalysisChart(param: Array<any>) {

        this.weighbridgeAnalysisChart = param;
    }

    @computed get getWeighbridgeAnalysisChart() {
        return this.weighbridgeAnalysisChart;
    }


    /**
     * @description weighbridge-analysis 누적운반횟수, 누적운반량
     */

    @computed get getAccCount() {
        // @ts-ignore
        return this.weighbridgeAnalysisData.accCount;
    }

    @computed get getAccAmount() {
        // @ts-ignore
        return this.weighbridgeAnalysisData.accAmount;
    }

// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒weighbridge-realtime▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    /**
     * @description weighbridge-realtime 카드데이터(생산실적, 구매실적)
     */
    @observable weighbridgeRealCardData: Array<RealtimeCardType> = [];
    @observable weighbridgeRealCardData2: Array<RealtimeCardType> = [];

    @action setWeighbridgeRealCardData(param: Array<RealtimeType>) {
        // @ts-ignore
        this.weighbridgeRealCardData = param.procurement;
        // @ts-ignore
        this.weighbridgeRealCardData2 = param.production;
    }

    @computed get gettWeighbridgeRealCardData() {
        return this.weighbridgeRealCardData;
    }

    /**
     * @description 테이블 데이터
     */
    @observable weighbridgeRealTable: Array<RealtimeTableType> = [];

    @action setWeighbridgeRealTable(param: Array<RealtimeTableType>) {
        this.weighbridgeRealTable = param;
        // @ts-ignore
        this.test123 = param.production;
        // @ts-ignore
        this.test456 = param.procurement;
    }

    @computed get getWeighbridgeRealTable() {
        return this.weighbridgeRealTable;
    }


    @observable test123: Array<any> = [];

    @computed get getTest123() {
        return this.test123;
    }

    @observable test456: Array<any> = [];

    @computed get getTest456() {
        return this.test456;
    }

    @observable lastNumber: number = 0;

    @action setLastNumber(param: number) {
        this.lastNumber = param;
    }

    @computed get getLastNumber() {
        return this.lastNumber;
    }


    /**
     * @description weighbridge-realtime 카드데이터( 구매실적)
     */
    @computed get getProductCurrent() {
        // 현재재고
        // @ts-ignore
        return this.weighbridgeRealCardData.current;
    }

    @computed get getProductMonth() {
        // @ts-ignore
        return this.weighbridgeRealCardData.month;
    }

    @computed get getProductYear() {
        // @ts-ignore
        return this.weighbridgeRealCardData.year;
    }

    @computed get getProductCurrentOut() {
        // @ts-ignore
        return this.weighbridgeRealCardData.currentOut;
    }

    @computed get getProductMonthOut() {
        // @ts-ignore
        return this.weighbridgeRealCardData.monthOut;
    }

    @computed get getProductYearOut() {
        // @ts-ignore
        return this.weighbridgeRealCardData.yearOut;
    }


    /**
     * @description weighbridge-realtime 카드데이터( 구매실적)
     */
    @computed get getProductCurrent2() {
        // @ts-ignore
        return this.weighbridgeRealCardData2.current;
    }

    @computed get getProductMonth2() {
        // @ts-ignore
        return this.weighbridgeRealCardData2.month;
    }

    @computed get getProductYear2() {
        // @ts-ignore
        return this.weighbridgeRealCardData2.year;
    }

    @computed get getProductCurrentOut2() {
        // @ts-ignore
        return this.weighbridgeRealCardData2.currentOut;
    }

    @computed get getProductMonthOut2() {
        // @ts-ignore
        return this.weighbridgeRealCardData2.monthOut;
    }

    @computed get getProductYearOut2() {
        // @ts-ignore
        return this.weighbridgeRealCardData2.yearOut;
    }

    /**
     * @description weighbridge-realtime 선택날짜
     */
    @observable weighbridgeRealDate: string = '';

    @computed get getWeighbridgeRealDate() {
        // @ts-ignore
        return this.weighbridgeRealCardData.datetime;
    }

    @observable weighbridgeChartDate: Array<any> = [];

    /**
     * @description 회차 역순을 위한 배열정리
     */
    @observable weighbridgeRealList: Array<number> = [];

    @action setWeighbridgeRealList(param: Array<number>) {
        this.weighbridgeRealList = param;
    }

    @computed get getWeighbridgeRealList() {
        return this.weighbridgeRealList
    }


// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒product-planning▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒

    @observable planningData: Array<PlanningType> = [];
    @observable planningDataVersion: Array<PlanningVersion> = [];

    @action setPlanningData(param: Array<PlanningType>) {

        this.planningData = param;
        // @ts-ignore
        this.planningDataVersion = param.version;
    }

    @computed get getPlanningData() {
        return this.planningData;
    }

    @computed get getPlanningDataVer() {
        return this.planningDataVersion;
    }

    /**
     * @description 버전차수 등록
     */
    @observable versionData: any = [];

    @action setVersionData(param: any) {
        this.versionData = param;
    }

    @computed get getVersionData() {
        return this.versionData;
    }

    /**
     * @description 버전차수 등록
     */
    @observable nowVersion: any = 1;

    @action setNowVersion(param: any) {
        this.nowVersion = param;
    }

    @computed get getNowVersion() {
        return this.nowVersion;
    }


    /**
     * @description 버전차수 등록
     */
    // @observable nowVersion: any = 1;
    // @action setNowVersion(param: any) {
    //     this.nowVersion = param;
    // }
    // @computed get getNowVersion() {
    //     return this.nowVersion;
    // }
// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒dashboard-product▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    /**
     * @description weighbridge-analysis 그리드데이터
     */
    @observable productDashAmount: Array<ProductionAmount> = [];
    @action setProductDashAmount(param: Array<ProductionAmount>) {
        this.productDashAmount = param;
    }
    @computed get getProductDashAmount() {
        return this.productDashAmount;
    }


    /**
     * @description 데이터 PROGRESS 퍼센테이지 위한 로직
     */
    @observable progress: any = 0;

    @computed get getProgress() {
        return this.progress;
    }

    /**
     * @description 차트데이터
     */
    @observable productDashChart: Array<ProductionDash> = [];
    @observable productDashChartAcc: any = [];
    @observable productDashChartDaily: any = [];

    @action setProductDashChart(param: Array<ProductionDash>) {
        this.productDashChart = param;
        // @ts-ignore
        const resultData2: [] = param.acc.map((param: any, index: number) => {
            return {date: param.date, acc: param.acc}
        })
        this.productDashChartAcc = resultData2;
        // @ts-ignore
        const resultData3: [] = param.daily.map((param: any, index: number) => {
            return {date: param.date, daily: param.daily}
        })
        this.productDashChartDaily = resultData3;
    }

    @computed get getProductDashChart() {
        return this.productDashChart;
    }

    @computed get getProductDashChartAcc() {
        return this.productDashChartAcc;
    }

    @computed get getProductDashChartDaily() {
        return this.productDashChartDaily;
    }
}

export default ProductionStore;
